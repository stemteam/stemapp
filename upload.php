<?php

date_default_timezone_set('Europe/Moscow');


isset($_POST['Token']) && $token = $_POST['Token'];
isset($_POST['type']) && $type = $_POST['type'];

(!isset($token)) && showCriticalError('ERROR: Token not set');
//(!checkToken($token)) && showCriticalError('ERROR: Invalid token');
(!count($_FILES)) && showCriticalError('ERROR: File not sended');

$tmp = explode('/', $_FILES['file']['type']);
$ext = $tmp[1];

(!isset($ext)) && showCriticalError('ERROR: Unknown file type');

if ($type)
    $dir = realpath(dirname(__FILE__) ) . '/uploads/' . $type . '/';
else
    $dir = realpath(dirname(__FILE__)) . '/uploads/';

$filename = (time() * 1000) . rand(10000, 99999) . '.' . $ext;

if (!is_dir($dir))
    @mkdir($dir);
if (copy($_FILES['file']['tmp_name'], $dir . $filename))
    echo(json_encode(array('filename' => $filename, 'fullPath' => $dir . $filename)));
else
    showCriticalError('ERROR: Failed copy file to destination folder on server');


function checkToken($token) {
    $url = 'http://' . $_SERVER['HTTP_HOST'] . '/jaxis/fuelcard/ClientGet.json';
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'Version=1&Token=' . $token);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    curl_close($ch);

    return (isset(json_decode($result)->rows) && is_array(json_decode($result)->rows)) ? true : false;
}

function showCriticalError($message) {
    header("HTTP/1.0 500 Internal Server Error");
    die(json_encode(array('error' => $message)));
}




