module.exports = function (grunt) {
    // Инициализация конфига GruntJS

    try {
        var fs = require('fs');
        var cfg = grunt.file.readJSON("grunt/config.json");

        var version = grunt.file.readJSON("package.json").version;
        var BUILD_PATH = cfg.build_path;
        var TEMP_PATH = cfg.temp_path;
        var css_build = BUILD_PATH + 'app.build.css';
        var js_build = BUILD_PATH + "app.build.js";


        // Получаем список модулей
        var modules = [];
        var files;
        var cssfiles = [];
        var lessfiles = ["resource/css/main.less"];
        var i;


        fs.readdirSync(cfg.module_dir).forEach(function (file) {
            if (fs.lstatSync(cfg.module_dir + '/' + file).isDirectory()) {
                modules.push(file);
            }
        });
        var cssMin = {
            combine: {
                files: {}
            }
        };
        var lessConfigFiles = {};
        for (i = 0; i < lessfiles.length; i++) {
            var file = TEMP_PATH + '/' + lessfiles[i].replace('.less', '.css');
            lessConfigFiles[file] = lessfiles[i];
            cssfiles.push(file);
        }
        var requirejsOptions = {},
            jsHintConfig = {
                options: {
                    jshintrc: '.jshintrc',
                    reporter: require('jshint-stylish')
                }
            },
            jscsConfig = {},
            lessConfig = {
                development: {
                    options: {
                        compress: true,
                        yuicompress: true,
                        optimization: 2
                    },
                    files: lessConfigFiles
                }
            };

        var include = [cfg.start_file];
        modules.forEach(function (module) {
            jscsConfig[module] = {
                src: cfg.module_dir + '/' + module + '/js/',
                options: {
                    config: true
                }
            };

            jsHintConfig[module] = scanDir(cfg.module_dir + '/' + module + '/js/', 'js');
            cssMin['combine']['files'][css_build] = cssfiles;
        });


        requirejsOptions['default'] = {
            options: {
                baseUrl: '',
                //name: 'node_modules/almond/almond',
                name: 'app/js/require.min',
                out: js_build,
                mainConfigFile: "config.js",
                preserveLicenseComments: false,
                optimize: 'uglify',
                include: include,
                insertRequire: [cfg.start_file]
            }
        };

        jscsConfig['global'] = {
            src: 'www/assets/js/app/',
            options: {
                config: true
            }
        };
        jsHintConfig['global'] = scanDir('app/js/stemapp/', 'js');

    } catch (e) {
        console.log(e);
    }

    cfg = {
        clean: {
            build: [BUILD_PATH + "*.js", BUILD_PATH + "*.css"],
            temp: [TEMP_PATH]
        },
        requirejs: requirejsOptions,
        jscs: jscsConfig,
        jshint: jsHintConfig,
        less: lessConfig,
        cssmin: cssMin,
        exec: {
            update_modules: 'node grunt/update_modules.js'
        },
        postcss: {
            options: {
                map: false, // inline sourcemaps
                processors: [
                    require('pixrem')(), // add fallbacks for rem units
                    require('autoprefixer')({browsers: 'last 2 versions'}), // add vendor prefixes
                    require('cssnano')() // minify the result
                ]
            },
            dist: {
                src: css_build
            }
        },
        imagemin: function () {
            var config = {};
            modules.forEach(function (module) {
                config[module] = {
                    files: [
                        {
                            expand: true,
                            cwd: cfg.module_dir + '/' + module + '/www/assets/img',
                            src: ['**/*.{png,jpg,gif,ico}'],
                            dest: 'www/assets/img/module_' + module + '/'
                        }
                    ]
                }
            });
            return config;
        }()
    };


    grunt.initConfig(cfg);

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-jscs');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.registerTask('default', [/*'jshint', 'jscs', */'clean:build', 'requirejs', 'less', 'cssmin', 'postcss', 'clean:temp'  /*'imagemin',*/]);
    grunt.registerTask('updateModules', ['exec:update_modules']);

    //read config
    function scanDir(dir, ext) { //console.log(config);
        var files = [];
        try {
            files = files.concat(getFilesRecursive(dir, ext));
        }
        catch (e) {
            console.warn('ERROR: scaning directory ' + dir + ' - not found');
        }
        return files;
    }

    function getFilesRecursive(directory, ext) {
        var files = [];
        var directories = fs.readdirSync(directory);

        files = files.concat(getFilesFromDir(directory, ext));

        for (var i = 0; i < directories.length; i++) {
            if (fs.lstatSync(directory + directories[i]).isDirectory())
                files = files.concat(getFilesRecursive(directory + directories[i] + '/', ext));
        }

        return files;
    }

    function getFilesFromDir(directory, ext) {
        var files = [];
        directory = directory.replace('*', '').trim();

        if (fs.lstatSync(directory).isDirectory()) {

            fs.readdirSync(directory).forEach(function (file) {
                if (((new RegExp('\.' + ext + '$')).test(file)) && fs.lstatSync(directory + file).isFile())
                    files.push(directory + file);
            });

        } else {
            console.warn('ERROR: path ' + directory + ' by mask ' + directory + '* not founded');
        }

        return files;
    }
};