<?php

date_default_timezone_set('Europe/Moscow');
define('ROOT',realpath($_SERVER['DOCUMENT_ROOT']).DIRECTORY_SEPARATOR);
require_once(ROOT.'app/php/classes/fileManager.php');

$dir = realpath(dirname(__FILE__).'/../../').'/uploads/';
$matches = array();


isset($_POST['date']) && $date = $_POST['date']; //YYYY-MM
isset($_GET['date']) && $date = $_GET['date'];

isset($_POST['clientCode']) && $clientCode = $_POST['clientCode'];
isset($_GET['clientCode']) && $clientCode = $_GET['clientCode'];

isset($_POST['contractCode']) && $contractCode = $_POST['contractCode'];
isset($_GET['contractCode']) && $contractCode = $_GET['contractCode'];

isset($_POST['filename']) && $filename = $_POST['filename'];
isset($_GET['filename']) && $filename = $_GET['filename'];

isset($_COOKIE['token']) && $token = $_COOKIE['token'];

isset($_POST['type']) && $type = $_POST['type'];
isset($_GET['type']) && $type = $_GET['type'];


(!isset($token)) && die('ERROR: Token not set');

(!checkToken($token)) && die('ERROR: Invalid token');

checkExist($dir);
checkExist($dir.'docs');

if(isset($filename))
    getFile($filename, $dir, $type);
elseif(isset($date) && isset($clientCode) && isset($contractCode))
    showFileList($date, $clientCode, $contractCode, $dir);
else
    die('ERROR: Query parameters not set');

function checkToken($token) {
    $url = 'http://'.$_SERVER['HTTP_HOST'].'/jaxis/fuelcard/ClientGet.json';
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'Version=1&Token='.$token);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    curl_close($ch);
    return (isset(json_decode($result)->rows) && is_array(json_decode($result)->rows)) ? true : false;
}

function showFileList($date, $clientCode, $contractCode, $dir) {
    $filenameList = array();
    $fileList = fileManager::getFileList($dir,array('docs' => '*'),'pdf');
    $regex = "/^($date-\d\d)_($clientCode)_($contractCode)_(\w+)_(\w+)\.pdf$/siU";

    foreach($fileList as $file)
        preg_match($regex, basename($file)) && array_push($filenameList, basename($file));

    echo(json_encode($filenameList));
}

function getFile($filename, $dir, $type) {
    if(isset($type)) {
        $tmp = explode('.',$filename);
        $ext = $tmp[count($tmp)-1];
        $fileList = fileManager::getFileList($dir,array($type => '*'),$ext);
    }
    else
        $fileList = fileManager::getFileList($dir,array('docs' => '*'),'pdf');
    $founded = 0;

    foreach($fileList as $file)
        if(basename($file) == trim($filename)) {
            $founded++;
            $tmp = explode('_',basename($file));

            header('Content-type: application/pdf');
            if(isset($type))
                header('Content-Disposition: attachment; filename="'.basename($file).'"');
            else
                header('Content-Disposition: attachment; filename="'.$tmp[0].'_'.$tmp[3].'_'.$tmp[4].'"');
            readfile($file);
        }

    (!$founded) && die('ERROR: file '.$filename.' not found');

}

function checkExist($directory) {
    if(!file_exists($directory)) {
        if(@!mkdir($directory)) {
            die(json_encode(array('error'=>"Can't create directory: ".$directory)));
        }
    }
}




