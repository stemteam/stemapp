<?php
isset($_POST['data']) && $data = $_POST['data'];
isset($_POST['types']) && $types = $_POST['types'];
isset($_POST['filename']) && $filename = $_POST['filename'];
isset($_COOKIE['token']) && $token = $_COOKIE['token'];
//check Token
(!isset($token)) && showCriticalError('ERROR: Token not set(field: token)');
//(!checkToken($token)) && showCriticalError('ERROR: Invalid token');

(!isset($filename)) && showCriticalError('ERROR: Filename not sent.(field: filename)');


$filename = $filename . '.xls';

$title = 'Сгенерированный документ';
/*
$filename = 'test.xls';
$data = json_encode(array(
    array('Первая колонка','Вторая колонка','Третья колонка','И тут на сцену выходит колонка с длинным названием'),
    array('Значение 1','Значение 2','Значение 3','Мегаацкий комментарий'),
    array('Значение 1','Значение 2','Значение 3','Мегаацкий комментарий'),
    array('Значение 1','Значение 2','Значение 3','Мегаацкий комментарий'),
    array('Значение 1','Значение 2','Значение 3','Мегаацкий комментарий'),
));
*/

$data = json_decode($data);
if (isset($types)) {
    $types = json_decode($types);
}


$dataLength = count($data);

$root = realpath(dirname(__FILE__) . '/../') . DIRECTORY_SEPARATOR;
require_once($root . 'php' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'PHPExcel.php');

$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet();
$sheet->setTitle($title);


for ($i = 0; $i < $dataLength; $i++) {
    $j = 0;
    foreach ($data[$i] as $cell) {

        if (!isset($types) || !isset($types[$j]) || $types[$j] == 'string' || $i == 0) {
            $sheet->setCellValueExplicit(getNameFromNumber($j) . ($i + 1), $cell, PHPExcel_Cell_DataType::TYPE_STRING);
        } elseif ($types[$j] == 'numeric') {
            $sheet->setCellValueExplicit(getNameFromNumber($j) . ($i + 1), $cell, PHPExcel_Cell_DataType::TYPE_NUMERIC);
        } else {
            $sheet->setCellValueExplicit(getNameFromNumber($j) . ($i + 1), $cell, PHPExcel_Cell_DataType::TYPE_STRING);
        }

        $sheet->getStyle(getNameFromNumber($j) . ($i + 1))->getBorders()->getLeft()->applyFromArray(
            array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '000000')));
        $sheet->getStyle(getNameFromNumber($j) . ($i + 1))->getBorders()->getRight()->applyFromArray(
            array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '000000')));
        $sheet->getStyle(getNameFromNumber($j) . ($i + 1))->getBorders()->getTop()->applyFromArray(
            array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '000000')));
        $sheet->getStyle(getNameFromNumber($j) . ($i + 1))->getBorders()->getBottom()->applyFromArray(
            array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '000000')));


        if (!$i) { //if first field set field as table header
            $sheet->getStyle(getNameFromNumber($j) . ($i + 1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $sheet->getStyle(getNameFromNumber($j) . ($i + 1))->getFont()->setBold(true);
            $sheet->getColumnDimension(getNameFromNumber($j))->setAutoSize(true);
            $sheet->getStyle(getNameFromNumber($j) . ($i + 1))->getFill()->getStartColor()->setRGB('EEEEEE');
            $sheet->getStyle(getNameFromNumber($j) . ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $j++;
    }
}

header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel");
//header ( "Content-Disposition: attachment; filename=".$filename );

$objWriter = new PHPExcel_Writer_Excel5($xls);
$objWriter->save('php://output');

//---==========================
function checkToken($token) {
    $url = 'http://' . $_SERVER['HTTP_HOST'] . '/jaxis/fuelcard/ClientGet.json';
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'Version=1&Token=' . $token);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    curl_close($ch);

    return (isset(json_decode($result)->rows) && is_array(json_decode($result)->rows)) ? true : false;
}

function showCriticalError($message) {
    header("HTTP/1.0 500 Internal Server Error");
    die(json_encode(array('error' => $message)));
}

function getNameFromNumber($num) {
    $numeric = $num % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval($num / 26);
    if ($num2 > 0) {
        return getNameFromNumber($num2 - 1) . $letter;
    } else {
        return $letter;
    }
}