<?php
/**
 * Created by PhpStorm.
 * User: phplamer
 * Date: 20.08.14
 * Time: 11:21
 */

function checkToken($token) {
    $url = 'http://'.$_SERVER['HTTP_HOST'].'/jaxis/fuelcard/ClientGet.json';
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'Version=1&Token='.$token);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    curl_close($ch);

    return (isset(json_decode($result)->rows) && is_array(json_decode($result)->rows)) ? true : false;
}
