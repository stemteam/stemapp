<?php
/**
 * Created by PhpStorm.
 * User: phplamer
 * Date: 29.04.14
 * Time: 15:55
 */

class fileManager {

    static public function getFileList($dir,$list,$ext) {
        $files = array();

        foreach($list as $key => $value)
        {

            if(is_int($key))
                if(is_file($dir.$value) && file_exists($dir.$value))
                    if(preg_match('/(\.'.$ext.')/',trim($dir.$value)))
                        array_push($files,$dir.$value);
                    else
                    {
                        echo("Error: $dir$value incorrect extension. Valid extension: $ext . \n ");
                        exit;
                    }

                else
                {
                    echo("Error: $dir$value file not found \n");
                    exit;
                }

            if($value == '*')
            {
                $directory = array();
                $startDir = $dir.$key.DIRECTORY_SEPARATOR;
                if(is_dir($dir.$key))
                {
                    do
                    {
                        if(count($directory))
                            $startDir = array_shift($directory).DIRECTORY_SEPARATOR;
                        $scandir = scandir($startDir);
                        foreach($scandir as $tmp)
                        {
                            if(!preg_match('/^\./',$tmp))
                            {
                                if(is_file($startDir.$tmp) && preg_match('/(\.'.$ext.')/',trim($tmp)))
                                    array_push($files,$startDir.$tmp);
                                elseif(is_dir($startDir.$tmp))
                                    array_push($directory,$startDir.$tmp);
                            }
                        }
                    } while(count($directory));

                }
                else
                {
                    echo("Error: $dir$key not a directory. Please check config file. \n");
                    exit;
                }

            }

            if(is_array($value))
            {
                if(is_dir($dir.$key))
                    $files = array_merge($files, self::getFileList($dir.$key.DIRECTORY_SEPARATOR,$value,$ext));
                else
                {
                    echo("Error: $dir$key not a directory. Please check config file. \n");
                    exit;
                }
            }


        }

        return $files;
    }

}