<?php

date_default_timezone_set('Europe/Moscow');

$file = $_POST['file'];
$data = $_POST['data'];

$output = gzencode("\xEF\xBB\xBF".$data);

$file= str_ireplace(' ','_',$file);


$headers = array(
	'Pragma' => 'no-cache',
	'Cache-control' => 'private',
	'Content-Type' => 'text/csv; charset=utf-8',
	'Content-Length' => strlen($output),
	'Content-Encoding' => 'gzip',
	//'Content-Disposition' => 'attachment;filename=\"'.$file.date('_d-m-Y_H-i-s').'.csv\"',
);

foreach ($headers as $key => $value) {

	header($key.': '.$value);
}

echo $output;
die;
