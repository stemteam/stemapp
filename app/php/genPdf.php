<?php
/**
 * Created by PhpStorm.
 * User: phplamer
 * Date: 03.08.14
 * Time: 18:33
 */

isset($_POST['html']) && $html = $_POST['html'];
isset($_GET['html']) && $html = $_GET['html'];

isset($_POST['css']) && $css = $_POST['css'];
isset($_GET['css']) && $css = $_GET['css'];

isset($_POST['path']) && $path = $_POST['path'];
isset($_GET['path']) && $path = $_GET['path'];

isset($_POST['filename']) && $filename = str_replace('.pdf','',strtolower($_POST['filename']));
isset($_GET['filename']) && $filename = str_replace('.pdf','',strtolower($_GET['filename']));

isset($_COOKIE['token']) && $token = $_COOKIE['token'];


//check Token
(!isset($token)) && die('ERROR: Token not set(field: Token)');
(!checkToken($token)) && die('ERROR: Invalid token');

(!isset($html)) && die('ERROR: HTML not sent. Please sent HTML code for generate PDF file.(field: html)');
(!isset($filename)) && die('ERROR: Filename not sent.(field: filename)');
(!isset($path)) && $path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR).'/uploads/';

if(!is_dir($path))
    @mkdir($path);

$path = $path.'order'.DIRECTORY_SEPARATOR;

if(!is_dir($path))
    @mkdir($path);

$root = realpath(dirname(__FILE__) . '/../') . DIRECTORY_SEPARATOR;

require_once($root.'php'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf.php');

//Generate PDF
$mpdf = new mPDF('utf-8', 'A4', '8', '', 5, 5, 5, 27, 10, 20);

$mpdf->charset_in = 'utf8';

$mpdf->setFooter('{PAGENO}');

if(isset($css))
    $mpdf->WriteHTML($css, 1);

$mpdf->list_indent_first_level = 0;

$mpdf->WriteHTML($html, 2);

//$mpdf->Output('mpdf.pdf', 'I');

$mpdf->Output($path.$filename.'.pdf','F');
echo(json_encode(array('filename' => $path.$filename.'.pdf')));


//---==========================
function checkToken($token) {
    $url = 'http://'.$_SERVER['HTTP_HOST'].'/jaxis/fuelcard/ClientGet.json';
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'Version=1&Token='.$token);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    curl_close($ch);

    return (isset(json_decode($result)->rows) && is_array(json_decode($result)->rows)) ? true : false;
}


