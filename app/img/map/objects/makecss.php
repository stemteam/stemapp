<?php

define('URL', '/img/map/objects/');

$path = isset($argv[1]) ? $argv[1] : dirname(__FILE__);

$dir = new DirectoryIterator($path);
foreach ($dir as $fileInfo) {
    if ($fileInfo->isDot()) {
        continue;
    }

    $fileName = $fileInfo->getFilename();
    if ((strpos($fileName, '_Big') !== false) || substr($fileName, -3) !== 'png') {
        continue;
    }

    echo '.icon-', mb_strtolower(mb_substr($fileName, 0, -11)), ' { background-image:url(', URL, $fileName, '); }', PHP_EOL;
}