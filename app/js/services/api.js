define(['cookie'], function () {
    'use strict';


    /**
     * @class api
     * @export
     */
    var api = {
        version: Stemapp.config.apiVersion,
        token: '',
        apiKey: Stemapp.config.apiKey,
        default: {
            context: 'api',
            namespace: '',
            transport: 'json2',
            type: 'POST'
        }
    };

    /**
     * Получить сформированный урл для GET запросов
     * @param options
     * @returns {string}
     */
    api.makeUrl = function (options) {
        options = api.prepareOptions(options);
        var params = [];
        for (var i in options.data) {
            params.push(i + '=' + options.data[i]);
        }
        return options.url + '?' + params.join('&');
    };

    /**
     * Подготовка настроек для запроса
     * @param options
     * @returns {*}
     */
    api.prepareOptions = function (options) {
        api.version = Stemapp.config.apiVersion;
        api.apiKey = Stemapp.config.apiKey;
        var opts = $.extend(true, {}, options);
        if (!options) {
            throw new Error('API: set require parameters({method/url: })');
        }

        if (!api.token) {
            api.token = $.cookie('token');
        }

        if (!options.url) {
            var context = options.context ? options.context : api.default.context;
            options.context && delete options.context;

            var transport = options.transport ? options.transport : api.default.transport;
            options.transport && delete options.transport;


            if (options.method) {
                options.url = (options.namespace === false)
                    ? '/' + context + '/' + options.method + '.' + transport
                    : '/' + context + '/' + ((options.namespace) ? options.namespace + '/' : api.default.namespace) + options.method + '.' + transport;

                options.method && delete options.method;
                options.namespace && delete options.namespace;
            }
            else
                throw new Error('API: set url or method');

        }

        var type = options.type ? options.type : api.default.type;

        options = $.extend(true, {}, {
            data: {
                Version: api.version,
                Token: api.token,
                ApiKey: api.apiKey
            },

            dataType: 'text',
            type: 'POST',
            url: null
        }, options, {
            error: _.bind(api.error, this, opts),
            success: _.bind(api.success, this, opts)
        });
        return options;
    };


    /**
     * Вызов метода сервиса
     * @param options настройки вызова
     * @param {string} options.method  метод, вызываемый на сервере
     * @param {string} options.namespace группа методов
     * @param {object} options.data данные
     * @param {function} options.error callback ошибки
     * @param {function} options.success callback успешного вызова
     */
    api.call = function (options) {
        options = api.prepareOptions(options);
        api.lastOperation = options;
        $.ajax(options);
    };

    api.error = function (opts, xhr, status) {
        if (opts.error) {
            opts.error(xhr);
        }
    };

    api.success = function (opts, data, status, xhr) {
        if (data) {
            data = $.parseJSON(data);
        }
        if (opts.success) {
            opts.success(data, status, xhr);
        }
    };

    api.lastOperation = '';

    window.api = api;
});