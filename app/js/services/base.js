define([
    'jquery',
    'cookie'
], function ($) {
    'use strict';


    // Namespace initialization
    var I;
    window.I = I = {
        VERSION: '1.0',

        timezone: {
            /**
             * Timezone difference between Local Time and GMT in milliseconds
             */
            offset: (new Date()).getTimezoneOffset() * -60000
        },

        salt: 'user navigates between selected tabs'
    };

    /**
     * AJAX helper for calling services based on the Infokinetika Jaxis Server
     */
    I.jaxis = function (options) {

        options = $.extend(true, {}, {
            data: {
                Version: Stemapp.config.apiVersion,
                Token: $.cookie('token') ? $.cookie('token') : null,
                TimeOffset: new Date().getTimezoneOffset() / -60
            },
            exclude: null, // regex for keys
            dataType: 'json',
            type: 'POST',
            form: null,
            url: null,
            action: null,
            service: 'back'
        }, options);

        if (!options.url) {
            if (options.action) {
                options.url = '/jaxis/' + options.service + '/' + options.action + '.' + options.dataType;
                delete options.action;
                delete options.service;
            } else {
                throw new Error('Jaxis requires request url or action');
            }
        }

        if (options.form) {
            // Parse form fields
            $.extend(options.data, I.form.params(options.form));
        }

        // Fix boolean variables and pack transport data
        $.each(options.data, function (i, val) {
            if (typeof val === 'boolean') {
                // Convert boolean variables to integers
                options.data[i] = +val;
            } else if (I.isTranny(val)) {
                options.data[i] = (new I.Tranny(val, true)).to_string();
            }
        });

        // Hash password field
        if (options.data.hasOwnProperty('Passwd')) {
            options.data.DeprecatedPasswd = hex_md5(options.data.Passwd + I.salt);
            options.data.Passwd = md5b64(options.data.Passwd);
        }

        if (options.data.hasOwnProperty('Password')) {

            options.data.Password = md5b64(options.data.Password);
        }

        // Filter out excluded params
        if (options.exclude) {
            $.each(options.data, function (key) {
                if (key.match(options.exclude)) {
                    delete options.data[key];
                }
            });
        }
        I.jaxis.lastOperation = options;
        options.dataType = 'text';
        $.ajax(options);
        options = null;
    };

    /**
     * String helpers
     *
     * @type {Object}
     */
    I.str = {
        /**
         * Pads string to the given length
         *
         * @param {String}  str   String to pad
         * @param {Number}  len   Target length
         * @param {String}  chr   Char (or string) to use as padding
         * @param {Boolean} right Pad from the right side instead of left if True
         *
         * @return {String} Result string
         */
        pad: function (str, len, chr, right) {
            str = str ? str : '';
            str = str.toString();
            var pad = str;

            while (pad.length < len) {
                if (right) {
                    pad += chr;
                } else {
                    pad = chr + pad;
                }
            }

            return (pad.length > len) ? pad.slice(right ? len * -1 : 0, len) : pad;
        },

        trim: function (text, left, right, letter) {
            left = left || true;
            right = right || true;
            letter = letter || ' ';

            text = text.toString();

            if (left) {
                text = text.replace(new RegExp('^' + letter + '+'), '');
            }

            if (right) {
                text = text.replace(new RegExp(letter + '+$'), '');
            }

            return text;
        }
    };

    /**
     * Number helpers
     *
     * @type {Object}
     */
    I.num = {
        pad: function (num, len, chr) {
            return I.str.pad(num, len || 2, chr || '0');
        }
    };

    /**
     * Template helpers
     * Gets applied as Mus helpers too
     *
     * @type {Object}
     */
    I.helpers = {
        device_selector_item: function () {
            //noinspection JSUnresolvedVariable
            return (I.str.pad(this.CODE, 20, ' ', true) + ' '
            + I.str.pad(this.SERIALNUMBER, 20, ' ', true) + ' '
            + I.str.pad(this.PORT + ': ' + this.PORTNAME.slice(5), 40, ' ', true) + ' '
            + this.PHONE)
                .replace(/ /g, '&nbsp;');
        },

        /**
         * Returns current date as Date object
         *
         * @return {Date}
         */
        now: function () {
            return new Date();
        },

        /**
         * Formats Date to 'dd.MM.yyyy' string
         *
         * @param {Date} date Date object
         *
         * @return {String}
         */
        date: function (date) {
            if (!date && (this instanceof Date)) {
                date = this;
            }


            if (date instanceof Date) {
                var dot = '.';
                return ('0' + date.getDate()).slice(-2) + dot + ('0' + (date.getMonth() + 1)).slice(-2) + dot + date.getFullYear();
            }

            return date;
        },

        /**
         * Formats Date to 'dd.MM.yyyy' string
         *
         * @param {Date} date Date object
         *
         * @return {String}
         */
        dateUTC: function (date) {
            if (!date && (this instanceof Date)) {
                date = this;
            }

            if (date instanceof Date) {
                var dot = '.',
                    d = new Date(date.getTime() + I.timezone.offset);
                return ('0' + d.getDate()).slice(-2) + dot + ('0' + (d.getMonth() + 1)).slice(-2) + dot + d.getFullYear();
            }

            return date;
        },

        /**
         * Formats Date as 'hh:mm:ss' string
         *
         * @param {Date} date
         *
         * @return {String}
         */
        time: function (date) {
            if (!date && (this instanceof Date)) {
                date = this;
            }

            if (date instanceof Date) {
                var colon = ':';
                return [
                    ('0' + date.getHours()).slice(-2),
                    colon,
                    ('0' + date.getMinutes()).slice(-2),
                    colon,
                    ('0' + date.getSeconds()).slice(-2)
                ].join('');
            }

            return this;
        },

        /**
         * Formats Date as 'hh:mm:ss' string
         *
         * @param {Date} date
         *
         * @return {String}
         */
        timeUTC: function (date) {
            if (!date && (this instanceof Date)) {
                date = this;
            }

            if (date instanceof Date) {
                var colon = ':',
                    d = new Date(date.getTime() + I.timezone.offset);
                return [
                    ('0' + d.getHours()).slice(-2),
                    colon,
                    ('0' + d.getMinutes()).slice(-2),
                    colon,
                    ('0' + d.getSeconds()).slice(-2)
                ].join('');
            }

            return this;
        },

        /**
         * Formats Date as 'dd.MM.yyyy hh:mm:ss' string
         *
         * @param {Date} date
         *
         * @return {String}
         */
        datetime: function (date) {
            if (!date && (this instanceof Date)) {
                date = this;
            }

            return I.helpers.date(date) + ' ' + I.helpers.time(date);
        },

        /**
         * Formats Date as 'dd.MM.yyyy hh:mm:ss' string
         *
         * @param {Date} date
         *
         * @return {String}
         */
        datetimeUTC: function (date) {
            if (!date && (this instanceof Date)) {
                date = this;
            }

            return I.helpers.dateUTC(date) + ' ' + I.helpers.timeUTC(date);
        },

        debug: function () {
            console.log(this);
        }
    };
    // Mix helpers to the Mus
//    $.extend(Mus.helpers, I.helpers);

    /**
     * Form helpers
     *
     * @type {Object}
     */
    I.form = {
        params: function (form, options) {
            var data = {},
                beginning = null;

            options = $.extend({}, {exclude: null}, options);

            // Get values and types from the form fields
            $(form).find('input[name],select[name],textarea[name]').each(function () {
                var el = $(this),
                    name = el.attr('name'),
                    type = el.attr('type'),
                    custom = el.data('tranny-type') || I.Tranny.types.STRING,
                    value = el.attr('value'),
                    key,
                    index,
                    match;

                if (type === 'checkbox') {
                    // Fix values for checkboxes
                    value = +el.prop('checked');
                } else if ((type === 'radio') && (!el.prop('checked'))) {
                    // Skip unckecked radio buttons
                    return true;
                }

                switch (custom) {

                    case 'datetime':
                    case I.Tranny.types.DATETIME:
                        match = value.trim().match(/^(\d{2})\.(\d{2})\.(\d{2})(\d{2})( (\d{2}):(\d{2})(?::(\d{2}))?)?/);
                        if (match) {
                            // Make date (day should be padded with zero if it's less than 10)
                            value = (match[4] + match[2] + match[1]) + (match[5] ? (match[6] + match[7]) : '0000') + (match[8] || '00');
                        }
                        break;

                    case I.Tranny.types.FLOAT:
                        value = parseFloat(value);
                        break;

                    case I.Tranny.types.INT16:
                    case I.Tranny.types.INT32:
                    case I.Tranny.types.INT64:
                        value = parseInt(value, 10);
                        break;

                    case I.Tranny.types.BOOLEAN:
                        value = parseInt(value, 10) ? 1 : 0;
                        break;
                }

                if ((name.indexOf('[') !== -1) && (name.indexOf(']') !== -1)) {
                    // Parse array parameters
                    key = name.replace(/\]/g, '').split('[');
                    index = parseInt(key[1], 10);

                    if (beginning === null) {
                        beginning = index;
                    }

                    if (!data.hasOwnProperty(key[0])) {
                        // Create dataset if it not exists
                        data[key[0]] = {
                            cols: [],
                            types: [],
                            rows: []
                        };
                    }

                    if (beginning === index) {
                        // Create column name and type if parsing first row
                        data[key[0]].cols.push(key[2]);
                        data[key[0]].types.push(custom);
                    }

                    // Push values into dataset
                    if (!data[key[0]].rows[index]) {
                        // Create empty row if row with current index isn't exist
                        data[key[0]].rows[index] = [];
                    }
                    data[key[0]].rows[index].push(value);
                } else {
                    data[name] = value;
                }
            });

            // Filter out excluded params
            if (options.exclude) {
                $.each(data, function (key) {
                    if (key.match(options.exclude)) {
                        delete data[key];
                    }
                });
            }

            return data;
        },

        types: function (form) {
            var $form = $(form),
                types = {},
                result;

            $form.find('input,select,button').each(function () {
                var el = $(this),
                    name = el.attr('name'),
                    type = el.data('type');
                if (name && type) {
                    types[name] = type;
                }
            });

            // Parse array parameters
            result = {};
            $.each(types, function (key, param) {
                // I there are brackets, parameter is value in the array
                if ((key.indexOf('[') !== -1) && (key.indexOf(']') !== -1)) {
                    // Split string
                    key = key.replace(/\]/g, '').split('[');

                    // Make recursive walk over parameter name chunks
                    var f = function (arr, path, value) {
                        if (path.length === 1) {
                            // If it's last element, apply it
                            arr[path[0]] = value;
                        } else {

                            if (!arr.hasOwnProperty(path[0])) {
                                // Otherwise create element if it doesn't exist already
                                arr[path[0]] = {};
                            }

                            // Apply rest of the path
                            arr[path[0]] = f(arr[path[0]], path.slice(1), value);
                        }

                        return arr;
                    };
                    f(result, key, param);
                } else {
                    result[key] = param;
                }
            });

            $.each(result, function (key, param) {
                var plain = true,
                    last;

                if (typeof param === 'object') {
                    // If object, check keys for being integer and save them in the array

                    $.each(param, function (i) {
                        if (isNaN(parseInt(i, 10))) {
                            plain = false;
                        } else {
                            last = i;
                        }
                    });

                    if (plain) {
                        result[key] = param[last];
                    }
                }
            });

            return result;
        }
    };

});
