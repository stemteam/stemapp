define([
    'jquery',
    '_services/base'
], function ($) {
    'use strict';

    I.Tranny = function (dataset, complex, timezoneOffset) {

        timezoneOffset = typeof timezoneOffset == 'undefined' ? true : false;

        /**
         * @var {array} Dataset columns
         */
        this._cols = [];

        /**
         * @var {array} Dataset column types
         */
        this._types = [];

        /**
         * @var {array} Dataset rows
         */
        this._rows = [];

        /**
         * @var {Number} Dataset length
         */
        this._length = null;

        /**
         * @var {Boolean} Will be True when dataset will be valid, False if don't
         */
        this._valid = false;

        /**
         * Constructor
         *
         * @constructor
         * @param {Object|String} dataset   Infokinetika.Dataset to apply
         * @param {Boolean}       [complex] Return operable Tranny object instead of plain Array if initialized with string
         *
         * @return {Object|Array}
         */
        this.init = function (dataset, complex) {
            if ($.isPlainObject(dataset)) {
                this.apply(dataset);
            } else if (typeof dataset === 'string') {
                this.from_string(dataset);
            }
            // Return plain array if not asked to return self
            if (dataset && (complex !== true)) {
                return this.valid() ? this.rows() : [];
            }

            return true;
        };

        /**
         * Apply dataset
         *
         * @param {Object} dataset Infokinetika.Dataset to apply
         *
         * @return {Boolean}
         */
        this.apply = function (dataset) {
            var cols,
                types,
                rows = [],
                totalCols,
                stopped = false;
            this.clear();

            // Assume that data is invalid
            this._valid = false;

            if (!I.Tranny.isValidTransport(dataset)) {
                return false;
            }

            // Apply column names and data types
            cols = this._cols = dataset[I.Tranny.COLS];
            types = this._types = dataset[I.Tranny.TYPES];
            totalCols = this._cols.length;

            // Apply rows one by one
            $.each(dataset[I.Tranny.ROWS], function (k, val) {
                if (typeof val === 'undefined') {
                    // Skip undefined rows
                    return true;
                } else if (val.length !== totalCols) {
                    // Stop applying rows if wrong one is found
                    stopped = true;
                    return false;
                }

                var row = {};

                // Apply columns one by one
                $.each(val, function (c, match) {
                    var m,
                        d;

                    // Null values are applied immediately
                    if (match === null) {
                        row[cols[c]] = null;
                    } else {
                        switch (types[c]) {


                            case I.Tranny.types.DATE:
                            case I.Tranny.types.DATETIME:
                                m = 0;
                                // Check date format
                                if (match.match(/\d{12}\.\d+/)) {
                                    // Cut and save milliseconds
                                    d = match.split('.');
                                    m = parseInt(d[1], 10);
                                    d = d[0];
                                } else if (!match.match(/\d{12}/)) {
                                    // Wrong date received, return Null
                                    row[cols[c]] = null;
                                    break;
                                }

                                // Split date string
                                d = match.match(/\d\d/g);

                                d[0] = parseInt(d[0], 10);

                                // Apply date
                                row[cols[c]] = new Date(
                                    ((d[0] < 70) ? 2000 : 1900) + d[0],
                                    parseInt(d[1], 10) - 1,
                                    parseInt(d[2], 10),
                                    parseInt(d[3], 10),
                                    parseInt(d[4], 10),
                                    parseInt(d[5], 10),
                                    m
                                );

                                row[cols[c]] = timezoneOffset ? row[cols[c]].getTime() / 1000 - (new Date()).getTimezoneOffset() * 60 : row[cols[c]].getTime() / 1000;

                                // Fix local timezone offset
                                /*
                                 if (I.timezone.offset !== 0) {
                                 row[cols[j]].setTime(row[cols[j]].getTime() + I.timezone.offset);
                                 }
                                 */

                                break;

                            case I.Tranny.types.TIMESTAMP:
                            case I.Tranny.types.TIMESTAMP2:


                                row[cols[c]] = Stemapp.util.convert.jaxisDatetimeToDate(match.slice(2, 14)).getTime() / 1000;
                                break;

                            case I.Tranny.types.INT16:
                            case I.Tranny.types.INT32:
                            case I.Tranny.types.INT64:
                                row[cols[c]] = parseInt(match, 10);
                                break;

                            case I.Tranny.types.FLOAT:
                                row[cols[c]] = parseFloat(match);
                                break;

                            case I.Tranny.types.STRING:
                                row[cols[c]] = match.toString();
                                break;

                            case I.Tranny.types.BOOLEAN:
                                row[cols[c]] = !!match;
                                break;

                            case I.Tranny.types.DATASET:
                                row[cols[c]] = new I.Tranny(match);
                                break;

                            default:
                                row[cols[c]] = match;
                        }
                    }
                });

                // Push row to the dataset
                rows.push(row);
            });

            if (!stopped) {
                // Save length
                this._length = rows.length;
                this._rows = rows;
                this._valid = true;
                return true;
            }

            return false;
        };

        /**
         * Clears current dataset
         *
         * @return {void}
         */
        this.clear = function () {
            this._cols = [];
            this._types = [];
            this._rows = [];
            this._length = null;
            this._valid = false;
        };

        /**
         * Returns dataset
         *
         * @return {Array}
         */
        this.rows = function () {
            return this._rows;
        };

        /**
         * Returns validness state of the dataset
         *
         * @return {Boolean}
         */
        this.valid = function () {
            return this._valid;
        };

        /**
         * Returns number of rows in dataset
         *
         * @return {Number}
         */
        this.length = function () {
            return this._length;
        };

        /**
         * Serialize to JSON
         *
         * @return {String}
         */
        this.to_string = function () {
            var dataset = {},
                self = this,
                rows = [];

            dataset[I.Tranny.COLS] = this._cols;
            dataset[I.Tranny.TYPES] = this._types;

            $.each(this._rows, function () {
                var row = [],
                    columns = this;

                $.each(self._cols, function (i, name) {
                    // Boolean values passed as numbers
                    if (self._types[i] === I.Tranny.types.BOOLEAN) {
                        row.push(columns[name] ? 1 : 0);
                    } else {
                        row.push(columns[name]);
                    }
                });

                rows.push(row);
            });

            dataset[I.Tranny.ROWS] = rows;

            return JSON.stringify(dataset);
        };

        /**
         * Deserialize Transport from JSON string
         *
         * @param {String} string String to deserialize from
         *
         * @return {Boolean}
         */
        this.from_string = function (string) {
            if (string) {
                return this.apply(JSON.parse(string));
            } else {
                return false;
            }
        };

        return this.init(dataset, complex);
    };


    /**
     * Version
     */
    I.Tranny.VERSION = Stemapp.config.apiVersion;

    /**
     * Data types constants
     */
    I.Tranny.types = {
        INT16: 'Int16',
        INT32: 'Int32',
        INT64: 'Int64',
        FLOAT: 'Decimal',
        STRING: 'String',
        BOOLEAN: 'Boolean',
        GEOMETRY: 'Geometry',
        DATASET: 'DataSet',
        DATE: 'Date',
        DATETIME: 'DateTime',
        TIMESTAMP2: 'Timestamp',
        TIMESTAMP: 'TimeStamp'
    };


    /**
     * Property names
     */
    I.Tranny.COLS = 'cols';
    I.Tranny.TYPES = 'types';
    I.Tranny.ROWS = 'rows';

    I.Tranny.isValidTransport = function (obj) {
        return $.isPlainObject(obj)
            && obj.hasOwnProperty(I.Tranny.COLS)
            && obj.hasOwnProperty(I.Tranny.TYPES)
            && obj.hasOwnProperty(I.Tranny.ROWS)
            && $.isArray(obj.cols)
            && $.isArray(obj.types)
            && $.isArray(obj.rows)
            && (obj.cols.length === obj.types.length)
            && (obj.rows.length ? (obj.rows[obj.rows.length - 1].length === obj.cols.length) : true);
    };

    // Make nice shortcut
    I.isTranny = I.Tranny.isValidTransport;

});