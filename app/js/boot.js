(function () {
    'use strict';


    var Boot = {

        devmode: false,

        progress: 0,
        maxprogress: 10,

        minStartTime: 1500,

        startTime: null,

        /**
         * Запускает загрузку скриптов
         */
        init: function (success) {
            this.success = success ? success : null;
            this.devmode = this.checkDevMode();
            if (this.devmode) {
                console.log('devmode');
            }
            var me = this;
            if (this.checkBrowser()) {
                this.ajax('build?rnd=' + Math.random(), function (data) {
                    me.setVersion(data);
                });
            }
            this.startTime = new Date();
        },


        start: function (html) {
            window.boot.setProgress();
            $('body').append(html);
            Stemapp.App = new Stemapp.View.App({login: false});
        },

        /**
         * Проверка продакшн это или девелоп
         * @returns {boolean}
         */
        checkDevMode: function () {
            var cookie = this.parseCookie();
            if (window.location.search) {
                var query = this.parseQuery(window.location.search);
                if (query.devmode && (query.devmode == 'on' || query.devmode == 'true')) {
                    console.log('Development mode on');
                    document.cookie = "devmode=on; expires=" + (new Date(new Date().getFullYear() + 1, 1, 1));
                    alert('Development mode on');
                }

                if (query.devmode && (query.devmode == 'off' || query.devmode == 'false')) {
                    console.log('Development mode off');
                    document.cookie = "devmode=on; expires=" + (new Date(0));
                    alert('Development mode off');
                }
            }
            return (cookie && cookie.devmode == 'on') ? true : false;
        },

        /**
         * Разбор строки
         * @param query
         * @returns {{}}
         */
        parseQuery: function (query) {
            var result = {};
            var keyValue = query.replace('?', '').split('&');
            for (var i = 0; i < keyValue.length; i++) {
                var tmp = keyValue[i].split('=');
                if (tmp[1]) {
                    result[tmp[0].trim()] = tmp[1].trim();
                } else {
                    result[tmp[0]] = 'on';
                }
            }
            return result;
        },

        /**
         * Разбор куков
         * @returns {{}}
         */
        parseCookie: function () {
            var cookie = document.cookie;
            var result = {};
            if (cookie) {
                var keyValue = cookie.split(';');
                for (var i = 0; i < keyValue.length; i++) {
                    var tmp = keyValue[i].split('=');
                    result[tmp[0].trim()] = tmp[1].trim();
                }
            } else {
                result = false;
            }
            return result;

        },


        /**
         *
         * @param url
         * @param callback
         */
        ajax: function (url, callback) {
            var xmlhttp;

            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }

            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    if (xmlhttp.status == 200) {
                        callback(xmlhttp.responseText);
                    }
                    else if (xmlhttp.status == 400) {
                        alert('There was an error 400')
                    }
                    else {
                        alert('something else other than 200 was returned')
                    }
                }
            };

            xmlhttp.open("GET", url, true);
            xmlhttp.send();
        },

        /**
         *
         * @param data
         */
        setVersion: function (data) {
            var version = data;
            var revision = this.calcRevision(version);
            this.setInitParam(version, revision);
        },

        /**
         *
         * @param version
         * @returns {number}
         */
        calcRevision: function (version) {
            var ver = version.split('.');
            var revision = 0;
            for (var i = 0; i < ver.length; i++) {
                revision += Math.pow(10, ver.length - i - 1) * ver[i];
            }
            return revision;
        },


        /**
         *
         * @param version
         * @param revision
         */
        setInitParam: function (version, revision) {
            window.App = {};
            window.Stemapp = {};

            Stemapp.version = version;
            Stemapp.opts = {
                revision: revision,
                path: 'v' + revision,
                jspack: 'build/v' + revision + '/app.build.js',
                csspack: 'build/v' + revision + '/app.build.css'
            };
            Stemapp.unload = true; // Флаг для проверки выхода из приложения(выйти с вопросом или без)
            this.loadBootScript();


            /**
             * Проверка на наличие модуля
             * @param {String} name
             * @returns {boolean}
             */
            Stemapp.moduleExists = function (name) {
                if (name[0] !== '_') {
                    name = '_' + name;
                }
                return !!Stemapp._modules && !!Stemapp._modules[name];
            };
        },

        addScript: function (src, onload) {

            var root = document.getElementsByTagName('body')[0];
            var sc = document.createElement('script');
            sc.setAttribute('async', false);
            sc.src = src;
            sc.onload = onload ? onload : function () {
            };
            root.insertBefore(sc, root.firstChild);

        },

        addCss: function (src, isLess) {
            var root = document.getElementsByTagName('body')[0];
            var css = document.createElement('link');
            css.rel = 'stylesheet';
            css.type = 'text/css';
            css.media = 'all';
            css.setAttribute('async', false);
            css.href = src;
            if (isLess) {
                css.rel = "stylesheet/less";
            }
            root.insertBefore(css, root.firstChild);
        },


        /**
         *
         */
        loadBootScript: function () {
            var me = this;
            var i;
            if (this.devmode) {
                window.less = {env: 'development'};
                me.addCss('resource/css/main.less', true);
                me.addScript('config.js', function () {
                    me.addScript('node_modules/less/dist/less.min.js', function () {
                        me.ajax('config_modules.json', function (data) {
                            data = JSON.parse(data);
                            Stemapp.modules = {};
                            for (i = 0; i < data.length; i++) {
                                Stemapp.modules[data[i]] = {};
                                me.addScript('app/js/require/require.js', function () {
                                    if (me.success) {
                                        me.success();
                                    } else {
                                        require(["_app/init"]);
                                    }
                                });
                            }
                        });
                    });
                });
            } else {

                this.addCss(Stemapp.opts.csspack);
                this.addScript(Stemapp.opts.path + '/config.js', function () {
                    require.baseUrl = Stemapp.opts.path;
                    me.ajax(Stemapp.opts.path + '/config_modules.json', function (data) {
                        data = JSON.parse(data);
                        Stemapp.modules = {};
                        for (i = 0; i < data.length; i++) {
                            Stemapp.modules[data[i]] = {};
                        }
                        me.addScript(Stemapp.opts.jspack);
                    });
                });

            }
        },

        loadCssList: function (data) {
            var files = JSON.parse(data);
            for (var k in files) {
                if (files.hasOwnProperty(k)) {
                    for (var i = 0; i < files[k].length; i++) {

                        this.addCss(files[k][i], k == 'less');
                    }
                }
            }
        },

        getBrowser: function () {
            var N = navigator.appName, ua = navigator.userAgent, tem;
            var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
            if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
            M = M ? [M[1], M[2]] : [N, navigator.appVersion, '-?'];
            return M;
        },

        checkBrowser: function () {
            var ua = this.getBrowser();
            var version = parseFloat(ua[1]);
            var name = ua[0];
            var old = false;

            if (name.toLowerCase() == 'msie' && version < 9) {
                old = true;
            }

            if (name.toLowerCase() == 'opera' && version < 11) {
                old = true;
            }

            if (name.toLowerCase() == 'firefox' && version < 15) {
                old = true;
            }

            if (name.toLowerCase() == 'chrome' && version < 23) {
                old = true;
            }

            if (old) {
                document.getElementById('oldversion').style.display = 'block';
                document.getElementById('preload').style.display = 'none';
            }

            return !old;
        },

        setProgress: function () {

            var val;
            var me = this;
            if (arguments.length == 2) {
                val = arguments[1];
            } else {
                val = arguments[0];
            }
            val = val ? val : this.progress++;
            this.progress = val;
            this.progress = Math.min(this.progress, this.maxprogress);
            $('.js-bar').each(function () {
                $(this).width($(this).parent().width() / me.maxprogress * me.progress);
            });
        }

    };

    window.Boot = Boot;
    // Если запущено не для тестов
    if (!window.jasmine) {
        Boot.init();
    }


})();
