$.extend($.layout.defaults, {
    showErrorMessages: false
});

$.extend($.layout.defaults.panes. tips,{
    Open: "Open"		// eg: "Open Pane"
    , Close: "Close", Resize: "Изменить размер", Slide: "Slide Open", Pin: "Pin", Unpin: "Un-Pin", noRoomToOpen: "Not enough room to show this panel."	// alert if user tries to open a pane that cannot
    , minSizeWarning: "Panel has reached its minimum size"	// displays in browser statusbar
    , maxSizeWarning: "Panel has reached its maximum size"	// ditto
});