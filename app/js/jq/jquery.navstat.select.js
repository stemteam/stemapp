
(function( factory ) {
    "use strict";
    if(typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    }
    else if(jQuery && !jQuery.fn.qtip) {
        factory(jQuery);
    }
}
(function ($) {
    "use strict";


    var NavstatSelect = function () {

        this.notblur = false;

        this.init = function (input) {

            this.$input = $(input);
            this.$list = this.$input.closest('.select-input-block').find('.select-input-list');
            this.$hidden = this.$input.closest('.select-input-block').find('[data-input-main]');
            this.$input.on('blur', _.bind(this.onInputBlur, this));
            this.$input.on('focus', _.bind(this.onInputFocus, this));
            this.$input.on('click', _.bind(this.onInputClick, this));

            this.$input.on('keydown', _.bind(this.onKeyDown, this));
            this.$list.on('keydown', _.bind(this.onKeyDown, this));

            this.$list.on('mousedown', _.bind(this.onMouseDownList, this));
            this.$list.on('click', _.bind(this.onClickList, this));

            this.$list.on('click', 'a', _.bind(this.onClickSelectA, this));

            this.$hidden.on('change update', _.bind(this.onChangeHidden, this));
            $('#content').on('scroll', _.bind(this.onWindowScroll, this));
        };

        this.onChangeHidden = function () {
            var text = this.$list.find('li a[data-id="' + this.$hidden.val() + '"]').text();
            this.$input.val(text);
            if (this.$hidden.val() == 0)
                this.$hidden.parent().removeClass('active');
            else
                this.$hidden.parent().addClass('active');
        };

        this.onWindowScroll = function(e){

            this.hideList();
        };


        this.onClickSelectA = function (e) {

            this.notblur = false;
            if ($(e.currentTarget).attr('disabled')) return false;
            this.hideList();

            if (this.$hidden.val() != $(e.currentTarget).attr('data-id'))
                this.$hidden.val($(e.currentTarget).attr('data-id')).change();
            this.$input.focus();
            return false;
        };

        this.onMouseDownList = function (e) {
            this.notblur = true;
            e.stopPropagation();
        };

        this.onClickList = function (e) {
            this.notblur = false;
            e.stopPropagation();
        };

        this.onInputBlur = function (e) {

            var id = this.$hidden.val();
            var text = this.$list.find('li a[data-id="' + id + '"]').text();
            this.$input.val(text);

            this.$input.removeClass('focus');

            _.delay(_.bind(
                function () {

                    if (this.$input.hasClass('focus') || this.notblur) {
                        return;
                    }
                    this.hideList();
                }, this), 100);
        };

        this.onInputClick = function (e) {

            this.notblur = false;

            if (!this.$list.is(':visible')) {
                this.$list.find('li a.active').removeClass('active');
                this.$list.find('li a[data-id="' + this.$hidden.val() + '"]').addClass('active');
                this.showList();
                this.showElement(this.$list.find('a.active'), true);
            }
            else {
                this.hideList();
            }
            e.stopPropagation();
        };

        this.onInputFocus = function () {

            if (!this.$list.is(':visible')) {
                this.$list.find('li a.active').removeClass('active');
                this.$list.find('li a[data-id="' + this.$hidden.val() + '"]').addClass('active');
                this.showElement(this.$list.find('a.active'), true);
            }
        };

        this.onKeyDown = function (e) {

            if (e.keyCode == 13 || e.keyCode == 10 || e.keyCode == 27) {
                if (this.$list.is(':visible')) {
                    this.hideList();
                    this.$input.focus();
                }
            }

            // page down
            if (e.keyCode == 34) {

                var hide = false;
                if (!this.$list.is(':visible')) {

                    this.$list.show();
                    hide = true;
                }

                var lh = this.$list.height(),
                    ih = this.$list.find('li:first').height(),
                    count = Math.ceil(lh / ih) - 1;

                var active = this.$list.find('.active'),
                    index = active.parent().index() + 1,
                    all = this.$list.children().length,
                    item;

                if (all > index + count) {

                    item = this.$list.children().eq(index + count - 1);

                } else {

                    item = this.$list.children().eq(all - 1);
                }
                this.showElement(item.find('a'), true);

                if (hide) {
                    this.hideList();
                }
                this.$hidden.change();
                return false;
            }

            // end
            if (e.keyCode == 35) {

                this.showElement(this.$list.find('li:last a'), true);
                this.$hidden.change();
                return false;
            }

            // home
            if (e.keyCode == 36) {

                this.showElement(this.$list.find('li:first a'), false);
                this.$hidden.change();
                return false;
            }

            // page up
            if (e.keyCode == 33) {

                var hide = false;
                if (!this.$list.is(':visible')) {

                    this.$list.show();
                    hide = true;
                }

                var lh = this.$list.height(),
                    ih = this.$list.find('li:first').height(),
                    count = Math.ceil(lh / ih) - 1;

                var active = this.$list.find('.active'),
                    index = active.parent().index() + 1,
                    all = this.$list.children().length,
                    item;

                if (0 < index - count) {

                    item = this.$list.children().eq(index - count);

                } else {

                    item = this.$list.children().eq(0);
                }
                this.showElement(item.find('a'), false);


                if (hide) {
                    this.hideList();
                }
                this.$hidden.change();
                return false;
            }

            // arrow down
            if (e.altKey && (e.keyCode == 40 || e.keyCode == 38)) {

                if (!this.$list.is(':visible')) {
                    this.showList();
                }
                else {
                    this.hideList();
                }
                this.$input.focus();
                return false;
            }

            // arrow down
            if (e.keyCode == 40) {

                var active = this.$list.find('.active');
                var item = active.parent().next().find('a');
                if (item.length == 0) {
                    return false;
                }
                this.showElement(item, true);
                this.$hidden.change();
                return false;
            }
            // arrow up
            if (e.keyCode == 38) {

                var active = this.$list.find('.active');
                var item = active.parent().prev().find('a');
                if (item.length == 0) {
                    return false;
                }
                this.showElement(item, false);
                this.$hidden.change();
                return false;
            }

            if (e.keyCode == 9) {
                this.notblur = false;
                this.hideList();
            }

            if (e.keyCode != 9 && e.keyCode != 13 && e.keyCode != 10) {
                return false;
            }
        };

        this.showList = function () {

            var offset = this.$input.offset(),
                top = offset.top + this.$input.outerHeight();
            this.$list.css({zIndex: 101, position: 'absolute', left: offset.left, top: top}).appendTo('body').animate({opacity: 'show'}, 200);//.click();
            this.$list.outerWidth(this.$input.outerWidth());
            var h = this.$list.outerHeight();
            if ($(window).height() < top + h) {
                this.$list.css({top: offset.top - h});
            }
        };

        this.hideList = function () {

            this.$list.appendTo(this.$input.parent());
            this.$list.hide();
        };

        this.showElement = function (item, down) {
            down = !!down;

            if (!item.length) {
                return;
            }
            this.$list.find('.active').removeClass('active');

            item.addClass('active');

            if (!item.length) {
                return;
            }
            if (down) {

                if (item.offset().top < this.$list.offset().top) {
                    this.$list.scrollTo(item);
                }

                if (this.$list.height() + this.$list.offset().top < item.offset().top + item.outerHeight()) {

                    this.$list.scrollTo(item.offset().top - this.$list.offset().top + this.$list.scrollTop() - this.$list.height() + item.outerHeight());
                }

            } else {

                if (item.offset().top > this.$list.offset().top + this.$list.height()) {
                    this.$list.scrollTo(item);
                }

                if (item.offset().top < this.$list.offset().top) {

                    this.$list.scrollTo(item.offset().top - this.$list.offset().top + this.$list.scrollTop());
                }

            }
            this.$hidden.val(item.attr('data-id'));

        }
    };


    $(document).on('click focus', '.select-input', function (e) {

        if ($(this).data('ns')) {
            return;
        }
        var ns = new NavstatSelect();
        $(this).data('ns', ns);
        ns.init(this);
    });

    $(document).on('change', '.select-input-block [data-input-main]', function (e) {
        var $input = $(this).closest('.select-input-block').find('.select-input');
        var ns = $input.data('ns');
        if (!ns) {
            ns = new NavstatSelect();
            $input.data('ns', ns);
            ns.init($input);
            ns.onChangeHidden();
        }
        var val = $(this).val();
        $input.setVal(val);
    });
    /**
     * For select-input
     */
    $.fn.setVal = function (val) {

        var id, parent;
        if ($(this).attr('data-input-main')) {

            id = $(this).attr('data-input-main');
            parent = $(this).parent('.select-input-block');
            if (parent.length == 0) {
                console.error('Not found parent');
                return this;
            }
            text = parent.find('ul[data-input="' + id + '"] li a[data-id="' + val + '"]').text();
            $('#' + id).val(text);
            $(this).val(val);
        }
        else {

            id = $(this).attr('id');

            parent = $(this).parent('.select-input-block');
            if (parent.length == 0) {
                console.error('Not found parent');
                return this;
            }

            var text = parent.find('ul[data-input="' + id + '"] li a[data-id="' + val + '"]').text();

            $(this).val(text);

            parent.find('input[data-input-main="' + id + '"]').val(val);
        }
        return this;
    };


    $.fn.getVal = function () {
        if ($(this).attr('data-input-main')) {
            return $(this).val();
        }
        else {
            var id = $(this).attr('id'),
                parent = $(this).parent('.select-input-block');
            if (parent.length == 0) {
                console.error('Not found parent');
                return this;
            }

            return parent.find('input[data-input-main="' + id + '"]').val();
        }
    };

}));
