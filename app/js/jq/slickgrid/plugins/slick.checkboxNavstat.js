(function ($) {
    // register namespace
    $.extend(true, window, {
        "Slick": {
            "CheckboxNavstat": CheckboxNavstat
        }
    });


    function CheckboxNavstat(options) {
        var _grid;
        var _self = this;
        var _handler = new Slick.EventHandler();
        var _selectedRowsLookup = {};
        var _defaults = {
            columnId: "checkbox",
            cssClass: null,
            toolTip: "",
            width: 32,
            onChange: function () {}
        };
        var count = 0;

        var _options = $.extend(true, {}, _defaults, options);

        function init(grid) {
            _grid = grid;

            _handler
                .subscribe(_grid.onSelectedRowsChanged, handleSelectedRowsChanged)
                .subscribe(_grid.onClick, handleClick)
                .subscribe(_grid.onHeaderClick, handleHeaderClick)
                .subscribe(_grid.onKeyDown, handleKeyDown);

        }

        function handleHeaderClick(e, args) {
            if (args.column.id == _options.columnId && $(e.target).is(":checkbox")) {
                // if editing, try to commit
                $(e.target).removeAttr('class');
                if (_grid.getEditorLock().isActive() && !_grid.getEditorLock().commitCurrentEdit()) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    return;
                }

                if ($(e.target).is(":checked")) {
                    toggleAllRowSelection('select');
                } else {
                    toggleAllRowSelection('deselect');
                }
                e.stopPropagation();
                e.stopImmediatePropagation();
            }
            //$(e.target).attr('class','indeterminate');
        }


        function handleSelectedRowsChanged(e, args) {
            var selectedRows = _grid.getSelectedRows();
            var lookup = {}, row, i;
            for (i = 0; i < selectedRows.length; i++) {
                row = selectedRows[i];
                lookup[row] = true;
                if (lookup[row] !== _selectedRowsLookup[row]) {
                    _grid.invalidateRow(row);
                    delete _selectedRowsLookup[row];
                }
            }
            for (i in _selectedRowsLookup) {
                _grid.invalidateRow(i);
            }
            _selectedRowsLookup = lookup;
            _grid.render();

        }

        function handleClick(e, args) {
            // clicking on a row select checkbox
            if (_grid.getColumns()[args.cell].id === _options.columnId && $(e.target).is(":checkbox")) {
                // if editing, try to commit
                if (_grid.getEditorLock().isActive() && !_grid.getEditorLock().commitCurrentEdit()) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    return;
                }

                toggleRowSelection(args.row);
                e.stopPropagation();
                e.stopImmediatePropagation();

                var selectedRows = _grid.getSelectedRows();

                var items = _grid.getData().getItems(),
                    res = [0, 0];


                var selectedItem = 0;
                for (var i = 0; i < _grid.getDataLength(); i++) {
                    selectedItem = (_grid.getData().getItem(i).checkbox > 0) ? selectedItem+1 : selectedItem;
                }

                //добавление/удаление checkbox для инкапсуляции в рамках slick
                if(!selectedItem) {
                    //console.log('Выключены все');
                    //$('#slickHeaderCkeckbox').prop('checked',false);
                    //$('#slickHeaderCkeckbox').prop('indeterminate',false);
                    _grid.updateColumnHeader(_grid.getColumns()[0].id,'<input type="checkbox" id="slickHeaderCkeckbox">');
                } else if(selectedItem === _grid.getDataLength()) {
                    //console.log('Включены все');
                    //$('#slickHeaderCkeckbox').prop('checked',true);
                    //$('#slickHeaderCkeckbox').prop('indeterminate',false);
                    _grid.updateColumnHeader(_grid.getColumns()[0].id,'<input type="checkbox" id="slickHeaderCkeckbox" checked="checked">');
                } else if(selectedItem < _grid.getDataLength()) {
                    //console.log('Часть отключена');
                    _grid.updateColumnHeader(_grid.getColumns()[0].id,'<input type="checkbox" id="slickHeaderCkeckbox" class="indeterminate">');
                    $('.indeterminate').prop('indeterminate',true);
                    //$('#slickHeaderCkeckbox').prop('checked',false);
                    //$('#slickHeaderCkeckbox').prop('indeterminate',true);
                }
            }
        }

        function handleKeyDown(e, args) {

            // break
            if (e.which == 32) {

                if (!_grid.getEditorLock().isActive() || _grid.getEditorLock().commitCurrentEdit()) {
                    toggleRowSelection(args.row);
                }
                e.preventDefault();
                e.stopImmediatePropagation();
            }
        }

        function toggleRowSelection(row) {

            var item = _grid.getData().getItem(row);

            item['checkbox'] = item['checkbox'] == 1 ? -1 : 1;

            _grid.getData().updateItem(item.id, item);

            _options.onChange(item);

        }

        function toggleAllRowSelection(action) {
            for (var i = 0; i < _grid.getDataLength(); i++) {
                var item = _grid.getData().getItem(i);
                item['checkbox'] = action == 'select' ? 1 : -1 ;
                _grid.getData().updateItem(item.id, item);
                _options.onChange(item);
            }

        }


        function destroy() {
            _handler.unsubscribeAll();
        }

        function getColumnDefinition() {
            return {
                id: _options.columnId,
                field: _options.columnId,
                name: "<input type='checkbox' checked='checked' id='slickHeaderCkeckbox'>",
                toolTip: _options.toolTip,
                width: _options.width,
                resizable: false,
                sortable: false,
                cssClass: _options.cssClass,
                formatter: checkboxSelectionFormatter
            };
        }

        function checkboxSelectionFormatter(row, cell, value, columnDef, dataContext) {

            if (dataContext) {

                value = value === false ? -1 : value;
                value = value === true ? 1 : value;

                if (value == -1) {
                    return "<input type='checkbox'>";
                }
                if (value == 1) {
                    return "<input type='checkbox' checked='checked'>";
                }
                if (value == 0) {
                    return "<input type='checkbox' class='indeterminate'>";
                }

            }
            return null;
        }

        $.extend(this, {
            "init": init,
            "destroy": destroy,

            "getColumnDefinition": getColumnDefinition
        });
    }
})(jQuery);