(function ($) {
    // register namespace
    $.extend(true, window, {
        "Slick": {
            "CounterColumn": CounterColumn
        }
    });


    function CounterColumn(options) {
        var _grid;
        var _self = this;
        var _handler = new Slick.EventHandler();
        var _selectedRowsLookup = {};
        var _defaults = {
            columnId: "counter",
            cssClass: null,
            toolTip: "",
            width: 30

        };

        var _options = $.extend(true, {}, _defaults, options);

        function init(grid) {
            _grid = grid;
        }

        function destroy() {
            _handler.unsubscribeAll();
        }

        function handleSelectedRowsChanged(e, args) {
//      var selectedRows = _grid.getSelectedRows();
//      var lookup = {}, row, i;
//
//      for (i in _selectedRowsLookup) {
//        _grid.invalidateRow(i);
//      }
//      _selectedRowsLookup = lookup;
//      _grid.render();
//
//      if (selectedRows.length && selectedRows.length == _grid.getDataLength()) {
//        _grid.updateColumnHeader(_options.columnId, "<input type='checkbox' checked='checked'>", _options.toolTip);
//      } else {
//        _grid.updateColumnHeader(_options.columnId, "<input type='checkbox'>", _options.toolTip);
//      }
        }


        function getColumnDefinition() {
            return {
                id: _options.columnId,
                name: '<div class="tar">№</div>',
                toolTip: _options.toolTip,
                field: "counter",
                width: _options.width,
                resizable: false,
                sortable: false,
                cssClass: _options.cssClass,
                formatter: checkboxSelectionFormatter
            };
        }

        function checkboxSelectionFormatter(row, cell, value, columnDef, dataContext) {
            if (dataContext) {
                if (value) {
                    return '<div class="counter"><span>' + value + '</span></div>';
                }
                return '<div class="counter"><span>' + (row + 1) + '</span></div>';
            }
            return null;
        }

        $.extend(this, {
            "init": init,
            "destroy": destroy,
            "getColumnDefinition": getColumnDefinition
        });
    }
})(jQuery);