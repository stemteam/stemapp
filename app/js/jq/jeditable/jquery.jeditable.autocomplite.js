/**
 * Created by phplamer on 19.10.14.
 */

$.editable.addInputType('autocomplite', {
    /* create input element */
    element : function(settings, original) {
        var input = $('<input />');
        if (settings.width  != 'none') { input.width(settings.width);  }
        if (settings.height != 'none') { input.height(settings.height); }
        input.attr('autocomplete','off');
        $(this).append(input);
        return(input);
    },
    content : function(string, settings, original) {
        var delimeter = settings.delimiter ? settings.delimiter : ',';
        $(':input:first', this).val(string ? string+delimeter : '');
    },
    plugin : function(settings, original) {
        var delimeter = settings.delimiter ? settings.delimiter : ',';

        function getLastValue(value) {
            return value.split(new RegExp(delimeter+"\s*")).pop().trim();
        }

        //console.log(this.find('input'));
        //console.log(settings.valueList);


        $(this.find('input')).autocomplete({
            minLength: 0,
            multiSelect: true,
            source: function( request, response ) {
                var filtered = $.ui.autocomplete.filter(
                    settings.valueList, getLastValue(request.term)
                );

                response(filtered);
            },
            response: function (event, ui) {
                if (ui.content.length < 1) {
                    ui.content.push({
                        'label': 'Не найдено "'+event.target.value+'"',
                        'value': '123'
                    });
                }
            },
            select: function( event, ui ) {
                //var terms = split( this.value );
                var terms = this.value.split(new RegExp(delimeter+"\s*")).map(function(value) { return value.trim(); });
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push( ui.item.value );
                // add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join(delimeter+" ");
                return false;
            }
        }).on('focus', function(event) {
            $(this).autocomplete( 'search', "");
        }).on('click', function(event) {
            $(this).autocomplete( 'search', "");
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            //Add the .ui-state-disabled class and don't wrap in <a> if value is empty
            if(item.value ==''){
                return $('<li class="ui-state-disabled">'+item.label+'</li>').appendTo(ul);
            }else{
                return $("<li>")
                    .append("<a>" + item.label + "</a>")
                    .appendTo(ul);
            }
        };

    },

    submit: function(settings, original) {
        var delimeter = settings.delimiter ? settings.delimiter : ',';
        original.reset(this);
        $(original).html(this.find('input').val().replace(new RegExp(delimeter+" *$"),''));
        return false;

    }

});
