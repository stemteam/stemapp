/**
 * Created by phplamer on 18.10.14.
 */

$.editable.addInputType('custtext', {
    /* create input element */
    element : function(settings, original) {
        var input = $('<input />');
        if (settings.width  != 'none') { input.width(settings.width);  }
        if (settings.height != 'none') { input.height(settings.height); }
        input.attr('autocomplete','off');
        $(this).append(input);
        return(input);
    },
    content : function(string, settings, original) {
        $(':input:first', this).val(string);
    },
    plugin : function(settings, original) {
        //console.log(original);
        /*var form = this;
        form.attr("enctype", "multipart/form-data");
        $("button:submit", form).bind('click', function() {
            //$(".message").show();
            $.ajaxFileUpload({
                url: settings.target,
                secureuri:false,
                fileElementId: 'upload',
                dataType: 'html',
                success: function (data, status) {
                    $(original).html(data);
                    original.editing = false;
                },
                error: function (data, status, e) {
                    alert(e);
                }
            });
            return(false);
        });*/
    },

    submit: function(settings, original) {
        original.reset(this);
        $(original).html(this.find('input').val());
        return false;

    }

});
