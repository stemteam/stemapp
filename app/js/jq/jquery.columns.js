/**
 * Columns.js v0.0.1
 *
 *
 * @package Columns
 *
 * Author:           Oleg Mikhaylenko <olegm@infokinetika.ru>
 * Coopyright 2012,  CSI Infokinetika Ltd. <http://infokinetika.ru/>
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/mit-license.php
 */

/*global jQuery: false, $: false */
(function ($) {
    "use strict";


    /**
     * Columns Class
     *
     * @param {HTMLElement} element Input element
     * @param {object}      options Options
     *
     * @constructor
     */
    var Columns = function (element, options) {
        this.element = element;
        this.$element = $(element);
        this.wrap = null;
        this.options = $.extend({}, $.fn.columns.defaults, options);

        this.init();
    };


    Columns.prototype = {

        constructor:Columns,

        element:null,

        init:function () {

            this.$element.find(this.options.selector).wrapAll($('<div>').addClass('jq-column-container'));
            this.wrap = this.$element.find('.jq-column-container:first');

            $(window).on('resize', $.proxy(this.refresh, this));
            this.refresh();
        },

        refresh:function () {

            if ($(window).width() > this.options.minWidth) {


                if (this.wrap.attr('data-column')) return;

                if (this.options.count < 2) return;

                this.wrap.attr('data-column', 1).css({ marginLeft:'-2.5%'});

                // Create columns
                for (var i = 1; i <= this.options.count; i++) {

                    this.wrap.append($('<div>').addClass('jq-column jq-column-' + i).css({float:'left', width:((100 / this.options.count) - 2.5) + '%', marginLeft:'2.5%'}));
                }

                // Add number
                i = 0;
                this.wrap.find(this.options.selector).each(function () {

                    $(this).attr('data-blocknum', i);
                    i++;
                });

                var me = this;
                for (i = 1; i <= this.options.count; i++) {

                    this.wrap.find(this.options.selector).filter('[data-colind="' + i + '"]').appendTo(me.wrap.children('.jq-column-' + i));
                }

            } else {

                // todo Вставлять их в правильном порядке
                if (!this.wrap.attr('data-column')) return;
                this.wrap.removeAttr('data-column').css({ marginLeft:0});

                var len = this.wrap.find(this.options.selector).filter('[data-colind]').length;

                for (var i = 0; i < len; i++) {
                    this.wrap.find(this.options.selector).filter('[data-blocknum="' + i + '"]').appendTo(this.wrap);
                }
                this.wrap.find('.jq-column').remove();
            }
        }
    };


    /**
     * Columns jQuery Plugin
     *
     * @param {object|string} params
     *
     * @return {*}
     */
    $.fn.columns = function (params) {

        var $this = $(this),
            columns = 'columns',
            data = $this.data(columns),
            options = $.isPlainObject(params) ? params : false;

        if (!data) {
            $this.data(columns, (data = new Columns(this, options)));
        }

        if ((typeof params === 'string') && (typeof data[params] === 'function')) {
            data[params]();
        }
    };

    /**
     * Columns defaults
     *
     * @type {object}
     */
    $.fn.columns.defaults = {

        "selector":'.column',

        "count":2,

        "min-width":0,

        "width":'auto'
    };
//
//    /**
//     * Columns data API
//     */
//    $('body').on('focus.dateinput.data-api', '[data-type="dateinput"]', function (ev) {
//        var $this = $(this);
//        if ($this.data('dateinput')) {
//            // Do not bind to the already initialized fields
//            return;
//        }
//
//        ev.preventDefault();
//        $this.dateinput($this.data());
//    });

}(jQuery));
