define([
    'jquery',
    '_jq/datepicker',
], function () {

    "use strict";

    var CustomDateInput = function () {


        var me = this;

        this.o = {
            type: 'date',
            max: null,
            min: null,
            datepicker: false,
            date: new Date(),
            defaultTime: null,
            timeLimit: '23:59:59',
            onEnterPress: function () {
            }
        };
        this.wrapper = null;
        this.content = null;
        this.id = 'custom-input-wrapper-' + Math.floor(Math.random() * 1000000);
        this.tabIndex = 0;
        this.inputs = [];
        this.element = null;
        this.date = null;
        this.string = '';
        this.datepicker = null;

        this.silent = false;
        this.calendar = null;

        this.init = function (input, opt) {

            try {
                this.element = input;
                this.o = $.extend(this.o, opt);

                this.date = this.o.date instanceof Date ? this.o.date : new Date(parseInt(this.o.date));
                this.createWrapper();
                this.wrapper = $(this.element).closest('.custom-input-wrapper').attr('id', this.id);
                this.content = this.wrapper.find('.custom-input-content');

                this.wrapper.on('click', function () {
                    me.setFocus(me.inputs[me.tabIndex]);
                });
                $(this.element).on('change', function () {
                    me.extSetVal($(this).val(), true);
                });

                $(this.element).hide();

                switch (this.o.type) {

                    case 'date':

                        this.appendDateInput();
                        break;

                    case 'time':

                        this.appendTimeInput();
                        break;

                    case 'datetime':

                        this.appendDateInput();
                        this.content.append(this.createSeparate('&nbsp;'));
                        this.appendTimeInput();
                        break;

                    default:

                        return;
                }
                this.validateAll();
                this.updateVal();

                if (this.o.datepicker) {

                    this.addDateButton();
                    $('body').append($('<div>').addClass('custom-input-calendar').attr('id', this.id + '_calendar'));
                    this.calendar = $('#' + this.id + '_calendar');
                    this.attachDatePicker();
                }
            }
            catch (e) {
                console.trace();
                console.error(e);
            }
        };

        this.createWrapper = function () {

            this.wrapper = $('<div class="custom-input-wrapper"></div>');
            this.content = $('<div class="custom-input-content"></div>');
            $(this.element).wrap(this.wrapper).wrap(this.content)
        };

        this.createInput = function (type, value) {

            var input = $('<input />').data('type', type).addClass('custom-input').attr({maxLength: '2'}).val(value)
                .bind('keydown', function (ev) {
                    return me.keypressInput(this, ev)
                })
                .bind('focus', function (ev) {
                    return me.focusInput(this, ev)
                })
//                    .bind('change', function (ev) {return me.changeInput(this, ev)})
                .bind('blur', function (ev) {
                    return me.blurInput(this, ev)
                })
                .bind('mousedown', function (ev) {
                    return me.clickInput(this, ev)
                })


            if (type == 'year') {
                input.attr({maxLength: '2'}).addClass('year-input');
            }

            return input;
        };

        this.validateAll = function (silent) {

            for (var i = 0; i < this.inputs.length; i++) {
                this.validateInput(this.inputs[i], '', null, silent);
            }
        };

        this.createSeparate = function (text) {

            var sep = $('<span />').addClass('custom-input-separate').html(text)
                .bind('mousedown', function (ev) {
                    return me.clickSeparate(this, ev)
                });

            return sep;
        };

        this.format = function (val, length) {

            for (var i = 0; i < length; i++) {
                val = '0' + val;
            }
            return val.slice(-length);
        };

        this.appendDateInput = function () {

            this.inputs.push(this.createInput('day', this.date.getDate()));

            this.inputs.push(this.createInput('month', this.date.getMonth() + 1));

            this.inputs.push(this.createInput('year', this.date.getFullYear()));

            this.content
                .append(this.inputs[this.inputs.length - 3])
                .append(this.createSeparate('.'))
                .append(this.inputs[this.inputs.length - 2])
                .append(this.createSeparate('.'))
                .append(this.inputs[this.inputs.length - 1]);
        };

        this.appendTimeInput = function () {

            this.inputs.push(this.createInput('hour', this.date.getHours()));

            this.inputs.push(this.createInput('minute', this.date.getMinutes()));

            this.inputs.push(this.createInput('second', this.date.getSeconds()));

            this.content
                .append(this.inputs[this.inputs.length - 3])
                .append(this.createSeparate(':'))
                .append(this.inputs[this.inputs.length - 2])
                .append(this.createSeparate(':'))
                .append(this.inputs[this.inputs.length - 1]);
        };

        this.addDateButton = function () {


            this.content.addClass('hasCalendar');
            this.wrapper.append($('<div />').addClass('button-calendar').append($('<i>').height(this.content.height())));

            this.content.height(this.content.height()); // For IE

            this.wrapper.find('.button-calendar').bind('click', function (e) {
                me.datepicker.DatePickerSetDate(me.string, true);
                if (me.calendar.is(':visible')) {
                    me.calendar.hide();
                    me.setFocus(me.inputs[me.tabIndex]);
                } else {
                    me.hideAllDatapicker();

                    me.showCalendar();
                }
                e.stopPropagation();
            });
        };

        this.showCalendar = function () {

            this.calendar.show();
            var of = this.wrapper.offset();

            this.calendar.css({left: of.left, top: of.top + this.wrapper.height()});
        };

        this.toogleCalendar = function () {

            if (this.calendar.is(':visible')) {
                this.calendar.hide();
            }
            else {
                this.showCalendar();
            }
        };

        this.attachDatePicker = function () {

            this.datepicker = this.calendar.DatePicker({
                date: this.date.getDate() + '.' + (this.date.getMonth() + 1) + '.' + this.date.getFullYear(),
                format: 'd.m.Y',
                position: 'bottom',
                prev: '',
                next: '',
                flat: true,
                onChange: function (formated, dates) {
                    var date = me.strToDate(formated);

                    if (date !== false) {

                        date.setHours(me.date.getHours());
                        date.setMinutes(me.date.getMinutes());
                        date.setSeconds(me.date.getSeconds());
                        me.date = date;
                        me.updateInputs();
                    }
                    me.calendar.hide();
                },
                locale: {
                    days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
                    daysShort: ["Вос", "Пон", "Вто", "Сре", "Чет", "Пят", "Суб", "Вос"],
                    daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
                    months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                    monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
                    weekMin: ''
                }
            });

            var today = new Date(),
                a;

            this.datepicker.find('.datepicker').append($('<div class="datapicker-today"></div>').append(
                a = $('<a href="#"></a>').text('Сегодня: ' + this.format(today.getDate(), 2) + '.' + this.format(today.getMonth() + 1, 2) + '.' + today.getFullYear())
            ));

            a.click(function () {
                var today = new Date();
                me.date.setFullYear(today.getFullYear());
                me.date.setMonth(today.getMonth());
                me.date.setDate(today.getDate());
                me.updateInputs();
                me.calendar.hide();
                return false;
            });
            var dp = this.datepicker.find('.datepicker');
            dp.height(dp.height() + 15);
            dp.children('.datepickerContainer').height(dp.children('.datepickerContainer').height() + 15);

            this.datepicker.hide();
            $(document).on('click', function () {
                me.hideAllDatapicker();
            });
        };

        this.hideAllDatapicker = function () {

            $('[data-ni]').niHideAll();
        };

        this.keypressInput = function (input, e) {

            if (e.keyCode == 13 || e.keyCode == 10) {
                this.o.onEnterPress();
                return false;
            }

            // arrow right
            if (e.keyCode == 39) {
                if (this.inputs.length - 1 > this.tabIndex) {
                    this.tabIndex++;
                } else {
                    this.tabIndex = 0;
                }

                this.setFocus(this.inputs[this.tabIndex]);

                return false;
            }
            // arrow left
            if (e.keyCode == 37) {

                if (this.tabIndex > 0) {
                    this.tabIndex--;
                }
                else {
                    this.tabIndex = this.inputs.length - 1;
                }
                this.setFocus(this.inputs[this.tabIndex]);

//                    $(this).prevAll('.custom-input:first').focus();
                return false;
            }

            // Alt + arrow up or Esc
            if (e.keyCode == 38 && e.altKey || e.keyCode == 27) {

                this.calendar.hide();
                return false;
            }


            // Alt + arrow down
            if (e.keyCode == 40 && e.altKey) {
                this.toogleCalendar();
                return false;
            }

            // arrow up or plus
            if (e.keyCode == 38 || e.keyCode == 107) {

                $(input).val(parseInt($(input).val(), 10) + 1);
                this.validateInput(input, '', '+');
                return false;
            }

            // arrow down or minus
            if (e.keyCode == 40 || e.keyCode == 109) {

                $(input).val(parseInt($(input).val(), 10) - 1);
                this.validateInput(input, '', '-');
                return false;
            }

            // Home
            if (e.keyCode == 36) {
                $(input).val('max');
                this.validateInput(input, '');
                return false;
            }

            // End
            if (e.keyCode == 35) {
                $(input).val('min');
                this.validateInput(input, '');
                return false;
            }

            // backspace or delete
            if (e.keyCode == 8 || e.keyCode == 46) {

                return false
            }
            // Tab
            if (e.keyCode == 9) {

                var allInputs = $(':input:visible, a:visible');

                if (!e.shiftKey) {

                    for (var i = 0; i < allInputs.length; i++) {


                        if (allInputs[i] != input) {
                            continue;
                        }
                        while (allInputs[i] && $(allInputs[i]).closest('.custom-input-wrapper').attr('id') == this.id) {
                            i++;
                        }

                        if (allInputs[i]) {
                            this.setFocus(allInputs[i]);
                        }
                        else {
                            this.setFocus(allInputs[0]);
                        }
                        break;
                    }
                }
                else {
                    for (var i = allInputs.length - 1; i >= 0; i--) {


                        if (allInputs[i] != input) {
                            continue;
                        }
                        while (allInputs[i] && $(allInputs[i]).closest('.custom-input-wrapper').attr('id') == this.id) {
                            i--;
                        }

                        if (allInputs[i]) {
                            this.setFocus(allInputs[i]);
                        }
                        else {
                            this.setFocus(allInputs[allInputs.length - 1]);
                        }
                        break;
                    }
                }

                return false;
            }

            // numbers on numpad
            if (e.keyCode >= 96 && e.keyCode <= 105) {
                e.keyCode = e.keyCode - 48;
            }


            // main numbers
            if (e.keyCode >= 48 && e.keyCode <= 57) {

                return this.validateInput(input, e.keyCode - 48);
            }
        };


        this.validateInput = function (input, char, mode, silent) {


            var val = $(input).val();
            val += char;

            this.silent = silent;

            switch ($(input).data('type')) {

                case 'day':


                    var year = this.getInput('year').val();
                    year = year > 50 ? '19' + year : '20' + year;
                    var max = new Date(year, this.getInput('month').val(), 0).getDate();
                    this.setValue(input, val, 1, max, 2, char, mode);
                    break;

                case 'month':
                    this.setValue(input, val, 1, 12, 2, char, mode, false, function () {
                        me.validateInput(me.getInput('day'), '', null, silent);
                    });
//                        this.validateInput(this.getInput('day'), '');
                    break;

                case 'year':
                    var min = 1900;
                    if (char === '' && parseInt(val, 10) < min) {
                        val = 2000 + parseInt(val, 10);
                    }
                    this.setValue(input, val, min, 3000, 4, char, mode, true, function () {
                        me.validateInput(me.getInput('day'), '', null, silent);
                    });
                    break;

                case 'hour':
                    this.setValue(input, val, 0, 23, 2, char, mode);
                    break;

                case 'minute':
                    this.setValue(input, val, 0, 59, 2, char, mode);
                    break;

                case 'second':
                    this.setValue(input, val, 0, 59, 2, char, mode);
                    break;

            }

            this.silent = false;

            return false;
        };

        this.setValue = function (input, val, min, max, length, char, mode, notrepeat, callback) {

            var setMin = val == 'min',
                setMax = val == 'max';

            callback = callback ? callback : function () {
            };

            if (setMin) {
                this.setVal(input, min, length);
                callback();
                return;
            }

            if (setMax) {
                this.setVal(input, max, length);
                callback();
                return;
            }
            if (mode == 'blur') {
                if (parseInt(val) < min || parseInt(val) > max) {
                    $(input).val($(input).data('lastval'));
                }
                else {
                    this.setVal(input, val, length);
                    callback();
                }
                return;
            }


            val = $.isNumeric(val) ? val : 0;

            notrepeat = notrepeat ? true : false;

            // если это валидации инпута
            if (char === '') {
                // Если введенное значение привышает максимум
                if (parseInt(val, 10) > parseInt(max, 10)) {


                    if (mode && mode == '+' && !notrepeat) {
                        this.setVal(input, min, length);
                        callback();
                    }
                    else {
                        this.setVal(input, max, length);
                        callback();
                    }

                } else if (parseInt(val, 10) <= min - 1) {

                    if (mode && mode == '-' && !notrepeat) {
                        this.setVal(input, max, length);
                        callback();
                    }
                    else {
                        this.setVal(input, min, length);
                        callback();
                    }
                } else {

                    this.setVal(input, parseInt(val, 10), length);
                    callback();
                }
            }
            else {
                // Это ввод чисел
                if (val.length > length) {
                    $(input).val(char);
                }
                else {

                    if (parseInt(val, 10) > parseInt(max, 10)) {

                        $(input).val(char);

                    } else if ((parseInt(val, 10) <= min - 1) && (val.length == length)) {

                        $(input).val($(input).data('lastval'));

                    } else if (val.length == length) {

                        this.setVal(input, parseInt($(input).val() + char, 10), length);
                        callback();
                    }
                    else {
                        $(input).val(val);
                    }
                }
            }

        };

        this.setVal = function (input, value, length) {


            var val = this.format(value, length);

            $(input).data('lastval', val).val(val);

            this.updateVal();


            if (this.o.max) {

                var max = $(this.o.max).niGetValue();

                if (max < this.date) {
                    /*
                     var dt = this.strToTime($(this.o.max).niGetOption('defaultTime')),
                     date = this.date;
                     if (dt !== false) {

                     date.setHours(dt.getHours());
                     date.setMinutes(dt.getMinutes());
                     date.setSeconds(dt.getSeconds());
                     }
                     */
                    var date = new Date(this.date);
                    if (max) {
                        date.setHours(max.getHours());
                        date.setMinutes(max.getMinutes());
                        date.setSeconds(max.getSeconds());
                    }

                    if (date < this.date) {
                        $(this.o.max).niSetValue(new Date(this.date));
                    } else {
                        $(this.o.max).niSetValue(date);
                    }
                }
            }

            if (this.o.min) {

                var min = $(this.o.min).niGetValue();
                if (min > this.date) {
                    /*
                     var dt = this.strToTime($(this.o.min).niGetOption('defaultTime')),
                     date = this.date;
                     if (dt !== false) {

                     date.setHours(dt.getHours());
                     date.setMinutes(dt.getMinutes());
                     date.setSeconds(dt.getSeconds());
                     }
                     */
                    var date = new Date(this.date);
                    if (min) {
                        date.setHours(min.getHours());
                        date.setMinutes(min.getMinutes());
                        date.setSeconds(min.getSeconds());
                    }

                    if (date > this.date) {
                        $(this.o.min).niSetValue(new Date(this.date));
                    } else {
                        $(this.o.min).niSetValue(date);
                    }
                }
            }

            if ($(this.element).data('lastVal') == this.date.getTime()) {
                return;
            }
            if (!this.silent) {
                $(this.element).data('lastVal', this.date.getTime()).val(this.date.getTime()).trigger('change');
            }
        };

        this.updateVal = function () {

            this.string = '';
            var me = this;
            this.content.children('input.custom-input,span.custom-input-separate').each(function () {

                if ($(this).is('input')) {
                    me.string += $(this).val();
                }
                else {
                    me.string += $(this).text();
                }
            });

            $(this.element).val(me.string);

            switch (this.o.type) {

                case 'date':

                    this.date = new Date(parseInt(this.getInput('year').val(), 10), parseInt(this.getInput('month').val(), 10) - 1, parseInt(this.getInput('day').val(), 10));
                    break;

                case 'time':
                    this.date = new Date(1900, 0, 1, parseInt(this.getInput('hour').val(), 10), parseInt(this.getInput('minute').val(), 10), parseInt(this.getInput('second').val(), 10));
                    break;

                case 'datetime':

                    this.date = new Date(parseInt(this.getInput('year').val(), 10), parseInt(this.getInput('month').val(), 10) - 1, parseInt(this.getInput('day').val(), 10),
                        parseInt(this.getInput('hour').val(), 10), parseInt(this.getInput('minute').val(), 10), parseInt(this.getInput('second').val(), 10));
                    break;

                default:

                    return;
            }
        };

        this.updateInputs = function (silent) {


            switch (this.o.type) {

                case 'date':

                    this.getInput('year').val(this.date.getFullYear());
                    this.getInput('month').val(this.date.getMonth() + 1);
                    this.getInput('day').val(this.date.getDate());

                    break;

                case 'time':

                    this.getInput('hour').val(this.date.getHours());
                    this.getInput('minute').val(this.date.getMinutes());
                    this.getInput('second').val(this.date.getSeconds());

                    break;

                case 'datetime':
                    this.getInput('year').val(this.date.getFullYear());
                    this.getInput('month').val(this.date.getMonth() + 1);
                    this.getInput('day').val(this.date.getDate());
                    this.getInput('hour').val(this.date.getHours());
                    this.getInput('minute').val(this.date.getMinutes());
                    this.getInput('second').val(this.date.getSeconds());
                    break;

                default:

                    return;
            }

            this.validateAll(silent);
        };

        this.changeInput = function (input, e) {

            this.validateInput(input, '');
        };

        this.focusInput = function (input, e) {

            if ($(this.inputs[this.tabIndex]).hasClass('focus'))
                return true;

            this.setFocus(this.inputs[this.tabIndex]);

            e.stopPropagation();
            return false;
        };

        this.setFocus = function (input) {

            var v = $(input).val();
            $(input).addClass('focus').val('').val(v).focus();
        };

        this.blurInput = function (input, e) {

            this.validateInput(input, '', 'blur');
            $(input).removeClass('focus');
        };

        this.clickInput = function (input, e) {
            this.tabIndex = $(this.wrapper).find(':input').index(input) - 1;
            this.setFocus(this.inputs[this.tabIndex]);
            e.preventDefault();
            return false;
        };

        this.clickSeparate = function (sep, e) {
            this.tabIndex = $(this.wrapper).find('span').index(sep);
            this.setFocus(this.inputs[this.tabIndex]);
            e.preventDefault();
            return false;
        };

        this.getInput = function (type) {

            for (var i = 0; i < this.inputs.length; i++) {
                if ($(this.inputs[i]).data('type') == type) {
                    return this.inputs[i];
                }
            }
        };

        this.strToTime = function (str) {

            if (!str) {
                return false;
            }
            var arr = str.split(':');
            if (arr.length == 3) {
                return new Date(1900, 0, 1, arr[0], arr[1], arr[2]);
            }
            if (arr.length == 2) {
                return new Date(1900, 0, 1, arr[0], arr[1]);
            }
            return false;
        };

        this.strToDate = function (str) {
            if (!str) {
                return false;
            }
            var arr = str.split('.');
            if (arr.length == 3)
                return new Date(parseInt(arr[2], 10), parseInt(arr[1], 10) - 1, parseInt(arr[0], 10));
            return false;
        };

        // Extension
        this.extGetValue = function (type) {

            type = type ? type : 'date';

            switch (type) {

                case 'string':
                    return this.string;

                case 'seconds':
                    return this.date.getSeconds() + this.date.getMinutes() * 60 + this.date.getHours() * 3600;

                // date
                default:
                    return this.date;
            }
        };

        this.extSetVal = function (value, silent) {

            if (value instanceof Date) {
                this.date = value;
                this.updateInputs(silent);
                return;
            }

            if (parseInt(value) == value) {
                this.date = new Date(parseInt(value));
                this.updateInputs(silent);
                return;
            }

            if (typeof value == 'string') {
                console.log('Unsupported format (String)');
            }


        };
        this.extGetOpt = function (name) {

            return this.o[name] ? this.o[name] : null;
        };

        this.extHideAll = function () {

            if (this.calendar) {
                this.calendar.hide();
            }
        };

        return this;
    };

    $.fn.customDateInput = function (a) {

        $(this).each(function () {

            var ni = new CustomDateInput();
            ni.init(this, a);
            $(this).data('ni', ni).attr('data-ni', ni);

            return this;
        });
        return this;
    };

    $.fn.niGetValue = function (type) {

        if (!$(this).data('ni')) {
            return false;
        }

        return $(this).data('ni').extGetValue(type);
    };
    $.fn.niSetValue = function (value) {

        if (!$(this).data('ni')) {
            return false;
        }
        $(this).data('ni').extSetVal(value);
        return this;
    };
    $.fn.niGetOption = function (value) {

        if (!$(this).data('ni')) {
            return false;
        }
        return $(this).data('ni').extGetOpt(value);
    };

    $.fn.niHideAll = function () {

        $(this).each(function () {
            if (!$(this).data('ni')) {
                return false;
            }
            $(this).data('ni').extHideAll();
        });
        return this;
    };


});