(function (factory) {
    "use strict";
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    }
    else if (jQuery && !jQuery.fn.qtip) {
        factory(jQuery);
    }
}
(function ($) {
    "use strict";

    /**
     * Animate
     */

    $.fn.scaleShow = function (callback) {

        callback = callback ? callback : function () {
        };
        $(this).show().removeClass('hideScale').removeClass('showScale').addClass('showScale');
        var th = this;
        setTimeout(function () {
            $(th).removeClass('showScale');
            ($.proxy(callback, $(th)) )();
        }, 390);
        return this;
    };

    $.fn.scaleHide = function (callback) {

        callback = callback ? callback : function () {
        };
        $(this).removeClass('hideScale').removeClass('showScale').addClass('hideScale');
        var th = this;
        setTimeout(function () {
            $(th).hide().removeClass('hideScale');
            ($.proxy(callback, $(th)) )();
        }, 290);
        return this;
    };

    $.fn.autoSize = function (html, callback) {

        callback = callback ? callback : function () {
        };
        if (typeof html == 'string') {

            var $div = $('<div>').html(html);
            $div.appendTo(this);
            var h = $div.outerHeight();
            $div.remove();

            $(this).css({
                'overflow': 'hidden',
                'height': $(this).height() + 'px'
            }).wrapInner('<div class="wrapInner"></div>').animate({height: h}, 300, function () {
                $(this).css({
                    'overflow': 'visible',
                    'height': 'auto'
                }).find('.wrapInner').html(html).stop(true).animate({opacity: 1}, 300, function () {
                    $(this).parent().html($(this).html());
                    callback();
                });
            });
            $('div.wrapInner', this).stop(true).animate({opacity: 0}, 300);
        }
        else if (typeof html == 'object') {

            if ($(html).is(':visible')) return;

            var h = $(html).show().outerHeight();
            $(html).hide();
            $(this).css({'overflow': 'hidden', 'height': $(this).height() + 'px'}).stop(true).animate({height: h}, 300, function () {
                $(this).css({'overflow': 'visible', 'height': 'auto'});
                $(html).css('opacity', 0).show().stop(true).animate({opacity: 1}, 300);
            })
                .children().not(html).stop(true).animate({opacity: 0}, 300, function () {
                    $(this).hide()
                });

        }
        return this;
    };


    /**
     * Validation
     */
    $.fn.showError = function (html) {

        var at = [
                'bottom center',
                'top center',
                'left center',
                'right center'
            ],
            my = [
                'top center',
                'bottom center',
                'right center',
                'left center'
            ];

        if (!$(this).closest('.block').is(':visible')) {
            $(this).closest('.gen-block').find('h2 a').click();

        }
//        var of = $(this).offset();
//
////        if (!$(this).is(':visible'))
//
//        var me = this;
//
//        var $div = $('<div></div>').addClass('errorMessage').innerWidth($(this).innerWidth()).on('click',function () {
//            $(this).css({opacity:1}).stop(true).animate({opacity: 0}, 300, function () {$(this).remove()});
////            $(this).scaleHide();
//            me.focus();
//        }).css({opacity:0}).animate({opacity: 1}, 200).insertAfter($(this));
//
//
//        var $mess = $('<div></div>').addClass('message').html(html).appendTo($div);
//        //$div.append($mess);
//
//            $mess = $('.errorMessage .message');
//            $div.offset({top: of.top - $mess.outerHeight(), left: of.left }).outerHeight($(me).outerHeight() + $mess.outerHeight());

        $(this).closest('div.label').addClass('errorInput');

        // Устанавливаем позицию подсказок, в зависимости от класс формы
        var formstyle;

        formstyle = formstyle || 4;

        var $input = $(this);
        // Устанавливаем позицию подсказаок, в зависимости от класса поля
        var inputstyle;
        if ($input.hasClass('qtip-bottom'))  inputstyle = 1;
        if ($input.hasClass('qtip-top'))  inputstyle = 2;
        if ($input.hasClass('qtip-left'))  inputstyle = 3;
        if ($input.hasClass('qtip-right')) inputstyle = 4;
        inputstyle = inputstyle || formstyle;
        var id = Math.floor(Math.random() * 100);

        // Добавляем класс error для bootstrap
        $input.parents('.control-group').addClass('error');

        // Добавляем ошибку
        var $api = $input.qtip({
            content: {
                text: html
            },
            position: {
                my: 'bottom right',//my[inputstyle - 1],
                at: 'top right'//at[inputstyle - 1]
            },
            style: {
                // tip: true,
                classes: 'ui-tooltip-shadow ui-tooltip-red ui-tooltip-rounded'
            },
            show: {
                event: false,
                ready: true,
                effect: 'fade'
            },
            hide: {
                event: 'focus',
                target: $input.parent().find('.required, input')
            },
            id: 'hint' + id
        });

        return this;
    };

    $.fn.validate = function (html) {

        if ($(this).val() == "" || ($(this).is('[type=hidden]') && $(this).val() == "0"))
            $(this).showError(html);
    }

    // toolbar

    $(window).on('resize', (function () {

        $('.toolbar').each(function () {

            var maxHeight = 36;
            var $this = $(this);
            if ($this.height() > maxHeight) {

                if ($this.hasClass('hideSpan')) {


                } else {

                    $this.addClass('hideSpan');
                    $this.data('minWidth', $this.width());
                    $this.find('a.button').addClass('button-icon');
                    $this.find('a.button span').addClass('hidden');
                }

            } else {

                if ($this.data('minWidth') < $this.width()) {

                    $this.find('a.button span').removeClass('hidden');
                    if ($this.height() > maxHeight) {

                        $this.data('minWidth', $this.width());
                        $this.find('a.button span').addClass('hidden');
                    }
                    else {
                        $this.removeClass('hideSpan');
                        $this.find('a.button').removeClass('button-icon');
                    }
                }
            }

        });
    }));


    $(document).on('click', '.radiobuttons>a', function (e) {

        e.preventDefault();
        if ($(this).attr('disabled')) return false;
        if ($(this).closest('.radiobuttons').data('type') == 'multi') {
            $(this).toggleClass('active');
            $(this).find('input[type=checkbox]').prop('checked', $(this).hasClass('active'));
        } else {
            $(this).parent().find('a').removeClass('active');
            $(this).addClass('active').find('input[type=radio]').prop('checked', true);
        }
    });

    $(document).on('click', '.js-clear-input', function (e) {
        e.preventDefault();
        $(this).closest('.js-input-block').find('input:visible').val('').change();
        $(this).closest('.js-input-block').find('input[type="hidden"]').val('').change();

    });

}));


CanvasRenderingContext2D.prototype.setDash = function (array, offset) {

    offset = offset ? offset : 0;

    if (typeof this.setLineDash == 'undefined') { //Firefox
        this.mozDash = array;
        this.mozDashOffset = offset;

    } else { //Chrome

        this.setLineDash(array);
        this.lineDashOffset = offset;
    }
}
