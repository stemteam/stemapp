(function( factory ) {
    "use strict";
    if(typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    }
    else if(jQuery && !jQuery.fn.qtip) {
        factory(jQuery);
    }
}
(function ($) {
    "use strict";


    var NavstatTabs = function () {

        var me = this;

        this.options = {

            controls: false,

            activate: function () {}

        };

        this.element = null;
        this.controls = null;
        this.tabs = null;

        this.activeTab = null;

        this.init = function (element, options) {

            this.options = $.extend(this.options, options);
            this.element = element;

            if (this.options.controls) {
                this.controls = $(this.options.controls);
            } else {
                this.controls = $(element).find('.tabsControl a');
            }
            this.tabs = $(element).children('div').not('.tabsControl');
            this.tabs.css({opacity: 0, left: 100, right: -100}).hide();
            this.activateTab(0);
            this.controls.on('click', this.clickControl);
        };

        this.clickControl = function (e) {

            var tab = $(this).attr('data-tab');
            if (tab == me.activeTab) {
                return false;
            }
            me.activateTab(tab);
            return false;
        };

        this.activateTab = function (tab) {
//            $(me.tabs.get(me.activeTab)).hide();
            var $tab = $(me.tabs.get(me.activeTab)),
                size = 100;
//            $tab.addClass('tabPanelHide');
            if (me.activeTab != null) {
                if (me.activeTab < tab) {
                    $tab.animate({left: -size, right: size, opacity: 0, scale: 0.5}, 300, function () {$(this).removeClass('tabPanelHide').hide();});
                }
                else {
                    $tab.animate({left: size, right: -size, opacity: 0}, 300, function () {$(this).removeClass('tabPanelHide').hide();});
                }
            }
            me.activeTab = tab;
            $tab = $(me.tabs.get(me.activeTab));
            $tab.show().animate({left: 0, right: 0, opacity: 1}, 300);
            $(me.controls).removeClass('active');
            $(me.controls.get(tab)).addClass('active');
            me.options.activate($tab, $(me.controls.get(tab)));
        };
    };

    $.fn.customTabs = function (options) {

        $(this).each(function () {

            var nt = new NavstatTabs();
            nt.init(this, options);
            $(this).data('nt', nt).attr('data-nt', nt);

            return this;
        });
        return this;
    };

}));