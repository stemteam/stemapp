(function ($) {
    "use strict";

    var lifespan;
    $.jgrowl = function (title, message, cls, delay) {
        lifespan = delay;

        var target = $('.qtip.jgrowl:visible:last'),
            me = this,
            persistent = false;

        $(document.body).qtip({
            content: {
                text: message,
                title: {
                    text: title,
                    button: true
                }
            },
            position: {
                my: 'top center',
                at: (target.length ? 'bottom' : 'top') + ' center',
                target: target.length ? target : $(window),
                adjust: { y: 5 },
                effect: function (api, newPos) {
                    $(this).animate(newPos, {
                        duration: 200,
                        queue: false
                    });
                    api.cache.finalPos = newPos;
                }
            },
            show: {
                event: false,
                ready: true,
                effect: function () {
                    $(this).stop(0, 1).fadeIn(400);
                },
                delay: 0,
                persistent: persistent
            },
            hide: {
                event: false,
                effect: function (api) {
                    $(this).stop(0, 1).fadeOut(400).queue(function () {
                        api.destroy();
                        updateGrowls();
                    })
                }
            },
            style: {
                classes: 'jgrowl ' + cls,
                tip: false
            },
            events: {
                render: function (event, api) {
                    // Trigger the timer (below) on render
                    timer.call(api.elements.tooltip, event);
                }
            }
        }).removeData('qtip');


    };
    function timer(event) {
        var api = $(this).data('qtip'),
            me  = this;
        lifespan = lifespan || 5000;
        if (api.get('show.persistent') === true) { return; }

        clearTimeout(api.timer);

        if (event.type !== 'mouseover') {
            api.timer = setTimeout(function(){
                $(me).remove();
                api.destroy();
                updateGrowls();
            }, lifespan);
        }
    }

    function updateGrowls() {
        var each = $('.qtip.jgrowl'),
            width = each.outerWidth(),
            height = each.outerHeight(),
            gap = each.eq(0).qtip('option', 'position.adjust.y'),
            pos;
        each.each(function (i) {
            var api = $(this).data('qtip');
            api.options.position.target = !i ? $(window) : [
                pos.left + width / 2, pos.top + (height * i) + Math.abs(gap * (i - 1))
            ];
            api.set('position.at', 'top center');

            if (!i) { pos = api.cache.finalPos; }
        });
    }


    $(document).delegate('.qtip.jgrowl', 'mouseover mouseout', timer);

})(jQuery);