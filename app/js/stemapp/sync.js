define([
    'backbone'
], function () {

    Backbone.sync = function (method, object, attrs) {

        var func = object.operations[method];
        var is_model = object instanceof Backbone.Model;
        if (!func) {
            console.error(is_model ? 'model' : 'collection' + '.operations.' + method + ' undefined');
            return;
        }
        if (method == 'create' || method == 'update' || method == 'patch') {
            if (is_model) {
                attrs = _.extend({data: object.attributes}, attrs);
            }
        }
        if (method == 'read' || method == 'delete') {
            attrs.data = {};
            if (object.idAttribute) {
                attrs.data[object.idAttribute] = object.id;
            } else {
                attrs.data = _.extend({}, attrs.data);
            }
        }
        attrs.error = attrs.error ? attrs.error : _.bind(object.requestError, object);
        func.apply(object, [attrs]);
    };
});