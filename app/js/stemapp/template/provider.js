define([
    '_app/error'
], function () {

    Stemapp.Providers = [];

    /**
     * @class Template_Provider
     * @constructor
     */
    var Template_Provider = {

        requestError: function (xhr) {
            Stemapp.Error.requestError(xhr);
        },

        success: function () {

        },

        extend: function (children) {
            return _.extend(this, children);
        }
    };

    Template_Provider = _.extend(Template_Provider, Backbone.Events);

    return Template_Provider;
});