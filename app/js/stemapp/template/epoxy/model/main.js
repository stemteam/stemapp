define([
    'backbone',
    'epoxy',
    '_app/error',
    '_jq/jquery.base64.min',
    '_app/sync'
], function (Backbone) {

    /**
     * @class Epoxy_Model_Main
     * @constructor
     * @extends Backbone.Epoxy.Model
     */
    var Epoxy_Model_Main = Backbone.Epoxy.Model.extend({

        idAttribute: null,

        parentAttribute: null, // Задает поле, обозначающее родительский идентификатор (используется для сортировки tree grid)

        childCountAttribute: 'CCOUNT', // Задает поле, обозначающее количество дочерних элментов (используется для отрисовки стрелки "раскрыть" в tree grid)

        countLevelAttribute: 'CLEVEL', // Задает поле, обозначающее уровень вложенности (используется для отрисовки отступа tree grid)

        /**
         * Array of CRUD operations. They should be called "create", "update", "patch", "delete", "read"
         */
        operations: {
            'create': null,
            'update': null,
            'patch': null,
            'delete': null,
            'read': null
        },

        view: null,

        initialize: function (attrs, options) {
            this.operations = _.extend({}, this.operations);
            options = options ? options : {};
            if (options.view) {
                this.view = options.view;
                options.view.initModel(this);
            } else {
                console.warn("Model doesn't have link with view", this);
            }
            this.on('error', _.bind(this.requestError, this));
            //Backbone.Epoxy.Model.prototype.initialize.apply(this, arguments);
        },

        /**
         * Set operation
         * @param name (add, edit, delete, get)
         * @param func
         */
        setOperation: function (name, func) {
            this.operations[name] = func;
        },

        requestError: function (model, xhr, options) {
            Stemapp.Error.requestError(xhr, this);
        },

        extend: function (children) {
            return _.extend(this, children);
        },

        errorDelete: function (xhr) {
            this.requestError(xhr);
            this.set({_delete: false});
        },

        successDelete: function () {
            if (this.collection) {
                this.collection.remove(this.id);
            }
        }
    });
    return Epoxy_Model_Main;
});