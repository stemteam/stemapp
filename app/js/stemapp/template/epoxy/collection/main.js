define([
    'backbone',
    'epoxy',
    '_jq/jquery.base64.min'
], function (Backbone) {

    /**
     * @class Epoxy_Collection_Main
     * @constructor
     * @extends Backbone.Collection
     */
    var Epoxy_Collection_Main = Backbone.Collection.extend({

        /**
         * Array of CRUD operations. They should be called "create", "update", "patch", "delete", "read"
         */
        operations: {
            'create': null,
            'update': null,
            'patch': null,
            'delete': null,
            'read': null
        },

        initialize: function (attrs, options) {
            this.operations = _.extend({}, this.operations);
            options = options ? options : {};
            if (options.view) {
                this.view = options.view;
                options.view.initModel(this);
            } else {
                console.warn("Collection doesn't have link with view", this);
            }
            //Backbone.Collection.prototype.initialize.apply(this, arguments);
        },

        set: function (models, options) {
            this.trigger('beforeSet');
            options = options ? options : {};
            options.view = this.view;
            arguments[1] = options;
            Backbone.Collection.prototype.set.apply(this, [models, options]);
            this.trigger('afterSet');
        },

        /**
         * Set operation
         * @param name (add, edit, delete, get)
         * @param func
         */
        setOperation: function (name, func) {
            this.operations[name] = func;
        },
    });

    return Epoxy_Collection_Main;
});