define([
    '_app/template/epoxy/views/main',
    '_app/views/control/toolbar',
    'tmpl',
    'zeroclipboard',
    'columns'
], function (Template_Main, Control_Toolbar, tmpl, ZeroClipboard) {

    /**
     * @class Template_Tab
     * @constructor
     * @extends Template_Main
     */
    var Template_Tab = Template_Main.extend({

        tagName: 'div',

        className: 'tab',

        container: null,

        template: '',

        tabButton: null,

        toolbar: null,

        data: {},

        ind: 0,

        id: '',

        caption: '', // заголовок таба

        caption_set: '', // был ли установлен заголовок таба

        name: '',

        url: '',

        urlData: {},

        single: false, // Single tab

        autoToolbar: true, // Auto create toolbar

        tmplData: {},

        _arguments: {},

        notRender: false,

        icon: null, // Если нужно задать иконку не соответсвующую названию

        /**
         * @class Control_Table
         */
        table: null,

        hash: '',

        items: null, // переопределяется в табах, является функцией, возвращающей массив

        initialize: function (opts) {

            this.setOptions(opts);

            this.tmplData.id = this.cid;
            this.icon = this.icon ? this.icon : this.name;

            this.tabButton = this.createTab();
            this.tabButton.find('.tab-show').on('mousedown', _.bind(this.clickTabHeader, this));
            this.tabButton.find('.tab-close').on('mousedown', _.bind(this.remove, this));
            if (this.items) {
                this.createToolbar();
            }

            this.bind('focus', this.focus, this);
            this.bind('blur', this.blur, this);
            this.bind('redraw', this.redraw, this);
            this.bind('refresh', this.refresh, this);
            Template_Main.prototype.initialize.apply(this);
        },

        /**
         * @public
         */
        createToolbar: function () {
            this.toolbar = new Control_Toolbar(this.autoToolbar ? this.items() : [], this);
        },

        /**
         * Applying options to view
         * @param opts
         * @public
         */
        setOptions: function (opts) {
            var obj = Stemapp.util.clearObjects(opts);
            this.hash = hex_md5(JSON.stringify(obj)); // Уникальный хэш формы

            this._arguments = opts.arguments ? opts.arguments : {};
            this.id = opts.id ? opts.id : this.id;
            this.ind = opts.ind ? opts.ind : this.ind;
            this.name = opts.name ? opts.name : this.name;
        },

        events: {
            "click .toolbar a": "toolbarItemClick",

            "click .toolbar a.refresh": "refresh",

            "keyup .toolbar input.search": "search",

            "click .toolbar a.filter": "filter",

            "click .toolbar a.export": "export",

            "click .toolbar a.excel": "exportExcel",

            "click .toolbar a.copyexcel": "clickCopyExcel",

            "mouseover .toolbar a.copyexcel": "mouseoverCopyExcel"
        },

        keyup: function (e) {

        },

        /**
         * Set toolbar mode
         * @param val
         * @public
         */
        setMode: function (val) {
            if (this.toolbar) {
                this.toolbar.setMode(val);
            }
        },

        /**
         *
         * @param opts
         * @returns {boolean}
         * @public
         */
        checkTab: function (opts) {

            if (!opts) {
                return true;
            }

            if (!_.isEqual(opts, this._arguments)) {
                return false;
            }

            return true;
        },

        /**
         * @public
         */
        afterInsert: function () {

            if (this.toolbar) {
                this.toolbar.afterInsert();
            }
        },

        /**
         *
         * @param e
         * @returns {boolean}
         * @private
         */
        toolbarItemClick: function (e) {
            if ($(e.currentTarget).attr('disabled')) {
                e.stopImmediatePropagation();
                return false;
            }
        },

        /**
         * Render tab
         * @returns {Template_Tab}
         * @public
         */
        render: function () {

            this.$el.empty();

            if (this.toolbar) {
                this.$el.append(this.toolbar.render().$el);
                this.$el.append('<div class="tollbar-sep"></div>');
                this.afterRender();
            }
            if (this.table && !this.notRender) {

                this.$el.append(this.table.render().$el);
                this.afterRender();
            } else {
                if (!this.notRender) {
                    this.$el.append(tmpl(this.template, this.tmplData));
                    this.afterRender();
                }
            }
            this.applyBindings();
            return this;
        },

        /**
         * Redraw tab
         * @public
         */
        redraw: function () {

            if (this.table) {
                this.table.trigger('redraw');
            }
            if (this.toolbar) {
                this.toolbar.trigger('redraw');
            }
        },

        beforeRemove: function () {

        },

        /**
         * Remove tab
         * @public
         */
        remove: function () {
            this.beforeRemove();
            this.$el.remove();
            this.tabButton.addClass('close');
            this.tabButton.css({overflow: 'hidden'}).animate({width: '0'}, 700);
            _.delay(_.bind(function () {
                this.tabButton.remove();
            }, this), 650);
            this.trigger('close', this.cid);
        },

        afterRender: function () {
        },

        /**
         * Delete row from table
         * @param item object|Backbone.Model
         * @param collection
         * @param attrs
         * @public
         */
        deleteRow: function (item, collection, attrs) {
            if (item instanceof Backbone.Model) {
                item.set({_delete: true});
                item.destroy();

            } else {
                var model = new collection.model();
                model = collection.get(item[model.idAttribute]);
                if (model) {
                    model.set({_delete: true});
                    model.destroy(attrs);
                }
            }
        },

        /**
         * @private
         */
        invalidateButtons: function () {

            var items = this.items();
            for (var i = 0; i < items.length; i++) {
                if (items[i].invalidate) {
                    items[i].invalidate(items[i]);
                }
            }
        },

        /**
         * @private
         */
        rowChanged: function () {
            this.invalidateButtons();
        },

        /**
         * @public
         */
        blur: function () {
            this.tabButton.removeClass('focus');
            this.$('.qtip').remove();
            this.hide();
        },

        /**
         *
         * @param e
         * @private
         */
        clickTabHeader: function (e) {
            if (e.button == 0) { //left click
                this.focus();
            }
            if (e.button == 1) { //middle click
                this.remove();
            }
        },

        /**
         *
         * @returns {boolean}
         * @public
         */
        focus: function () {
            this.trigger('unfocus', this.cid);
            this.tabButton.addClass('focus');
            this.show();
            this.navigate();
            this.trigger('redraw');
            if (this.toolbar) {
                this.toolbar.trigger('resize');
            }
            return false;
        },

        /**
         *
         * @returns {void|*|jQuery}
         * @private
         */
        createTab: function () {
            return $('<li>').addClass('tab-button').append($('<a>').addClass('tab-show').append($('<i>').addClass('icon icon-ajax icon-' + this.icon.toLowerCase())).append($('<span>').addClass('tab-caption').text(this.caption)))
                .append($('<a>').addClass('tab-close').append($('<i>').addClass('icon icon-tab-close')));
        },

        /**
         * Set tab caption
         * @param caption
         * @public
         */
        setCaption: function (caption) {

            this.caption = caption;
            this.caption_set = true;
            this.tabButton.find('.tab-show .tab-caption').text(caption);
        },

        /**
         * Hide all ajax loaders from tab
         * @public
         */
        hideAjax: function () {
            this.tabButton.find('.icon-ajax').removeClass('icon-ajax');
            this.hideGlobalAjax();
            Template_Main.prototype.hideAjax.apply(this);
        },

        /**
         * Show ajax loader
         * @public
         */
        tabAjax: function () {
            this.showAjax(this.tabButton);
        },


        /**
         * Show global ajax loader
         * @public
         */
        showGlobalAjax: function () {
            this.$el.append($('<div>').addClass('ajax-overlay'));
        },

        /**
         * Hide global ajax loader
         * @public
         */
        hideGlobalAjax: function () {
            this.$('.ajax-overlay').remove();
        },

        requestError: function (element, errors) {
            this.hideAjax();
            Template_Main.prototype.requestError.apply(this, [element, errors]);
        },

        fatalError: function (xhr) {
            this.hideAjax();
            this.trigger('fatalError', xhr);
        },

        beforeHide: function () {

        },

        hide: function () {
            this.beforeHide();
            this.$el.hide();
        },

        show: function () {
            this.$el.show();
        },

        toggleBlock: function (e) {

            $(e.currentTarget).closest('div.gen-block').find('.block:first').slideToggle('fast');
            $(e.currentTarget).parent().toggleClass('active');
            return false;
        },

        // For tables
        filter: function () {

            this.table.toggleFilter();
            return false;
        },

        search: function (e) {
            this.table.setFilter(e.currentTarget);
        },


        refresh: function () {
            this.showAjax(this.$('.toolbar a.refresh'));
            this.tabAjax();
            this.load();
            return false;
        },

        navigate: function () {

            var str = this.ind + '';
            while (str.length < 2) {
                str = '0' + str;
            }
            // Google analitycs
            _gaq.push(["_trackPageview", "/#" + str + '/' + $.render(this.url, this.urlData)]);
            this.trigger('navigate', str + '/' + $.render(this.url, this.urlData));
        },

        itemInvalidate: function (button) {
            if (this.table && this.toolbar) {
                var item = this.table.getActiveItem();
                if (item) {
                    if (item.__group) {
                        this.toolbar.$el.find('a.' + button.cls).attr('disabled', 'disabled');
                    } else {
                        this.toolbar.$el.find('a.' + button.cls).removeAttr('disabled');
                    }
                }
            }
        },

        itemInvalidateParent: function (parentField, button) {
            if (this.table && this.toolbar) {
                var item = this.table.getActiveItem();
                if (item) {
                    if (item.__group || item[parentField] !== 0) {
                        this.toolbar.$el.find('a.' + button.cls).attr('disabled', 'disabled');
                    } else {
                        this.toolbar.$el.find('a.' + button.cls).removeAttr('disabled');
                    }
                }
            }
        },

        itemInvalidateRows: function (button) {
            var item = this.table.getActiveItem();
            var rows = this.table.getSelectedRows();
            if (item || rows.length) {
                if (item.__group && !rows.length) {
                    this.toolbar.$el.find('a.' + button.cls).attr('disabled', 'disabled');
                } else {
                    this.toolbar.$el.find('a.' + button.cls).removeAttr('disabled');
                }
            }
        },

        applyDelete: function (item) {
            if (this.table) {
                this.table.deleteItem(item);
            }
        },

        errorDelete: function (item) {
            if (this.table) {
                item._delete = false;
                this.table.updateItem(item);
            }
        },

        clickCopyExcel: function (e) {
            e.preventDefault();
        },

        mouseoverCopyExcel: function (e) {
            this.clipboard(e.currentTarget, this.table.export(true), _.bind(function () {
                this.sendMessage('Таблица скопирована в буфер обмена');
            }, this));
            return false;
        },

        export: function () {

            if (!this.table) return false;

            this.showAjax(this.$('.toolbar .export'));

            var csv = this.table.export(),
                filename = this.caption + '_' + this.name;

            if (!csv) {
                this.sendError("Таблица не содержит данных.");
                this.hideAjax(this.$('.toolbar .export'));
                return false;
            }

            var form = $('.js-export-form');

            if (!form.length) {
                form = $('<form style="display:none;" action="/app/php/export.php/' + filename + '.csv" class="js-export-form" method="post">'
                    + '<input type="hidden" name="file" class="file">'
                    + '<input type="hidden" name="data" class="data">'
                    + '</form>')
                    .appendTo('body');
            }
            form.find('.file').val(filename);
            form.find('.data').val(csv);
            Stemapp.unload = false;
            form.submit();
            _.delay(_.bind(function () {
                Stemapp.unload = true;
                this.hideAjax(this.$('.toolbar .export'));
            }, this), 2000);
            return false;
        },

        exportExcel: function (e, table) {

            if (e) {
                e.preventDefault();
            }
            table = table ? table : this.table;
            if (!table) return false;

            this.showAjax(this.$('.toolbar .excel'));

            var csv = table.export(),
                filename = this.caption + '_' + this.name,
                dataObj = [];

            if (!csv) {
                this.sendError("Таблица не содержит данных.");
                this.hideAjax(this.$('.toolbar .excel'));
                return false;
            }
            ;

            csv.replace(/"/g, '').split('\n').forEach(function (field) {
                dataObj.push(field.split(';'));
            });

            var excelTypes = [];
            for (var i = 0; i < Stemapp.data[table.dataName].columns.length; i++) {
                if (Stemapp.data[table.dataName].columns[i].exportToExcelAs) {
                    excelTypes.push(Stemapp.data[table.dataName].columns[i].exportToExcelAs)
                } else {
                    excelTypes.push('string');
                }
            }

            filename = filename.replace('/', '_');

            var form = $('<form style="display:none;" action="app/php/genExcel.php/' + filename + '.xls" method="post" target="_blank">' +
                '<input type="hidden" name="filename" class="filename">' +
                '<input type="hidden" name="data" class="data">' +
                '<input type="hidden" name="types" class="types">' +
                '<input type="hidden" name="token" class="token">' +
                '</form>').appendTo('body');

            form.find('.filename').val(filename);
            form.find('.data').val(JSON.stringify(dataObj));
            form.find('.types').val(JSON.stringify(excelTypes));
            form.find('.token').val(Stemapp.Token);
            form.submit();
            form.remove();

            this.hideAjax(this.$('.toolbar .excel'));

        },

        clipboard: function (element, text, success) {

            success = success ? success : function () {
            };
            this.clipSuccess = success;

            var clip = new ZeroClipboard(element, {
                moviePath: '/app/swf/ZeroClipboard.swf',
                hoverClass: 'hover',
                activeClass: 'active'
            });
            clip.setHandCursor(true);
            clip.setText(text);


            var listener = _.bind(function () {
                this.clipSuccess();
            }, this);

            clip.removeEventListener('complete', listener);
            clip.addEventListener('complete', listener);
            clip.on('noflash', _.bind(function () {
                this.sendMessage("У вас не установлен Flash");
            }, this));
            clip.on('wrongflash', _.bind(function (client, args) {
                this.sendMessage("У вас установлен неподходящий Flash версии " + args.flashVersion);
            }, this));

            clip.glue(element);
        },

        /** Сохранить переданную информацию для текущей вкладки в localStorage.
         *
         * @param key В localStorage будет добавлен префикс из ГУИД, название вкладки и номер (435-Fuelcards1-)
         * @param value
         */
        saveDataToStorage: function (key, value) {

            if (!window.localStorage) {
                return false;
            }
            var GUID = $.cookie('guid');

            var str = JSON.stringify(value, function (key, value) {
                if (typeof value === 'object' && value !== null
                    && value.parentNode) {// html element
                    // Discard key
                    return;
                }
                return value;
            });

            localStorage.setItem(GUID + '-' + this.name + this.ind + '-specs-' + key, str);
            return true;

        },

        /**
         * Получить информацию для текущей вкладки из localStorage.
         * !!! Обратите внимание, что если вы используете эту функцию в initialize, то у вас скорей всего не будет еще name и ind
         * Вам нужно будет в начале вашего initialize вызвать функцию this.setOptions(opts);
         * @param key Если передать без параметра, то вернется объект со всеми ключами текущего таба
         */
        getDataFromStorage: function (key) {

            if (!window.localStorage) {
                return false;
            }

            var res;
            var GUID = $.cookie('guid');
            var prefix = GUID + '-' + this.name + this.ind + '-specs-';
            if (key) {
                res = JSON.parse(localStorage.getItem(prefix + key));
            } else {
                res = {};
                for (var i in localStorage) {
                    if (localStorage.hasOwnProperty(i)) {
                        if (i.indexOf(prefix) === 0) {
                            res[i.replace(prefix, '')] = JSON.parse(localStorage.getItem(i));
                        }
                    }
                }
            }
            return res;
        },

        /**
         * Удаление всех данных для текущего таба из localStorage
         */
        clearDataStorage: function () {
            if (!window.localStorage) {
                return false;
            }
            var GUID = $.cookie('guid');
            var prefix = GUID + '-' + this.name + this.ind + '-specs-';
            for (var i in localStorage) {
                if (localStorage.hasOwnProperty(i)) {
                    if (i.indexOf(prefix) === 0) {
                        localStorage.removeItem(i);
                    }
                }
            }
        },

        /** Сохранить аргументы в localStorage, передаваемые в конструктор при создании вкладки
         *  во время восстановления вкладок после перезагрузки приложения.
         *
         * @param key В localStorage будет добавлен префикс из ГУИД, название вкладки и номер (435-Fuelcards1-)
         * @param value
         */
        saveArgsToStorage: function (args) {

            if (!window.localStorage) {
                return false;
            }
            var tabs = [],
                GUID = $.cookie('guid');

            if (localStorage.getItem(GUID + '-tabsJSON')) {
                tabs = JSON.parse(localStorage.getItem(GUID + '-tabsJSON'));
            }

            for (var i = tabs.length; i--;) {
                if (tabs[i].name === this.name && tabs[i].ind === this.ind) {
                    tabs[i].args = $.extend(tabs[i].args, args);
                    break;
                }
            }

            // Конвертировать в JSON исключая некоторые (предположительно ненужные) объекты
            var str = JSON.stringify(tabs, function (key, value) {
                if (typeof value === 'object' && value !== null
                    && value.parentNode) {// html element

                    // Discard key
                    return;
                }
                return value;
            });
            localStorage.setItem(GUID + '-tabsJSON', str);

            return true;

        },

        /**
         * Получить аргументы из localStorage, передаваемые в конструктор при создании вкладки
         *  во время восстановления вкладок после перезагрузки приложения.
         * @returns args
         */
        getArgsFromStorage: function () {

            if (!window.localStorage) {
                return false;
            }

            var args = {},
                tabs = [],
                GUID = $.cookie('guid');

            if (localStorage.getItem(GUID + '-tabsJSON')) {
                tabs = JSON.parse(localStorage.getItem(GUID + '-tabsJSON'));
            }

            for (var i = tabs.length; i--;) {
                if (tabs[i].name === this.name && tabs[i].ind === this.ind) {
                    args = tabs[i].args;
                    break;
                }
            }

            return args;

        },

        getTableItems: function () {

            return ['>',
                {
                    type: 'input',
                    cls: 'search',
                    placeholder: 'Поиск по таблице',
                    modes: {modal: true}
                },
                {
                    cls: 'refresh',
                    text: 'Обновить',
                    icon: true,
                    compact: true,
                    modes: {modal: true}
                },
                {
                    cls: 'filter',
                    text: 'Фильтры',
                    icon: true,
                    toggle: true,
                    compact: true,
                    modes: {modal: true}
                },
                {
                    cls: 'export',
                    text: 'Экспорт в csv',
                    icon: true,
                    compact: true
                },
                {
                    cls: 'excel',
                    text: 'Экспорт в Excel',
                    icon: true,
                    compact: true
                }
                //,{
                //    cls: 'copyexcel',
                //    text: 'Скопировать таблицу в буфер обмена',
                //    icon: true,
                //    compact: true
                //}
            ];
        }

    });
    return Template_Tab;
});