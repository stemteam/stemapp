<form action="#" method="post" id="confirm-form" class="out-form js-form">
    <div class="banner"><h1>Активация</h1></div>
    <div class="form">
        <div class="checkconfirm" style="display: none">
            <div class="status js-status"></div>
            <div class="text js-text"><span></span></div>
        </div>
        {if $invite}
        <div class="js-container">
            <span>Выполняется активация</span>
        </div>
        {else}
        <div class="js-container">
            <div class="label">
                <label for="mobilePhone">
                    <span>Мобильный  телефон<sup class="required">*</sup></span>
                    <div class="forinput"><input type="text" name="mobilePhone" class="js-login" placeholder="79123456789" value="{$phone || ''}"/></div>
                </label>
            </div>

            <div class="label">
                <label for="code">
                    <span>Код регистрации<sup class="required">*</sup></span>
                    <div class="forinput"><input type="text" name="code" class="js-code" placeholder="Код регистрации" value=""/></div>
                </label>
            </div>

        </div>
        {/if}
    </div>
    <div class="buttons tar">
        <button type="submit" name="submit" class="default button success js-enter" style="display: none;"><i class="icon icon-ok"></i> Войти</button>
        {if !$invite}
        <button type="submit" name="submit" class="button success js-submit"><i class="icon icon-save"></i> Активировать</button>
        {/if}
        <button name="submit" class="default button js-close"><i class="icon icon-cancel"></i> Закрыть</button>
    </div>
</form>
