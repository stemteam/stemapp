<form action="#" method="post" id="confirm-form" class="out-form js-form">
    <div class="banner"><h1>Сброс пароля</h1></div>
    <div class="form">
        <div class="checkconfirm" style="display: none">
            <div class="status js-status"></div>
            <div class="text js-text"><span></span></div>
        </div>

        <div class="js-container">
            <div class="label">
                <label for="Login">
                    <span>Email или телефон<sup class="required">*</sup></span>
                    <div class="forinput"><input type="text" name="Login" class="js-login" placeholder="79123456789" value="{$login}"/></div>
                </label>
            </div>

            <div class="label">
                <label for="code">
                    <span>Код регистрации<sup class="required">*</sup></span>
                    <div class="forinput"><input type="text" name="code" class="js-code" placeholder="Код регистрации" value="{$code}"/></div>
                </label>
            </div>

            <div class="label">
                <label for="password">
                    <span>Новый пароль<sup class="required">*</sup></span>
                    <div class="forinput"><input type="password" name="password" class="js-password" placeholder="Пароль" value=""/></div>
                </label>
            </div>

        </div>
    </div>
    <div class="buttons tar">
            {*<button type="submit" name="submit" class="button success js-submit"><i class="icon icon-save"></i> Изменить</button>*}
            <button type="submit" name="submit" class="button success js-submit"><i class="icon icon-save"></i> Сбросить</button>
            <button type="submit" name="submit" class="default button success js-enter" style="display: none;"><i class="icon icon-ok"></i> Войти</button>
            <button name="submit" class="button cancel js-close"><i class="icon icon-cancel"></i> Закрыть</button>
    </div>
</form>
