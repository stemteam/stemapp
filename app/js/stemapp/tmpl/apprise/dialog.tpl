{$ok = true}
<div class="banner small"><h1>{$title}</h1></div>
<div class="form small"><p>{$text|nl2br nofilter}</p></div>
<div class="buttons tar">
    {if $buttons=='confirm'}{$ok = false}
        <button class="ok button" value="ok"><i class="icon icon-ok"></i> {$button.ok}</button>
        <button class="cancel button" value="cancel">
        <i class="icon icon-cancel"></i>
        {$button.cancel}</button>{/if}
    {if $buttons=='yesnocancel'}{$ok = false}
        <button class="yes button" value="yes">{$button['yes']}</button>
        <button class="no button" value="no">{$button['no']}</button>
        <button class="cancel button" value="cancel">{$button['cancel']}</button>{/if}
    {if $buttons=='savecancel'}{$ok = false}
        <button class="save success button" value="save"><i class="icon icon-ok"></i> {$button.save}</button>
        <button class="cancel button" value="cancel">
        <i class="icon icon-cancel"></i>
        {$button.cancel}</button>{/if}
    {if $buttons=='yesno'}{$ok = false}
        <button class="yes button" value="yes"><i class="icon icon-ok"></i> {$button['yes']}</button>
        <button class="no button" value="no">
        <i class="icon icon-cancel"></i>
        {$button['no']}</button>{/if}
    {if $buttons=='close'}{$ok = false}
        <button class="close button" value="close">
        <i class="icon icon-cancel"></i>
        {$button.close}</button>{/if}
    {if $buttons=='ok' || $ok}
        <button class="ok button" value="ok">
        <i class="icon icon-ok"></i>
        {$button.ok}</button>{/if}
</div>
