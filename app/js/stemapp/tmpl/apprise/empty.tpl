<div class="banner small"><h1>{$title}</h1></div>
<div class="js-window-content js-all-size window-content">
    {$text nofilter}
</div>
<div class="buttons tar">
    <button class="button success js-submit"><i class="icon icon-ok"></i> Выбрать</button>
    <button class="button cancel js-close" type="button"><i class="icon icon-cancel"></i> Закрыть</button>
</div>