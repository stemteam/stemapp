<form action="#" method="post" id="options-form" class="generic window vertical-form">
    <div class="aTitle aSmall"><h1>Параметры</h1></div>
    <div class="aText">
        <div class="row-fluid" style="margin-top: 15px">
            <div class="span8">
                <label for="guid">Ключ:</label>
                <div class="row-fluid">
                <input type="text" name="Guid" id="guid" value="" class="field options-key span12" spellcheck="false" placeholder="Укажите ключ"/>
                    </div>
            </div>
            <div class="span4 tar">
                <a href="#" id="demoGuid" class="button options-test-company" style="margin-top: 17px"><i class="icon icon-key"></i> Демо клиент</a>
            </div>
        </div>
    </div>
    <div class="aButtons tar">
        <button type="submit" class="default button success" value="yes"><i class="icon icon-ok"></i> Ок</button>
        <button class="default button cancel" value="no"><i class="icon icon-cancel"></i> Отменить</button>
    </div>
</form>