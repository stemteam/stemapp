{strip}
    {foreach $items as $item}
        <li {if $item.children}class="mp-childrens"{/if}>
            {if $item.href && !$item.child_html}
                <a class="{if $item.cls} {$item.cls}{/if}"  {if $item.db} data-server="{$item.db}"{/if} href="{$item.href}">
                    {if $item.icon}<i class="icon icon-{$item.icon}"></i>{else}<i class="icon icon-menu"></i>{/if}
                    <span>{$item.caption}</span>
                </a>
            {else}
                {if $item.auto || $item.child_html}
                    <a href="#" {if $item.cls}class="{$item.cls}"{/if} data-id="{$item.id}">
                        {if $item.icon}<i class="icon icon-{$item.icon}"></i>{else}
                            {if $item.child_html}
                                <i class="icon icon-arrow-left-white"></i>
                            {else}
                                <i class="icon icon-menu"></i>
                            {/if}
                        {/if}

                        {$item.caption}</a>
                    {if $item.child_html}
                        <div class="mp-level">
                            <h2 class="mp-back">{$item.caption}
                                <span class="mp-h2-back"><i class="icon icon-arrow-right-white"></i></span>
                            </h2>
                            <ul>
                                {$item.child_html nofilter}
                            </ul>
                        </div>
                    {/if}
                {else}
                    <span {if $item.cls} class="{$item.cls}{/if}"> {if $item.icon}<i class="icon icon-{$item.icon}"></i>{else}
                            <i class="icon icon-menu"></i>
                        {/if}
                        <span>{$item.caption}</span>
                        </span>
                {/if}
            {/if}
        </li>
    {/foreach}
{/strip}