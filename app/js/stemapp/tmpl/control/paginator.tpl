<div class="toolbar">
    {strip}
        {$vpage = 2}
        {$start = $page - $vpage}
        {$stop = $page + $vpage}
        {if $start<1}{$start=1}{/if}
        {if $stop>$maxpage}{$stop = $maxpage}{/if}
        {if $start!=$stop && $maxpage>1}
            <ul>
                <li class="toolbar-item"><span>Страницы:</span></li>
                {if $page>1}
                    <li class="toolbar-item"><a class="button" data-page="{$page-1}" href="#" title="Предыдущая страница">←</a></li>
                {/if}
                {if $start > 1}
                    <li class="toolbar-item"><a class="button" data-page="1" href="#">1</a></li>
                    {if ($start>3)}
                        <li class="toolbar-item">...</li>
                    {else}
                        {if $start!=2}
                            <li class="toolbar-item"><a class="button" data-page="2" href="#">2</a></li>
                        {/if}
                    {/if}
                {/if}
                {section name=p start=$start loop=$stop +1 step=1}
                    {if $smarty.section.p.index==$page}
                        <li class="toolbar-item"><span class="selected">{$smarty.section.p.index}</span></li>
                    {else}
                        <li class="toolbar-item"><a class="button" data-page="{$smarty.section.p.index}"
                                                    href="#">{$smarty.section.p.index}</a></li>
                    {/if}
                {/section}
                {if $stop < $maxpage}
                    {if ($maxpage-$stop>2)}
                        <li class="toolbar-item">...</li>
                    {else}
                        {if $maxpage-$stop!=1}
                            <li class="toolbar-item"><a class="button" data-page="{$maxpage-1}" href="#">{$maxpage-1}</a></li>
                        {/if}
                    {/if}
                    <li class="toolbar-item"><a class="button" data-page="{$maxpage}" href="#">{$maxpage}</a></li>
                {/if}
                {if $page!=$maxpage}
                    <li class="toolbar-item"><a class="button" data-page="{$page+1}" title="Сделующая страница" href="#">→</a></li>
                {/if}
            </ul>
        {/if}
    {/strip}
</div>