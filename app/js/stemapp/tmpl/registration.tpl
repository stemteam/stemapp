<form action="#" method="post" id="registration-form" class="out-form js-form">
    <div class="banner"><h1>Регистрация</h1></div>
    <div class="form horizontal">
        <div class="">
            {if !$invite}
                <div class="label">
                    <label for="lastName">
                        <span>Фамилия<sup class="required">*</sup></span>

                        <div class="forinput"><input type="text" name="lastName" class="js-lastName" spellcheck="true" placeholder="Фамилия"
                                                     value=""/></div>
                    </label>
                </div>
                <div class="label">
                    <label for="firstName">
                        <span>Имя<sup class="required">*</sup></span>

                        <div class="forinput"><input type="text" name="firstName" class="js-firstName" spellcheck="true" placeholder="Имя"
                                                     value=""/></div>
                    </label>
                </div>
                <div class="label">
                    <label for="middleName">
                        <span>Отчество</span>

                        <div class="forinput"><input type="text" name="middleName" class="js-middleName" placeholder="Отчество" value=""/>
                        </div>
                    </label>
                </div>
                <div class="label">
                    <label for="mobilePhone">
                        <span>Мобильный  телефон<sup class="required">*</sup></span>

                        <div class="forinput"><input type="text" name="mobilePhone" class="js-mobilePhone" placeholder="79123456789"
                                                     value=""/></div>
                    </label>
                </div>
            {/if}

            {if $invite}
                <div class="label">
                    <label for="invite">
                        <span>Инвайт<sup class="required">*</sup></span>

                        <div class="forinput"><input type="text" name="invite" class="js-invite" placeholder="укажите инвайт" value=""/>
                        </div>
                    </label>
                </div>
            {/if}

            <div class="label">
                <label for="email">
                    <span>Электронная почта<sup class="required">*</sup></span>

                    <div class="forinput"><input type="text" name="email" class="js-email" placeholder="name@example.com" value=""/></div>
                </label>
            </div>

            <div class="label">
                <label for="password">
                    <span>Пароль<sup class="required">*</sup></span>

                    <div class="forinput"><input type="password" name="password" class="js-password" value=""/></div>
                </label>
            </div>

        </div>
        <div class="goodregistration js-good" style="display: none;">
            Вы зарегистрированы!<br/><br/>
            {if $invite}
                На указанный вами email выслано письмо для активации аккаунта. Пожалуйста пройдите по указанной в письме ссылке.
            {else}
                На указанный вами номер телефона выслан код подтверждения регистрации.
            {/if}
        </div>
    </div>
    <div class="buttons tar">
        <button type="submit" name="submit" class="button success js-submit"><i class="icon icon-save"></i> Зарегистрироваться</button>
        {if !$invite}
            <button class="button js-confirm" style="display: none;"><i class="icon icon-key"></i> Ввести полученный код</button>
        {/if}
        <button class="button js-cancel" style="display: none;"><i class="icon icon-cancel"></i> Закрыть</button>
        <button class="button js-finish" style="display: none;"><i class="icon icon-ok"></i> Вход</button>
    </div>
    <div class="afterButtons">
        {if !$invite}
        <a href="#/confirm/registration" class="js-confirm">Ввести код регистрации</a></br>
        {/if}
        <a href="#/signin" class="js-signin">Вход</a>
    </div>
</form>
