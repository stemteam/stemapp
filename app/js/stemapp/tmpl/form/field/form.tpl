<form class="generic-new">
    <div class="mediform">
        {if $item.FormFieldId}
            <h1 class="alias js-name">Редактирование поля #{$item.FormFieldId}</h1>
        {else}
            <h1 class="js-name">Создание поля</h1>
        {/if}
        <div class="label">
            <label for="{$id}-Caption" class="name">
                <span>Заголовок:<sup class="required_field">*</sup></span>
            </label>

            <div class="forinput">
                <input type="text" name="Caption" class="js-Caption field" id="{$id}-Caption"
                       value="{$item.Caption || ''}"
                       placeholder="Заголовок">
            </div>
        </div>
        <div class="label">
            <label for="{$id}-FieldId">Поле:</label>

            <div class="forinput js-input-block">
                <input type="text" name="FieldName" class="js-FieldName field" id="{$id}-FieldName"
                       value="{$item.FieldName}"
                       placeholder="Поле" readonly="readonly">
                <span class="input-icon"><a href="#" class="button button-icon no-border js-clear-input" title="Очистить поле">✕</a></span>
                <span class="input-addon">
                    <button class="button button-icon js-field-form" type="button">...</button>
                </span>
                <input type="hidden" name="FieldId" class="js-FieldId" value="{$item.FieldId}">
            </div>
        </div>
        <div class="label">
            <label for="{$id}-OrderNum" class="name">
                <span>Порядок вывода:<sup class="required_field">*</sup></span>
            </label>

            <div class="forinput">
                <input type="text" name="OrderNum" class="js-OrderNum field" id="{$id}-OrderNum"
                       value="{$item.OrderNum}"
                       placeholder="Порядок вывода">
            </div>
        </div>
        <fieldset>
            <legend>Селект/Мультиселект</legend>
            Для того, чтобы поле отображалось как мультиселект или селект, укажите ниже форму, которая будет отвечать за возвращаемые
            данные, а также
            тип отображения поля
            <div class="label">
                <label for="{$id}-RelationFormId">Связанная форма:</label>

                <div class="forinput js-input-block">
                    <input type="text" name="RelationFormName" class="js-RelationFormName field" id="{$id}-RelationFormName"
                           value="{$item.RelationFormName || ''}"
                           placeholder="Связанная форма" readonly="readonly">
                    <span class="input-icon"><a href="#" class="button button-icon no-border js-clear-input"
                                                title="Очистить поле">✕</a></span>
                <span class="input-addon">
                    <button class="button button-icon js-form" type="button">...</button>
                </span>
                    <input type="hidden" name="RelationFormId" value="{$item.RelationFormId || ''}">
                </div>
            </div>

            <div class="label">
                <label for="{$id}-RelationFormId">Связанное поле:</label>
                <div class="forinput js-input-block">
                    <input type="text" name="RelationFieldName" class="js-RelationFieldName field" id="{$id}-RelationFieldName"
                           value="{$item.RelationFieldName || ''}"
                           placeholder="Связанное поле" readonly="readonly">
                    <span class="input-icon"><a href="#" class="button button-icon no-border js-clear-input"
                                                title="Очистить поле">✕</a></span>
                <span class="input-addon">
                    <button class="button button-icon js-form-field" type="button">...</button>
                </span>
                    <input type="hidden" name="RelationFieldId" value="{$item.RelationFieldId || ''}">
                </div>
            </div>

            <div class="label">
                <label for="{$id}-RelationFormType">Тип отображения:</label>

                <div class="select-input-block forselect">
                    <input type="text" class="field select-input" id="{$id}-RelationFormType"
                           value="{if $item.RelationFormType ==1}Одна запись{elseif $item.RelationFormType==2}Таблица{elseif $item.RelationFormType==3}Новый таб{/if}"
                           placeholder="Выберите тип поля" autocomplete="off">
                    <ul class="model_list select-input-list" data-input="{$id}-RelationFormType" style="display: none;">
                        <li class="item"><a href="#" data-id="0">&nbsp;</a></li>
                        <li class="item"><a href="#" data-id="1">Одна запись</a></li>
                        <li class="item"><a href="#" data-id="2">Таблица</a></li>
                        <li class="item"><a href="#" data-id="3">Новый таб</a></li>
                    </ul>
                    <input type="hidden" name="RelationFormType" class="js-RelationFormType" data-input-main="{$id}-RelationFormType"
                           value="{$item.RelationFormType || ''}">
                </div>
            </div>
            <div class="label">
                <label for="{$id}-LookupFormId">Форма из которой идет выбор элемента/элементов(только для мультиселектов):</label>

                <div class="forinput js-input-block">
                    <input type="text" name="LookupFormName" class="js-LookupFormName field" id="{$id}-LookupFormName"
                           value="{$item.LookupFormName || ''}"
                           placeholder="Связанная форма" readonly="readonly">
                    <span class="input-icon"><a href="#" class="button button-icon no-border js-clear-input"
                                                title="Очистить поле">✕</a></span>
                <span class="input-addon">
                    <button class="button button-icon js-lookup" type="button">...</button>
                </span>
                    <input type="hidden" name="LookupFormId" value="{$item.LookupFormId || ''}">
                </div>
            </div>
        </fieldset>
        <div class="label">
            <label for="{$id}-IsOrdered" class="checkbox paid">
                <input type="checkbox" name="IsOrdered" class="js-IsOrdered checkbox"
                       id="{$id}-IsOrdered"{if $item.IsOrdered} checked="checked"{/if}> Таблица отсортирована по этому полю по умолчанию
            </label>
        </div>
        <div class="label">
            <label for="{$id}-IsOrderedDesc" class="checkbox paid">
                <input type="checkbox" name="IsOrderedDesc" class="js-IsOrderedDesc checkbox"
                       id="{$id}-IsOrderedDesc"{if $item.IsOrderedDesc} checked="checked"{/if}> Сортируется по убыванию (если отключено, то
                по возрастанию)
            </label>
        </div>

        <div class="label">
            <label for="{$id}-IsPrimary" class="checkbox paid">
                <input type="checkbox" name="IsPrimary" class="js-IsPrimary checkbox"
                       id="{$id}-IsPrimary"{if $item.IsPrimary} checked="checked"{/if}> Является определяющим полем набора
            </label>
        </div>

        <div class="label">
            <label for="{$id}-IsHidden" class="checkbox paid">
                <input type="checkbox" name="IsHidden" class="js-IsHidden checkbox"
                       id="{$id}-IsHidden"{if $item.IsHidden} checked="checked"{/if}> Поле невидимо
            </label>
        </div>

        <div class="label">
            <label for="{$id}-IsReadonly" class="checkbox paid">
                <input type="checkbox" name="IsReadonly" class="js-IsReadonly checkbox"
                       id="{$id}-IsReadonly"{if $item.IsReadonly} checked="checked"{/if}> Поле только для чтения
            </label>
        </div>

        <div class="buttons label split">
            <button type="submit" class="button success js-submit">
                <i class="icon icon-save"></i> {if !$item.FormFieldId}Создать поле{else}Сохранить изменения{/if}</button>
        </div>
    </div>
</form>