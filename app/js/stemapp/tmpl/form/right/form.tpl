<form class="generic-new">
    <div class="mediform">
        {if $item.FormRightId}
            <h1 class="alias js-name">Редактирование прав #{$item.FormRightId}</h1>
        {else}
            <h1 class="js-name">Создание прав для формы</h1>
        {/if}
        <div class="label">
            <label for="{$id}-FormId">Форма:</label>
            {if !$item.FormId}
                <div class="forinput js-input-block">
                    <input type="text" name="FormName" class="js-FormName field" id="{$id}-FormName"
                           value="{$item.FormName || ''}"
                           placeholder="Форма" readonly="readonly">
                    <span class="input-icon"><a href="#" class="button button-icon no-border js-clear-input"
                                                title="Очистить поле">✕</a></span>
                <span class="input-addon">
                    <button class="button button-icon js-form" type="button">...</button>
                </span>
                    <input type="hidden" name="FormId" value="{$item.FormId || ''}">
                </div>
            {else}
                {$item.FormName || ''}
            {/if}
        </div>
        <div class="resource field label">
            <label for="{$id}-RoleId">Роль:</label>
            {if !$item.RoleId}
                <div class="select-input-block forselect">
                    <input type="text" class="field select-input" id=" {$id}-RoleId"
                           value="{$mtname || ''}"
                           placeholder="Выберите связанный метод" autocomplete="off">
                    <ul class="model_list select-input-list" data-input="{$id}-RoleId" style="display: none;">
                        {foreach $roles as $role}
                            <li class="item"><a href="#" data-id="{$role.RoleId}">{$role.RoleName}</a>
                            </li>
                        {/foreach}
                    </ul>
                    <input type="hidden" name="RoleId" class="js-RoleId" data-input-main="{$id}-RoleId"
                           value="{$mtid}">
                </div>
            {else}
                {if $item.RoleId}{foreach $roles as $role}{if $role.RoleId==$item.RoleId}{$mtname = $role.RoleName}{$mtid = $role.RoleId}{/if}{/foreach}{/if}
                {$mtname}
            {/if}
        </div>
        <div class="label">
            <label for="{$id}-IsCreate" class="checkbox paid">
                <input type="checkbox" name="IsCreate" class="js-IsCreate checkbox"
                       id="{$id}-IsCreate"{if $item.IsCreate} checked="checked"{/if}> IsCreate
            </label>
        </div>
        <div class="label">
            <label for="{$id}-IsRead" class="checkbox paid">
                <input type="checkbox" name="IsRead" class="js-IsRead checkbox"
                       id="{$id}-IsRead"{if $item.IsRead} checked="checked"{/if}> IsRead
            </label>
        </div>
        <div class="label">
            <label for="{$id}-IsUpdate" class="checkbox paid">
                <input type="checkbox" name="IsUpdate" class="js-IsUpdate checkbox"
                       id="{$id}-IsUpdate"{if $item.IsUpdate} checked="checked"{/if}> IsUpdate
            </label>
        </div>
        <div class="label">
            <label for="{$id}-IsDelete" class="checkbox paid">
                <input type="checkbox" name="IsDelete" class="js-IsDelete checkbox"
                       id="{$id}-IsDelete"{if $item.IsDelete} checked="checked"{/if}> IsDelete
            </label>
        </div>

        <div class="label">
            <label for="{$id}-Filter" class="name">
                <span>Фильтр (заполняется автоматически):</span>
            </label>

            <div class="forinput">
                <textarea name="Filter" class="js-Filter field" id="{$id}-Filter" readonly=""
                          placeholder="Фильтр">{$item.Filter || ''}</textarea>
            </div>
        </div>
        <div class="block js-filter-table block-table border-table"></div>
        <button type="button" class="button js-delete-record">Удалить строку</button>

        <fieldset>
            <div class="label">
                <label for="{$id}-FieldId">Поле:</label>

                <div class="forinput js-input-block">
                    <input type="text" name="FieldName" class="js-FieldName field" id="{$id}-FieldName"
                           value=""
                           placeholder="Поле" readonly="readonly">
                    <span class="input-icon"><a href="#" class="button button-icon no-border js-clear-input"
                                                title="Очистить поле">✕</a></span>
                <span class="input-addon">
                    <button class="button button-icon js-field" type="button">...</button>
                </span>
                    <input type="hidden" name="FieldId" value="">
                </div>
            </div>
            <div class="resource field label">
                <label for="{$id}-OperationId">Операция:</label>
                {if !$item.OperationId}
                    <div class="select-input-block forselect">
                        <input type="text" class="field select-input" id=" {$id}-OperationId"
                               value="{$opname || ''}"
                               placeholder="Выберите связанный метод" autocomplete="off">
                        <ul class="model_list select-input-list" data-input="{$id}-OperationId" style="display: none;">
                            {foreach $operations as $operation}
                                <li class="item"><a href="#" data-id="{$operation.OperationId}">{$operation.OperationName}</a>
                                </li>
                            {/foreach}
                        </ul>
                        <input type="hidden" name="OperationId" class="js-OperationId" data-input-main="{$id}-OperationId"
                               value="{$opid}">
                    </div>
                {else}
                    {if $item.OperationId}{foreach $operations as $operation}{if $operation.OperationId==$item.OperationId}{$opname = $operation.OperationName}{$opid = $operation.OperationId}{/if}{/foreach}{/if}
                    {$opname}
                {/if}
            </div>
            <div class="label">
                <label for="{$id}-Value" class="name">
                    <span>Значение:</span>
                </label>

                <div class="forinput">
                    <input type="text" name="Value" class="js-Value field" id="{$id}-Value"
                           value="{$item.Value}"
                           placeholder="Значение">
                </div>
            </div>
            <div class="label radiogroup type">
                <label>Тип:
                    <span class="note js-disable-type">
                    Недоступно для первой записи
                </span>
                </label>

                <div class="radiobuttons js-enable-type">
                    <a href="#" class="active"><input type="radio" name="Type" value="or" class="type type1 radio" id="{$id}-atype-1"
                                                      checked="checked"> Или</a>
                    <a href="#"><input type="radio" name="Type" value="and" class="type type1 radio" id="{$id}-atype-2"> И</a>
                </div>

            </div>
            <button type="button" class="button js-add-record">Добавить строку в фильтр</button>
        </fieldset>
        <div class="buttons label split">
            <button type="submit" class="button success js-submit">
                <i class="icon icon-save"></i> {if !$item.FormRightId}Создать права для формы{else}Сохранить изменения{/if}</button>
        </div>
    </div>
</form>