<form class="generic-new">
    <div class="mediform">
        {if $item.FormId}
            <h1 class="alias js-name">Редактирование формы #{$item.FormId}</h1>
        {else}
            <h1 class="js-name">Создание {if $parent}дочерней{/if} формы</h1>
        {/if}
        {if $parent}
            <div class="label">
                Родительская форма: <a href="#" class="js-to-parent">{$parent.Description} ({$parent.Name})</a>
            </div>
        {/if}


        <div class="label">
            <label for="{$id}-Name" class="name">
                <span>Имя на латинице(URL):<sup class="required_field">*</sup></span>
            </label>

            <div class="forinput">
                <input type="text" name="Name" class="js-Name field" id="{$id}-Name" value="{$item.Name || ''}"
                       placeholder="Название на латинице(URL)">
            </div>
        </div>
        <div class="label">
            <label for="{$id}-Description" class="name">
                <span>Название формы:<sup class="required_field">*</sup></span>
            </label>

            <div class="forinput">
                <input type="text" name="Description" class="js-Description field" id="{$id}-Description"
                       value="{$item.Description || ''}"
                       placeholder="Заголовок таба с формой">
            </div>
        </div>
        <div class="label">
            <label for="{$id}-ListDescription" class="name">
                <span>Название таблицы:<sup class="required_field">*</sup></span>
            </label>

            <div class="forinput">
                <input type="text" name="ListDescription" class="app-ListDescription field" id="{$id}-ListDescription"
                       value="{$item.ListDescription || ''}"
                       placeholder="Заголовок таба с таблицей">
            </div>
        </div>
        <div class="label">
            <label for="{$id}-OrderNum" class="name">
                <span>Порядок сортировки на форме:<sup class="required_field">*</sup></span>
            </label>

            <div class="forinput">
                <input type="text" name="OrderNum" class="app-OrderNum field" id="{$id}-OrderNum"
                       value="{$item.OrderNum || ''}"
                       placeholder="Порядок сортировки на форме">
            </div>
        </div>
        <div class="resource field label">
            <label for="{$id}-MethodId">Метод:</label>
            {if $item.MethodId}{foreach $methods as $method}{if $method.MethodId==$item.MethodId}{$mtname = $method.MethodName}{$mtid = $method.MethodId}{/if}{/foreach}{/if}
            <div class="select-input-block forselect">
                <input type="text" class="field select-input" id=" {$id}-MethodId"
                value="{$mtname || ''}"
                placeholder="Выберите связанный метод" autocomplete="off">
                <ul class="model_list select-input-list" data-input="{$id}-MethodId" style="display: none;">
                    <li class="item"><a href="#" data-id=""></a></li>
                    {foreach $methods as $method}
                        <li class="item"><a href="#" data-id="{$method.MethodId}">{$method.MethodName}</a></li>
                    {/foreach}
                </ul>
                <input type="hidden" name="MethodId" class="js-MethodId" data-input-main="{$id}-MethodId"
                       value="{$mtid}">
            </div>
        </div>

        <fieldset>
            <legend>Параметры связывания фрейма и формы</legend>

            <div class="label">
                <label for="{$id}-FieldName" class="name">
                    <span>FieldName:<sup class="required_field">*</sup></span>
                </label>

                <div class="forinput">
                    <input type="text" name="FieldName" class="js-FieldName field" id="{$id}-FieldName"
                           value="{$item.FieldName || ''}"
                           placeholder="FieldName">
                </div>
            </div>
            <div class="label">
                <label for="{$id}-ParentFieldName" class="name">
                    <span>ParentFieldName:<sup class="required_field">*</sup></span>
                </label>

                <div class="forinput">
                    <input type="text" name="ParentFieldName" class="js-ParentFieldName field"
                           id="{$id}-ParentFieldName" value="{$item.ParentFieldName || ''}"
                           placeholder="ParentFieldName">
                </div>
            </div>
            <div class="note">
                Два параметра необходимы для связывания формы и фрейма. В функцию Get фрейма эти параметры будут
                передаваться в виде { FieldName: ParentFieldName }, где
                FieldName — имя поля, а ParentFieldName — поле из формы родителя. ParentFieldName также может быть задан
                как <strong>#</strong>, что будет означать передачу в качестве значения
                идентификатора из адресной строки. К примеру для формы клиента следует указать FieldName = id_client,
                ParentFieldName = #
            </div>
        </fieldset>


        <div class="buttons label split">
            <button type="submit" class="button success js-submit">
                <i class="icon icon-save"></i> {if !$item.FormId}Создать форму{else}Сохранить изменения{/if}</button>
        </div>
    </div>
</form>