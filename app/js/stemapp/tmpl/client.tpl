<div class="mediform">
    <h1>Удаление пользователя</h1>
    <form class="generic-new" action="#">

        <div class="user field label">
            <label for="{{id}}-user">Телефон/email:</label>
            <div class="forinput">
                <input type="text" name="user" class="js-user field required" id="id-user" value="" placeholder="79123456789/user@company.com" />
            </div>
        </div>

        <div class="buttons submit label">
            <button type="submit" class="default button js-submit success" id="{{id}}-uc-submit"><i class="icon icon-ok"></i>Удалить пользователя</button>
        </div>
    </form>
</div>

