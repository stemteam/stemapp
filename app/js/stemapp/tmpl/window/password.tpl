<div class="banner small"><h1>Смена пароля</h1></div>
<div class="form small">
    <div class="label">
        <label>Текущий пароль:</label>
        <div class="forinput"><input type="password" name="OldPasswd" class="field js-change-pass-current" placeholder="Введите текущий пароль"/></div>
    </div>
    <div class="label">
        <label>Новый пароль:</label>
        <div class="forinput"><input type="password" name="NewPasswd" class="field js-change-pass-new" placeholder="Введите новый пароль"/></div>
    </div>
    <div class="label">
        <label>Подтверждение:</label>
        <div class="forinput"><input type="password" name="NewPasswd2" class="field js-change-pass-repeat" placeholder="Повторите новый пароль"/></div>
    </div>
</div>
<div class="buttons tar">
    <button type="submit" name="submit" class="default button submit success"><i class="icon icon-save"></i> Изменить пароль</button>
    <button class="default button cancel"><i class="icon icon-cancel"></i> Отменить</button>
</div>