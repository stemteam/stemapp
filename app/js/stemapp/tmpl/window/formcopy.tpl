<div class="banner small"><h1>{$title}</h1></div>
<div class="form small">
    <form id="change_password_form" xmlns="http://www.w3.org/1999/html">
        <div class="label">Вы хотите скопировать форму <strong>{$item.Name}</strong></div>
        <div>
            <label for="{$id}-Name" class="name">
                <span>Префикс для новой формы на латинице:</span>
            </label>
            <div class="forinput">
                <input type="text" name="Name" class="js-Name field" id="{$id}-Name"
                       value=""
                       placeholder="Укажите префикс для новой формы">
            </div>
        </div>

    </form>
</div>
<div class="buttons tar">
    <button type="submit" class="default button success" value="yes"><i class="icon icon-ok"></i> Скопировать</button>
    <button class="default button cancel" value="no"><i class="icon icon-cancel"></i> Отменить</button>
</div>