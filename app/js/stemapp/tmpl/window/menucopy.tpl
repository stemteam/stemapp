<div class="banner small"><h1>{$title}</h1></div>
<div class="form small">
    <form id="change_password_form" xmlns="http://www.w3.org/1999/html">
        <div class="label">Вы хотите скопировать пункт меню <strong>{$item.MenuName}</strong></div>
        <div>
            <label for="{$id}-MenuId">В родительское меню:</label>

            <div class="forinput js-input-block">
                <input type="text" name="MenuName" class="js-MenuName field" id="{$id}-MenuName"
                       value=""
                       placeholder="Выберите меню" readonly="readonly">
                    <span class="input-icon"><a href="#" class="button button-icon no-border js-clear-input"
                                                title="Очистить поле">✕</a></span>
                <span class="input-addon">
                    <button class="button button-icon js-choose-menu" type="button">...</button>
                </span>
                <input type="hidden" name="MenuId" value="">
            </div>
        </div>

    </form>
</div>
<div class="buttons tar">
    <button type="submit" class="default button success" value="yes"><i class="icon icon-ok"></i> Скопировать</button>
    <button class="default button cancel" value="no"><i class="icon icon-cancel"></i> Отменить</button>
</div>