<form action="#" method="post" class="out-form js-form">
    <div class="banner small"><h1>Параметры</h1></div>
    <div class="form">
        <div class="row-fluid" style="margin-top: 15px">
            <div class="span8">
                <label for="guid">Ключ:</label>
                <div class="row-fluid">
                <input type="text" name="Guid" value="" class="js-guid span12" spellcheck="false" placeholder="Укажите ключ"/>
                    </div>
            </div>
            <div class="span4 tar">
                <a href="#" class="button js-demoGUID" style="margin-top: 22px"><i class="icon icon-key"></i> Демо клиент</a>
            </div>
        </div>
    </div>
    <div class="buttons tar">
        <button type="submit" class="default button success" value="yes"><i class="icon icon-ok"></i> Ок</button>
        <button class="default button cancel" value="no"><i class="icon icon-cancel"></i> Отменить</button>
    </div>
</form>