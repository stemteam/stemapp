<div class="banner small"><h1>{$title}</h1></div>
<div class="form small">
    <form id="change_password_form" xmlns="http://www.w3.org/1999/html">
        <div class="label">Вы хотите сменить пароль для пользователя <strong>{$login}</strong></div>
        <div class="label">
            <label>Новый пароль:</label>
            <div class="forinput"><input type="password" name="NewPasswd" class="js-password" placeholder="Введите новый пароль"/></div>
        </div>
        <div class="label">
            <label>Подтверждение:</label>
            <div class="forinput"><input type="password" name="NewPasswd2" class="js-conf-password" placeholder="Повторите новый пароль"/></div>
        </div>
    </form>
</div>
<div class="buttons tar">
    <button type="submit" class="default button success" value="yes"><i class="icon icon-ok"></i> Изменить пароль</button>
    <button class="default button cancel" value="no"><i class="icon icon-cancel"></i> Отменить</button>
</div>