<form action="#" method="post" class="out-form js-form">
    <div class="banner"><h1>Восстановление пароля</h1></div>
    <div class="form">
        <div class="js-fields" style="margin-top: 35px">
            <div class="label">
                <label for="mobilePhone">
                    <span>Email или телефон</span>
                    <div class="forinput"><input type="text" name="Login" value="" class="js-mobilePhone" spellcheck="false" placeholder="Мобильный телефон или email"/></div>
                </label>
            </div>
        </div>
        <div class="js-good goodregistration" style="display: none;">
            <br/>
            Вам был выслан код для сброса пароля. Пожалуйста, перейдите на форму смены пароля и введите его
        </div>
    </div>
    <div class="buttons tar">
        <button type="submit" name="submit" class="button success js-success"><i class="icon icon-ok"></i> Получить код</button>
        <button class="button js-confirm" style="display: none;"><i class="icon icon-key"></i> Ввести полученный код</button>
        <button class="default button cancel js-close" type="button" style="display: none;"><i class="icon icon-cancel"></i> Закрыть</button>
    </div>
    <div class="afterButtons">
        <a href="#/confirm/remember" class="js-confirm">Ввести код сброса пароля</a></br>
        <a href="#/signin">Вход</a>
    </div>
</form>
