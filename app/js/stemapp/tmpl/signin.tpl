<form action="#" method="post" class="out-form js-form">
    <div class="banner"></div>
    <div class="form horizontal">
        <div class="label" style="margin-top: 17px">
            <label for="email">
                <span>Email / Телефон</span>
                <div class="forinput"><input type="text" name="Email" value="" class="js-email" spellcheck="false" placeholder="Укажите электронную почту" autofocus/></div>
            </label>
        </div>
        <div class="label">
            <label for="password">
                <span>Пароль</span>
                <div class="forinput"><input type="password" name="Passwd" value="" class="js-pass" spellcheck="false" placeholder="Укажите пароль"/></div>
            </label>
        </div>
    </div>
    <div class="buttons tar">
        <button type="submit" name="submit" class="button success js-submit" value="yes"><i class="icon icon-ok"></i> Войти</button>
    </div>
    <div class="afterButtons">
        {if $demoEnable}<a href="#/demo" class="js-demo demo">Демонстрационный вход</a><br/>{/if}
        {if $restorePasswordEnable}<a href="#/remember" class="js-remember remember">Забыли пароль?</a><br/>{/if}
        {if $registerEnable}<a href="#/registration"  class="js-registration registration">Регистрация</a>{/if}
    </div>
    <div class="progress js-progress">
        <div class="bar js-bar"></div>
    </div>
</form>