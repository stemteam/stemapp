<form class="generic-new">
    <div class="mediform">
        {if $item.MenuId}
            <h1 class="alias js-name">Редактирование меню #{$item.MenuId}</h1>
        {else}
            <h1 class="js-name">Создание {if $parent}дочернего{/if} меню</h1>
        {/if}
        {if $parent}
            <div class="label">
                Родительское меню: <a href="#" class="js-to-parent">{$parent.MenuName} ({$parent.Name})</a>
            </div>
        {/if}

        <div class="label">
            <label for="{$id}-MenuName" class="name">
                <span>Название:<sup class="required_field">*</sup></span>
            </label>

            <div class="forinput">
                <input type="text" name="MenuName" class="js-MenuName field" id="{$id}-MenuName" value="{$item.MenuName || ''}"
                       placeholder="Название">
            </div>
        </div>

        <div class="resource field label">
            <label for="{$id}-menus">Родительское меню:</label>
            {if $item.MenuParentId || $parent}{foreach $menus as $menu}{if $menu.MenuId==$item.MenuParentId || ($parent && $parent.MenuId==$menu.MenuId)}{$mnname = "{$menu.MenuName}"}{$mnid = $menu.MenuId}{/if}{/foreach}{/if}
            <div class="select-input-block forselect">
                <input type="text" class="field select-input" id="{$id}-menus"
                       value="{$mnname}"
                       placeholder="Выберите связанный фрейм" autocomplete="off">
                <ul class="model_list select-input-list" data-input="{$id}-menus" style="display: none;">
                    {foreach $menus as $menu}
                        <li class="item"><a href="#" data-id="{$menu.MenuId}">{$menu.MenuName}</a>
                        </li>
                    {/foreach}
                </ul>
                <input type="hidden" name="MenuParentId" class="js-menus" data-input-main="{$id}-menus"
                       value="{$mnid}">
            </div>
        </div>
        <div class="label">
            <label for="{$id}-Icon" class="name">
                <span>Иконка:<sup class="required_field">*</sup></span>
            </label>

            <div class="forinput">
                <input type="text" name="Icon" class="js-Icon field" id="{$id}-Icon" value="{if $item.Icon}{$item.Icon}{/if}"
                       placeholder="Иконка">
            </div>
        </div>

        <div class="label">
            <label for="{$id}-MenuTooltip" class="name">
                <span>Всплывающая подсказка:<sup class="required_field">*</sup></span>
            </label>

            <div class="forinput">
                <input type="text" name="MenuTooltip" class="js-MenuTooltip field" id="{$id}-MenuTooltip"
                       value="{$item.MenuTooltip || ''}"
                       placeholder="Всплывающая подсказка">
            </div>
        </div>
        <div class="label">
            <label for="{$id}-MenuNum" class="name">
                <span>Порядковый номер:<sup class="required_field">*</sup></span>
            </label>
            <div class="forinput">
                <input type="text" name="MenuNum" class="js-MenuNum field" id="{$id}-MenuNum"
                       value="{$item.MenuNum || ''}"
                       placeholder="Порядковый номер">
            </div>
        </div>
        <div class="label">
            <label for="{$id}-MenuUrl" class="name">
                <span>Сссылка на таб(если форма не привязана):</span>
            </label>

            <div class="forinput">
                <input type="text" name="MenuUrl" class="js-MenuUrl field" id="{$id}-MenuUrl" value="{if $item.MenuUrl}{$item.MenuUrl}{/if}"
                       placeholder="Название таба, который нужно открыть">
            </div>
        </div>
        <div class="resource field label">
            <label for="{$id}-forms">Форма:</label>
            {if $item.FormId}{foreach $forms as $form}{if $form.FormId==$item.FormId}{$frname = "{$form.Description} ({$form.Name})"}{$frid = $form.FormId}{/if}{/foreach}{/if}
            <div class="select-input-block forselect">
                <input type="text" class="field select-input" id="{$id}-forms"
                value="{$frname}"
                placeholder="Выберите связанный фрейм" autocomplete="off">
                <ul class="model_list select-input-list" data-input="{$id}-forms" style="display: none;">
                    {foreach $forms as $form}
                        <li class="item"><a href="#" data-id="{$form.FormId}">{$form.Description} ({$form.Name})</a>
                        </li>
                    {/foreach}
                </ul>
                <input type="hidden" name="FormId" class="js-forms" data-input-main="{$id}-forms"
                       value="{$frid}">
            </div>
        </div>
        <div class="label">
            <label for="{$id}-Param" class="name">
                <span>Глобальный параметр, который передается форму:<sup class="required_field">*</sup></span>
            </label>
            <div class="forinput">
                <input type="text" name="Param" class="js-Param field" id="{$id}-Param"
                       value="{if $item.Param}{$item.Param}{/if}"
                       placeholder="Глобальный параметр">
            </div>
        </div>
        <div class="label">
            <label for="{$id}-ViewType" class="name">
                <span>Способ открытия:<sup class="required_field">*</sup></span>
            </label>

            {foreach $viewTypes as $key => $type}
                {if $key == $item.ViewType}
                    {$vtid = $key}
                    {$vtname = $type}
                {/if}
            {/foreach}
            <div class="select-input-block forselect">
                <input type="text" class="field select-input id=" {$id}-ViewType" value="{$vtname}"
                placeholder="Способ открытия формы" autocomplete="off">
                <ul class="model_list select-input-list" data-input="{$id}-ViewType" style="display: none;">
                    {foreach $viewTypes as $key => $type}
                        <li class="item"><a href="#" data-id="{$key}">{$type}</a></li>
                    {/foreach}
                </ul>
                <input type="hidden" name="ViewType" class="js-ViewType" data-input-main="{$id}-ViewType" value="{$vtid}">
            </div>
        </div>
        <div class="label">
            <label for="{$id}-RoleName" class="name">
                <span>Роль:<sup class="required_field">*</sup></span>
            </label>
            <div class="forinput">
                <input type="text" name="RoleName" class="js-RoleName field" id="{$id}-RoleName"
                       value="{$item.RoleDescription} ({$item.RoleName})" disabled="disabled" readonly="readonly"
                       placeholder="Роль">
            </div>
        </div>
        <div class="buttons label split">
            <button type="submit" class="button success js-submit">
                <i class="icon icon-save"></i> {if !$item.MenuId}Создать форму{else}Сохранить изменения{/if}</button>
        </div>
    </div>
</form>