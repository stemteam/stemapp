<div class="mp-pusher" id="mp-pusher">
    <nav id="mp-menu" class="mp-menu">
        <div class="mp-level">
            <h2>{$menu_title}</h2>
            <ul>
                {$menu_html nofilter}
            </ul>
        </div>
    </nav>
    <div class="mp-overlay"></div>
    <div id="header">
        <button id="menu-button">
        </button>
        <div id="tab-wrapper">
            <ul id="tab-bar">
            </ul>
        </div>
        <a href="#" class="tab-arrow left-arrow js-left-tab js-tab-arrows" style="display: none;"></a>
        <a href="#" class="tab-arrow right-arrow js-right-tab js-tab-arrows" style="display: none;"></a>
        <a href="#" class="tab-list-button js-tab-list-button"></a>
        <a href="#" class="tab-update-button js-tab-update-button"></a>
        <ul id="tab-list" class="tab-list" style="display: none;"></ul>
    </div>
    <div id="content" class="main">

        <div id="tab-panel">
            <div id="tab-default" class="default active">
                {strip}
                    {if $showStartPage}
                        <div class="start-page-menu">
                            <h2>{$menu_title}</h2>
                            <ul class="big-menu">
                                {$menu_html nofilter}
                            </ul>
                        </div>
                    {/if}
                {/strip}
            </div>
        </div>
    </div>

</div>
<div id="newversion" class="newversion"><p>

    <div class="main">
        <p>Новая версия Личного кабинета!</p>

        <p>Для корректной работы рекомендуем завершить операцию и перезапустить приложение!</p>

        <p>При перезапуске приложения все текущие изменения не сохраняться!</p>

        <p><a href="#" class="button js-ok"><i class="icon icon-ok"></i> Перезапустить</a> <a href="#" class="button js-cancel"><i
                        class="icon icon-close"></i> Закрыть</a></p>
    </div>
    <div class="restart" style="display: none">
        <a href="#" class="button js-ok w100"><i class="icon icon-ok"></i> Перезапустить приложение</a>
    </div>
</div>