<form action="#" method="post" class="out-form js-form">
    <div class="banner"></div>
    <div class="form horizontal">
        <div class="label">
            <label for="company">
                <span>Организация</span>
                <div class="select-input-block forselect">
                    <input type="text" id="company" class="model field select-input js-company" value="" placeholder="Выберите организацию" autocomplete="off" disabled="disabled">
                    <ul class="model_list select-input-list" data-input="company" style="display: none;" tabindex="-1"></ul>
                    <input type="hidden" name="Guid" data-input-main="company" value="">
                </div>
            </label>
        </div>
        <div class="label">
            <label for="email">
                <span>Пользователь</span>
                <div class="forinput"><input type="text" name="Login" id="login" value="" class="js-login" spellcheck="false" placeholder="Укажите логин"/></div>
            </label>
        </div>
        <div class="label">
            <label for="password">
                <span>Пароль</span>
                <div class="forinput"><input type="password" name="Password" id="password" value="" class="js-pass" spellcheck="false" placeholder="Укажите пароль"/></div>
            </label>
        </div>
    </div>
    <div class="buttons tar">
        <button type="submit" name="submit" id="submit" value="yes" class="default button success"><i class="icon icon-ok"></i> Войти</button>
    </div>
    <div class="afterButtons">
        <a href="#!/demo" class="js-demo">Демонстрационный вход</a><br/>
    </div>
    <div class="progress js-progress">
        <div class="bar js-bar"></div>
    </div>
</form>