<div class="generic-new js-columns">{if $caption}<h1>{$caption}</h1>

    <div class="submit">
        <button type="submit" class="js-submit button-up button success" id="{$id}-asubmit"><i class="icon icon-save"></i>Сохранить
            изменения
        </button>
    </div>
    {/if}
    <div>{$auto}</div>
    <div class="downpanel tar">
        <button type="submit" name="Save" class="js-submit button success" id="{$id}-asubmit-down">
                <i class="icon icon-save"></i>
                Сохранить изменения</button>
    </div>
</div>