<form action="#" method="get">
    {foreach $fields as $field}
        {if !$field.IsHidden}
            {if $field.IsReadonly}
                <div class="label">
                    {if $field.IsId}
                        <label for="{$cid}-{$field.Name}" class="field">{$field.Caption}:{if $field.IsMandatory}
                            <sup class="required_field">*</sup>{/if}</label>
                        <div class="forinput">
                            <input type="text" name="{$field.Name}" class="field" id="{$cid}-{$field.Name}" readonly
                                   value="{if $data[$field.Name] || $data[$field.Name]==0}{$data[$field.Name]}{else}{if $field.DefaultValue}{$field.DefaultValue}{/if}{/if}"
                                   placeholder="{$field.Description || ""}">
                        </div>
                    {else}
                        <label for="{$cid}-{$field.Name}" class="field">{$field.Caption}:
                            <span class="read-only-value">
                                {if $field.Presentation == 'DateTime'}
                                    {if $data[$field.Name] || $data[$field.Name]==0}{{{$data[$field.Name]/1000}|string_format:"%d"}|date_format:"%e.%m.%G %H:%M:%S"}{else}{if $field.DefaultValue}{$field.DefaultValue}{/if}{/if}
                                {elseif $field.Presentation == 'Date'}
                                    {if $data[$field.Name] || $data[$field.Name]==0}{$data[$field.Name]|date_format:"%e.%m.%G"}{else}{if $field.DefaultValue}{$field.DefaultValue}{/if}{/if}
                                {elseif $field.Presentation == 'Text' || $field.Presentation == 'String'}
                                    {if $data[$field.Name] || $data[$field.Name]==0}{$data[$field.Name]}{else}{if $field.DefaultValue}{$field.DefaultValue}{/if}{/if}
                                {elseif $field.Presentation == 'TextId'}
                                    <input type="text" name="{$field.Name}" class="field" id="{$cid}-{$field.Name}" readonly
                                           value="{if $data[$field.Name] || $data[$field.Name]==0}{$data[$field.Name]}{else}{if $field.DefaultValue}{$field.DefaultValue}{/if}{/if}"
                                           placeholder="{$field.Description || ""}">
                                {elseif $field.Presentation == 'DataSet'}
                                    {if $field.DataSet}
                                        {if $data[$field.Name]}{foreach $field.DataSet as $dataset}{if $dataset[$field.RelationField]==$data[$field.Name]}{$ddname = $dataset._Caption}{$ddid = $dataset[$field.RelationField]}{/if}{/foreach}{/if}
                                        {$ddname}
                                    {else}
                                        Загрузка...
                                    {/if}
                                {elseif $field.Presentation == 'Multiselect'}
                                    {if $field.DataSet}
                                        {foreach $field.DataSet as $item}
                                            {$selected = false}
                                            {foreach $meta.DataSet as $data_item}

                                                {if $data_item[$field.relation.field] == $item[$field.relation.field]}
                                                    {$selected = $data_item.Id}
                                                {/if}
                                            {/foreach}
                                            {if $selected} {$item._Caption} {/if}
                                        {/foreach}
                                    {else}
                                        Загрузка...
                                    {/if}
                                {elseif $field.Presentation == 'Boolean'}
                                    {if $data[$field.Name]}Да{else}{if $field.DefaultValue}Да{else}Нет{/if}{/if}
                                {elseif $field.Presentation == 'TextBox'}
                                    {if $data[$field.Name] || $data[$field.Name]==0}{$data[$field.Name]|nl2br nofilter}{else}{if $field.DefaultValue}{$field.DefaultValue|nl2br nofilter}{/if}{/if}
                                {elseif $field.Presentation == 'Image'}
                                    <div>
                                        <img src="http://test.api.doctor.telemediclab.ru/ExpertImageGet.json?Version=8&ApiKey=44E95F087233A4FF76681DE7A4BE3C3E1E88BAB5CA51CC884BE4A1B22F8B683C&Image={if $data[$field.Name] || $data[$field.Name]==0}{$data[$field.Name]}{else}{if $field.DefaultValue}{$field.DefaultValue}{/if}{/if}"
                                             alt="{$field.Description || ""}" title="{$field.Description || ""}">
                                    </div>
                                {else}
                                    {if $data[$field.Name] || $data[$field.Name]==0}{$data[$field.Name]}{else}{if $field.DefaultValue}{$field.DefaultValue}{/if}{/if}
                                {/if}
                            </span>
                        </label>
                    {/if}

                </div>
            {else}
                <div class="label">

                    {if $field.Presentation == 'Date' || $field.Presentation == 'DateTime' || $field.Presentation == 'Time'}
                        <label for="{$cid}-{$field.Name}" class="card field">{$field.Caption}:{if $field.IsMandatory}
                            <sup class="required_field">*</sup>{/if}</label>
                        <div class="forinput">
                            <input type="text" name="{$field.Name}" class="js-dateinput field" id="{$cid}-{$field.Name}"
                                   {if $field.Presentation == 'Date'}data-format="date"{/if}
                                    {if $field.Presentation == 'DateTime'}data-format="datetime"{/if}
                                    {if $field.Presentation == 'Time'}data-format="time"{/if}
                                   value="{if $data[$field.Name] || $data[$field.Name]==0}{$data[$field.Name]}{else}{if $field.DefaultValue}{$field.DefaultValue}{/if}{/if}"
                                   placeholder="{$field.Description || ""}">
                        </div>
                    {elseif $field.RelationFormType == 3 && $field.RelationFormId}
                    <label for="{$cid}-{$field.Name}" class="field">{$field.Caption}: <a href="#" class="js-show-external-table" data-field="{$field.Name}">перейти к списку</a></label>
                    {elseif $field.RelationFormType == 2 && $field.RelationFormId}
                    <label for="{$cid}-{$field.Name}">{$field.Caption}:</label>
                        <div class="block js-multiselect js-{$field.Name} block-table border-table"></div>
                        <div><a href="#" class="js-show-external-table" data-field="{$field.Name}">Редактировать список</a></div>
                        {*<button type="button" class="button button-icon js-add-record" data-name="{$field.Name}">+</button>*}
                        {*<button type="button" class="button button-icon js-delete-record" data-name="{$field.Name}">-</button>*}
                        {*<input type="hidden" name="{$field.Name}" value="">*}
                    {elseif $field.RelationFormType == 1 && $field.RelationFormId}
                    <label for="{$cid}-{$field.Name}Name">{$field.Caption}:</label>

                    <div class="forinput js-input-block">
                    <input type="text" class="js-{$field.Name}Name field choose-select" id="{$id}-{$field.Name}Name"
                    value="{strip}{if $field.Data}{foreach $field.RelationFormMetaData as $metadata}
                                                                {if $metadata.IsCaption}
                                                                    {"{$field.Data[0][$metadata.Name]} "}
                                                                {/if}
                                                            {/foreach}{/if}{/strip}"
                    placeholder="{$item.Description || ''}" readonly="readonly">
                    <span class="input-icon"><a href="#" class="button button-icon no-border js-clear-input"
                    title="Очистить поле">✕</a></span>
                    <span class="input-addon">
                    <button class="button button-icon js-select" data-name="{$field.Name}" type="button">...</button>
                    </span>
                    <input type="hidden" name="{$field.Name}" value="{strip}{foreach $field.RelationFormMetaData as $metadata}
                                                                {if $metadata.IsId}
                                                                    {$field.Data[0][$metadata.Name]}
                                                                {/if}
                                                            {/foreach}{/strip}">
                    </div>


                    {elseif $field.Presentation == 'Multiselect'}
                        <label for="{$cid}-{$field.Name}">{$field.Caption}:</label>
                        <div class="formulti">
                            <select multiple id="{$cid}-{$field.Name}" class="select js-chosen"
                                    data-placeholder="{$field.Description || ""}"
                                    meta-relation-method="{$field.relation.method}"
                                    meta-relation-field="{$field.relation.field}"
                                    meta-relation-link="{$field.relation.link}"
                                    meta-relation-link-value="{$data[$field.relation.link]}">
                                {if $field.DataSet}
                                    {foreach $field.DataSet as $item}
                                        {$selected = false}
                                        {foreach $meta.DataSet as $data_item}

                                            {if $data_item[$field.relation.field] == $item[$field.relation.field]}
                                                {$selected = $data_item.Id}
                                            {/if}
                                        {/foreach}
                                        <option {if $selected} data-id="{$selected}"selected="selected" {/if}
                                                value="{$item.Id}">{$item._Caption}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </div>
                    {elseif $field.Presentation == 'Boolean'}
                        <label for="{$cid}-{$field.Name}" class="checkbox js-checkbox-block">
                            <input type="checkbox" class="js-checkbox checkbox"
                                   id="{$cid}-{$field.Name}"{if $data[$field.Name] || $field.DefaultValue} checked="checked"{/if}>
                            <input type="hidden" name="{$field.Name}"
                                   value="{if $data[$field.Name]}1{else}{if $field.DefaultValue}{$field.DefaultValue}{else}0{/if}{/if}">
                            {$field.Caption}
                        </label>
                    {elseif $field.Presentation == 'TextBox'}
                        <label for="{$cid}-{$field.Name}" class="field">{$field.Caption}:{if $field.IsMandatory}
                            <sup class="required_field">*</sup>{/if}</label>
                        <div class="forinput">
                            <textarea type="text" name="{$field.Name}" class="field" id="{$cid}-{$field.Name}"
                                      placeholder="{$field.Description || ""}">{$data[$field.Name] || $field.DefaultValue || ""}</textarea>
                        </div>
                    {elseif $field.Presentation == 'Text' || $field.Presentation == 'String'}
                        <label for="{$cid}-{$field.Name}" class="field">{$field.Caption}:{if $field.IsMandatory}
                            <sup class="required_field">*</sup>{/if}</label>
                        <div class="forinput">
                            <input type="text" name="{$field.Name}" class="field" id="{$cid}-{$field.Name}"
                                   {if $field.Width}style="width:{$field.Width}px;"{/if}
                                   value="{$data[$field.Name] || $field.DefaultValue || ""}"
                                   placeholder="{$field.Description || ""}">
                        </div>
                    {elseif $field.Presentation == 'Image'}
                        <label class="field">{$field.Caption}</label>
                        <div class="image-preview js-image-preview">
                            <img src="/app/img/empty.png" class="js-preview-{$field.Name}" alt="{$field.Description || ""}" title="{$field.Description || ""}" style="width:300px; height:300px;"><br>
                            <a href="#" class="js-upload-file"  data-name="{$field.Name}">Загрузить файл</a> | <a href="#" class="js-delete-file"  data-name="{$field.Name}">Очистить изображение</a>
                        <div class="hide"><input type="file" id="{$field.Name}" value="{$data[$field.Name] || ''}" data-id="{$field.Name}"></div>
                        <input type="hidden" name="{$field.Name}" value="{$data[$field.Name]}">
                        </div>
                    {elseif $field.Presentation == 'Longitude'}
                    <label for="{$cid}-{$field.Name}" class="field">{$field.Caption}:{if $field.IsMandatory}
                    <sup class="required_field">*</sup>{/if}</label>
                    <div class="forinput">
                    <div class="map-mini js-field-map" id="{$cid}-{$field.Name}-map" data-field="{$field.Name}">
                    </div>

                    <input type="hidden" name="{$field.Name}" class="field" id="{$cid}-{$field.Name}"
                    {if $field.Width}style="width:{$field.Width}px;"{/if}
                    value="{$data[$field.Name] || $field.DefaultValue || ""}"
                    placeholder="{$field.Description || ""}">
                        <input type="hidden" name="{$field.Latitude}" class="field" id="{$cid}-{$field.Latitude}"
                               {if $field.Width}style="width:{$field.Width}px;"{/if}
                               value="{$data[$field.Latitude] || $field.DefaultValue || ""}"
                               placeholder="{$field.Description || ""}">

                    </div>
                    {elseif $field.Presentation == 'Latitude'}

                    {else}
                        <label for="{$cid}-{$field.Name}" class="field">{$field.Caption}:{if $field.IsMandatory}
                            <sup class="required_field">*</sup>{/if}</label>
                        <div class="forinput">
                            <input type="text" name="{$field.Name}" class="field" id="{$cid}-{$field.Name}"
                                   {if $field.Width}style="width:{$field.Width}px;"{/if}
                                   value="{$data[$field.Name] || $field.DefaultValue || ""}"
                                   placeholder="{$field.Description || ""}">
                        </div>
                    {/if}
                </div>
            {/if}
        {else}
             <input type="hidden" name="{$field.Name}" value="{$data[$field.Name]}">
        {/if}
    {/foreach}
<div class="buttons label split">
<button type="submit" class="button success js-submit">
<i class="icon icon-save"></i> Сохранить изменения</button>
</div>
</form>