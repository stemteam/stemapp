{foreach $rows as $row}
<tr id="software_{$row.GUID}">
	<td class="rowselect"><input type="checkbox" class="checkbox" value="1"></td>
    <td class="counter"><small>{$row@index + 1}</small></td>
	<td class="dealer"style="white-space: nowrap">{$row.DILLERNAME}</td>
	<td class="alias icon"style="white-space: nowrap"><a href="#!/objects/{$row.DBNAME}/{$row.GUID}">{$row.ABONENTNAME}</a></td>
	<td class="version"style="white-space: nowrap">{$row.VERSION}</td>
	<td class="date" style="white-space: nowrap">{$row.CREATEDATE|date_format:"%d.%m.%Y %H:%M:%S"}</td>
	<td class="info" style="white-space: nowrap">{$row.INFO|regex_replace:"/\/n/":""}</td>
</tr>
{/foreach}