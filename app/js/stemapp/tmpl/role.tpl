<div class="mediform">
    <h1>Управление ролями пользователя</h1>
    <form class="generic-new" action="#">

        <div class="user field label">
            <label for="{{id}}-user">Телефон/email:</label>
            <div class="forinput">
                <input type="text" name="user" class="js-user field required" id="id-user" value="" placeholder="79123456789/user@company.com" />
            </div>
        </div>

        <div class="user field label">
            <label for="{{id}}-user">Роль:</label>
            <div class="forinput">
                <select class="js-role" id="id-role" name="role">
                    {foreach $roles as $role}
                    <option value="{$role.name}">{$role.description}</option>
                    {/foreach}
                </select>
            </div>
        </div>

        <div class="buttons submit label">
            <button type="submit" class="default button js-submit success" id="{{id}}-uc-submit"><i class="icon icon-ok"></i>Добавить роль</button>
            <button type="button" class="default button js-downShift success" id="{{id}}-downShift"><i class="icon icon-cancel"></i>Удалить роль</button>
        </div>
    </form>
</div>

