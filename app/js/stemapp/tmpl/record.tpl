<div class="generic-new js-columns">
    <h1>{$caption}</h1>
    <div class="submit js-submit-block">
        <button type="submit" class="js-submit button-up button success"><i class="icon icon-save"></i> Сохранить изменения</button>
    </div>
    <div>{$auto}</div>
    <div class="downpanel tar js-submit-block">
        <button type="submit" class="js-submit button success"><i class="icon icon-save"></i> Сохранить изменения</button>
    </div>
</div>