define([
    'jquery'
], function ($) {


    return {
        init: function () {

            ymaps.ready(function () {

                var mainMarker = ymaps.templateLayoutFactory.createClass('<div class="map-marker"><div class="icon"></div><div class="shadow"></div></div>'),
                    emptyClinicMarker = ymaps.templateLayoutFactory.createClass('<div class="map-marker opacity-marker"><div class="icon"></div><div class="shadow"></div></div>'),
                    floatingMarker = ymaps.templateLayoutFactory.createClass('<div class="map-marker floating-marker animate-floating-marker"><div class="icon"></div><div class="shadow"></div></div>'),
                    meMarker = ymaps.templateLayoutFactory.createClass('<div class="map-marker"><div class="icon icon-me"></div><div class="shadow"></div></div>'),
                    mainBaloon = ymaps.templateLayoutFactory.createClass(
                        '<div class="balloon-marker top"><div class="ymaps-2-1-15-balloon__close-button close-button"></div><div class="balloon-arrow"></div><div class="balloon-inner">$[[options.contentLayout observeSize minWidth=310 maxWidth=310 maxHeight=350]]</div></div>', {

                            build: function () {
                                this.constructor.superclass.build.call(this);

                                this._$element = $('.balloon-marker', this.getParentElement());

                                this.applyElementOffset();

                                this._$element.find('.close-button')
                                    .on('click', $.proxy(this.onCloseClick, this));
                                this.map = this.getData().geoObject.getMap();
                                var me = this;
                                this.map.events.add('boundschange', function () {
                                    me.applyElementOffset();
                                });
                            },

                            applyElementOffset: function () {

                                this.height = this._$element.height();
                                this.width = this._$element.width();

                                var map = this.getData().geoObject.getMap();
                                var markerPosition = map.converter.globalToPage(map.options.get('projection').toGlobalPixels(this.getData().geometry.getCoordinates(), map.getZoom()));
                                var el_map = $(map.container.getElement()),
                                    map_offset = el_map.offset();
                                markerPosition[0] -= map_offset.left;
                                markerPosition[1] -= map_offset.top;

                                var top, left;

                                var popup_width = this._$element[0].offsetWidth;

                                if (markerPosition[0] < popup_width / 2) {
                                    this._$element.find('.balloon-arrow').css({left: 10, right: 'auto'});
                                    left = -10;
                                } else if (el_map.width() - markerPosition[0] < this._$element[0].offsetWidth / 2) {
                                    this._$element.find('.balloon-arrow').css({right: 0, left: 'auto'});
                                    left = -popup_width + 10;
                                }
                                else {
                                    this._$element.find('.balloon-arrow').css({left: '50%', right: 'auto'});
                                    left = -(this._$element[0].offsetWidth / 2);
                                }


                                if (markerPosition[1] < this._$element.height()) {
                                    this._$element.removeClass('top').addClass('bottom');
                                    top = this._$element.find('.balloon-arrow').height();
                                }
                                else {
                                    this._$element.removeClass('bottom').addClass('top');
                                    top = -(this._$element[0].offsetHeight + this._$element.find('.balloon-arrow').height());
                                }
                                this._$element.css({
                                    left: left,
                                    top: top
                                });
                                this._$element.find('.balloon-inner>ymaps').css('width', 'auto');
                            },

                            onCloseClick: function (e) {
                                e.preventDefault();
                                this.events.fire('userclose');
                            },

                            onSublayoutSizeChange: function () {
                                mainBaloon.superclass.onSublayoutSizeChange.apply(this, arguments);

                                if (!this._isElement(this._$element)) {
                                    return;
                                }

                                this.applyElementOffset();

                                this.events.fire('shapechange');
                            },

                            getShape: function () {

                                return null;
                                if (!this._isElement(this._$element)) {
                                    return mainBaloon.superclass.getShape.call(this);
                                }

                                var position = this._$element.position();

                                return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
                                    [position.left, position.top], [
                                        position.left + this._$element[0].offsetWidth,
                                        position.top + this._$element[0].offsetHeight + this._$element.find('.balloon-arrow')[0].offsetHeight
                                    ]
                                ]));
                            },

                            _isElement: function (element) {
                                return element && element[0] && element.find('.balloon-arrow')[0];
                            }
                        }
                    );

                ymaps.option.presetStorage.add('app#default', {
                    iconLayout: mainMarker,
                    // Своё изображение иконки метки.
                    iconShape: {
                        type: 'Rectangle',
                        // Прямоугольник описывается в виде двух точек - верхней левой и нижней правой.
                        coordinates: [
                            [-12, -41],
                            [12, 0]
                        ]
                    },
                    balloonLayout: mainBaloon,
                    openEmptyBalloon: false,
                    hideIconOnBalloonOpen: true
                });

                ymaps.option.presetStorage.add('app#withouBaloon', {
                    iconLayout: mainMarker,
                    // Своё изображение иконки метки.
                    iconShape: {
                        type: 'Rectangle',
                        // Прямоугольник описывается в виде двух точек - верхней левой и нижней правой.
                        coordinates: [
                            [-12, -41],
                            [12, 0]
                        ]
                    },
                    openEmptyBalloon: false,
                    hideIconOnBalloonOpen: true
                });


                ymaps.option.presetStorage.add('app#me', {
                    iconLayout: meMarker,
                    // Своё изображение иконки метки.
                    iconShape: {
                        type: 'Rectangle',
                        // Прямоугольник описывается в виде двух точек - верхней левой и нижней правой.
                        coordinates: [
                            [-12, -41],
                            [12, 0]
                        ]
                    },
                    openEmptyBalloon: false,
                    hideIconOnBalloonOpen: true
                });


                ymaps.option.presetStorage.add('app#emptyClinic', {
                    iconLayout: emptyClinicMarker,
                    // Своё изображение иконки метки.
                    iconShape: {
                        type: 'Rectangle',
                        // Прямоугольник описывается в виде двух точек - верхней левой и нижней правой.
                        coordinates: [
                            [-12, -41],
                            [12, 0]
                        ]
                    },
                    openEmptyBalloon: false,
                    hideIconOnBalloonOpen: true
                });


                ymaps.option.presetStorage.add('app#floating', {
                    iconLayout: floatingMarker,
                    // Своё изображение иконки метки.
                    iconShape: {
                        type: 'Rectangle',
                        // Прямоугольник описывается в виде двух точек - верхней левой и нижней правой.
                        coordinates: [
                            [-12, -41],
                            [12, 0]
                        ]
                    },
                    balloonLayout: mainBaloon,
                    openEmptyBalloon: false,
                    hideIconOnBalloonOpen: true
                });
            });
        }
    };


});