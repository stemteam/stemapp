define([], function () {
    'use strict';

    Stemapp.TreeNode = function (data) {
        this.data = data;
        this.parent = null;
        this.children = [];
    };

    Stemapp.TreeNode.prototype.sortRecursive = function (f, field, sortAsc) {
        this.children.sort(function (a, b) {
            return f(a, b, field, sortAsc);
        });

        for (var i = 0, l = this.children.length; i < l; i++) {
            this.children[i].sortRecursive(f, field, sortAsc);
        }
        return this;
    };

    Stemapp.TreeNode.prototype.getArray = function () {
        var items = [];
        for (var i = 0, l = this.children.length; i < l; i++) {
            var child = this.children[i];
            items.push(child.data)
            items = items.concat(child.getArray.apply(child, arguments));
        }
        return items;
    };

    //toTree
    return function (data, f, field, sortAsc) {
        var nodeById = {}, i = 0, l = data.length, node;
        nodeById[0] = new Stemapp.TreeNode(); // that's the root node

        for (i = 0; i < l; i++) {  // make TreeNode objects for each item
            nodeById[data[i].id] = new Stemapp.TreeNode(data[i]);
        }
        for (i = 0; i < l; i++) {  // link all TreeNode objects
            node = nodeById[data[i].id];

            node.parent = nodeById[node.data.parent];
            if (!node.parent) {
                node.parent = nodeById[0];
            }
            node.parent.children.push(node);
        }
        return nodeById[0].sortRecursive(f, field, sortAsc);
    }
});