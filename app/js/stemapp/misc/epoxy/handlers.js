define([
    'backbone',
    'epoxy'
], function (Backbone) {
    'use strict';
    Backbone.Epoxy.binding.addHandler("valueEvent", {
        set: function ($element, value) {
            // Set data into the bound element...
            $element.val(value);
            $element.change();
        },
        get: function( $element, value, event ) {
            return $element.val();
        }
    });


    Backbone.Epoxy.binding.addHandler("nieEvent", {
        set: function ($element, value) {
            // Set data into the bound element...
            $element.val(value);
            $element.change();
        },
        get: function( $element, value, event ) {
            return $element.data('lastVal');
        }
    });

});