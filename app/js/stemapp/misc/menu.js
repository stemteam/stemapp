define([
    'underscore'
], function () {
    'use strict';


    Stemapp.menu = {

        servers: [],

        items: [

            {name: 'role', href: '#', icon: 'user', caption: 'Роли', cls: "menu-role", validate: true},
            {name: 'client', href: '#', icon: 'abonent-type2', caption: 'Клиенты', cls: "menu-client", validate: true},
            {
                name: 'history',
                href: '#/history',
                icon: 'history',
                caption: 'История изменений',
                cls: "menu-history",
                validate: true
            },
            {
                name: 'password',
                href: '#',
                icon: 'password',
                caption: 'Смена пароля',
                validate: false,
                cls: "menu-change-pass"
            },
            {
                name: 'support',
                href: '#',
                icon: 'support',
                caption: 'Служба поддержки',
                cls: "menu-support",
                validate: true
            },
            {name: 'exit', href: '#', icon: 'door', caption: 'Выйти', cls: "menu-exit", validate: false, is_hide_startpage: true}
        ],

        checkMenuItem: function (item) {

            var allow = true;
            if (item.services) {
                allow = false;
                for (var i in  Stemapp.userServices) {
                    if (Stemapp.userServices.hasOwnProperty(i)) {
                        allow = allow || item.services.indexOf(Stemapp.userServices[i].Code) != -1;
                    }
                }
            }
            if (_.isFunction(item.validation)) {
                return item.validation();
            } else {
                return (!Boolean(item.validate) || typeof Stemapp.config.menu[item.name] == 'undefined' || Boolean(Stemapp.config.menu[item.name])) && allow;
            }
        },

        loadModuleMenu: function () {

            for (var moduleName in Stemapp.modules) {

                if (Stemapp.modules.hasOwnProperty(moduleName) && Stemapp.modules[moduleName].menu) {

                    Stemapp.modules[moduleName].menu.reverse();
                    Stemapp.modules[moduleName].menu.forEach(function (item) {
                        this.items.unshift(item);
                    }, this);
                    if (Stemapp.modules[moduleName].menuServers) {
                        Stemapp.modules[moduleName].menuServers.reverse();
                        Stemapp.modules[moduleName].menuServers.forEach(function (item) {
                            this.servers.unshift(item);
                        }, this);

                    }
                }
            }
        },

        create: function (servers) {

            this.loadModuleMenu();

            var caption, href, res = [], i, j;

            if (servers) {

                //Если более одного сервера, добавляем в каждый пункт меню префиксом имя сервера
                if (servers.length > 1) {
                    for (j = 0; j < Stemapp.menu.servers.length; j++) {
                        if (Stemapp.menu.servers[j].multipleServersPrefix) {
                            Stemapp.menu.servers[j].caption = Stemapp.menu.servers[j].multipleServersPrefix + ' ' + Stemapp.menu.servers[j].caption;
                        }
                    }
                }

                for (i = 0; i < servers.length; i++) {

                    for (j = 0; j < Stemapp.menu.servers.length; j++) {
                        if (!this.checkMenuItem(Stemapp.menu.servers[j]))
                            continue;

                        if (Stemapp.menu.servers[j].validate && Stemapp.menu.servers[j].name && !Stemapp.sites.checkMenu(Stemapp.menu.servers[j].name)) {
                            continue;
                        }
                        caption = $.render(Stemapp.menu.servers[j].caption, {DBNAME: servers[i].DBNAME});

                        if (Stemapp.menu.servers[j].href) {
                            // Это ссылка
                            href = $.render(Stemapp.menu.servers[j].href, {DB: servers[i].DB});

                            res.push({
                                href: href,
                                cls: Stemapp.menu.servers[j].cls,
                                db: servers[i].DB,
                                icon: Stemapp.menu.servers[j].icon,
                                caption: caption,
                                is_hide_startpage: Stemapp.menu.servers[j].is_hide_startpage
                            });
                        }
                        else {
                            // Это текст

                            res.push({
                                caption: caption,
                                cls: "server",
                                icon: Stemapp.menu.servers[j].icon,
                                is_hide_startpage: Stemapp.menu.servers[j].is_hide_startpage
                            });
                        }
                    }
                }
            }


            for (i = 0; i < Stemapp.menu.items.length; i++) {

                if (!this.checkMenuItem(Stemapp.menu.items[i]))
                    continue;

                caption = $.render(Stemapp.menu.items[i].caption, {});

                if (Stemapp.menu.items[i].href) {
                    // Это ссылка

                    href = $.render(Stemapp.menu.items[i].href, {me: Stemapp.me});

                    res.push({
                        href: href,
                        cls: Stemapp.menu.items[i].cls,
                        target: Stemapp.menu.items[i].target,
                        id: Stemapp.menu.items[i].id,
                        auto: Stemapp.menu.items[i].auto,
                        icon: Stemapp.menu.items[i].icon,
                        caption: caption,
                        children: Stemapp.menu.items[i].children,
                        is_hide_startpage: Stemapp.menu.items[i].is_hide_startpage
                    });


                } else {
                    // Это текст

                    res.push({
                        cls: Stemapp.menu.items[i].cls,
                        target: Stemapp.menu.items[i].target,
                        id: Stemapp.menu.items[i].id,
                        auto: Stemapp.menu.items[i].auto,
                        icon: Stemapp.menu.items[i].icon,
                        href: Stemapp.menu.items[i].href,
                        caption: caption,
                        children: Stemapp.menu.items[i].children,
                        is_hide_startpage: Stemapp.menu.items[i].is_hide_startpage
                    });
                }
            }
            return res;
        },

        findMenu: function (id) {
            var res = [];
            for (var i in Stemapp.UserMenu) {
                if (Stemapp.UserMenu.hasOwnProperty(i) && Stemapp.UserMenu[i].MenuParentId == id) {

                    var childs = this.findMenu(Stemapp.UserMenu[i].MenuId);
                    if (childs.length) {
                        res.push({
                            cls: "",
                            caption: Stemapp.UserMenu[i].MenuName,
                            id: Stemapp.UserMenu[i].MenuId,
                            icon: Stemapp.UserMenu[i].Icon,
                            auto: true,
                            children: childs,
                            level: Stemapp.UserMenu[i].MenuClevel

                        });
                        //res = res.concat(childs);
                    } else {
                        res.push({
                            cls: "js-usermenu",
                            auto: true,
                            form: Stemapp.UserMenu[i].FormName,
                            icon: Stemapp.UserMenu[i].Icon,
                            caption: Stemapp.UserMenu[i].MenuName,
                            id: Stemapp.UserMenu[i].MenuId,
                            children: false
                        });
                    }
                }
            }
            return res;
        }
    };


    /**
     * Новый класс работы с меню. Изначально будет служить преобразователем нового формата в старый. Хорошо было бы после перевести
     * все проекты на новое меню, а после добавить рендер меню.
     *
     * todo если появится необходимость использовать идентичные пункты меню со сложной логикой сразу в нескольких метсах можно будет добавить именнованные пресеты
     * Пример:
     * Stemapp.Menus.preset('password', {view: 'password'});
     * Stemapp.Menus.add('Изменить пароль', {preset:'password'});
     *
     * @type {{}}
     */
    Stemapp.Menus = {

        items: [],

        ord: 0,

        id: 0,

        levels: [], // Массив вложенных объектов

        hashmap: {},

        /**
         * Add menu to current level
         * @param caption
         * @param options
         * @returns {Stemapp.Menus}
         */
        add: function (caption, options) {

            var opts = {
                icon: ''
            };
            opts.ord = this.ord++;
            opts = _.extend(opts, options);
            var hash = JSON.stringify({caption: caption, options: opts});
            var item = {caption: caption, options: opts, id: this.id++};
            if (this.hashmap[hash]) {
                return this;
            }
            this.hashmap[hash] = item;
            var parent = this.getCurrentParent();
            parent.items.push(item);
            this.lastItem = item;
            return this;
        },

        /**
         * Create new parent level. After executing this function you can add children into last added menu
         *
         * Stemapp.Menus
         *          .add('Parent')
         *          .children()
         *          .add('Child 1')
         *          .add('Child 2')
         *          .end();
         */
        children: function () {
            var item = this.getLastItem();
            if (item) {
                this.levels.push(item);
                item.items = [];
            } else {
                console.error("You don't add any menu");
            }
            return this;
        },

        /**
         * Return previous level
         */
        end: function () {
            if (this.levels.length) {
                this.levels.pop();
            } else {
                console.error("Variable levels is empty");
            }
            return this;
        },

        /**
         * Return last added item
         * @returns {*}
         */
        getLastItem: function () {
            return this.lastItem;
        },

        /**
         * Return current parent
         * @returns {*}
         */
        getCurrentParent: function () {
            var parent;
            if (this.levels.length) {
                parent = this.levels[this.levels.length - 1];
            } else {
                parent = this;
            }
            return parent;
        },

        /**
         * Reset all data
         */
        reset: function () {
            this.items = [];
            this.ord = 0;
            this.levels = [];
            this.lastItem = null;
            return this;
        },

        convertToOldFormat: function (items) {
            items = items ? items : this.items;
            var result;
            var me = this;

            result = items.map(function (value, index, array) {
                return {
                    name: 'menu-' + value.id,
                    href: '#',
                    icon: value.options.icon,
                    caption: value.caption,
                    cls: 'js-menu-' + value.id,
                    children: value.items ? me.convertToOldFormat(value.items) : null,
                    validation: function () {
                        if (!value.options.role) {
                            return true;
                        }
                        return !!Stemapp.roles.find(function (element) {
                            if (element.RoleName == value.options.role) {
                                return true;
                            }
                        });
                    },
                    callback: function (e, View) {
                        if (value.options.view) {
                            View.openTab(value.options.view);
                        }
                    }
                };
            });

            return result;
        }

    };

});