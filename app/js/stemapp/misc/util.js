define([
    'jquery',
    'crypto',
    '_misc/md5'
], function ($, CryptoJS) {
    'use strict';

    if (!Stemapp.util) {
        Stemapp.util = {};
    }

    Stemapp.util = $.extend(Stemapp.util, {

        paramToLink: function (data) {

            var res = [];
            for (var key in data) {
                res.push(key + '=' + encodeURIComponent(data[key]));
            }
            return res.join('&');
        },

        wrapSpan: function (value) {

            return '<span>' + value + '</span>';
        },

        timestampToDate: function (val, utc, miliseconds) {

            utc = !!utc;
            var date;
            if (miliseconds) {
                date = new Date(val);
            } else {
                date = new Date(val * 1000);
            }
            return ('0' + (utc ? date.getUTCDate() : date.getDate())).slice(-2) + '.' +
                ('0' + ((utc ? date.getUTCMonth() : date.getMonth()) + 1)).slice(-2) + '.' +
                (utc ? date.getUTCFullYear() : date.getFullYear());
        },

        timestampToTime: function (val, utc, miliseconds) {

            utc = !!utc;
            var date;
            if (miliseconds) {
                date = new Date(val);
            } else {
                date = new Date(val * 1000);
            }

            return ('0' + (utc ? date.getUTCHours() : date.getHours())).slice(-2) + ':' +
                ('0' + (utc ? date.getUTCMinutes() : date.getMinutes())).slice(-2) + ':' +
                ('0' + (utc ? date.getUTCSeconds() : date.getSeconds())).slice(-2);
        },

        timestampToDateTime: function (val, utc, miliseconds) {

            utc = !!utc;
            return Stemapp.util.timestampToDate(val, utc, miliseconds) + ' ' + Stemapp.util.timestampToTime(val, utc, miliseconds);
        },

        ToUpperFirstLetter: function (str) {

            return str.replace(/(^)([a-zа-я])/ig, function (m, p1, p2) {
                return p1 + p2.toUpperCase();
            });
        },

        convert: {

            dateToJaxisTime: function (date, utc) {

                utc = utc ? true : false;
                var res = [
                    ('0' + (utc ? date.getUTCHours() : date.getHours())).slice(-2),
                    ('0' + (utc ? date.getUTCMinutes() : date.getMinutes())).slice(-2),
                    ('0' + (utc ? date.getUTCSeconds() : date.getSeconds())).slice(-2)
                ].join('');
                if (date.getMilliseconds() != 0) {
                    res += '.' + ('000' + (utc ? date.getUTCMilliseconds() : date.getMilliseconds())).slice(-3);
                }
                return res;
            },
            dateToJaxisDate: function (date, utc) {

                utc = utc ? true : false;
                return ((utc ? date.getUTCFullYear() : date.getFullYear())) +
                    ('0' + ((utc ? date.getUTCMonth() : date.getMonth()) + 1)).slice(-2) +
                    ('0' + (utc ? date.getUTCDate() : date.getDate())).slice(-2);
            },

            dateToJaxisShortDate: function (date, utc) {
                utc = utc ? true : false;
                return ((utc ? (date.getUTCFullYear()).toString().slice(2) : (date.getFullYear()).toString().slice(2))) +
                    ('0' + ((utc ? date.getUTCMonth() : date.getMonth()) + 1)).slice(-2) +
                    ('0' + (utc ? date.getUTCDate() : date.getDate())).slice(-2);
            },

            dateToJaxisDatetime: function (date, utc) {

                utc = utc ? true : false;
                return Stemapp.util.convert.dateToJaxisDate(date, utc) + Stemapp.util.convert.dateToJaxisTime(date, utc);
            },

            dateToJaxisShortDatetime: function (date, utc) {

                utc = utc ? true : false;
                return Stemapp.util.convert.dateToJaxisShortDate(date, utc) + Stemapp.util.convert.dateToJaxisTime(date, utc);
            },

            stringToJaxisDatetime: function (value) {

                return value.substring(0, 10).split('.').reverse().join('').slice(2) + value.substring(11).split(':').join('');
            },

            jaxisDatetimeToDate: function (value) {


                var date = (new Date((parseInt(value[0] + value[1], 10) > 50 ? '19' : '20') + value[0] + value[1], parseInt(value[2] + value[3], 10) - 1, value[4] + value[5], value[6] + value[7], value[8] + value[9], value[10] + value[11]));

                // Есть милисекунды
                if (value.length > 12 && value.indexOf('.') != -1) {

                    date.setMilliseconds(value.substring(12));
                }

                return date;
            },

            jaxisFullDatetimeToDate: function (value) {
                return Stemapp.util.convert.jaxisDatetimeToDate(value.slice(2, value.length))
            },

            timestampToJaxisFullDateTime: function (value) {
                //return (new Date(value).getFullYear()).toString().slice(0,2) + Stemapp.util.convert.dateToJaxisDatetime(new Date(value));
                return Stemapp.util.convert.dateToJaxisDatetime(new Date(value));
            },

            strToProperty: function (data) {

                var properties,
                    style,
                    config = {
                        stroke: true,
                        color: '#0088ff',
                        weight: 1,
                        opacity: .9,
                        borderStyle: 'solid',
                        fill: true,
                        fillColor: '#0088ff',
                        fillOpacity: .6
                    };

                if (!(data && data.length)) {
                    return config;
                }

                try {
                    properties = $.parseJSON(data);
                } catch (e) {
                    return;
                }

                if (properties && properties.hasOwnProperty('ver')) {
                    //noinspection JSUnresolvedVariable
                    style = properties.style;
                    switch (properties.ver) {
                        case '001':
                            config.stroke = parseBoolean(style[0]);
                            config.color = parseColor(style[1]);
                            config.weight = parseNumber(style[2], 1);
                            config.opacity = parseNumber(style[3]) / 255;
                            config.borderStyle = style[4] || 'Solid';
                            config.fill = parseBoolean(style[5]);
                            config.fillColor = parseColor(style[6]);
                            config.fillOpacity = parseNumber(style[7]) / 255 * 1.5;
                            break;
                        default:
                            return;
                    }
                }

                function parseBoolean(bool) {
                    return (bool === 'True');
                }

                function parseColor(color) {
                    if (color.match(/^[A-Fa-f\d]{8}$/)) {
                        return '#' + color.slice(2);
                    }
                    return Stemapp.data.Color.getColor(color); // todo
                }

                function parseNumber(num, def) {
                    var numeric = +num;
                    return (!numeric && def) ? +def : numeric;
                }

                return config;
            },

            detailInfoToGridData: function (data, fid, model) {

                var res = [],
                    parent;
                for (var i = 0; i < model.captions.length; i++) {

                    if (model.captions[i].inDetail) {

                        res[res.length] = {
                            name: model.captions[i].caption,
                            parent: 0,
                            indent: 0,
                            id: fid + '-' + (res.length + 1)
                        };
                        parent = fid + '-' + res.length;
                        for (var j = 0; j < model.columns.length; j++) {

                            if (model.columns[j].inDetail && model.columns[j].parent == i) {

                                res[res.length] = {
                                    name: model.columns[j].caption,
                                    val: (typeof data[model.columns[j].name] == 'undefined' ? '' : model.columns[j].format(j, 1, data[model.columns[j].name], null, data)),
                                    parent: parent,
                                    indent: 1,
                                    id: fid + '-' + (res.length + 1)
                                };
                            }
                        }
                    }
                }
                return res;
            },

            objectInfoToGridData: function (data, fid) {
                return this.detailInfoToGridData(data, fid, Stemapp.data.monObject);
            },


            responseClientsToGridData: function (abonents, groups, objects, fid, objectData, groupRelative, objectRelative, params) {

                var res = [],
                    isLeaf = true,
                    key = 0,
                    id = '';

                var diffLvl = abonents[0].clevel;
                for (var i = 0; i < abonents.length; i++) {

                    key = res.length;
                    id = fid + '_id_abonent_' + abonents[i].id_abonent;
                    res[key] = {description: abonents[i].alias, id_abonent: abonents[i].id_abonent, parent: 0, id: id};


                    for (var j = 0; j < groups.length; j++) {

                        if (groups[j][groupRelative.parent.value] == abonents[i].id_abonent) {
                            addGroup(j, diffLvl + 1, id, key);
                            isLeaf = false;
                        }
                    }
                    res[key]._folder = !isLeaf;
                    res[key]._isOpen = !isLeaf;
                    res[key].expanded = !isLeaf;
                    res[key].isLeaf = false;
                    res[key].checkbox = 1;
                    res[key].indent = 0;
                }

                function addGroup(j, lvl, parent, parentKey) {

                    var key = res.length,
                        isLeaf = true,
                        id = fid + '_' + groupRelative.id.value + '_' + groups[j][groupRelative.id.value];

                    res[key] = {parent: parent, parentKey: parentKey, id: id};

                    for (var o in groupRelative) {

                        if (groupRelative[o].copy)
                            res[key][o] = groups[j][groupRelative[o].value];
                    }
                    o = null;

                    for (var k = 0; k < groups.length; k++) {
                        if (groups[k][groupRelative.parent.value] == groups[j][groupRelative.id.value]) {
                            addGroup(k, lvl + 1, id, key);
                            isLeaf = false;
                        }
                    }
                    for (k = 0; k < objects.length; k++) {

                        if (objects[k][objectRelative.parent.value] == groups[j][groupRelative.id.value]) {
                            addObject(objects[k], lvl + 1, id, key);
                            isLeaf = false;
                        }
                    }
                    k = null;

                    res[key]._folder = !isLeaf;
                    res[key]._isOpen = !isLeaf;
                    res[key].expanded = !isLeaf;
                    res[key].isLeaf = false;
                    res[key].checkbox = 1;
                    res[key].indent = lvl - diffLvl;

                    key = isLeaf = id = j = lvl = parent = parentKey = null;
                }

                function addObject(object, lvl, parent, parentKey) {

                    var key = res.length,
                        o;

                    res[key] = {};

                    // Модифицируем данные, добавляем единицы измерения и проверяем валидность

                    for (o in objectRelative) {

                        if (objectRelative[o].copy)
                            res[key][o] = object[objectRelative[o].value];
                    }

                    for (o in  object) {
                        res[key][o] = object[o];//objectData.getColumnAtName(o).format(object[o], object);
                    }

                    res[key].parent = parent;
                    res[key].isLeaf = true;
                    res[key].expanded = false;
                    res[key][objectRelative.type.value] = true;
                    res[key].id = fid + '_' + objectRelative.id.value + '_' + object[objectRelative.id.value];
                    res[key].checkbox = 1;
                    res[key].indent = lvl - diffLvl;
                    res[key].parentKey = parentKey;

                    o = key = object = lvl = parent = parentKey = null;
                }

                abonents = groups = objects = fid = objectData = groupRelative = objectRelative = params = isLeaf = key = id = null;
                return res;
            },

            responseChatrboxToGridData: function (abonents, groups, objects, fid) {
                return this.responseClientsToGridData(abonents, groups, objects, fid, Stemapp.data.clients,
                    // Groups data
                    {
                        //copy - копируем ли в конечные данные
                        description: {value: 'alias', copy: true},
                        id_abonent: {value: 'id_abonent', copy: true},
                        parent: {value: 'id_parent', copy: false},
                        id: {value: 'id_abonent', copy: false},
                        abonent: {value: 'id_parent', copy: false}

                    },
                    // Object data
                    {
                        description: {value: 'alias', copy: true},
                        parent: {value: 'id_parent', copy: false},
                        id: {value: 'id_abonent', copy: false},
                        type: {value: '_object', copy: false}

                    },
                    {nullParent: 0}
                );

            }


        },

        format: {

            html: function (value) {
                return value ? value : '';
            },

            velocity: function (value) {
                return (value ? value : 0) + ' км/ч';
            },


            degree: function (value) {
                return (value ? value : 0) + ' °';
            },

            date: function (value, miliseconds) {
                return Stemapp.util.timestampToDate(value, false, miliseconds);
            },

            time: function (value, miliseconds) {
                return Stemapp.util.timestampToTime(value, false, miliseconds);
            },

            datetime: function (value, miliseconds) {
                return Stemapp.util.timestampToDate(value, false, miliseconds) + ' ' + Stemapp.util.timestampToTime(value, false, miliseconds);
            },

            dateUTC: function (value, miliseconds) {
                return Stemapp.util.timestampToDate(value, true, miliseconds);
            },

            timeUTC: function (value, miliseconds) {
                return Stemapp.util.timestampToTime(value, true, miliseconds);
            },

            datetimeUTC: function (value, miliseconds) {
                return Stemapp.util.timestampToDate(value, true, miliseconds) + ' ' + Stemapp.util.timestampToTime(value, true, miliseconds);
            },

            geo: function (value) {

                var g = ('0' + Math.floor(value)).slice(-2);
                value = (value - g) * 60;
                var m = ('0' + Math.floor(value)).slice(-2);
                var s = (('0' + ((value - m) * 60).toFixed(2)).slice(-5)).replace('.', ',');
                return g + '° ' + m + "' " + s + "''";
            },

            length: function (value) {


                if (value > 1000) {
                    return Math.round(value / 100) / 10 + ' км';
                } else {
                    return (value ? value : 0) + ' м';
                }
            },

            volume: function (value) {
                return (value ? value : 0) + ' л';
            },

            boolean: function (value) {
//                return value ? '<i class="ic icon-on"></i>' : '<i class="ic icon-off"></i>';

                return value ? '<input type="checkbox" readonly="readonly" disabled="disabled" checked="checked" class="margin0"> ' : '<input type="checkbox"  class="margin0" readonly disabled>';
            },

            temperature: function (value) {
                return (value ? value : 0) + ' °C';
            },

            quantity: function (value) {
                return (value ? value : 0);
            },

            voltage: function (value) {
                return (value ? value : 0) + ' В';
            },

            custom: function (value) {
                return value;
            },

            diffTime: function (value) {


                var arr = [60, 60, 24],
                    desc = ['с', 'м', 'ч', 'д'],
                    res = '',
                    step = 0;

                while (value / arr[step] > 1) {

                    res = (value % arr[step]) + desc[step] + ' ' + res;
                    value = Math.floor(value / arr[step]);
                    step++;
                }

                res = value + desc[step] + ' ' + res;

                return res;
            },

            currency: function (value) {
                var str = value.toString(),
                    i = 0,
                    j = 0;
                var array = str.split(/[\.\,]/);

                while (array[0].length > j) {

                    i++;
                    j++;
                    if (i == 3) {
                        array[0] = array[0].substring(0, array[0].length - j) + ' ' + array[0].substring(array[0].length - j);
                        i = -1;
                    }
                }
                return array.join(',');
            },

            text: function (value) {


                return (value + '').replace(/&/g, "&amp;")
                    .replace(/</g, "&lt;")
                    .replace(/>/g, "&gt;")
                    .replace(/"/g, "&quot;")
                    .replace(/'/g, "&#039;");
            }
        },

        hsl2rgb: function (h, s, l, array) {
            array = array ? true : false;
            var r, g, b,
                temp1,
                temp2,
                t3,
                clr,
                i;

            if (l == 0) {
                r = g = b = 0;
            } else {
                if (s == 0) {
                    r = g = b = l;
                } else {
                    temp2 = ((l <= 0.5) ? l * (1.0 + s) : l + s - (l * s));
                    temp1 = 2 * l - temp2;

                    //double[] t3=new double[]{normalisedH+1.0/3.0,normalisedH,normalisedH-1.0/3.0};
                    t3 = [h + 1.0 / 3.0, h, h - 1.0 / 3.0];
                    clr = [0, 0, 0];

                    for (i = 0; i < 3; i++) {
                        if (t3[i] < 0) {
                            t3[i] += 1.0;
                        }

                        if (t3[i] > 1) {
                            t3[i] -= 1.0;
                        }

                        if (6.0 * t3[i] < 1.0) {
                            clr[i] = temp1 + (temp2 - temp1) * t3[i] * 6.0;
                        } else {
                            if (2.0 * t3[i] < 1.0) {
                                clr[i] = temp2;
                            } else {
                                if (3.0 * t3[i] < 2.0) {
                                    clr[i] = (temp1 + (temp2 - temp1) * ((2.0 / 3.0) - t3[i]) * 6.0);
                                } else {
                                    clr[i] = temp1;
                                }
                            }
                        }

                    }

                    r = clr[0];
                    g = clr[1];
                    b = clr[2];
                }

            }
            return array ? [r * 255, g * 255, b * 255] : Stemapp.util.toHex(r * 255, g * 255, b * 255);
        },

        rgb2hsl: function (r, g, b) {
            r /= 255, g /= 255, b /= 255;
            var max = Math.max(r, g, b), min = Math.min(r, g, b);
            var h, s, l = (max + min) / 2;

            if (max == min) {
                h = s = 0; // achromatic
            } else {
                var d = max - min;
                s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
                switch (max) {
                    case r:
                        h = (g - b) / d + (g < b ? 6 : 0);
                        break;
                    case g:
                        h = (b - r) / d + 2;
                        break;
                    case b:
                        h = (r - g) / d + 4;
                        break;
                }
                h /= 6;
            }

            return [h, s, l];
        },

        toHex: function (red, green, blue) {
            var clamp = Stemapp.util.constrain,
                r = clamp(red, 0, 255),
                g = clamp(green, 0, 255),
                b = clamp(blue, 0, 255),
                round = Math.round;

            r = round(r).toString(16);
            g = round(g).toString(16);
            b = round(b).toString(16);

            r = (r.length == 1) ? '0' + r : r;
            g = (g.length == 1) ? '0' + g : g;
            b = (b.length == 1) ? '0' + b : b;
            return ['#', r, g, b].join('');
        },

        constrain: function (number, min, max) {
            return Math.min(Math.max(parseFloat(number), min), max);
        },


        calculateColor: function (color1, color2, operation) {

            // delete #
            color1 = color1.length == 7 ? color1.substring(1) : color1;
            color2 = color2.length == 7 ? color2.substring(1) : color2;

            operation = operation ? operation : '+';
            var c1, c2, res = [];

            c1 = [
                parseInt(color1.substr(0, 2), 16),
                parseInt(color1.substr(2, 2), 16),
                parseInt(color1.substr(4, 2), 16)
            ];
            c2 = [
                parseInt(color2.substr(0, 2), 16),
                parseInt(color2.substr(2, 2), 16),
                parseInt(color2.substr(4, 2), 16)
            ];

            for (var i = 0; i <= 2; i++) {
                switch (operation) {

                    case '-':
                        res[i] = (c1[i] - c2[i]) < 0 ? '00' : ('0' + (c1[i] - c2[i]).toString(16)).slice(-2);
                        break;

                    case '+':
                        res[i] = (c1[i] + c2[i]) > 255 ? 'FF' : ('0' + (c1[i] - c2[i]).toString(16)).slice(-2);
                        break;

                    default :
                        return 'unknow operation';
                }
            }
            return res.join('');
        },


        getPassHash: function (method, pass, salt) {

            //todo навести порядок с солью. Сделать более очевидно
            switch (method) {
                case 'sha1':
                    if (Array.isArray(salt)) {
                        return CryptoJS.SHA1(salt[0] + pass.trim() + salt[1]).toString();
                    } else if (salt) {
                        return CryptoJS.SHA1(pass.trim() + salt).toString();
                    } else {
                        return CryptoJS.SHA1(pass.trim()).toString();
                    }
                    break;
                case 'md5b64':
                    if (Array.isArray(salt)) {
                        return md5b64(salt[0] + pass.trim() + salt[1]);
                    } else if (typeof salt == 'string') {
                        return md5b64(pass.trim() + salt);
                    } else {
                        return md5b64(pass.trim());
                    }
                    break;
            }
        },

        /**
         * Remove all object property from object
         * @param object
         * @private
         */
        clearObjects: function (object) {
            object = $.extend({}, object);
            for (var i in object) {
                if (object.hasOwnProperty(i)) {
                    if (_.isObject(object[i]) && !_.isArray(object[i])) {
                        object[i] = null;
                    }
                } else {
                    object[i] = null;
                }

            }
            return object;
        }
    });
});
