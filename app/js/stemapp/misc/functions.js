define(['jquery'], function ($) {
    'use strict';

    $.fn.serializeObject = function () {
        "use strict";

        var result = {};
        var extend = function (i, element) {
            var node = result[element.name];

            // If node with same name exists already, need to convert it to an array as it
            // is a multi-value field (i.e., checkboxes)

            if ('undefined' !== typeof node && node !== null) {
                if ($.isArray(node)) {
                    node.push(element.value);
                } else {
                    result[element.name] = [node, element.value];
                }
            } else {
                result[element.name] = element.value;
            }
        };

        $.each(this.serializeArray(), extend);
        return result;
    };

    Stemapp.Function = {

        checkUserServices: function (service) {
            if (!Stemapp.userServices || !Stemapp.userServices.length) {
                return false;
            }
            var res = false;

            for (var i in Stemapp.userServices) {
                if (Stemapp.userServices.hasOwnProperty(i)) {
                    if (Stemapp.userServices[i].Code == service) {
                        res = true;
                        break;
                    }
                }
            }
            return res;
        }
    };

});