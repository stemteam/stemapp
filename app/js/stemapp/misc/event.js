define([], function () {
    'use strict';


    Stemapp.event = {


        events: [],

        log: false,

        /**
         * @private
         * @param context
         * @param name
         * @param callback function
         */
        addEventListener: function (context, name, callback) {

            this.events.push({
                context: context, name: name, callback: callback
            });
        },

        on: function (name, callback, context) {
            this.addEventListener(context, name, callback);
        },


        off: function (name, context) {
            for (var i = 0; i < this.events.length; i++) {
                if (this.events[i] && ((!context || this.events[i].context == context) && this.events[i].name == name)) {
                    this.events[i] = null;
                }
            }
        },


        trigger: function (name, paramsArray, context) {
            this.sendEvent(context, name, paramsArray);
        },


        /**
         * @private
         * @param context
         * @param name
         * @param paramsArray
         */
        sendEvent: function (context, name, paramsArray) {

            try {
                if (this.log) {
                    console.log(name, [].concat(paramsArray).pop());
                }
                for (var i = 0; i < this.events.length; i++) {
                    if (this.events[i] && this.events[i].context == context && this.events[i].name == name) {

                        this.events[i].callback.apply(context, [].concat(paramsArray));
                        context = paramsArray = null;
                        break;
                    }
                }
            } catch (e) {
                console.log('error ', e);
            }
        },

        clear: function (param) {

            for (var i = 0; i < this.events.length; i++) {
                if (this.events[i] && (this.events[i].context == param || this.events[i].name == param)) {
                    this.events[i] = null;
                }
            }
        }
    };

});