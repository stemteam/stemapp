define([
    'jquery',
    '_app/misc/TreeNode'
], function ($, toTree) {
    'use strict';

    if (!Stemapp.grid) {
        Stemapp.grid = {};
    }

    Stemapp.grid = $.extend(true, Stemapp.grid, {

        init: {


            adminPlainGrid: function (table, data, name, columns) {

                var options = {
                        editable: true,
                        enableAddRow: false,
                        enableCellNavigation: true,
                        asyncEditorLoading: false,
                        rowHeight: 30,
//                        showHeaderRow: true,
//                        forceFitColumns: true,
//                        fullWidthRows: true,
                        headerRowHeight: 30,
                        explicitInitialization: true,
                        enableTextSelectionOnCells: true,

                        autoEdit: false
//                        autoHeight: true
                    },
                    model = Stemapp.data[name],
                    relative = Stemapp.relation[name];
                if (!model) {
                    console.error('Model Not Found "' + name + '"!')
                    return;
                }

                var l = data.length,
                    id = table.attr('id');


                var mode = 'in' + ( Stemapp.mode.charAt(0).toUpperCase() + Stemapp.mode.slice(1));

                for (var i = 0; i < model.columns.length; i++) {

                    if (model.columns[i][mode] || model.columns[i].inGrid) {

                        columns.push({
                            id: model.columns[i].name,
                            name: model.columns[i].caption,
                            field: model.columns[i].name,
                            width: model.columns[i].width ? model.columns[i].width : 100,
                            minWidth: model.columns[i].minWidth ? model.columns[i].minWidth : model.columns[i].width,
                            formatter: model.columns[i].format,
                            linkId: model.columns[i].linkId,
                            cssClass: model.columns[i].cssClass,
                            headerCssClass: model.columns[i].headerCssClass,
                            editor: model.columns[i].hasOwnProperty('editor') ? model.columns[i].editor : Slick.ROEditors.Text,
                            sortable: true
                        });
                    }
                }

                table.searchString = '';
                table.columnSearch = {};

                table.model = model;

                table.dataView = new Slick.Data.DataView({inlineFilters: true});
                table.dataView.beginUpdate();
                table.dataView.setItems(data);
                table.dataView.setFilter(Stemapp.grid.filter.adminPlainGrid);
                table.dataView.endUpdate();
                table.dataView.onRowsChanged.subscribe(function (e, args) {
                    table.slick.invalidateRows(args.rows);
                    table.slick.render();
                });

                table.dataView.onRowCountChanged.subscribe(function (e, args) {
                    table.slick.updateRowCount();
                    table.slick.render();
                });


                Stemapp.grid.appendMetaData(table);


                table.slick = new Slick.Grid('#' + table.attr('id'), table.dataView, columns, options);
                table.slick.setSelectionModel(new Slick.RowSelectionModel({selectActiveRow: false}));
                table.dataView.syncGridSelection(table.slick, true);

                table.slick.onHeaderCellRendered.subscribe(function () {
                    table.find('.indeterminate').prop('indeterminate', true).removeClass('indeterminate');
                });


                table.slick.onSort.subscribe(table._sort = function (e, args) {


                    var field = args.sortCol.field,
                        sortAsc = args.sortAsc;

                    var items = table.dataView.getItems(),
                        l = items.length;

                    // save selection row

                    var rows = (table.slick.getSelectedRows()),
                        active = table.slick.getActiveCell() ? table.dataView.getItemByIdx(table.slick.getActiveCell().row) : null,
                        col = table.slick.getActiveCell() ? table.slick.getActiveCell().cell : null,
                        i,
                        sel = [];
                    for (i = 0; i < rows.length; i++) {

                        sel.push(table.dataView.getItemByIdx(rows[i]))
                    }

                    items.sort(function (a, b) {

                        if (a[field] == b[field]) {
                            return 0;
                        }
                        return (a[field] < b[field] ? -1 : 1) * ( sortAsc ? 1 : -1);
                    });

                    table.dataView.beginUpdate();
                    table.dataView.setItems(items);
                    table.dataView.endUpdate();

                    if (sel.length) {
                        // restore selected row
                        var srows = [];

                        for (i = 0; i < sel.length; i++) {

                            srows.push(table.dataView.getIdxById(sel[i].id));
                        }
                        table.slick.setSelectedRows(srows, 1);
                    }

                    if (active) {

                        table.slick.setActiveCell(table.dataView.getIdxById(active.id), col);
                    }
                });

                table.slick.onHeaderRowCellRendered.subscribe(function (e, args) {
                    $(args.node).empty();
                    if (args.column.id == 'counter' || args.column.id == '_checkbox_selector') {
                        return '';
                    }
                    $("<input type='text'>")
                        .data("columnId", args.column.id)
                        .appendTo(args.node)
                        .on('keyup', function () {
                            table.columnSearch[args.column.id] = $(this).val();
                            table.dataView.refresh();
                        });

                    table.columnSearch[args.column.id] = '';

                });

                table.slick.init();

                table.slick.updateColumnHeader('_checkbox_selector');

            },

            // для таблиц, работающих на метаданных сервера
            adminListGrid: function (table, data, name, columns, metadata) {
                var i;
                var options = {
                    editable: true,
                    enableAddRow: false,
                    enableCellNavigation: true,
                    asyncEditorLoading: false,
                    rowHeight: 30,
                    headerRowHeight: 30,
                    explicitInitialization: true,
                    enableTextSelectionOnCells: true,
                    multiColumnSort: true,
                    autoEdit: false
//                        autoHeight: true
                };
                var model = Stemapp.data[name];
                var l = data.length,
                    id = table.attr('id');

                for (i = 0; i < l; i++) {
                    data[i]._table = table
                }

                for (i = 0; i < metadata.length; i++) {
                    if (metadata[i].Presentation == 'Time') {
                        metadata[i].format = Stemapp.grid.formatter.timepstamp_time;
                        metadata[i].exportFormatter = Stemapp.grid.exportFormatter.timepstamp_time;
                    }
                    if (metadata[i].Presentation == 'Date') {
                        metadata[i].format = Stemapp.grid.formatter.timepstamp_date;
                        metadata[i].exportFormatter = Stemapp.grid.exportFormatter.timepstamp_date;
                    }
                    if (metadata[i].Presentation == 'DateTime') {
                        metadata[i].format = Stemapp.grid.formatter.timepstamp_datetime;
                        metadata[i].exportFormatter = Stemapp.grid.exportFormatter.timepstamp_datetime;
                    }
                    if (metadata[i].Presentation == 'TimeUTC') {
                        metadata[i].format = Stemapp.grid.formatter.timepstamp_timeUTC;
                        metadata[i].exportFormatter = Stemapp.grid.exportFormatter.timepstamp_timeUTC;
                    }
                    if (metadata[i].Presentation == 'DateUTC') {
                        metadata[i].format = Stemapp.grid.formatter.timepstamp_dateUTC;
                        metadata[i].exportFormatter = Stemapp.grid.exportFormatter.timepstamp_dateUTC;
                    }
                    if (metadata[i].Presentation == 'DateTimeUTC') {
                        metadata[i].format = Stemapp.grid.formatter.timepstamp_datetimeUTC;
                        metadata[i].exportFormatter = Stemapp.grid.exportFormatter.timepstamp_datetimeUTC;
                    }

                    //check tree attribute and set dependence parameters
                    if (metadata[i].IsTreeField) {
                        metadata[i].format = Stemapp.grid.formatter.treeFormat;
                        metadata[i].exportFormatter = Stemapp.grid.exportFormatter.treeFormat;
                        metadata[i].noSortable = true;
                    }

                    metadata[i].format = metadata[i].format ? metadata[i].format : Stemapp.grid.formatter.text;
                    metadata[i].exportFormatter = metadata[i].exportFormatter ? metadata[i].exportFormatter : Stemapp.grid.exportFormatter.text;
                    columns.push({
                        id: metadata[i].FieldId,
                        name: metadata[i].Caption,
                        field: metadata[i].Name,
                        inGrid: metadata[i].IsHidden ? false : true,
                        width: metadata[i].Width ? metadata[i].Width * 1.5 : 100,
                        minWidth: metadata[i].MinWidth ? metadata[i].MinWidth : metadata[i].Width ? metadata[i].Width : 10,
                        formatter: metadata[i].format,
                        cssClass: metadata[i].cssClass,
                        headerCssClass: metadata[i].headerCssClass,
                        //editor: metadata[i].hasOwnProperty('editor') ? metadata[i].editor : Slick.ROEditors.Text,
                        sortable: metadata[i].noSortable ? false : true,
                        exportFormatter: metadata[i].exportFormatter
                    });
                }


                //console.log(columns);

                table.model = {columns: columns, getColumnAtName: model.getColumnAtName, metadata: true};

                table.searchString = '';
                table.columnSearch = {};

                //table.model = model;

                table.dataView = new Slick.Data.DataView({inlineFilters: true});
                table.dataView.beginUpdate();
                table.dataView.setItems(data);
                //table.dataView.setFilter(Stemapp.grid.filter.listTreeGrid);
                table.dataView.endUpdate();
                table.dataView.onRowsChanged.subscribe(function (e, args) {
                    table.slick.invalidateRows(args.rows);
                    table.slick.render();
                });

                table.dataView.onRowCountChanged.subscribe(function (e, args) {
                    table.slick.updateRowCount();
                    table.slick.render();
                });

                Stemapp.grid.appendMetaData(table);

                table.slick = new Slick.Grid('#' + table.attr('id'), table.dataView, columns, options);
                table.slick.setSelectionModel(new Slick.RowSelectionModel({selectActiveRow: false}));
                table.dataView.syncGridSelection(table.slick, true);

                table.slick.onHeaderCellRendered.subscribe(function () {
                    table.find('.indeterminate').prop('indeterminate', true).removeClass('indeterminate');
                });

                table.slick.onClick.subscribe(function (e, args) {

                    if ($(e.target).hasClass("toggle")) {
                        var item = table.dataView.getItem(args.row);
                        if (item) {
                            if (!item._collapsed) {
                                item._collapsed = true;
                            } else {
                                item._collapsed = false;
                            }

                            table.dataView.updateItem(item.id, item);
                        }
                        e.stopImmediatePropagation();
                    }
                });


                table.slick.onHeaderRowCellRendered.subscribe(function (e, args) {
                    $(args.node).empty();
                    if (args.column.id == 'counter' || args.column.id == '_checkbox_selector') {
                        return '';
                    }
                    $("<input type='text'>")
                        .data("columnId", args.column.id)
                        .appendTo(args.node)
                        .on('keyup', function () {
                            table.columnSearch[args.column.id] = $(this).val();
                            table.trigger('updateHeaderSearch');
                        });

                    table.columnSearch[args.column.id] = '';

                });

                table.slick.init();

                table.slick.updateColumnHeader('_checkbox_selector');

            },


            adminDialogPlainGrid: function (table, data, name, columns) {

                var options = {
                        editable: true,
                        enableAddRow: false,
                        enableCellNavigation: true,
                        asyncEditorLoading: false,
                        rowHeight: 30,
//                        forceFitColumns: true,
                        headerRowHeight: 30,
                        explicitInitialization: true,
                        enableTextSelectionOnCells: true,
                        autoEdit: false,
                        afterRender: function () {
                            table.find('.countSlider.slider').each(function () {
                                var slider = $(this),
                                    val = slider.parent().find('.countValue');
                                $(this).slider({
                                    max: slider.attr('data-max'),
                                    min: 1,
                                    value: slider.attr('data-value'),
                                    slide: function (event, ui) {
                                        val.text(ui.value);
                                        var item = table.dataView.getItemById(slider.attr('data-id'));
                                        item.COUNT = ui.value;
                                        table.dataView.updateItem(slider.attr('data-id'), item);
                                    },
                                    change: function (event, ui) {
                                        val.text(ui.value);
                                        var item = table.dataView.getItemById(slider.attr('data-id'));
                                        item.COUNT = ui.value;
                                        table.dataView.updateItem(slider.attr('data-id'), item);
                                    }
                                }).removeClass('slider');
                            });
                        }
                    },
                    model = Stemapp.data[name],
                    relative = Stemapp.relation[name];

                var l = data.length,
                    id = table.attr('id');


                for (var i = 0; i < model.columns.length; i++) {

                    if (model.columns[i].inDialog) {

                        columns.push({
                            id: model.columns[i].name,
                            name: model.columns[i].caption,
                            field: model.columns[i].name,
                            width: model.columns[i].width ? model.columns[i].width : 100,
                            minWidth: model.columns[i].width ? model.columns[i].width : 100,
                            formatter: model.columns[i].format,
                            linkId: model.columns[i].linkId,
                            cssClass: model.columns[i].cssClass,
                            headerCssClass: model.columns[i].headerCssClass,
                            sortable: true
                        });
                    }
                }

                table.searchString = '';
                table.columnSearch = {};

                table.model = model;

                table.dataView = new Slick.Data.DataView({inlineFilters: true});
                table.data('dataView', table.dataView);
                table.dataView.beginUpdate();
                table.dataView.setItems(data);
                table.dataView.endUpdate();
                table.dataView.onRowsChanged.subscribe(function (e, args) {
                    table.slick.invalidateRows(args.rows);
                    table.slick.render();
                });

                table.dataView.onRowCountChanged.subscribe(function (e, args) {
                    table.slick.updateRowCount();
                    table.slick.render();
                });

                Stemapp.grid.appendMetaData(table);

                table.slick = new Slick.Grid('#' + table.attr('id'), table.dataView, columns, options);
                table.slick.setSelectionModel(new Slick.RowSelectionModel({selectActiveRow: false}));

                table.dataView.syncGridSelection(table.slick, true);
                table.data('slick', table.slick);
//                table.slick.onClick.subscribe(function (e, args) {
//
//                    if ($(e.target).hasClass("toggle")) {
//                        var item = table.dataView.getItem(args.row);
//                        if (item) {
//                            if (!item._collapsed) {
//                                item._collapsed = true;
//                            } else {
//                                item._collapsed = false;
//                            }
//
//                            table.dataView.updateItem(item.id, item);
//                        }
//                        e.stopImmediatePropagation();
//                    }
//                });
                table.slick.onHeaderCellRendered.subscribe(function () {
                    table.find('.indeterminate').prop('indeterminate', true).removeClass('indeterminate');
                });

                table.slick.onSort.subscribe(table._sort = function (e, args) {


                    var field = args.sortCol.field,
                        sortAsc = args.sortAsc;

                    var items = table.dataView.getItems(),
                        l = items.length;

                    // save selection row

                    var rows = (table.slick.getSelectedRows()),
                        active = table.slick.getActiveCell() ? table.dataView.getItemByIdx(table.slick.getActiveCell().row) : null,
                        col = table.slick.getActiveCell() ? table.slick.getActiveCell().cell : null,
                        i,
                        sel = [];
                    for (i = 0; i < rows.length; i++) {

                        sel.push(table.dataView.getItemByIdx(rows[i]))
                    }

                    items.sort(function (a, b) {

                        if (a[field] == b[field]) {
                            return 0;
                        }
                        return (a[field] < b[field] ? -1 : 1) * ( sortAsc ? 1 : -1);
                    });

                    table.dataView.beginUpdate();
                    table.dataView.setItems(items);
                    table.dataView.endUpdate();

                    if (sel.length) {
                        // restore selected row
                        var srows = [];

                        for (i = 0; i < sel.length; i++) {

                            srows.push(table.dataView.getIdxById(sel[i].id));
                        }
                        table.slick.setSelectedRows(srows, 1);
                    }

                    if (active) {

                        table.slick.setActiveCell(table.dataView.getIdxById(active.id), col);
                    }
                });

                table.slick.onHeaderRowCellRendered.subscribe(function (e, args) {
                    $(args.node).empty();
                    if (args.column.id == 'counter' || args.column.id == '_checkbox_selector') {
                        return '';
                    }
                    $("<input type='text'>")
                        .data("columnId", args.column.id)
                        .appendTo(args.node)
                        .on('keyup', function () {
                            table.columnSearch[args.column.id] = $(this).val();
                            table.dataView.refresh();
                        });

                    table.columnSearch[args.column.id] = '';

                });


                table.slick.init();

                table.slick.updateColumnHeader('_checkbox_selector');

            },

            adminAbonents: function (table, data, name, columns) {

                var options = {
                        editable: false,
                        enableAddRow: false,
                        enableCellNavigation: true,
                        asyncEditorLoading: false,
                        rowHeight: 30,
                        showHeaderRow: false,
//                        forceFitColumns: true,
                        headerRowHeight: 30,
                        explicitInitialization: true,
                        enableTextSelectionOnCells: true,
                        autoEdit: false
//                        autoHeight: true
                    },
                    model = Stemapp.data[name],
                    relative = Stemapp.relation[name];

                var l = data.length,
                    id = table.attr('id');


                for (var i = 0; i < model.columns.length; i++) {

                    if (model.columns[i].inGrid) {

                        columns.push({
                            id: model.columns[i].name,
                            name: model.columns[i].caption,
                            field: model.columns[i].name,
                            width: model.columns[i].width ? model.columns[i].width : 100,
                            minWidth: model.columns[i].width ? model.columns[i].width : 100,
                            formatter: model.columns[i].format,
                            cssClass: model.columns[i].cssClass,
                            linkId: model.columns[i].linkId,
                            headerCssClass: model.columns[i].headerCssClass,
                            editor: model.columns[i].hasOwnProperty('editor') ? model.columns[i].editor : Slick.ROEditors.Text,
                            sortable: true
                        });
                    }
                }
                table.searchString = '';
                table.columnSearch = {};

                table.model = model;
                table.relative = relative;

                table.dataView = new Slick.Data.DataView({inlineFilters: true});
                table.dataView.beginUpdate();
                table.dataView.setItems(data);
                table.dataView.setFilter(Stemapp.grid.filter.adminTreeGrid);
                table.dataView.endUpdate();


                table.dataView.onRowsChanged.subscribe(function (e, args) {
                    table.slick.invalidateRows(args.rows);
                    table.slick.render();
                });

                table.dataView.onRowCountChanged.subscribe(function (e, args) {
                    table.slick.updateRowCount();
                    table.slick.render();
                });

                Stemapp.grid.appendMetaData(table);

                table.slick = new Slick.Grid('#' + table.attr('id'), table.dataView, columns, options);
                table.slick.setSelectionModel(new Slick.RowSelectionModel({selectActiveRow: false}));

                table.dataView.syncGridSelection(table.slick, true);

                table.slick.onClick.subscribe(function (e, args) {


                    if ($(e.target).hasClass("toggle")) {
                        var item = table.dataView.getItem(args.row);
                        if (item) {
                            if (!item._collapsed) {
                                item._collapsed = true;
                            } else {
                                item._collapsed = false;
                            }

                            table.dataView.updateItem(item.id, item);
                        }
                        e.stopImmediatePropagation();
                    }
                });

                table.slick.onHeaderCellRendered.subscribe(function () {
                    table.find('.indeterminate').prop('indeterminate', true).removeClass('indeterminate');
                });


                table.slick.onSort.subscribe(table._sort = function (e, args) {
                    var sortcol = args.sortCol.field;

                    var items = table.dataView.getItems(),
                        l = items.length;

                    // save selection row

                    var rows = (table.slick.getSelectedRows()),
                        active = table.slick.getActiveCell() ? table.dataView.getItemByIdx(table.slick.getActiveCell().row) : null,
                        col = table.slick.getActiveCell() ? table.slick.getActiveCell().cell : null,
                        i,
                        sel = [];


                    for (i = 0; i < rows.length; i++) {

                        sel.push(table.dataView.getItemByIdx(rows[i]))
                    }
                    var tree = toTree(items, function (a, b, field, sortAsc) {

                        if (a.data[field] == b.data[field]) {
                            return 0;
                        }
                        return (a.data[field] < b.data[field] ? -1 : 1) * ( sortAsc ? 1 : -1);
                    }, sortcol, args.sortAsc);

                    items = (tree.getArray());
                    table.dataView.beginUpdate();
                    table.dataView.setItems(items);
                    table.dataView.endUpdate();

                    if (sel.length) {
                        // restore selected row
                        var srows = [];

                        for (i = 0; i < sel.length; i++) {

                            srows.push(table.dataView.getIdxById(sel[i].id));
                        }
                        table.slick.setSelectedRows(srows, 1);
                    }

                    if (active) {

                        table.slick.setActiveCell(table.dataView.getIdxById(active.id), col);
                    }
                });

                table.slick.onHeaderRowCellRendered.subscribe(function (e, args) {
                    $(args.node).empty();
                    if (args.column.id == 'counter' || args.column.id == '_checkbox_selector') {
                        return '';
                    }
                    $("<input type='text'>")
                        .data("columnId", args.column.id)
                        .appendTo(args.node)
                        .on('keyup', function () {
                            table.columnSearch[args.column.id] = $(this).val();
                            table.dataView.refresh();
                        });

                    table.columnSearch[args.column.id] = '';

                });

                table.slick.init();

            },

            adminDialogGrid: function (table, data, name, columns) {

                var options = {
                        editable: true,
                        enableAddRow: false,
                        enableCellNavigation: true,
                        asyncEditorLoading: false,
                        rowHeight: 30,
//                        showHeaderRow: true,
//                        forceFitColumns: true,
                        headerRowHeight: 30,
                        explicitInitialization: true,
                        enableTextSelectionOnCells: true,
                        autoEdit: false
//                        autoHeight: true
                    },
                    model = Stemapp.data[name],
                    relative = Stemapp.relation[name];

                var l = data.length,
                    id = table.attr('id');


                for (var i = 0; i < model.columns.length; i++) {

                    if (model.columns[i].inDialog) {

                        columns.push({
                            id: model.columns[i].name,
                            name: model.columns[i].caption,
                            field: model.columns[i].name,
                            width: model.columns[i].width ? model.columns[i].width : 100,
                            minWidth: model.columns[i].width ? model.columns[i].width : 100,
                            formatter: model.columns[i].dialogFormater ? model.columns[i].dialogFormater : model.columns[i].format,
                            cssClass: model.columns[i].cssClass,
                            linkId: model.columns[i].linkId,
                            headerCssClass: model.columns[i].headerCssClass,
                            sortable: true
                        });
                    }
                }
                if (!table.attr('id')) {
                    console.error('table must have id');
                    return;
                }

                table.searchString = '';
                table.columnSearch = {};

                table.model = model;

                table.dataView = new Slick.Data.DataView({inlineFilters: true});
                table.data('dataView', table.dataView);
                table.dataView.beginUpdate();
                table.dataView.setItems(data);
                table.dataView.endUpdate();


                table.dataView.onRowsChanged.subscribe(function (e, args) {
                    table.slick.invalidateRows(args.rows);
                    table.slick.render();
                });

                table.dataView.onRowCountChanged.subscribe(function (e, args) {
                    table.slick.updateRowCount();
                    table.slick.render();
                });


                table.slick = new Slick.Grid('#' + table.attr('id'), table.dataView, columns, options);
                table.slick.setSelectionModel(new Slick.RowSelectionModel({selectActiveRow: true}));
                table.data('slick', table.slick);
                table.dataView.syncGridSelection(table.slick, true);
                table.slick.onHeaderCellRendered.subscribe(function () {
                    table.find('.indeterminate').prop('indeterminate', true).removeClass('indeterminate');
                });

                table.slick.onSort.subscribe(table._sort = function (e, args) {
                    var field = args.sortCol.field,
                        sortAsc = args.sortAsc;

                    var items = table.dataView.getItems(),
                        l = items.length;

                    // save selection row


                    var rows = (table.slick.getSelectedRows()),
                        active = table.slick.getActiveCell() ? table.dataView.getItemByIdx(table.slick.getActiveCell().row) : null,
                        col = table.slick.getActiveCell() ? table.slick.getActiveCell().cell : null,
                        i,
                        sel = [];


                    for (i = 0; i < rows.length; i++) {

                        sel.push(table.dataView.getItemByIdx(rows[i]))
                    }


                    items.sort(function (a, b) {

                        if (a[field] == b[field]) {
                            return 0;
                        }
                        return (a[field] < b[field] ? -1 : 1) * ( sortAsc ? 1 : -1);
                    });


                    table.dataView.beginUpdate();
                    table.dataView.setItems(items);
                    table.dataView.endUpdate();

                    if (sel.length) {
                        // restore selected row
                        var srows = [];

                        for (i = 0; i < sel.length; i++) {

                            srows.push(table.dataView.getIdxById(sel[i].id));
                        }
                        table.slick.setSelectedRows(srows, 1);
                    }

                    if (active) {

                        table.slick.setActiveCell(table.dataView.getIdxById(active.id), col);
                    }

                });

                table.slick.onHeaderRowCellRendered.subscribe(function (e, args) {
                    $(args.node).empty();
                    if (args.column.id == 'counter' || args.column.id == '_checkbox_selector') {
                        return '';
                    }
                    $("<input type='text'>")
                        .data("columnId", args.column.id)
                        .appendTo(args.node)
                        .on('keyup', function () {
                            table.columnSearch[args.column.id] = $(this).val();
                            table.dataView.refresh();
                        });

                    table.columnSearch[args.column.id] = '';

                });


                table.slick.init();

                table.slick.updateColumnHeader('_checkbox_selector');
            },

            adminRawGrid: function (table, data, rawData) {

                var options = {
                    editable: true,
                    enableAddRow: false,
                    enableCellNavigation: true,
                    asyncEditorLoading: false,
                    rowHeight: 30,
                    showHeaderRow: false,
//                    forceFitColumns: true,
                    explicitInitialization: true,
                    enableTextSelectionOnCells: true,
                    autoEdit: false
                };

                if (typeof(rawData) == 'string') {
                    rawData = JSON.parse(rawData);
                }
                var l = data.length,
                    id = table.attr('id'),
                    lenCol = rawData.cols.length,
                    columns = [],
                    model = Stemapp.data.raw;
                if (!model) {
                    console.error('Not found Stemapp.data.raw');
                    return;
                }

                for (var i = 0; i < lenCol; i++) {


                    var nameSep = String(rawData.cols[i]).indexOf('|'),
                        name = ~nameSep ? rawData.cols[i].slice(0, nameSep) : rawData.cols[i],
                        formatter = Stemapp.grid.formatter.text,
                        Exformatter = Stemapp.grid.exportFormatter.html,
                        cls = '',
                        width = 150,
                        type = '';

                    if (_.isFunction(rawData.types[i])) {
                        formatter = rawData.types[i];
                    } else {
                        if (rawData.types[i] === 'Boolean') {
                            formatter = Stemapp.grid.formatter.boolPoint;
                            width = 40;
                            type = 'bool';
                            Exformatter = Stemapp.grid.formatter.bool;
                        } else if ((rawData.types[i] === 'DateTime') || (rawData.types[i] === 'Date') || (rawData.types[i] === 'TimeStamp')) {
                            formatter = Stemapp.grid.formatter.datetime;
                            Exformatter = Stemapp.grid.exportFormatter.datetime;
                            type = 'date';
                        } else if (~['Int16', 'Int32', 'Int64', 'Decimal'].indexOf(rawData.types[i])) {
                            cls = 'tar';
                            type = 'int';
                        }
                    }
                    if (rawData.width && rawData.width[i]) {
                        width = rawData.width[i];
                    }

                    columns.push({
                        id: rawData.cols[i],
                        name: name,
                        field: rawData.cols[i],
                        data: rawData.data && rawData.data[i] ? rawData.data[i] : ( rawData.rows && rawData.rows[i] ? rawData.rows[i] : null),
                        width: width,
                        minWidth: 50,
                        toolTip: ~nameSep ? name.slice(nameSep + 1) : null,
                        formatter: formatter,
                        exportFormatter: Exformatter,
                        cssClass: cls,
                        editor: Slick.ROEditors.Text,
                        sortable: true
                    });


                    model.columns.push({
                        name: name,
                        exportFormatter: Exformatter,
                        type: type,
                        T: rawData.types[i]

                    });

                }
                if (!table.attr('id')) {
                    console.error('table must have id');
                    return;
                }
                table.model = model;


                table.searchString = '';
                table.columnSearch = {};

                table.dataView = new Slick.Data.DataView({inlineFilters: true});
                table.dataView.beginUpdate();
                table.dataView.setItems(data);
                table.dataView.setFilter(Stemapp.grid.filter.adminRawGrid);
                table.dataView.endUpdate();


                table.dataView.onRowsChanged.subscribe(function (e, args) {
                    table.slick.invalidateRows(args.rows);
                    table.slick.render();
                });

                table.dataView.onRowCountChanged.subscribe(function (e, args) {
                    table.slick.updateRowCount();
                    table.slick.render();
                });


                table.slick = new Slick.Grid('#' + table.attr('id'), table.dataView, columns, options);
                table.slick.setSelectionModel(new Slick.RowSelectionModel({selectActiveRow: true}));

                table.data('slick', table.slick);
                table.dataView.syncGridSelection(table.slick, true);

                table.slick.onSort.subscribe(table._sort = function (e, args) {
                    var field = args.sortCol.field,
                        sortAsc = args.sortAsc;

                    var items = table.dataView.getItems(),
                        l = items.length;

                    // save selection row

                    var row = (table.slick.getSelectedRows()).pop();

                    var sel = table.dataView.getItemByIdx(row);


                    items.sort(function (a, b) {

                        if (a[field] == b[field]) {
                            return 0;
                        }
                        return (a[field] < b[field] ? -1 : 1) * ( sortAsc ? 1 : -1);
                    });
                    table.dataView.beginUpdate();
                    table.dataView.setItems(items);
                    table.dataView.endUpdate();

                    if (sel) {
                        // restore selected row
                        table.slick.resetActiveCell();
                        table.slick.setActiveCell([table.dataView.getIdxById(sel.id)], 1);
                    }

                });

                table.slick.onHeaderRowCellRendered.subscribe(function (e, args) {
                    $(args.node).empty();
                    if (args.column.id == 'counter' || args.column.id == '_checkbox_selector') {
                        return '';
                    }
                    $("<input type='text'>")
                        .data("columnId", args.column.id)
                        .appendTo(args.node)
                        .on('keyup', function () {
                            table.columnSearch[String(args.column.id).split('|')[0]] = $(this).val();
                            table.dataView.refresh();
                        });
                    table.columnSearch[String(args.column.id).split('|')[0]] = '';

                });

                table.slick.init();

            }

        },

        formatter: {

            filterHtml: function (row, cell, value, columnDef, dataContext) {
                value = value.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
                var spacer = "<span style='display:inline-block;height:1px;width:" + (26 * dataContext["level"]) + "px'></span>";
                if (dataContext.level == 0) {

                    if (dataContext._collapsed) {

                        return spacer + " <i class='icon icon-header-tree-e toggle'></i> &nbsp;<span>" + Stemapp.util.format.text(value) + '</span>';
                    } else {

                        return spacer + " <i class='icon icon-header-tree-c toggle'></i> &nbsp;<span>" + Stemapp.util.format.text(value) + '</span>';
                    }
                } else {

                    return spacer + "<span>" + value + '</span>';
                }
            },



            treeFormat: function (row, cell, value, columnDef, dataContext) {
                value = value.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
                var spacer = "<span style='display:inline-block;height:1px;width:" + (26 * dataContext["indent"]) + "px'></span>";
                if (dataContext.indent == 0) {

                    if (dataContext._collapsed) {

                        return spacer + " <i class='icon icon-header-tree-e toggle'></i> &nbsp;<span>" + Stemapp.util.format.text(value) + '</span>';
                    } else {

                        return spacer + " <i class='icon icon-header-tree-c toggle'></i> &nbsp;<span>" + Stemapp.util.format.text(value) + '</span>';
                    }
                } else {

                    return spacer + "<span>" + value + '</span>';
                }
            },

            abonentDescription: function (row, cell, value, columnDef, dataContext) {


                value = value.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
                var res = '';
                if (dataContext.CCOUNT != 0) {

                    var spacer = "<span style='display:inline-block;height:1px;width:" + (20 * (dataContext["CLEVEL"] - 1)) + "px'></span>";

                    if (dataContext._collapsed) {

                        res = spacer + " <i class='icon icon-header-tree-e toggle'></i>";
                    } else {

                        res = spacer + " <i class='icon icon-header-tree-c toggle'></i>";
                    }

                } else {

                    var spacer = "<span style='display:inline-block;height:1px;width:" + (20 * (dataContext["CLEVEL"])) + "px'></span>";
                    res = spacer;
                }
                res += '<i class="icon icon-abonent icon-abonent-type' + dataContext.ID_ABONENT_TYPE + '"></i> <a href="#/objects/' +
                dataContext.server + '/' + dataContext.GUID + '" class="abonent" title="' +
                Stemapp.util.format.text(dataContext.NAME) + '">' +
                Stemapp.util.format.text(value) + '</a>';

                res += '<span class="abonent-type">' + dataContext.ABONENT_TYPE + '</span>';

                return res;
            },


            clients: function (row, cell, value, columnDef, dataContext) {


                value = value.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
                var res = '';
                if (dataContext.ccount != 0) {

                    var spacer = "<span style='display:inline-block;height:1px;width:" + (20 * (dataContext["clevel"] - 1)) + "px'></span>";

                    if (dataContext._collapsed) {

                        res = spacer + " <i class='icon icon-header-tree-e toggle'></i>";
                    } else {

                        res = spacer + " <i class='icon icon-header-tree-c toggle'></i>";
                    }

                } else {

                    var spacer = "<span style='display:inline-block;height:1px;width:" + (20 * (dataContext["clevel"])) + "px'></span>";
                    res = spacer;
                }
                res += '<i class="icon icon-abonent icon-abonent-type' + dataContext.id_abonent_type + '"></i> <a href="#/clients/' +
                dataContext.id_abonent + '" class="client" data-guid="' + dataContext.guid + '" title="' +
                Stemapp.util.format.text(dataContext.name) + '">' +
                Stemapp.util.format.text(value) + '</a>';

                //res += '<span class="abonent-type">' + dataContext.abonent_type + '</span>';

                return res;
            },

            clientsBoldHolding: function (row, cell, value, columnDef, dataContext) {


                value = value.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
                var res = '';
                if (dataContext.ccount != 0) {

                    var spacer = "<span style='display:inline-block;height:1px;width:" + (20 * (dataContext["clevel"] - 1)) + "px'></span>";

                    if (dataContext._collapsed) {

                        res = spacer + " <i class='icon icon-header-tree-e toggle'></i>";
                    } else {

                        res = spacer + " <i class='icon icon-header-tree-c toggle'></i>";
                    }

                } else {

                    var spacer = "<span style='display:inline-block;height:1px;width:" + (20 * (dataContext["clevel"])) + "px'></span>";
                    res = spacer;
                }
                res += '<i class="icon icon-abonent icon-abonent-type' + dataContext.id_abonent_type + '"></i> <a href="#/clients/' +
                    dataContext.id_abonent + '" class="client" data-guid="' + dataContext.guid + '" title="' +
                    Stemapp.util.format.text(dataContext.name) + '">';
                if (dataContext.id_abonent_type == '1') {
                    res += Stemapp.util.format.text(value) + '</a>';
                } else {
                    res += '<b>' + Stemapp.util.format.text(value) + '</b></a>';
                }

                //res += '<span class="abonent-type">' + dataContext.abonent_type + '</span>';

                return res;
            },


            tree_item: function (row, cell, value, columnDef, dataContext) {

                var CCOUNT = dataContext._table.relative.CCOUNT ? dataContext._table.relative.CCOUNT : 'CCOUNT',
                    CLEVEL = dataContext._table.relative.CLEVEL ? dataContext._table.relative.CLEVEL : 'CLEVEL';

                value = value.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
                var res = '';
                if (dataContext[CCOUNT] != 0) {

                    var spacer = "<span style='display:inline-block;height:1px;width:" + (20 * (dataContext[CLEVEL] - 1)) + "px'></span>";

                    if (dataContext._collapsed) {

                        res = spacer + " <i class='icon icon-header-tree-e toggle'></i>";
                    } else {

                        res = spacer + " <i class='icon icon-header-tree-c toggle'></i>";
                    }

                } else {

                    var spacer = "<span style='display:inline-block;height:1px;width:" + (20 * (dataContext[CLEVEL])) + "px'></span>";
                    res = spacer;
                }
                res += Stemapp.util.format.text(value);

                return res;
            },

            tree_model_item: function (row, cell, value, columnDef, dataContext) {

                var CCOUNT = dataContext._model.childCountAttribute,
                    CLEVEL = dataContext._model.countLevelAttribute;


                value = value.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
                var res = '';
                var spacer;
                if (dataContext[CCOUNT] != 0) {

                    spacer = "<span style='display:inline-block;height:1px;width:" + (20 * (dataContext[CLEVEL] - 1)) + "px'></span>";

                    if (dataContext._collapsed) {

                        res = spacer + " <i class='icon icon-header-tree-e toggle'></i>";
                    } else {

                        res = spacer + " <i class='icon icon-header-tree-c toggle'></i>";
                    }

                } else {

                    spacer = "<span style='display:inline-block;height:1px;width:" + (20 * (dataContext[CLEVEL])) + "px'></span>";
                    res = spacer;
                }
                res += Stemapp.util.format.text(value);

                return res;
            },

            /*
             *
             *
             * For Admin
             *
             *
             * */



            userLogin: function (row, cell, value, columnDef, dataContext) {

                return '<a href="#/user/' + value + '">' + Stemapp.util.format.text(value) + '</a>';
            },

            userType: function (row, cell, value, columnDef, dataContext) {

                return Stemapp.ref.user.types[value] ? Stemapp.util.format.text(Stemapp.ref.user.types[value]) : 'Неизвестный';
            },


            userAbonent: function (row, cell, value, columnDef, dataContext) {

                return value ? '<a href="#!/abonent/' + dataContext.DB + '/' + dataContext.ABONENTGUID + '">' + value + '</a>' : '—';
            },

            deviceLastDate: function (row, cell, value, columnDef, dataContext) {

//                return value ? '<a href="#!/raw/' + dataContext.DB + '/' + dataContext.ABONENTGUID + '/">' + Stemapp.util.forma.datetime(value) + '</a>' : '—';
                return value ? ('<a href="#" class="js-raw">' + Stemapp.util.format.datetime(value) + '</a>') : '—';
            },

            countEdit: function (row, cell, value, columnDef, dataContext) {

                var val = '';
                if (parseInt(dataContext.MAXCOUNT, 10) > 1) {
                    val = '<div class="countValue">' + value + '</div>' +
                    '<div class="countSlider slider" data-max="' + dataContext.MAXCOUNT + '" data-value="' + value + '" data-id="' + dataContext.id + '"></div> ';
                } else {
                    val = '1';
                }

                return '<div class="tac">' + val + '</div>';
            },


            /**
             *
             *
             *  Universal
             *
             *
             * */

            cource: function (row, cell, value, columnDef, dataContext) {

                // for abonents
                if (_.isUndefined(dataContext.SSCource)) return '';

                // if null
                dataContext.SSCource = dataContext.SSCource ? dataContext.SSCource : '';

                dataContext.MOColor = dataContext.MOColor ? dataContext.MOColor : '1;1;0,7';
                var course = dataContext.SSCource.toString().replace(/[^\d]/g, ''),
                    hsl = dataContext.MOColor.replace(/,/g, '.').split(';'),
                    color = Stemapp.util.hsl2rgb(
                        (180 * (parseFloat(hsl[0]) || 1)) / 360,
                        1 * (parseFloat(hsl[1]) || 1),
                        0.5 * (parseFloat(hsl[2]) || 1)
                    ); // color is stored as multipliers for HSL values of Cyan
                var date = new Date();
                var dif = ((date.getTime() - date.getTimezoneOffset() * 1000) - dataContext.SSTimeFinish * 1000);
                var border = (dif / 1000 / 3600 / 24) > 7 ? 'icon-object-state-border' : '';

                return '<i class="icon icon-object-point" style="background-color:' + color + '">' +
                    '<i class="icon icon-object-course" style="' +
                    ('-moz-transform:rotate(*deg);-o-transform:rotate(*deg);-webkit-transform:rotate(*deg);transform:rotate(*deg);').replace(/\*/g, course) +
                    '">' +
                    '</i></i> <span>' + (dataContext.SSCource + 0) + '</span>';
            },

            voltage: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.wrapSpan(Stemapp.util.format.voltage(value));
            },

            temperature: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.wrapSpan(Stemapp.util.format.temperature(value));
            },

            quantity: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.wrapSpan(Stemapp.util.format.quantity(value));
            },

            boolean: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.wrapSpan(Stemapp.util.format.boolean(value));
            },

            volume: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.wrapSpan(Stemapp.util.format.volume(value));
            },

            length: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.wrapSpan(Stemapp.util.format.length(value));
            },

            geo: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.wrapSpan(Stemapp.util.format.geo(value));
            },

            degree: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.wrapSpan(Stemapp.util.format.degree(value));
            },

            state: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                var date = new Date();
                var state = dataContext.state;
                var dif = ((date.getTime() - date.getTimezoneOffset() * 1000) - dataContext.SSTimeFinish * 1000);
                var border = (dif / 1000 / 3600 / 24) > 7 ? 'icon-object-state-border' : '';

                return '<i class="icon icon-object-state icon-object-state-' + state + ' ' + border + '"></i><span>' + Stemapp.util.wrapSpan(Stemapp.util.format.state(value)) + '</span>';

            },

            velocity: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.wrapSpan(Stemapp.util.format.velocity(value));
            },

            time: function (row, cell, value, columnDef, dataContext) {


                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                var hours = Math.floor(value / 3600);
                var minutes = Math.floor(value / 60) % 60;
                var seconds = value % 60;

                return Stemapp.util.wrapSpan((hours == 0 ? '' : hours + 'ч ') + (minutes == 0 && hours == 0 ? '' : minutes + 'м ') + seconds + 'с');
            },

            percent: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '0';
                value = value ? parseFloat(value) : 0;
                return Stemapp.util.wrapSpan(value * 100);
            },


            time3: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.time(value)) : Stemapp.util.wrapSpan('—');
            },
            date: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.date(value)) : Stemapp.util.wrapSpan('—');
            },

            timepstamp_date: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.date(value, true)) : Stemapp.util.wrapSpan('—');
            },
            timepstamp_time: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.time(value, true)) : Stemapp.util.wrapSpan('—');
            },

            timepstamp_datetime: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.datetime(value, true)) : '';
            },

            timepstamp_dateUTC: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.dateUTC(value, true)) : Stemapp.util.wrapSpan('—');
            },
            timepstamp_timeUTC: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.timeUTC(value, true)) : Stemapp.util.wrapSpan('—');
            },

            timepstamp_datetimeUTC: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.datetimeUTC(value, true)) : '';
            },


            datetime: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.datetime(value)) : '';
            },


            jaxisFullDateToDateTime: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.datetime(Stemapp.util.convert.jaxisDatetimeToDate(value.slice(2, 14)).getTime() / 1000)) : '';
            },


            jaxisFullDateToTime: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.time(Stemapp.util.convert.jaxisDatetimeToDate(value.slice(2, 14)).getTime() / 1000)) : '';
            },

            jaxisFullDateToDate: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.date(Stemapp.util.convert.jaxisDatetimeToDate(value.slice(2, 14)).getTime() / 1000)) : '';
            },

            datetimeDashIfEmpty: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.format.datetime(value) : '—';
            },

            htmlDashIfEmpty: function (row, cell, value, columnDef, dataContext) {

                return (value && value != '') ? value : '—';
            },

            htmlWithZero: function (row, cell, value, columnDef, dataContext) {

                return value || value === 0 ? Stemapp.util.wrapSpan(value) : '';
            },

            code: function (row, cell, value, columnDef, dataContext) {

                return '<span class="guid">' + Stemapp.util.format.text(value) + '</span>';
            },

            boolPoint: function (row, cell, value, columnDef, dataContext) {

                return value ? '<div class="tac"><i class="icon icon-bool-yes"></i></div>' : '';
            },

            html: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(value) : '';
            },

            number: function (row, cell, value, columnDef, dataContext) {

                return Number(value);
            },

            journalUrl1: function (row, cell, value, columnDef, dataContext) {
                return value ? '<a href="' + value + '&Token=' + Stemapp.Token + '" target="_blank">Скачать</a>' : '';
            },

            weekday: function (row, cell, value, columnDef, dataContext) {
                var days =
                    {
                        1: 'Понедельник',
                        2: 'Вторник',
                        3: 'Среда',
                        4: 'Четверг',
                        5: 'Пятница',
                        6: 'Суббота',
                        7: 'Воскресенье'
                    }
                    ;
                return value ? Stemapp.util.wrapSpan(days[value]) : '';
            },

            money: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(value.toFixed(2)) : '0.00';
            },

            text: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.text(value)) : '';
            },

            link: function (row, cell, value, columnDef, dataContext) {
                return value ? Stemapp.util.wrapSpan('<a href="#" class="js-link js-' + columnDef.id + '" ' +
                'data-field="' + (columnDef.linkId ? columnDef.linkId : columnDef.id) + '">' + value + '</a>') : '';
            },

            currency : function (row, cell, value, columnDef, dataContext) {
                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.currency(value)) : '';
            }
        },


        exportFormatter: {

            jaxisFullDateToDateTime: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.format.datetime(Stemapp.util.convert.jaxisDatetimeToDate(value.slice(-12)).getTime() / 1000) : '';
            },

            /**
             * Universal
             */


            date: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.format.date(value) : '—';
            },

            datetime: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.format.datetime(value) : '—';
            },

            timepstamp_date: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.format.date(value, true) : '—';
            },
            timepstamp_time: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.format.time(value, true) : '—';
            },

            timepstamp_datetime: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.format.datetime(value, true) : '';
            },

            timepstamp_datetimeUTC: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.format.datetimeUTC(value, true) : '';
            },

            htmlDashIfEmpty: function (row, cell, value, columnDef, dataContext) {

                return (value && value != '') ? value : '—';
            },

            bool: function (row, cell, value, columnDef, dataContext) {

                return value == 'true' ? '1' : '0';
            },

            html: function (row, cell, value, columnDef, dataContext) {

                return value ? value.toString() : '';
            },

            url: function (row, cell, value, columnDef, dataContext) {

                return value ? location.origin + '/' + value.toString() : '';
                ;
            }
        },


        filterFormatter: {

            voltage: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.format.voltage(value);
            },

            temperature: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.format.temperature(value);
            },

            quantity: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.format.quantity(value);
            },

            boolean: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.format.boolean(value);
            },

            volume: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.format.volume(value);
            },

            length: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.format.length(value);
            },

            geo: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.format.geo(value);
            },

            degree: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.format.degree(value);
            },

            state: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.format.state(value);
            },

            velocity: function (row, cell, value, columnDef, dataContext) {

                if (typeof value == 'undefined') return '';
                value = value ? value : 0;
                return Stemapp.util.format.velocity(value);
            },

            date: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.format.date(value) : Stemapp.util.wrapSpan('—');
            },

            datetime: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.format.datetime(value) : '';
            },


            datetimeDashIfEmpty: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.format.datetime(value) : '—';
            },

            htmlDashIfEmpty: function (row, cell, value, columnDef, dataContext) {

                return (value && value != '') ? value : '—';
            },

            html: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(value) : '';
            },

            text: function (row, cell, value, columnDef, dataContext) {

                return value ? Stemapp.util.wrapSpan(Stemapp.util.format.text(value)) : '';
            },

            empty: function (row, cell, value, columnDef, dataContext) {
                return '';
            }
        },


        filter: {

            listTreeGrid: function (item) {
                var data = arguments[0],
                    table = item._table,
                    model = table.model,
                    items = table.dataView.getItems();

                if (item.parent != null && item.parent != 'undefined' && !item._table.noCollapse) {

                    var parent = data[table.dataView.getIdxById(item.parent)];

                    while (parent) {
                        if (parent._collapsed) {
                            return false;
                        }
                        parent = data[table.dataView.getIdxById(parent.parent)];
                    }
                }

                var res = false;

                function recSearch(id) {
                    var len = items.length;

                    for (var i = 0; i < len; i++) {
                        if (items[i].parent == id) {

                            itemSearch(items[i]);
                        }
                    }
                }

                function itemSearch(item) {

                    var l = model.columns.length,
                        search = item._table.searchString,
                        r1 = false,
                        r2 = true;

                    var formatter;
                    var j;
                    for (j = 0; j < l; j++) {
                        if (model.columns[j].inGrid) {
                            formatter = model.columns[j].exportFormatter ? model.columns[j].exportFormatter : model.columns[j].format;
                            //console.log(model.columns[j].field);
                            if (search != "" && formatter(table.dataView.getIdxById(item.id), j, item[model.columns[j].field] + '', model.columns[j], item).toLowerCase().indexOf(search) != -1) {
                                r1 = true;
                                break;
                            }
                        }
                    }
                    for (j = 0; j < l; j++) {
                        if (model.columns[j].inGrid) {
                            formatter = model.columns[j].exportFormatter ? model.columns[j].exportFormatter : model.columns[j].format;
                            var colSearch = item._table.columnSearch[model.columns[j].name];
                            if (typeof colSearch != 'undefined' && colSearch != "" && formatter(table.dataView.getIdxById(item.id), j, item[model.columns[j].field] + '', model.columns[j], item).toLowerCase().indexOf(colSearch) == -1) {
                                r2 = false;
                                break;
                            }
                        }
                    }

                    if (search == '') {
                        r1 = true;
                    }

                    res = res || (r1 && r2);
                    if (item.CCOUNT != 0) {
                        recSearch(item.id);
                    }
                }

                itemSearch(item);
                return res;
            },


            adminTreeGrid: function (item) {
                var data = arguments[0],
                    table = item._table,
                    model = table.model,
                    items = table.dataView.getItems();

                if (item.parent != null && item.parent != 'undefined' && !item._table.noCollapse) {

                    var parent = data[table.dataView.getIdxById(item.parent)];

                    while (parent) {
                        if (parent._collapsed) {
                            return false;
                        }
                        parent = data[table.dataView.getIdxById(parent.parent)];
                    }
                }

                var res = false;

                function recSearch(id) {
                    var len = items.length;

                    for (var i = 0; i < len; i++) {
                        if (items[i].parent == id) {

                            itemSearch(items[i]);
                        }
                    }
                }

                function itemSearch(item) {

                    var l = model.columns.length,
                        search = item._table.searchString,
                        r1 = false,
                        r2 = true;
                    var formatter;
                    var j;
                    for (j = 0; j < l; j++) {
                        if (model.columns[j].inGrid) {
                            formatter = model.columns[j].exportFormatter ? model.columns[j].exportFormatter : model.columns[j].format;
                            if (search != "" && formatter(table.dataView.getIdxById(item.id), j, item[model.columns[j].name] + '', model.columns[j], item).toLowerCase().indexOf(search) != -1) {
                                r1 = true;
                                break;
                            }
                        }
                    }
                    for (j = 0; j < l; j++) {
                        if (model.columns[j].inGrid) {
                            var colSearch = item._table.columnSearch[model.columns[j].name];
                            formatter = model.columns[j].exportFormatter ? model.columns[j].exportFormatter : model.columns[j].format;
                            if (typeof colSearch != 'undefined' && colSearch != "" && formatter(table.dataView.getIdxById(item.id), j, item[model.columns[j].name] + '', model.columns[j], item).toLowerCase().indexOf(colSearch) == -1) {
                                r2 = false;
                                break;
                            }
                        }
                    }

                    if (search == '') {
                        r1 = true;
                    }

                    res = res || (r1 && r2);
                    if (item.CCOUNT != 0) {
                        recSearch(item.id);
                    }
                }

                itemSearch(item);
                return res;
            },

            adminPlainGrid: function (item) {

                var data = arguments[0];

                var table = item._table,
                    model = table.model,
                    search = table.searchString,
                    r1 = false,
                    r2 = true,
                    res = false,
                    l = model.columns.length;


                var mode = 'in' + ( Stemapp.mode.charAt(0).toUpperCase() + Stemapp.mode.slice(1));
                var j;
                var formatter;
                for (j = 0; j < l; j++) {
                    if (model.columns[j][mode] || model.columns[j].inGrid) {
                        formatter = model.columns[j].exportFormatter ? model.columns[j].exportFormatter : model.columns[j].format;
                        if (search != "" && formatter(table.dataView.getIdxById(item.id), j, item[model.columns[j].name] + '', model.columns[j], item).toLowerCase().indexOf(search) != -1) {
                            r1 = true;
                            break;
                        }
                    }
                }
                for (j = 0; j < l; j++) {
                    if (model.columns[j][mode] || model.columns[j].inGrid) {
                        var colSearch = item._table.columnSearch[model.columns[j].name];
                        formatter = model.columns[j].exportFormatter ? model.columns[j].exportFormatter : model.columns[j].format;

                        if (typeof colSearch != 'undefined' && colSearch != "" &&
                            formatter(table.dataView.getIdxById(item.id), j, item[model.columns[j].name] + '', model.columns[j], item).toLowerCase().indexOf(colSearch) == -1) {
                            r2 = false;
                            break;
                        }
                    }
                }
                if (search == '') {
                    r1 = true;
                }
                res = (r1 && r2);
                return res;
            },

            adminRawGrid: function (item) {
                var data = arguments[0];
//
                if (!item._table.slick) {
                    return true;
                }
                var table = item._table,
                    search = table.searchString,
                    r1 = false,
                    r2 = true,
                    res = false,
                    columns = table.slick.getColumns(),
                    l = columns.length;
                var model = item._table.model;
                var j;
                var formatter;
                var funcs = [], f, find = false;
                for (j = 0; j < l; j++) {

                    if (columns[j].field !== 'checkbox') {

                        f = null;
                        find = false;
                        if (model) {
                            f = model.getColumnAtName(columns[j].field).exportFormatter ? model.getColumnAtName(columns[j].field).exportFormatter : model.getColumnAtName(columns[j].field).format;
                            find = true;
                        }

                        if (!_.isFunction(f)) {

                            if (_.isBoolean(item[columns[j].field])) {
                                f = Stemapp.grid.exportFormatter.bool;
                            } else {
                                f = Stemapp.grid.exportFormatter.html;
                            }
                        }
                        funcs[j] = f;
                    }
                }

                for (j = 0; j < l; j++) {
                    formatter = columns[j].exportFormatter ? columns[j].exportFormatter : columns[j].format;
                    if (search != "" && formatter(table.dataView.getIdxById(item.id), j, item[columns[j].name] + '', columns[j], item).toLowerCase().indexOf(search) != -1) {
                        r1 = true;
                        break;
                    }
                }
                for (j = 0; j < l; j++) {

                    var colSearch = item._table.columnSearch[columns[j].name];
                    var column = model.getColumnAtName(columns[j].name);

                    if (!_.isUndefined(colSearch) && colSearch != "" &&
                        funcs[j](table.dataView.getIdxById(item.id), j, item[columns[j].name] + '', columns[j], item).toLowerCase().indexOf(colSearch) == -1) {
                        r2 = false;
                        break;
                    }
                }

                if (search == '') {
                    r1 = true;
                }
                res = (r1 && r2);
                return res;
            }
        },

        filterTreeGrid: function (table, value, input) {
            if (value != $(input).val().toLowerCase()) {
                return;
            }
            table.searchString = value;
            table.dataView.refresh();
        },


        //todo перенести в Control Table
        getRealSelected: function (table) {

            var value = table.searchString;
            table.searchString = '';
            var rows = table.slick.getSelectedRows();
            table.searchString = value;
            return rows;
        },

        appendMetaData: function (table) {
            var metadata;
            if (table.dataView.getItemMetadata) {
                metadata = table.dataView.getItemMetadata;
            }

            table.dataView.getItemMetadata = function (row) {

                var met = {cssClasses: ''};
                if (metadata) {
                    met = metadata(row);
                    if (!met) {
                        met = {cssClasses: ''};
                    }
                }

                var item = table.dataView.getItem(row),
                    cls = '';

                if (item.red) {
                    cls += ' no';
                } else {
                    cls += ' yes';
                }

                if (item.gray) {
                    cls += ' gray';
                }

                if (item._delete) {
                    cls += ' delete';
                }

                if (item._css) {
                    cls += ' ' + item._css;
                }


                return $.extend(met, {cssClasses: cls + ' ' + met.cssClasses});
            };
        }


    });

});