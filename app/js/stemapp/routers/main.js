define([
    'backbone',
], function (Backbone) {
    return (Backbone.Router.extend({

        current: null,

        opts: {
            login: true
        },

        initialize: function (params) {
            this.opts = $.extend(this.opts, params);
        },

        /**
         * Для добавление роутов в приложение нужно
         * В инициализации модуля передать роуты Stemapp.modules[name].routes = {inner:{}, outer:{}};
         * inner - роуты, которые будут выполняться внутри приложения, после запуска панели (после входа пользователя)
         * outer - роуты, которые будут выполняться до запуска панели (до входа пользователя)
         *
         * inner и outer заполняются аналогично Backbone.Router http://backbonejs.org/#Router-routes
         * Пример:
         * inner : {
         *     routes : {
         *         "foo/:num" : "testFunc",
         *         "bar" : function(){}
         *     },
         *     "testFunc" : function(num){}
         * }
         */

        routes: {

            "signin": "signin",

            "registration": "registration",

            "remember": "remember",

            "confirm/:operation/:phone": "confirm",

            "confirm/:operation": "confirm",

            "demo": "demo"
        },


        setLogin: function (login) {

            this.opts.login = login;
        },

        signin: function () {
            this.open('signin');
        },

        registration: function () {
            this.open('registration');
        },

        remember: function () {
            this.open('remember');
        },

        open: function (name) {
            name = name ? name : 'signin';
            this.current = null;
            if (this.opts.login) return;
            this.changePage(name);
            this.trigger('routes', name);
        },

        confirm: function (operation, phone) {
            this.current = null;
            if (this.opts.login) return;
            this.changePage('confirm');
            this.trigger('routes', 'confirm', operation, phone);
        },

        demo: function () {
            this.trigger('demo');
        },

        changePage: function (name) {
            this.current = name;
        }

    }));
});