define([], function () {
    return {

        inner : {
            routes: {

                ":num/help": "help",

                ":num/history": "history",

                ":num/list": "list",

                ":num/fields/:namespace/:model": "fields",

                ":num/role": "role",

                ":num/client": "client",

                ":num/record/:namespace/:model/:id": "recordNsp",

                ":num/record/:model/:id": "record",

                ":num/record/:model": "record",

                ":num/meta/:method/:id": "meta",

                ":num/auto/:method/:id": "auto",

                ":num/clients/:guid/:new": "client_form",

                ":num/respondents/:guid": "respondents",

                ":num/respondents/:guid/:new": "respondent",

                ":num/maintainers/:guid": "maintainers",

                ":num/maintainers/:guid/:new": "maintainer",

                ":num/tariffs/:guid": "tariffs",

                ":num/tariffs/:guid/:new": "tariff",

                ":num/worktimes/:guid": "worktimes",

                ":num/worktimes/:guid/:new": "worktime",

                ":num/resourcetypes": "resourcetypes",

                ":num/resourcetypes/:new": "resourcetype",

                ":num/sensors/:guid": "sensors",

                ":num/sensors/:guid/:new": "sensor",

                ":num/chartbox": "chartbox",

                ":num/chartbox/:guid": "chartbox",

                ":num/sensors_templates/:guid/:id": "sensors_templates",

                ":num/sensors_templates/:guid/:id/:new": "sensors_template",


                ":num/forms": "forms",

                ":num/forms/:id/:new": "form_create",

                ":num/forms/:id": "form",

                ":num/menus": "menus",

                ":num/menus/:id/:new": "menu_create",

                ":num/menus/:id": "menu",

                ":num/form_fields/:id": "form_fields",

                ":num/form_fields/:frame/:id/:field": "form_field",

                ":num/form_fields/:frame/:id": "form_field",

                ":num/records/:name": "records",

                ":num/records/:name/grid": "records",

                ":num/records/:name/form/:id": "record",

                ":num/records/:name/grid/:id": "records"

            },

            unknown: function (splat) {
                console.log('unknown page Inner "'+ splat+ '"');
            },

            clients: function (num) {
                this.trigger('open', num, 'clients');
            },

            client_form: function (num, guid, isNew) {
                this.trigger('open', num, 'client', guid, isNew);
            },

            forms: function (num) {
                console.log('forms', this);
                this.trigger('open', num, 'forms');
            },

            form: function (num, id) {
                this.trigger('open', num, 'form', id);
            },

            form_create: function (num, id, isNew) {
                this.trigger('open', num, 'form', id, isNew);
            },

            menus: function (num) {
                this.trigger('open', num, 'menus');
            },

            menu: function (num, id) {
                this.trigger('open', num, 'menu', id);
            },

            menu_create: function (num, id, isNew) {
                this.trigger('open', num, 'menu', id, isNew);
            },

            form_fields: function (num, id) {
                this.trigger('open', num, 'Form_Fields', id);
            },

            form_field: function (num, frame, id, field) {
                this.trigger('open', num, 'Form_Field', frame, id, field);
            },

            records: function (num, name, id) {
                this.trigger('open', num, 'meta/records', name, id);
            },

            record: function (num, name, id) {
                this.trigger('open', num, 'meta/record', name, id);
            },

            respondents: function (num, guid) {
                this.trigger('open', num, 'respondents', guid);
            },

            respondent: function (num, guid, isNew) {
                this.trigger('open', num, 'respondent', guid, isNew);
            },

            maintainers: function (num, guid) {
                this.trigger('open', num, 'maintainers', guid);
            },

            maintainer: function (num, guid, isNew) {
                this.trigger('open', num, 'maintainer', guid, isNew);
            },

            tariffs: function (num, guid) {
                this.trigger('open', num, 'tariffs', guid);
            },

            tariff: function (num, guid, isNew) {
                this.trigger('open', num, 'tariff', guid, isNew);
            },

            worktimes: function (num, guid) {
                this.trigger('open', num, 'worktimes', guid);
            },

            worktime: function (num, guid, isNew) {
                this.trigger('open', num, 'worktime', guid, isNew);
            },

            resourcetypes: function (num, guid) {
                this.trigger('open', num, 'resourcetypes', guid);
            },

            resourcetype: function (num, guid, isNew) {
                this.trigger('open', num, 'resourcetype', guid, isNew);
            },

            sensors: function (num, guid) {
                this.trigger('open', num, 'sensors', guid);
            },

            sensor: function (num, guid, isNew) {
                this.trigger('open', num, 'sensor', guid, isNew);
            },

            sensors_templates: function (num, guid, id) {
                this.trigger('open', num, 'sensor_Templates', guid, id);
            },

            sensors_template: function (num, guid, id, isNew) {
                this.trigger('open', num, 'sensor_Template', guid, id, isNew);
            },

            chartbox: function (num, guid) {
                this.trigger('open', num, 'chartbox', guid);
            },

            raw: function (num, server, guid, id) {
                this.trigger('open', num, 'raw', server, guid, id);
            },

            meta: function (num, method, id) {
                this.trigger('open', num, 'meta', 'meta', method, id);
            },

            auto: function (num, method, id) {
                this.trigger('open', num, 'meta', 'auto', method, id);
            },

            help: function (num) {
                this.trigger('open', num, 'help');
            },

            history: function (num) {
                this.trigger('open', num, 'history');
            },

            list: function (num) {
                this.trigger('open', num, 'list');
            },

            fields: function (num, namespace, model) {
                this.trigger('open', num, 'fields', namespace, model);
            },

            role: function (num, namespace, model, field, value) {
                this.trigger('open', num, 'role');
            },

            client: function (num, namespace, model, field, value) {
                this.trigger('open', num, 'client');
            }
        }

    };
});