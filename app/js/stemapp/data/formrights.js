define([
    '_app/data/init'
], function () {
    'use strict';


    Stemapp.relation.formrights = {

        Id: "FormRightId",
        FormId: "FormId",
        FormName: "FormName",
        RoleId: "RoleId",
        RoleName: "RoleName",
        IsCreate: "IsCreate",
        IsRead: "IsRead",
        IsUpdate: "IsUpdate",
        IsDelete: "IsDelete",
        Filter: "Filter"

    };
    Stemapp.data.formrights = {

        columns: [
            {
                name: Stemapp.relation.formrights.Id,
                caption: Stemapp.relation.formrights.Id,
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                format: Stemapp.grid.formatter.html
            },
            {
                name: Stemapp.relation.formrights.FormId,
                caption: "FormId",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 200,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.formrights.FormName,
                caption: "Название формы",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 250,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.formrights.RoleId,
                caption: "RoleId",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 200,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.formrights.RoleName,
                caption: "Название роли",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 200,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.formrights.IsCreate,
                caption: "IsCreate",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 100,
                format: Stemapp.grid.formatter.bool,
                exportFormatter: Stemapp.grid.exportFormatter.bool
            },
            {
                name: Stemapp.relation.formrights.IsRead,
                caption: "IsRead",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 100,
                format: Stemapp.grid.formatter.bool,
                exportFormatter: Stemapp.grid.exportFormatter.bool
            },
            {
                name: Stemapp.relation.formrights.IsUpdate,
                caption: "IsUpdate",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 100,
                format: Stemapp.grid.formatter.bool,
                exportFormatter: Stemapp.grid.exportFormatter.bool
            },
            {
                name: Stemapp.relation.formrights.IsDelete,
                caption: "IsDelete",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 100,
                format: Stemapp.grid.formatter.bool,
                exportFormatter: Stemapp.grid.exportFormatter.bool
            },
            {
                name: Stemapp.relation.formrights.Filter,
                caption: "Filter",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 100,
                format: Stemapp.grid.formatter.bool,
                exportFormatter: Stemapp.grid.exportFormatter.bool
            }
        ]
    };

    Stemapp.data.formrights.getColumnAtName = function (name) {

        var l = Stemapp.data.formrights.columns.length;
        for (var i = 0; i < l; i++) {
            if (Stemapp.data.formrights.columns[i].name == name) {
                return Stemapp.data.formrights.columns[i];
            }
        }
        return {
            format: function (val) {
                return val;
            }
        };
    }


});