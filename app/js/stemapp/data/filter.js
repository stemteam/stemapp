define([
    '_app/data/init'
], function () {
    'use strict';


    Stemapp.relation.filter = {

        Id: "FilterId",
        Field: "f",
        Operation: "o",
        Value: "v",
        Type: "type",
        IdParent: "parent",
        CCOUNT: "ccount",
        CLEVEL: "level"
    };
    Stemapp.data.filter = {

        columns: [
            {
                name: Stemapp.relation.filter.Field,
                caption: "Field",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 200,
                format: Stemapp.grid.formatter.tree_item,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.filter.Operation,
                caption: "Operation",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 250,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.filter.Value,
                caption: "Value",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 200,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.filter.Type,
                caption: "Type",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 200,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            }

        ]
    };

    Stemapp.data.filter.getColumnAtName = function (name) {

        var l = Stemapp.data.filter.columns.length;
        for (var i = 0; i < l; i++) {
            if (Stemapp.data.filter.columns[i].name == name) {
                return Stemapp.data.filter.columns[i];
            }
        }
        return {
            format: function (val) {
                return val;
            }
        };
    }


});