define([
    '_app/data/init'
], function () {
    'use strict';


    Stemapp.relation.formfields = {

        Id: "FormFieldId",
        FormId: "FormId",
        FormName: "FormName",
        FieldId: "FieldId",
        FieldName: "FieldName",
        Caption: "Caption",
        OrderNum: "OrderNum",
        RelationFormId: "RelationFormId",
        RelationFieldId: "RelationFieldId",
        RelationFormName: "RelationFormName",
        RelationFieldName: "RelationFieldName",
        RelationFormType: "RelationFormType",
        IsOrdered: "IsOrdered",
        IsOrderedDesc: "IsOrderedDesc",
        IsPrimary: "IsPrimary",
        IsHidden: "IsHidden",
        IsReadonly: "IsReadonly"

    };
    Stemapp.data.formfields = {

        columns: [
            {
                name: Stemapp.relation.formfields.Id,
                caption: Stemapp.relation.formfields.Id,
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                format: Stemapp.grid.formatter.html
            },
            {
                name: Stemapp.relation.formfields.FormId,
                caption: "Форма",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 200,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.formfields.FieldName,
                caption: "Поле",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 250,
                linkId: Stemapp.relation.formfields.FieldId,
                format: Stemapp.grid.formatter.link,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.formfields.Caption,
                caption: "Заголовок",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 200,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.formfields.OrderNum,
                caption: "Порядок вывода",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 200,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.formfields.RelationFormName,
                caption: "Форма связи",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 100,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.formfields.RelationFieldId,
                caption: "Поле связи",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 100,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },

            {
                name: Stemapp.relation.formfields.RelationFormType,
                caption: "Форма типа выбора",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 100,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.formfields.IsOrdered,
                caption: "IsOrdered",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 100,
                format: Stemapp.grid.formatter.bool,
                exportFormatter: Stemapp.grid.exportFormatter.bool
            },
            {
                name: Stemapp.relation.formfields.IsOrderedDesc,
                caption: "IsOrderedDesc",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 100,
                format: Stemapp.grid.formatter.bool,
                exportFormatter: Stemapp.grid.exportFormatter.bool
            },
            {
                name: Stemapp.relation.formfields.IsPrimary,
                caption: "IsPrimary",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 100,
                format: Stemapp.grid.formatter.bool,
                exportFormatter: Stemapp.grid.exportFormatter.bool
            },
            {
                name: Stemapp.relation.formfields.IsHidden,
                caption: "IsHidden",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 100,
                format: Stemapp.grid.formatter.bool,
                exportFormatter: Stemapp.grid.exportFormatter.bool
            },
            {
                name: Stemapp.relation.formfields.IsReadonly,
                caption: "IsReadonly",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 100,
                format: Stemapp.grid.formatter.bool,
                exportFormatter: Stemapp.grid.exportFormatter.bool
            }
        ]
    };

    Stemapp.data.formfields.getColumnAtName = function (name) {

        var l = Stemapp.data.formfields.columns.length;
        for (var i = 0; i < l; i++) {
            if (Stemapp.data.formfields.columns[i].name == name) {
                return Stemapp.data.formfields.columns[i];
            }
        }
        return {
            format: function (val) {
                return val;
            }
        };
    }


});