define([
    '_app/data/init'
], function () {
    'use strict';


    Stemapp.relation.forms = {

        Id: "FormId",
        Name: "Name",
        Description: "Description",
        FieldName: "FieldName",
        ParentFieldName: "ParentFieldName",
        MethodId: "MethodId",
        IdParent: "ParentId",
        ListDescription: "ListDescription",
        OrderNum: "OrderNum",
        CCOUNT: "Ccount",
        CLEVEL: "Clevel"

    };
    Stemapp.data.forms = {

        columns: [
            {
                name: Stemapp.relation.forms.Id,
                caption: Stemapp.relation.forms.Id,
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                format: Stemapp.grid.formatter.html
            },
            {
                name: Stemapp.relation.forms.Name,
                caption: "Имя на латинице",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 200,
                format: Stemapp.grid.formatter.tree_item,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.forms.Description,
                caption: "Название в форме",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 150,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.forms.ListDescription,
                caption: "Название в списке",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 150,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.forms.OrderNum,
                caption: "Порядок сортировки на форме",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 150,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.forms.FieldName,
                caption: "FieldName",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 150,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.forms.ParentFieldName,
                caption: "ParentFieldName",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 150,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.forms.MethodId,
                caption: "MethodId",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 150,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            }
        ]
    };


    Stemapp.data.forms.getColumnAtName = function (name) {

        var l = Stemapp.data.forms.columns.length;
        for (var i = 0; i < l; i++) {
            if (Stemapp.data.forms.columns[i].name == name) {
                return Stemapp.data.forms.columns[i];
            }
        }
        return {
            format: function (val) {
                return val;
            }
        };
    }


});