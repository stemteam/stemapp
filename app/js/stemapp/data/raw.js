define([
    '_app/data/init'
], function () {
    'use strict';


    Stemapp.relation.raw = {
        Id: ''
    };

    Stemapp.data.raw = {

        columns: []
    };

    Stemapp.data.raw.getColumnAtName = function (name) {
        var l = Stemapp.data.raw.columns.length;
        for (var i = 0; i < l; i++) {
            if (Stemapp.data.raw.columns[i].name == name) {
                return Stemapp.data.raw.columns[i];
            }
        }

        return {
            format: function (val) {
                return val;
            }
        };
    }

});