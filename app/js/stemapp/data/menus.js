define([
    '_app/data/init'
], function () {
    'use strict';


    Stemapp.relation.menu = {

            Id: "MenuId",
            Name: "MenuName",
            MenuNum: "MenuNum",
            MenuTooltip: "MenuTooltip",
            FormId: "FormId",
            FormName: "FormName",
            RoleId: "RoleId",
            RoleName: "RoleName",
            RoleDescription: "RoleDescription",
            IdParent: "MenuParentId",
            Param: "Param",
            ViewType: "ViewType",
            Icon: "Icon",
            MenuUrl: "MenuUrl",
            CCOUNT: 'MenuCcount',
            CLEVEL: 'MenuClevel'

    };
    Stemapp.data.menu = {

        columns: [
            {
                name: Stemapp.relation.menu.Id,
                caption: Stemapp.relation.menu.Id,
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                format: Stemapp.grid.formatter.html
            },
            {
                name: Stemapp.relation.menu.Name,
                caption: "Название",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 200,
                format: Stemapp.grid.formatter.tree_item,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.menu.Icon,
                caption: "Иконка",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 200,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.menu.MenuTooltip,
                caption: "Всплывающая подсказка",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 150,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.menu.FormName,
                caption: "Форма",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 150,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.menu.MenuUrl,
                caption: "Ссылка",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 150,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.menu.MenuNum,
                caption: "Порядок",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 200,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.menu.RoleName,
                caption: "Роль",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 150,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.menu.RoleDescription,
                caption: "Описание роли",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 150,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.menu.Param,
                caption: "Параметр",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 150,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            },
            {
                name: Stemapp.relation.menu.IsGrid,
                caption: "Табличная ли форма",
                inGrid: true,
                inDetail: false,
                inHint: false,
                inSummary: false,
                parent: 0,
                width: 150,
                format: Stemapp.grid.formatter.html,
                exportFormatter: Stemapp.grid.exportFormatter.html
            }
        ]
    };


    Stemapp.data.menu.getColumnAtName = function (name) {

        var l = Stemapp.data.menu.columns.length;
        for (var i = 0; i < l; i++) {
            if (Stemapp.data.menu.columns[i].name == name) {
                return Stemapp.data.menu.columns[i];
            }
        }
        return {
            format: function (val) {
                return val;
            }
        };
    }


});