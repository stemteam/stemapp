define([
    '_app/data/init'
], function () {
    'use strict';


    Stemapp.relation.list = {
        Id: ''
    };

    Stemapp.data.list = {

        columns: []
    };

    Stemapp.data.list.getColumnAtName = function (name) {
        var l = Stemapp.data.list.columns.length;
        for (var i = 0; i < l; i++) {
            if (Stemapp.data.list.columns[i].name == name) {
                return Stemapp.data.list.columns[i];
            }
        }

        return {
            format: function (val) {
                return val;
            }
        };
    }

});