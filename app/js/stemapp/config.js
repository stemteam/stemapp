Stemapp.global = {};
Stemapp.config = {
    /*
     Iif salt is string, then password+salt;
     If salt is array, then array[0]+password+array[1]
     For example:
     salt : 'string salt'
     salt : ['array','salt']
     */
    salt: '',
    hashPassMethod: 'md5b64', //string(sha1/md5b64)
    apiKey: '11FD5AFD76243AE5448CD9E073D842B95D34D258651D33898C486937AC58079F',
    apiVersion: 9,
    registerEnable: true, //boolean
    registerByInvite: false, //boolean
    demoEnable: false, //boolean
    demoLogin: 'demo@demo.ru', //string
    demoPassword: 'demo', //string
    restorePasswordEnable: true, //boolean
    startPage: '',
    supportPage: '',
    helpUrl: '',
    platform: '',
    showStartPage: false,
    showStartMenu: false,
    askBeforeExit: true,
    autoMenu: false,
    menuTitle: 'Личный кабинет',
    title: 'Личный кабинет',
    cookieDomain: null,
    base_url: '/',
    useLocalStorage: true, // Включено ли использование сохранения вкладок в Local Storage

    menu: {
        'software': false
    }
};
Stemapp.config.ServiceName = '';