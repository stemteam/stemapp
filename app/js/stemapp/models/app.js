define([
    'backbone',
    'cookie',
    '_services/tranny'
], function (Backbone) {
    return Backbone.Model.extend({

        defaults: {
            roleCount: null,
            rights: [],
            errorCheckTokenXHR: null,
            context: 'api',
            method: 'Login'
        },

        errors: {},

        queues: [],

        letters: ['', 'D', 'A', 'C', 'LLS', 'W', 'CAN'],

        limitChannel: [0, 8, 2, 2, 8, 8, 1],

        diffValue: 2,

        defMaxValue: 7,

        maxValue: 9,

        initialize: function (opts) {

            Stemapp.roles = [];
            this.rights = [];

            this.kladr = {};
            this.kladr.token = '53760250fca916b7583e34e2';
            this.kladr.key = '7cb0d59d19baacf9f4c8456ed340293c7be1db3b';

            this.initQueue('general', _.bind(this.initApply, this));
            this.initQueue('afterLogin', _.bind(this.completeAfterLoginQueue, this));
            // Предзагрузка модулей
            for (var i in Stemapp.modules) {
                if (Stemapp.modules.hasOwnProperty(i)) {

                    if (Stemapp.modules[i].model) {
                        Stemapp.modules[i].boot = new Stemapp.modules[i].model({app: this, view: opts.view, name: i});
                    }
                }
            }

            Stemapp.mode = 'admin';

            this.addQueue('general', function () {

                if (this.guid) {
                    I.jaxis({
                        action: 'DeviceModelsGet',
                        data: {},
                        error: _.bind(this.error, this),
                        success: _.bind(this.loadModelApply, this)
                    });
                } else
                    this.completeQueue('general');
            }, this);

            this.addQueue('general', function () {

                if (this.guid) {
                    I.jaxis({
                        action: 'UserServersGet',
                        data: {},
                        error: _.bind(this.error, this),
                        success: _.bind(this.applyServersGet, this)
                    });
                } else
                    this.completeQueue('general');
            }, this);

            this.addQueue('general', function () {

                if (this.guid) {
                    I.jaxis({
                        action: 'AbonentsGet',
                        data: {
                            Guid: this.guid
                        },
                        service: 'map',
                        error: _.bind(this.error, this),
                        success: _.bind(this.applyLoadAbonents, this)
                    });
                } else
                    this.completeQueue('general');
            }, this);

        },


        init: function () {
            this.appCheckToken();
        },

        appCheckToken: function () {
            I.jaxis({
                action: 'UserCheckToken',
                service: 'map',
                data: {},
                error: _.bind(this.resCheckToken, this),
                success: _.bind(this.resCheckToken, this)
            });
        },

        resCheckToken: function (data) {
            if (!(data.status == '404')) {
                data = (new I.Tranny(data)).pop();
                this.guid = data ? data['UsrGuid'] : '';
                Stemapp.GUID = this.guid;
            }

            this.execQueue('afterLogin');
            //this.myRoleListGet();

        },

        getUserServices: function (callback) {

            if (typeof callback != 'function')
                return false;

            if (Stemapp.userServices) {
                callback(Stemapp.userServices);
            } else {
                api.call({
                    method: 'UserServicesListGet',
                    namespace: false,
                    success: function (data) {
                        Stemapp.userServices = data;
                        Stemapp.event.sendEvent(null, 'loadServices');
                        callback(Stemapp.userServices);
                    },
                    error: function (data) {
                        Stemapp.userServices = data;
                        callback(Stemapp.userServices);
                    }
                });
            }
        },


        myRoleListGet: function () {

            api.call({
                method: 'MyRoleListGet',
                namespace: 'admin',
                error: _.bind(this.errorMyRoleList, this),
                success: _.bind(this.successMyRoleList, this)
            });
        },

        successMyRoleList: function (data) {
            if (data.length) {
                Stemapp.roles = data;
                this.execQueue('general');
            }
            else {
                this.trigger('error', {}, 'Не найдено ни одной роли для данного пользователя');
                Stemapp.App.logout();
                setTimeout(function () {
                    location.reload();
                }, 5000);
            }

        },

        errorMyRoleList: function (xhr) {
            Stemapp.App.logout();
            location.reload();
            if (xhr) {
                this.requestError(xhr);
            }
        },

        sync: function (type, model, options) {

        },

        error: function (xhr, status) {
            $('#errorLoad').show().addClass('showScale');
            $('#preload').hide();
        },


        //Get user roles
        userRoleListGet: function () {
            if ($.cookie('token')) {
                I.jaxis({
                    action: 'MyRoleListGet',
                    service: 'crafter',
                    data: {},
                    error: _.bind(this.errorMyRoleList, this),
                    success: _.bind(this.successMyRoleList, this)
                });
            }
        },


        checkRight: function (checkRight) {
            var result = false;

            this.rights.forEach(function (right) {
                if (right.RightName == checkRight) {
                    result = true;
                    return;
                }
            });
            return result;
        },

        successMyRights: function (data) {
            data = (new I.Tranny(data));

            data.forEach(function (right) {
                this.rights.push(right);
            }, this);

            this.roleCount--;

            //Running application if all rights loaded
            if (!this.roleCount) {
                //this.execQueue('general');
                this.setAccess('crafter');
            }

        },

        errorMyRights: function (xhr) {
            this.setAccess('crafter');
            if (xhr) {
                this.requestError(xhr);
            }
        },


        initQueue: function (name, callback) {
            this.queues[name] = {};
            this.queues[name].funcs = [];
            this.queues[name].count = 0;
            this.queues[name].callback = callback;
        },

        addQueue: function (name, func, context) {

            this.queues[name].funcs.push(_.bind(func, context));
        },

        execQueue: function (name) {


            var count = this.queues[name].funcs.length;
            this.maxValue = this.defMaxValue + this.diffValue;
            this.value = this.diffValue;
            for (var i = 0; i < count; i++) {
                this.queues[name].funcs[i]();
            }
            if (count == 0) {
                this.queues[name].callback();
            }
        },

        completeQueue: function (name) {

            this.queues[name].count++;
            this.value++;
            window.Boot.setProgress(this.maxValue, this.value);
            if (this.queues[name].funcs.length == this.queues[name].count) {
                this.queues[name].callback();
            }
        },

        clearQueue: function (name) {

            this.queues[name] = [];
        },

        loadModelApply: function (data) {

            Stemapp.models = new I.Tranny(data);
            this.completeQueue('general');
        },

        applyLoadAbonents: function (data) {
            data = new I.Tranny(data);
            this.trigger('payed', data[0].ABPayed);
            this.completeQueue('general');
        },


        applyServersGet: function (data) {

            this.data = new I.Tranny(data);

            I.jaxis({
                action: 'UserAbonentsGet',
                data: {},
                service: 'auth',
                error: _.bind(this.error, this),
                success: _.bind(this.applyUserAbonentsGet, this)
            });
        },

        applyUserAbonentsGet: function (data) {

            Stemapp.me = (new I.Tranny(data)).pop();

            Stemapp.isAdmin = $.cookie('isAdmin');
            Stemapp.userType = $.cookie('Type');

            Stemapp.Token = $.cookie('token');

            $.tmpl.global({
                userType: Stemapp.userType,
                uType: Stemapp.opts.userType
            });

            Stemapp.DB = {
                list: this.data,
                getName: _.bind(function (id) {

                    for (var o in this.data) {
                        if (this.data[o].DB == id)
                            return this.data[o].DBNAME;
                    }
                    return '';
                }, this)
            };

            //todo выделить в модуль навигации
            if (!Stemapp.ref) {
                Stemapp.ref = {};
            }
            if (!Stemapp.ref.device) {
                Stemapp.ref.device = {};
            }
            if (!Stemapp.ref.device.sensor) {
                Stemapp.ref.device.sensor = {};
            }

            Stemapp.ref.device.sensor.list = [];

            this.initQueue('device', _.bind(this.completeQueue, this, 'general'));
            var j = 1;
            for (var i in Stemapp.ref.device.sensor.types) {


                Stemapp.ref.device.sensor.list[j - 1] = {id: i, type: Stemapp.ref.device.sensor.types[i]};

                _.bind(function (j, i) {

                    this.addQueue('device', function () {
                        I.jaxis({
                            action: 'DeviceSensorsGet',
                            data: {
                                Model: 1,
                                Type: i
                            },
                            error: _.bind(this.error, this),
                            success: _.bind(this.applyDeviceSensorGet, this, j)
                        });
                    }, this);

                }, this, j, i)();
                j++;
            }
            this.execQueue('device');
        },

        applyDeviceSensorGet: function (i, data) {

            i--;
            var response = new I.Tranny(data),
                find;

            // Данные приходят отсортированными но названию, и пункт Пусто где-то в середине. Вытаскиваем его
            var count = response.length;
            for (var j = 0; j < count; j++) {
                if (response[j].ID == 0) {

                    var empty = response.slice(j, j + 1);
                    response = empty.concat(response.slice(0, j).concat(response.slice(j + 1)));
                    break;
                }
            }

            if (!Stemapp.sensors) {
                Stemapp.sensors = [];
            }

            var groupId = parseInt(Stemapp.ref.device.sensor.list[i].id),
                letter = this.letters[groupId];


            for (var k = 0; k < response.length; k++) {
                find = false;
                if (response[k].ID == 0) {
                    continue;
                }
                if (response[k].NAME == 'Топливо 1 (Ч)') {
                    response[k].NAME = 'Топливо 1';
                }

                for (var j = 0; j < Stemapp.sensors.length; j++) {

                    if (Stemapp.sensors[j].name == response[k].NAME) {

                        find = true;
                        Stemapp.sensors[j].type.push(letter);
                        break;
                    }
                }
                if (!find) {
                    Stemapp.sensors.push({
                        name: response[k].NAME,
                        type: [letter],
                        count: 1,
                        fuel: response[k].NAME.indexOf('Топливо') != -1
                    });
                }
            }

            Stemapp.ref.device.sensor.list[i].sensors = response;
            Stemapp.ref.device.sensor.list[i].letter = letter;

            var limit = this.limitChannel[groupId];
            Stemapp.ref.device.sensor.list[i].channel = [];

            for (var sensor = 1; sensor < limit + 1; sensor += 1) {
                if (!((groupId === 1) && ((sensor === 5) || (sensor === 6)))) {
                    Stemapp.ref.device.sensor.list[i].channel.push({
                        channel_id: sensor,
                        channel_label: '0' + sensor,
                        channel_sensors: Stemapp.ref.device.sensor.list[i].sensors,
                        invertible: (groupId === 1)
                    });
                }
            }
            this.completeQueue('device');
        },

        completeAfterLoginQueue: function () {
            this.getUserServices(_.bind(this.myRoleListGet, this));//Получение сервисов доступных пользователю
        },

        initApply: function () {
            window.Boot.setProgress(this.maxValue, this.maxValue);
            this.trigger('loadApp', this.data);
        },

        requestError: function (xhr) {
            switch (xhr.status) {
                case 503:
                    this.trigger('error', {}, 'Происходит обновление сервиса, пожалуйста подождите' + '<br/>' + this.getErrorMessage(xhr.responseText));
                    this.reloadPage();
                    break;
                case 502:
                    this.trigger('error', {}, 'Сервер недоступен' + '<br/>' + this.getErrorMessage(xhr.responseText));
                    break;
                case 400:
                    this.trigger('error', {}, 'Неправильные параметры' + '<br/>' + this.getErrorMessage(xhr.responseText));
                    break;
                case 403:
                    //this.trigger('error', {}, 'Нет доступа'+'<br/>'+this.getErrorMessage(xhr.responseText));
                    this.trigger('error', {}, '<br/>' + this.getErrorMessage(xhr.responseText));
                    break;
                case 401:
                    this.trigger('error', {}, 'Пользователь неавторизован' + '<br/>' + this.getErrorMessage(xhr.responseText));
                    this.trigger('unauthorized');
                    break;
                default:
                    if (xhr.responseText) {
                        var js,
                            m = '';
                        try {
                            js = $.parseJSON(xhr.responseText);
                            m = js.error.match(/-=#([^#]+)#=-/);
                            m = (m && m[1]) ? m[1].trim() : m;
                        } catch (e) {
//                            m = xhr.responseText.toString().trim();
                        }
                    }
                    if (m) {
                        this.trigger('error', {}, m);
                    } else {
                        var o = I.jaxis.lastOperation,
                            url = window.location.protocol + '//' + window.location.hostname + o.url + '?' + Stemapp.util.paramToLink(o.data),
                            response = xhr.responseText;

                        try {
                            js = $.parseJSON(response);
                            if (js.error) {
                                response = js.error;
                            }
                        } catch (e) {

                        }
                        response = response.toString().trim().split('\n');
                        this.sendError(response[0], ['[b]Url:[/b] ' + url, '[b]Page:[/b] ' + window.location, '[b]Data:[/b] [code]' + JSON.stringify(o.data)].join('\n\n') + '[/code]\n\n' +
                            '[b]' + response[0] + '[/b]\n[code]' + response.join('\n') + '[/code]');

                        this.trigger('fatalError', xhr);

                    }
                    break;
            }
        },

        sendError: function (title, message) {

            var url = 'bugreport/Ticket.new',
                post = {
                    type: 'bug',
                    summary: Base64.encode(title),
                    message: Base64.encode(message),
                    auto: 1,
                    project: Stemapp.config.project,
                    apikey: Stemapp.config.bugKey,
                    field_os: 'Web'
                };

            $.post(url, post, function (text) {
            });
        },

        getErrorMessage: function (responseText) {
            try {
                js = $.parseJSON(responseText);
            }
            catch (exception) {
                console.log(exception);
                return '';
            }
            var js,
                m = '';

            m = js.error ? js.error.match(/-=#([^#]+)#=-/) : '';
            m = (m && m[1]) ? m[1].trim() : m;
            return m;
        },

        reloadPage: function () {
            setTimeout(function () {
                window.location.href = Stemapp.config.base_url;
                ;
            }, 10000);
        }
    });
});