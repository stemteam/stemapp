define([
    '_app/models/templates/main',
    'cookie'
], function (Model_Template) {
    'use strict';
    return Model_Template.extend({

        defaults: {
            FirstName: '',
            LastName: '',
            MiddleName: '',
            Phone: '',
            Email: '',
            Password: ''
        },

        initialize: function () {

        },

        validate: function (attrs, options) {

            this.errors = [];

            if (Stemapp.config.registerByInvite) {
                if (attrs.Invite == '') {
                    this.errors.push({field: "invite", text: 'Введите инвайт'});
                }
            } else {
                if (attrs.LastName == '') {
                    this.errors.push({field: "lastName", text: 'Введите фамилию'});
                }

                if (attrs.FirstName == '') {
                    this.errors.push({field: "firstName", text: 'Введите имя'});
                }

                if (attrs.Phone == '') {
                    this.errors.push({field: "mobilePhone", text: 'Введите мобильный телефон'});
                }
            }


            if (attrs.Email == '') {
                this.errors.push({field: "email", text: 'Введите email'});
            }

            if (attrs.Password == '') {
                this.errors.push({field: "password", text: 'Введите пароль'});
            }

            if (this.errors.length) {
                return this.errors;
            }

        },

        sync: function (type, model, options) {
            this.set('Password', Stemapp.util.getPassHash(Stemapp.config.hashPassMethod, this.attributes['Password'].trim(), Stemapp.config.salt));

            this.attributes['Host'] = window.location.origin;
            this.attributes['ServiceName'] = window.location.origin;

            api.call({
                method: 'Register',
                namespace: false,
                data: this.attributes,
                error: _.bind(this.requestError, this),
                success: _.bind(this.success, this)
            });

        },

        success: function (options, data) {
            var time = (new Date()).getTime() + 31536000000;
            $.cookie('email', this.attributes['Email'], {expires: time});
            this.trigger('apply', {Phone: this.get('Phone')});
        }

    });
});