define([
    '_app/models/templates/main',
], function (Model_Template) {
    'use strict';

    /**
     * @class Model_Role
     * @constructor
     * @extends Model_Template_Main
     */
    var Model_Role = Model_Template.extend({

        defaults: {},

        new: false,

        initialize: function (opts) {

        },

        load: function () {
            var data = {Limit: 1000000, Offset: 0};

            api.call({
                method: 'SysroleListGet',
                namespace: 'auto',
                data: data,
                error: _.bind(this.requestError, this),
                success: _.bind(function (data) {
                    this.trigger('loadedRoles', data);
                }, this)
            });

        },

        loadMetaRoles: function (attr) {
            attr = attr ? attr : {};
            api.call({
                method: 'RoleListGet',
                namespace: 'meta',
                data: attr.data ? attr.data : {},
                error: attr.error || _.bind(this.requestError, this),
                success: attr.success || _.bind(this.success, this)
            });
        },

        loadAllRoles: function () {

            api.call({
                method: 'RoleListGet',
                namespace: 'admin',
                error: _.bind(this.requestError, this),
                success: _.bind(function (data) {
                    this.trigger('loadAllRoles', data);
                }, this)
            });

        },

        addRole: function (user, role) {
            this.loadUsers('add', user, role);
        },

        removeRole: function (user, role) {
            this.loadUsers('remove', user, role);
        },

        loadUsers: function (action, user, role) {
            var data = {Limit: 1000000, Offset: 0};

            api.call({
                method: 'ClientListGet',
                namespace: 'auto',
                data: data,
                error: _.bind(this.requestError, this),
                success: _.bind(function (users) {
                    this.checkUser(action, users, user, role);
                }, this)
            });
        },

        checkUser: function (action, users, user, role) {
            user = user.trim();

            for (var i = 0; i < users.length; i++) {
                if (users[i].mobile_phone == user || users[i].email == user) {
                    //this.callMethod(action, users[i].mobile_phone, role);
                    this.callMethod(action, user, role);
                    return true;
                }
            }

            this.trigger('error', this, [{field: "user", text: 'Пользователь не найден.'}]);
        },

        callMethod: function (action, ClientId, RoleId) {

            var data = {
                RoleName: RoleId, //admin role id
                Login: ClientId
            };

            api.call({
                method: (action == 'add') ? 'GrantRole' : 'RevokeRole',
                namespace: 'back',
                data: data,
                error: _.bind(this.requestError, this),
                success: _.bind(function (data) {
                    this.trigger('success', action);
                }, this)
            });

        }

    });

    return Model_Role;
});