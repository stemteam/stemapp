define([
    '_app/models/templates/main',
], function (Model_Template_Main) {
    'use strict';

    /**
     * Модель для работы с формами
     * @class Model_SmartForm
     * @constructor
     * @extends Model_Template_Main
     */
    var Model_SmartForm = Model_Template_Main.extend({

        defaults: {},

        errors: {},

        new: false,


        initialize: function (opts) {

            this.new = opts ? opts.new : false;
        },

        /**
         * Получение метаданных полей по описанию формы
         * @param attr
         * @param {object} attr.data Данные
         * @param {function} attr.error Callback ошибки
         * @param {function} attr.success Callback удачного выполнения
         * @constructor
         */
        SmartFormFieldListGet: function (attr) {
            api.call({
                method: 'SmartFormFieldListGet',
                namespace: 'meta',
                data: attr.data,
                error: attr.error || _.bind(this.requestError, this),
                success: attr.success || _.bind(this.success, this)
            });
        },

        /**
         * Метод строит данные по описанию форм и фрэймов, возвращает DataSet с описанными в них полями
         * @param attr
         * @param {object} attr.data Данные
         * @param {function} attr.error Callback ошибки
         * @param {function} attr.success Callback удачного выполнения
         * @constructor
         */
        SmartFormListGet: function (attr) {
            api.call({
                method: 'SmartFormListGet',
                namespace: 'meta',
                data: attr.data,
                error: attr.error || _.bind(this.requestError, this),
                success: attr.success || _.bind(this.success, this)
            });
        },

        sync: function (type, model, options) {
            if (this.new) {
                api.call({
                    method: 'SmartFormAdd',
                    namespace: 'meta',
                    data: this.attributes,
                    error: options.error || _.bind(this.error, this),
                    success: options.success || _.bind(this.successAdd, this)
                });
            }
            else {

                api.call({
                    method: 'SmartFormEdit',
                    namespace: 'meta',
                    data: this.attributes,
                    error: options.error || _.bind(this.error, this),
                    success: options.success || _.bind(this.successAdd, this)
                });
            }
        },

        delete: function (item, attr) {

            api.call({
                method: 'SmartFormDelete',
                namespace: 'meta',
                data: attr.data,
                error: attr.error ? _.bind(attr.error, this, item) : _.bind(this.errorDelete, this),
                success: attr.success ? _.bind(attr.success, this, item) : _.bind(this.successDelete, this)
            });
        },

        success: function (data) {
            this.trigger('apply', data);
        },

        successAdd: function (data) {
            this.trigger('applyAdd', data);
        },

        successDelete: function (item, xhr) {
            this.trigger('applyDelete', item);
        },

        errorDelete: function (item, xhr) {
            this.trigger('errorDelete', item);
            this.requestError(xhr);
        }


    });
    return Model_SmartForm;
});
