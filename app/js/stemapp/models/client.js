define([
    '_app/models/templates/main',
], function (Model_Template) {
    'use strict';
    return Model_Template.extend({

        defaults: {},

        new: false,

        initialize: function (opts) {

        },

        load: function () {

        },

        removeUser: function (user) {
            this.loadUsers(user);
        },


        loadUsers: function (user) {
            var data = {Limit: 1000000, Offset: 0};

            api.call({
                method: 'ClientListGet',
                namespace: 'auto',
                data: data,
                error: _.bind(this.requestError, this),
                success: _.bind(function (users) {
                    this.checkUser(users, user);
                }, this)
            });
        },

        checkUser: function (users, user) {
            user = user.trim();

            for (var i = 0; i < users.length; i++) {
                if (users[i].mobile_phone == user || users[i].email == user) {
                    //this.callMethod(action, users[i].mobile_phone, role);
                    this.callMethod(user);
                    return true;
                }
            }

            this.trigger('error', this, [{field: "user", text: 'Пользователь не найден.'}]);
        },

        callMethod: function (user) {

            var data = {
                Login: user
            };

            api.call({
                method: 'UserDelete',
                namespace: 'back',
                data: data,
                error: _.bind(this.requestError, this),
                success: _.bind(function () {
                    this.trigger('success');
                }, this)
            });

        }

    });
});