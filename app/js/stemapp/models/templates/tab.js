define([
    '_app/models/templates/main',
], function (Model_Template_Main) {


    /**
     * @class Model_Template_Tab
     * @constructor
     * @extends Model_Template_Main
     */
    var Model_Template_Tab = Model_Template_Main.extend({

        successDelete: function (item, xhr) {
            this.trigger('applyDelete', item);
        },

        errorDelete: function (item, xhr) {
            this.trigger('errorDelete', item);
            this.requestError(xhr);
        }
    });

    return Model_Template_Tab;
});