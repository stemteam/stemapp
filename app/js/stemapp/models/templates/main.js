define([
    'backbone',
    '_jq/jquery.base64.min'
], function (Backbone) {

    /**
     * @class Model_Template_Main
     * @constructor
     * @extends Backbone.Model
     */
    return Backbone.Model.extend({

        apiVersion: 2,

        errors: [],

        requestError: function (xhr) {
            var errors;
            var json;
            switch (xhr.status) {
                case 503:
                    this.trigger('error', {}, 'Происходит обновление сервиса, пожалуйста подождите' + '<br/>' + this.getErrorMessage(xhr.responseText));
                    break;
                case 502:
                    this.trigger('error', {}, 'Сервер недоступен' + '<br/>' + this.getErrorMessage(xhr.responseText));
                    break;
                case 400:

                    try {

                        json = $.parseJSON(xhr.responseText);
                        if (json && json.length) {

                            errors = [];
                            for (var i = 0; i < json.length; i++) {
                                errors.push({field: json[i].ExceptionObject, text: json[i].Message, code: 400});
                            }

                        }
                        if (json && json.cols && json.rows && json.types) {
                            var t = I.Tranny(json);
                            errors = [];
                            for (i = 0; i < t.length; i++) {
                                errors.push({field: t[i].ExceptionObject, text: t[i].Message, code: 400});
                            }

                        }
                        this.trigger('error', this, errors);
                    } catch (e) {

                    }
                    break;
                case 404:
                    this.trigger('error', {}, this.getErrorMessage(xhr.responseText));
                    break;
                case 403:
                    this.trigger('error', {}, 'Нет доступа' + '<br/>' + this.getErrorMessage(xhr.responseText));
                    break;
                case 401:
                    this.trigger('error', {}, 'Пользователь неавторизован' + '<br/>' + this.getErrorMessage(xhr.responseText));
                    this.trigger('unauthorized');
                    break;
                default:
                    var m, response;
                    if (xhr.responseText) {
                        m = this.getErrorMessage(xhr.responseText);
                    }

                    if (m) {
                        this.trigger('error', {}, m);
                    } else {
                        response = xhr.responseText;
                        json = $.parseJSON(response);
                        errors = [];
                        var operation = '';
                        if (json.error) {  // старый формат json
                            operation = I.jaxis.lastOperation;
                            errors.push({
                                title: json.error.toString().trim().split('\n')[0],
                                error: json.error
                            });
                        } else { // новый формат json2
                            operation = api.lastOperation;
                            for (i = 0; i < json.length; i++) {
                                errors.push({
                                    title: json[i].Exception + ' ' + json[i].CallStack.toString().trim().split('\n')[0],
                                    error: json[i].CallStack
                                });
                            }
                        }
                        var url = '';
                        if (operation) {
                            url = window.location.protocol + '//' + window.location.hostname + operation.url + '?' + Stemapp.util.paramToLink(operation.data);
                        }

                        var errorMessage = ['[b]Url:[/b] ' + url, '[b]Page:[/b] ' + window.location, '[b]Data:[/b] [code]' + (operation ? JSON.stringify(operation.data) : '') + '[/code]'].join('\n\n');

                        for (i = 0; i < errors.length; i++) {
                            errorMessage += '\n\n' + '[b]' + errors[i].title + '[/b]\n[code]' + errors[i].error + '[/code]'
                        }


                        this.sendError(errors[0].title, errorMessage);

                        this.trigger('fatalError', xhr);

                    }
                    break;
            }
        },

        error: function (options, xhr) {
            this.requestError(xhr);
            if (options.error) {
                options.error(xhr);
            }
        },

        success: function (options, xhr) {

            if (options.success) {
                options.success(xhr);
            }
        },

        sendError: function (title, message) {

            var url = 'bugreport/Ticket.new',
                post = {
                    type: 'bug',
                    summary: Base64.encode(title),
                    message: Base64.encode(message),
                    auto: 1,
                    project: Stemapp.config.project,
                    apikey: Stemapp.config.bugKey,
                    field_os: 'Web'
                };

            $.post(url, post, function (text) {
            });
        },

        getMessage: function (responseText) {
            var responseJson = JSON.parse(responseText);
            return (responseJson[0] && responseJson[0].Message) ? responseJson[0].Message : '';
        },

        getErrorMessage: function (response) { //console.log(JSON.parse(response));
            var resText,
                m = '';
            try {
                resText = Array.isArray(JSON.parse(response)) ? JSON.parse(response)[0] : JSON.parse(response);
            } catch (e) {
                return false;
            }

            if (resText) {
                m = resText.Message;
                if (!m) {
                    resText.error = resText.error ? resText.error : resText.CallStack;
                    if (!resText.error) {
                        m = 'Ошибка севриса';
                    } else {
                        m = resText.error.match(/-=#([^#]+)#=-/);
                        m = (m && m[1]) ? m[1].trim() : m;
                    }
                }
            } else {
                m = response.toString().trim();
            }

            return m;
        },

        url: function () {
            return this.get('context') + '/' + this.get('method') + '.json2';
        }


    });
});