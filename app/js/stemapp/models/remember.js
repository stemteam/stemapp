define([
    '_app/models/templates/main',
], function (Model_Template) {
    'use strict';
    return Model_Template.extend({
        defaults: {

            Phone: ''
        },

        errors: {},

        initialize: function () {

        },

        validate: function (attrs, options) {

            this.errors = [];

            if (attrs.Login == '') {
                this.errors.push({field: "Login", text: 'Введите мобильный телефон или email'});
            }

            if (this.errors.length) {
                return this.errors;
            }
        },

        sync: function (type, model, options) {
            this.attributes.Host = window.location.host;
            this.attributes.ServiceName = Stemapp.config.ServiceName;
            api.call({
                method: 'ForgotPassword',
                namespace: false,
                data: this.attributes,
                error: _.bind(this.requestError, this),
                success: _.bind(this.success, this, options)
            });

        },

        success: function () {
            this.trigger('apply', {Phone: this.get('Phone')});
        },

        validateChangePassword: function (attr) {

            this.errors = [];
            if (attr.OldPasswd == '') {
                this.errors.push({field: "change-pass-current", text: 'Укажите старый пароль'});
            }
            if (attr.NewPasswd == '') {
                this.errors.push({field: "change-pass-new", text: 'Укажите пароль'});
            }
            if (attr.NewPasswd2 == '') {
                this.errors.push({field: "change-pass-repeat", text: 'Подтвердите пароль'});
            }
            if (attr.NewPasswd != attr.NewPasswd2) {

                this.errors.push({field: "change-pass-new", text: 'Пароли не совпадают'});
                this.errors.push({field: "change-pass-repeat", text: 'Пароли не совпадают'});
            }

            if (this.errors.length) {
                this.trigger('error', this, this.errors);
                return false;
            }
            return true;
        },

        changePassword: function (attr) {
            if (!this.validateChangePassword(attr)) {
                return false;
            }
            attr['NewPassword'] = Stemapp.util.getPassHash(Stemapp.config.hashPassMethod, attr['NewPasswd'], Stemapp.config.salt);
            attr['NewPasswordConfirm'] = Stemapp.util.getPassHash(Stemapp.config.hashPassMethod, attr['NewPasswd2'], Stemapp.config.salt);
            attr['CurrentPassword'] = Stemapp.util.getPassHash(Stemapp.config.hashPassMethod, attr['OldPasswd'], Stemapp.config.salt);

            delete attr['NewPasswd'];
            delete attr['NewPasswd2'];
            delete attr['OldPasswd'];

            api.call({
                method: 'ChangePassword',
                namespace: false,
                data: attr,
                error: _.bind(this.requestError, this),
                success: _.bind(this.successSync, this)
            });

        },

        successSync: function (options, response, data) {
            this.trigger('successChangePass', response);
        }


    });

});