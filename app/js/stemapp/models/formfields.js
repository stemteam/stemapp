define([
    '_app/models/templates/main',
], function (Model_Template_Main) {
    'use strict';

    /**
     * @class Model_Form_Fields
     * @constructor
     * @extends Model_Template_Main
     */
    var Model_Form_Fields = Model_Template_Main.extend({

        defaults: {},

        errors: {},

        new: false,


        initialize: function (opts) {

            this.FormId = opts ? opts.FormId : null;
            this.new = opts ? opts.new : false;
        },

        load: function (attr) {
            attr = attr ? attr : {};
            api.call({
                method: 'SmartFormFieldListGet',
                namespace: 'meta',
                data: $.extend({FormId: this.FormId}, attr.data ? attr.data : {}),
                error: attr.error || _.bind(this.requestError, this),
                success: attr.success || _.bind(this.success, this)
            });
        },

        getById: function (attr) {

            api.call({
                method: 'FormFieldGet',
                namespace: 'meta',
                data: attr.data,
                error: _.bind(this.requestError, this),
                success: attr.success || _.bind(this.success, this)
            });
        },

        fieldGetById: function (attr) {
            api.call({
                method: 'FieldGet',
                namespace: 'meta',
                data: attr.data,
                error: _.bind(this.requestError, this),
                success: attr.success || _.bind(this.success, this)
            });
        },

        applyCopy: function () {

        },

        copyError: function () {
            this.load_fields++;
            this.checkCopy();
        },

        copySuccess: function () {
            this.load_fields++;
            this.checkCopy();
        },

        checkCopy: function () {
            if (this.load_fields == this.count_fields) {
                this.applyCopy();
            }
        },

        validate: function (attr) {
            this.errors = [];
            if (this.errors.length) {
                return this.errors;
            }
        },

        sync: function (type, model, opts) {

            if (this.new) {
                api.call({
                    method: 'FormFieldAdd',
                    namespace: 'meta',
                    data: this.attributes,
                    error: _.bind(opts.error ? opts.error : this.requestError, this),
                    success: _.bind(opts.success ? opts.success : this.successAdd, this)
                });
            }
            else {
                api.call({
                    method: 'FormFieldEdit',
                    namespace: 'meta',
                    data: this.attributes,
                    error: _.bind(opts.error ? opts.error : this.requestError, this),
                    success: _.bind(opts.success ? opts.success : this.successAdd, this)
                });
            }
        },

        delete: function (item, id) {

            var attr = {};
            attr[id] = item[id];
            api.call({
                method: 'FormFieldDelete',
                namespace: 'meta',
                data: attr,
                error: _.bind(this.errorDelete, this, item),
                success: _.bind(this.successDelete, this, item)
            });
        },

        success: function (data) {
            this.trigger('apply', data);
        },

        successAdd: function (data) {
            this.trigger('applyAdd', data);
        },

        successDelete: function (item, xhr) {
            this.trigger('applyDelete', item);
        },

        errorDelete: function (item, xhr) {
            this.trigger('errorDelete', item);
            this.requestError(xhr);
        }


    });
    return Model_Form_Fields;
});