define([
    '_app/models/templates/main',
    'cookie'
], function (Model_Template) {
    'use strict';
    return Model_Template.extend({

        defaults: {},

        initialize: function (opts) {

        },

        validate: function (attrs) {

            this.errors = [];

            if (attrs.Login == '') {
                this.errors.push({field: "email", text: 'Укажите email/телефон'});
            }
            if (attrs.Password == '') {
                this.errors.push({field: "pass", text: 'Укажите пароль'});
            }

            if (this.errors.length) {
                return this.errors;
            }
        },

        sync: function () {
            this.attributes.ClientUrl = window.location.host;
            api.call({
                method: 'Login',
                namespace: false,
                data: this.attributes,
                error: _.bind(this.requestError, this),
                success: _.bind(this.success, this)
            });

        },

        success: function (response) {
            var time = (new Date()).getTime() + 31536000000;
            $.cookie('username', this.attributes['Username'], {expires: time});

            if (response && (response['token'] || response['Token'])) {

                Stemapp.App.trigger('logStatus', true);
                Stemapp.App.login(response);
                if (!this.simple) {
                    Stemapp.App.init();
                }
            }

            this.trigger('apply', response);
        }

    });
});