define([
    '_app/template/epoxy/model/main'
], function (Model_Template_Main) {
    'use strict';

    /**
     * @class Model_Empty
     *
     * @extends Model_Template_Main
     */
    var Model_Empty = Model_Template_Main.extend({

        idAttribute: 'Id',

        defaults: {

        }

    });
    return Model_Empty;
});