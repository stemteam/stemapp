define([
    '_app/models/templates/main',
], function (Model_Template_Main) {
    'use strict';

    /**
     * @class Model_Meta
     * @constructor
     * @extend Model_Template_Main
     */
    var Model_Meta = Model_Template_Main.extend({

        defaults: {},

        ApiKey: '11FD5AFD76243AE5448CD9E073D842B95D34D258651D33898C486937AC58079F',

        initialize: function (opts) {

            if (opts && opts.fields && opts.method) {
                this.fields = opts.fields;
                this.method = opts.method;
            }

        },

        isMethodAllowed: function (method, options) {
            api.call({
                method: 'IsMethodAllowed',
                namespace: 'meta',
                data: {Method: '/auto/' + method},
                error: options.error || _.bind(this.error, this),
                success: options.success || _.bind(this.success, this)
            });
        },

        getMethodMeta: function (method, options) {

            var action = this.getMethodName(method, 'get');
            api.call({
                method: 'MethodGet',
                namespace: 'meta',
                data: {
                    MethodName: 'auto/' + action,
                    ApiKey: this.ApiKey
                },
                error: options.error || _.bind(this.error, this),
                success: options.success || _.bind(this.success, this)
            });
        },

        loadFromFrame: function (method, attr) {

            var data = method.split('/');
            this.load(data[0], data[1].replace(/Get$/, ''), attr);

        },

        load: function (namespace, method, attr) {

            var action = this.getMethodName(method);

            this.success = attr.success || this.success;

            this.apply = _.after(2, _.bind(this.apply, this));

            this.loadMeta(namespace, action, attr);
            if (attr.new) {
                this.successLoadData([]);
            } else {
                this.loadData(namespace, action, attr);
            }
        },


        getMethodName: function (method) {
            var action = method.charAt(0).toUpperCase() + method.slice(1) + 'Get';
            return action;

        },

        loadData: function (namespace, action, attr) {
            api.call({
                method: action,
                namespace: namespace,
                data: $.extend(true, {Limit: 10000, Offset: 0}, attr.data),
                error: attr.error || _.bind(this.error, this),
                success: _.bind(this.successLoadData, this)
            });
        },


        /**
         * Загружаем метаданные для функции
         * @param namespace
         * @param action
         * @param attr
         */
        loadMeta: function (namespace, action, attr) {

            console.log('loadMeta', this);
            action = action.replace(/ListGet$/, 'Get');
            api.call({
                method: 'FieldListGet',
                namespace: 'meta',
                data: {
                    MethodName: namespace + '/' + action,
                    ApiKey: this.ApiKey
                },
                error: attr.error || _.bind(this.error, this),
                success: _.bind(this.successLoadMeta, this, attr, namespace, action)
            });
        },

        successLoadData: function (data) {
            this.data = data;
            this.apply();
        },

        successLoadMeta: function (attr, namespace, action, meta) {
            this.meta = meta;
            console.log('successLoadMeta', this);
            if (!attr.data && attr.id) {

                var data = {};
                for (var i in this.meta) {
                    if (this.meta[i].IsId) {
                        data[this.meta[i].Name] = attr.id;
                        break;
                    }
                }
                this.loadData(namespace, action, $.extend(attr, {data: data}));
            }
            this.apply();
        },


        // Переопределяется через функцию after
        apply: function () {
            var id = null,
                captions = [];
            for (var i = 0; i < this.meta.length; i++) {
                if (this.meta[i].IsId) {
                    id = this.meta[i].Name;
                }
                if (this.meta[i].IsCaption) {
                    captions.push(this.meta[i].Name);
                }
                if (!this.meta[i].Presentation) {

                    if (this.meta[i].RelationMethod && this.meta[i].RelationField) {
                        this.meta[i].Presentation = 'DataSet';
                    } else {
                        this.meta[i].Presentation = 'Text';
                    }
                }
            }
            // Для списка
            if (this.data.length) {
                for (i = 0; i < this.data.length; i++) {
                    if (id) {
                        this.data[i].Id = this.data[i][id];
                    }
                    if (captions.length) {

                        this.data[i]._Caption = '';
                        for (var j = 0; j < captions.length; j++) {

                            if (this.data[i][captions[j]]) {
                                this.data[i]._Caption += ' ' + this.data[i][captions[j]];
                            }

                        }

                    }

                }
            } else {
                // Для объекта
                if (id) {
                    this.data.Id = this.data[id];
                }
                if (captions.length) {
                    this.data._Caption = '';
                    for (var j = 0; j < captions.length; j++) {
                        if (this.data[captions[j]]) {
                            this.data._Caption += ' ' + this.data[captions[j]];
                        }
                    }
                }
            }
            this.success(this.data, this.meta);
        },

        validate: function (attr, options) {


            this.errors = [];

            for (var fieldName in attr) {

                this.fields.forEach(function (meta) {
                    if (meta.Name == fieldName && !this.validateOnMeta(meta, attr[fieldName])) {
                        this.errors.push({field: fieldName, text: 'Поле не заполнено'});
                        return;
                    }
                }, this);
            }

            if (this.errors.length) {
                return this.errors;
            }
        },

        validateOnMeta: function (meta, value) {
            if (meta.IsMandatory && !value) {
                return false
            } //Если поле обязательно, то оно должно быть заполнено


            return true;
        },

        sync: function (type, model, options) {
            var method;

            this.new = true;
            for (var i  in this.fields) {
                if (this.fields[i].IsId) {
                    this.new = !this.attributes[this.fields[i].Name];
                }
            }

            var namespace = this.attributes.namespace;

            delete this.attributes.fields;
            delete this.attributes.method;
            delete this.attributes.namespace;

            if (this.new) {
                method = this.method + 'Add';
                api.call({
                    method: method,
                    namespace: namespace,
                    data: this.attributes,
                    error: options.error,
                    success: options.success
                });
            } else {
                method = this.method + 'Edit';
                api.call({
                    method: method,
                    namespace: namespace,
                    data: this.attributes,
                    error: options.error,
                    success: options.success
                });
            }
        },

        delete: function (data, options) {
            var method = this.method + 'Delete';
            api.call({
                method: method,
                namespace: this.attributes.namespace,
                data: data,
                error: options.error,
                success: options.success
            });
        },

        success: function () {
            // Переопределяется через переданные параметры
        }


    });
    return Model_Meta;
});