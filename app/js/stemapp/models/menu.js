define([
    '_app/models/templates/main',
], function (Model_Template_Main) {
    'use strict';


    /**
     * @class Model_Menu
     * @constructor
     * @extends Model_Template_Main
     */
    var Model_Menu = Model_Template_Main.extend({

        defaults: {},

        errors: {},

        new: false,


        initialize: function (opts) {

            this.new = opts ? opts.new : false;
        },

        load: function (attr) {
            attr = attr ? attr : {};
            attr.data = attr.data ? attr.data : {};

            api.call({
                method: 'MenuListGet',
                namespace: 'meta',
                data: attr.data,
                error: attr.error || _.bind(this.requestError, this),
                success: attr.success || _.bind(this.success, this)
            });
        },

        getById: function (attr) {

            api.call({
                method: 'MenuGet',
                namespace: 'meta',
                data: attr.data,
                error: _.bind(this.requestError, this),
                success: attr.success || _.bind(this.success, this)
            });
        },

        validate: function (attr) {

            this.errors = [];

            if (this.errors.length) {
                return this.errors;
            }
        },


        sync: function (type, model, opts) {

            if (this.new) {
                api.call({
                    method: 'MenuAdd',
                    namespace: 'meta',
                    data: this.attributes,
                    error: opts.error ? opts.error : _.bind(this.error, this),
                    success: opts.success ? opts.success : _.bind(this.successAdd, this)
                });
            }
            else {

                api.call({
                    method: 'MenuEdit',
                    namespace: 'meta',
                    data: this.attributes,
                    error: opts.error ? opts.error : _.bind(this.error, this),
                    success: opts.success ? opts.success : _.bind(this.successAdd, this)
                });
            }
        },

        delete: function (item, id) {

            var attr = {};
            attr[id] = item[id];
            api.call({
                method: 'MenuDelete',
                namespace: 'meta',
                data: attr,
                error: _.bind(this.errorDelete, this, item),
                success: _.bind(this.successDelete, this, item)
            });
        },

        success: function (data) {
            this.trigger('apply', data);
        },

        successAdd: function (data) {
            this.trigger('applyAdd', data);
        },

        successDelete: function (item, xhr) {
            this.trigger('applyDelete', item);
        },

        errorDelete: function (item, xhr) {
            this.trigger('errorDelete', item);
            this.requestError(xhr);
        }


    });
    return Model_Menu;
});