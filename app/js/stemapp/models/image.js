define([
    '_app/models/templates/main',
    'cookie'
], function (Model_Template) {
    'use strict';


    /**
     * @class Model_Image
     * @constructor
     * @extends Model_Template_Main
     */
    var Model_Image = Model_Template.extend({

        defaults: {},

        url: '/api/doctor/ImageAdd.json2',

        initialize: function (opts) {

        },

        getImage: function (attr) {
            api.call({
                method: 'ImageUrlGet',
                namespace: 'doctor',
                data: attr.data,
                error: attr.error || _.bind(this.requestError, this),
                success: attr.success || _.bind(this.success, this)
            });
        },

        fileupload: function (file, callback) {

            var $file = $(file);
            var $parent = $file.parent();
            var id = '1';
            var idframe = 'file' + parseInt(Math.random() * 1000);

            // Create Iframe
            var $iframe = $('<iframe>').attr({id: idframe, name: idframe, src: '#'}).addClass('hide').load(function () {

                var text = $(this).contents().text();
                $file.data('change', true);
                $parent.append($file);
                //  $file.appendTo(fsb._form);
                $form.remove();

                // Хук, в фоксе если сразу удалить, то будет иконка в виде загрузки страницы
                setTimeout(function () {
                    $(this).remove();
                }, 100);

                callback(JSON.parse(text));
            });

            // Create form
            var $form = $('<form>').attr({
                action: this.url,
                enctype: "multipart/form-data",
                target: idframe,
                method: 'POST'
            }).addClass('hide').submit(function () {
            });

            // Add new id
            $form.append($('<input>').attr({type: 'hidden', name: 'id'}).val(id))
                .append($file);
            $file.attr('name', 'Image');
            $form.append($('<input>').attr({type: 'hidden', name: 'Version'}).val(api.version));
            $form.append($('<input>').attr({type: 'hidden', name: 'Token'}).val(api.token));
            $form.append($('<input>').attr({type: 'hidden', name: 'ApiKey'}).val(api.apiKey));


            // Append Form and Iframe to Body
            $('body').append($iframe).append($form);

            $form.submit();
        }
    });

    return Model_Image;
});