define([
    '_app/models/templates/main',
], function (Model_Template) {
    'use strict';
    return Model_Template.extend({

        defaults: {},

        new: false,

        initialize: function (opts) {
            /*this.modelName = opts.model;
             this.namespace = opts.namespace;
             this.new = opts.id ? false : true;*/
        },

        load: function () {
            this.getClientId(api.token);
        },

        getClientId: function (token) {
            api.call({
                method: 'ClientLoginListGet',
                namespace: 'auto',
                data: {
                    id_client_login: token
                },
                error: _.bind(this.requestError, this),
                success: _.bind(function (data) {
                    data = Array.isArray(data) ? data[0] : data;
                    this.getClient(data.id_client);
                }, this)
            });
        },

        getClient: function (clientId) {
            api.call({
                method: 'ClientGet',
                namespace: 'auto',
                data: {
                    id_client: clientId
                },
                error: _.bind(this.requestError, this),
                success: _.bind(function (data) {
                    data = Array.isArray(data) ? data[0] : data;
                    this.client = data;
                    this.getExpertId(data.id_client);
                }, this)
            });

        },

        getExpertId: function (clientId) {
            api.call({
                method: 'ExpertListGet',
                namespace: 'auto',
                data: {
                    id_client: clientId
                },
                error: _.bind(this.requestError, this),
                success: _.bind(function (data) {
                    data = Array.isArray(data) ? data[0] : data;
                    this.expert = data;
                    this.getExpertSpecialityList(data.id_expert);
                    //this.trigger('loadedData', {client: this.client, expert: this.expert});
                }, this)
            });
        },

        getExpertSpecialityList: function (expertId) {
            api.call({
                method: 'ExpertSpecialityListGet',
                namespace: 'auto',
                data: {
                    id_expert: expertId
                },
                error: _.bind(this.requestError, this),
                success: _.bind(function (data) {
                    this.expertSpeciality = data;
                    this.getSpecialityList();
                }, this)
            });
        },

        getSpecialityList: function () {
            api.call({
                method: 'SpecialityListGet',
                namespace: 'auto',
                data: {
                    count: 0,
                    limit: 10000000
                },
                error: _.bind(this.requestError, this),
                success: _.bind(function (data) {
                    this.specialityList = data;
                    this.getOwnSpeciality();
                }, this)
            });
        },

        getOwnSpeciality: function () {
            var length = this.expertSpeciality.length;
            var specListLength = this.specialityList.length;
            this.ownSpeciality = new Array();

            for (var i = 0; i < length; i++) {
                for (var j = 0; j < specListLength; j++) {
                    if (this.expertSpeciality[i].id_speciality == this.specialityList[j].id_speciality)
                        this.ownSpeciality.push(this.specialityList[j].name);
                }
            }

            this.expert.speciality = this.ownSpeciality.join(', ');

            this.trigger('loadedData', {client: this.client, expert: this.expert, specialityList: this.specialityList});
        },

        successSync: function (options, response, data) {
            this.trigger('success', response);
        },

        validateOnMeta: function (meta, value) {
            if (meta.IsMandatory && !value) {
                return false
            } //Если поле обязательно, то оно должно быть заполнено


            return true;
        },

        getChanges: function (src, dst) {
            var changed = 0;

            for (var field in src) {
                if (src[field] != dst[field]) {
                    changed++;
                    dst[field] = src[field];
                }
            }
            return changed;
        },

        getSpecialityChanges: function (src, dst) {
            src = _.uniq(src.split(', '));
            dst = _.uniq(dst.split(', '));
            var toDel = _.difference(dst, src);
            var toAdd = _.difference(src, dst);

            return {
                add: toAdd,
                delete: toDel
            };
        },

        getSpecialityId: function (name) {
            for (var item in this.specialityList) {
                if (this.specialityList[item].name == name)
                    return this.specialityList[item].id_speciality;
            }
        },

        getExpertSpecialityId: function (idSpeciality) {
            for (var item in this.expertSpeciality) {
                if (this.expertSpeciality[item].id_speciality == idSpeciality)
                    return this.expertSpeciality[item].id_expert_speciality;
            }
        },

        sync: function (type, model, options) {

            var speciality = this.attributes.expert.speciality;
            delete this.attributes.expert.speciality;
            var specialityChanges = this.getSpecialityChanges(speciality, this.expert.speciality);
            delete this.expert.speciality;

            console.log(specialityChanges);
            console.log(specialityChanges.add[0]);
            console.log(this.getSpecialityId(specialityChanges.add[0]));
            console.log(this.getExpertSpecialityId(this.getSpecialityId(specialityChanges.delete[0])));
            console.log(this.specialityList);
            console.log(this.expertSpeciality);

            if (this.getChanges(this.attributes.client, this.client)) {
                console.log('run client update methods');
                console.log(this.client);
                this.sendClient(this.client);
            }

            if (this.getChanges(this.attributes.expert, this.expert)) {
                console.log('run expert update methods');
                console.log(this.expert);
            }

            if (specialityChanges.add.length) {
                console.log('run add speciality methods');
            }

            if (specialityChanges.delete.length) {
                console.log('run delete speciality methods');
            }


            this.expert.speciality = speciality;

        },

        sendClient: function (client) {
            api.call({
                method: 'ClientEdit',
                namespace: 'auto',
                data: client,
                error: _.bind(this.requestError, this),
                success: _.bind(function (data) {
                    console.log('success edit');
                    console.log(data);
                }, this)
            });
        }

    });
});
