define([
    '_app/models/templates/main',
], function (Model_Template) {
    'use strict';
    return Model_Template.extend({

        defaults: {},

        new: false,

        initialize: function (opts) {
            this.modelName = opts.model;
            this.namespace = opts.namespace;
            this.new = opts.id ? false : true;
        },

        loadMetadata: function () {

            var model = this.modelName + 'Get';
            api.call({
                method: 'FieldListGet',
                namespace: 'meta',
                data: {
                    MethodName: this.namespace ? this.namespace + '/' + model : model
                },
                error: _.bind(this.requestError, this),
                success: _.bind(function (data) {
                    this.trigger('loadedMetadata', data);
                }, this)
            });
        },

        load: function (fieldIdName) {
            var method = this.modelName + 'Get';
            var data = new Object();
            data[fieldIdName] = this.id;


            api.call({
                method: method,
                namespace: this.namespace,
                data: data,
                error: _.bind(this.requestError, this),
                success: _.bind(function (data) {
                    this.trigger('success', data);
                }, this)
            });

        },

        successSync: function (options, response, data) {
            this.trigger('success', response);
        },

        validate: function (attr, options) {
            delete attr.id;
            delete attr.namespace;
            delete attr.model;

            this.errors = [];

            for (var fieldName in attr) {

                options.forEach(function (meta) {
                    if (meta.Name == fieldName && !this.validateOnMeta(meta, attr[fieldName])) {
                        this.errors.push({field: fieldName, text: 'Поле не заполнено'});
                    }
                }, this);

            }

            if (this.errors.length) {
                return this.errors;
            }
        },

        validateOnMeta: function (meta, value) {
            if (meta.IsMandatory && !value) {
                return false
            } //Если поле обязательно, то оно должно быть заполнено


            return true;
        },

        sync: function (type, model, options) {
            var method;

            if (this.new) {
                method = this.modelName + 'Add';
                api.call({
                    method: method,
                    namespace: this.namespace,
                    data: this.attributes,
                    error: _.bind(this.requestError, this),
                    success: _.bind(function () {
                        this.trigger('successAdd');
                    }, this)
                });
            } else {
                method = this.modelName + 'Edit';
                api.call({
                    method: method,
                    namespace: this.namespace,
                    data: this.attributes,
                    error: _.bind(this.requestError, this),
                    success: _.bind(function () {
                        this.trigger('successAdd');
                    }, this)
                });
            }
        }

    });
});