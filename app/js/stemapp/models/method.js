define([
    '_app/models/templates/main',
], function (Model_Template_Main) {
    'use strict';

    /**
     * Модель для работы с методами
     * @class Model_Method
     * @constructor
     * @extends Model_Template_Main
     */
    var Model_Method = Model_Template_Main.extend({

        defaults: {},

        errors: {},

        new: false,

        initialize: function (opts) {

            this.new = opts ? opts.new : false;
        },

        load: function (attr) {

            attr = attr ? attr : {};
            api.call({
                method: 'MethodListGet',
                namespace: 'meta',
                data: attr.data,
                error: attr.error || _.bind(this.requestError, this),
                success: attr.success || _.bind(this.success, this)
            });
        },

        /**
         * Получение метода по идентификатору
         * @param attr
         * @param {object} attr.data Данные
         * @param {string} attr.data.Name Название формы
         * @param {function} attr.error Callback ошибки
         * @param {function} attr.success Callback удачного выполнения
         * @constructor
         */
        getById: function (attr) {

            api.call({
                method: 'MethodGet',
                namespace: 'meta',
                data: attr.data,
                error: attr.error || _.bind(this.requestError, this),
                success: attr.success || _.bind(this.success, this)
            });
        }
    });
    return Model_Method;
});
