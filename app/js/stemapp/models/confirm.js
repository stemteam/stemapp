define([
    '_app/models/templates/main',
], function (Model_Template) {
    'use strict';
    return Model_Template.extend({

        defaults: {

            Page: '',
            Phone: '',
            ActivationCode: '',
            Code: ''
        },

        initialize: function () {

        },

        validate: function (attrs, options) {

            this.errors = [];
            if (!Stemapp.config.registerByInvite) {
                if (attrs.ActivationCode == '' && attrs.Code == '') {
                    this.errors.push({field: "code", text: 'Введите код подтверждения'});
                }

                if (!attrs.Login) {
                    this.errors.push({field: "login", text: 'Введите мобильный телефон или email'});
                }
            }

            if (this.errors.length) {
                return this.errors;
            }
        },

        sync: function (type, model, options) {
            var page = this.attributes['Page'];

            switch (page.toLowerCase()) {

                case 'remember':
                    delete this.attributes['Page'];
                    api.call({
                        method: 'ForgotPasswordConfirm',
                        namespace: false,
                        data: this.attributes,
                        error: _.bind(this.requestError, this),
                        success: _.bind(this.success, this, options)
                    });
                    break;

                case 'registration':

                    if (Stemapp.config.registerByInvite) {
                        I.jaxis({
                            service: 'auth',
                            action: 'Activate',
                            data: this.attributes,
                            error: _.bind(this.error, this),
                            success: _.bind(this.success, this)
                        });

                    } else {
                        delete this.attributes['Page'];
                        api.call({
                            method: 'Activate',
                            namespace: false,
                            data: this.attributes,
                            error: _.bind(this.requestError, this),
                            success: _.bind(this.success, this, options)
                        });
                    }
                    break;
            }
        },

        success: function () {
            this.trigger('success');
        },

        error: function (data) {
            this.trigger('success', this.getErrorMessage(data.responseText));
        }

    });

});