define([
    '_app/models/templates/main',
], function (Model_Template_Main) {
    'use strict';

    /**
     * Модель для работы с формами
     * @class Model_Form
     * @constructor
     * @extends Model_Template_Main
     */
    var Model_Form = Model_Template_Main.extend({

        defaults: {},

        errors: {},

        new: false,


        initialize: function (opts) {

            this.new = opts ? opts.new : false;
        },

        load: function (attr) {

            attr = attr ? attr : {};
            api.call({
                method: 'FormListGet',
                namespace: 'meta',
                data: attr.data,
                error: attr.error || _.bind(this.requestError, this),
                success: attr.success || _.bind(this.success, this)
            });
        },

        /**
         * Получение формы по идентификатору
         * @param attr
         * @param {object} attr.data Данные
         * @param {string} attr.data.Name Название формы
         * @param {function} attr.error Callback ошибки
         * @param {function} attr.success Callback удачного выполнения
         * @constructor
         */
        getById: function (attr) {

            api.call({
                method: 'FormGet',
                namespace: 'meta',
                data: attr.data,
                error: attr.error || _.bind(this.requestError, this),
                success: attr.success || _.bind(this.success, this)
            });
        },

        validate: function (attr) {

            this.errors = [];
            if (attr.code == '') {
                this.errors.push({field: "code", text: 'Укажите код'});
            }
            if (attr.name == '') {
                this.errors.push({field: "name", text: 'Укажите название'});
            }
            if (this.errors.length) {
                return this.errors;
            }
        },


        sync: function (type, model, opts) {

            if (this.new) {
                api.call({
                    method: 'FormAdd',
                    namespace: 'meta',
                    data: this.attributes,
                    error: _.bind(opts.error ? opts.error : this.requestError, this),
                    success: _.bind(opts.success ? opts.success : this.successAdd, this)
                });
            }
            else {

                api.call({
                    method: 'FormEdit',
                    namespace: 'meta',
                    data: this.attributes,
                    error: _.bind(opts.error ? opts.error : this.requestError, this),
                    success: _.bind(opts.success ? opts.success : this.successAdd, this)
                });
            }
        },

        delete: function (item, id) {

            var attr = {};
            attr[id] = item[id];
            api.call({
                method: 'FormDelete',
                namespace: 'meta',
                data: attr,
                error: _.bind(this.errorDelete, this, item),
                success: _.bind(this.successDelete, this, item)
            });
        },

        success: function (data) {
            this.trigger('apply', data);
        },

        successAdd: function (data) {
            this.trigger('applyAdd', data);
        },

        successDelete: function (item, xhr) {
            this.trigger('applyDelete', item);
        },

        errorDelete: function (item, xhr) {
            this.trigger('errorDelete', item);
            this.requestError(xhr);
        }


    });
    return Model_Form;
});
