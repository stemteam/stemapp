define([
    'jquery'
], function ($) {
    'use strict';


    Stemapp.sites = {

        modes: {},

        checkTab: function (tab) {

            var res;

            switch (Stemapp.sites.modes[Stemapp.mode].right) {

                case 'black':

                    for (var k in Stemapp.sites.modes[Stemapp.mode].rights) {

                        if (k == tab) {
                            if (typeof Stemapp.sites.modes[Stemapp.mode].rights[k] == 'function') {

                                res = Stemapp.sites.modes[Stemapp.mode].rights[k]();

                            } else {
                                res = Stemapp.sites.modes[Stemapp.mode].rights[k];
                            }
                            return res;
                        }
                    }
                    return true;

                case 'white':

                    for (var k in Stemapp.sites.modes[Stemapp.mode].rights) {

                        if (k == tab) {

                            if (typeof Stemapp.sites.modes[Stemapp.mode].rights[k] == 'function') {

                                res = Stemapp.sites.modes[Stemapp.mode].rights[k]();

                            } else {
                                res = Stemapp.sites.modes[Stemapp.mode].rights[k];
                            }
                            return res;
                        }
                    }
                    return false;

                default:
                    return false;
            }
        },

        checkMenu: function (menu) {

            var res;

            switch (Stemapp.sites.modes[Stemapp.mode].menu) {

                case 'black':

                    for (var k in Stemapp.sites.modes[Stemapp.mode].menus) {

                        if (k == menu) {
                            if (typeof Stemapp.sites.modes[Stemapp.mode].menus[k] == 'function') {

                                res = Stemapp.sites.modes[Stemapp.mode].menus[k]();

                            } else {
                                res = Stemapp.sites.modes[Stemapp.mode].menus[k];
                            }
                            return res;
                        }
                    }
                    return true;

                case 'white':

                    for (var k in Stemapp.sites.modes[Stemapp.mode].menus) {

                        if (k == menu) {
                            if (typeof Stemapp.sites.modes[Stemapp.mode].menus[k] == 'function') {

                                res = Stemapp.sites.modes[Stemapp.mode].menus[k]();

                            } else {
                                res = Stemapp.sites.modes[Stemapp.mode].menus[k];
                            }
                            return res;
                        }
                    }
                    return false;

                default:
                    return false;
            }
        }

    };

    Stemapp.opts = $.extend(Stemapp.opts, {

        userType: {

            SUPERUSER: 1,
            DILLER: 2,
            CLIENT: 3
        }

    });

});