define([
    '_app/sites/init'
], function () {
    'use strict';

    //todo перенести отсюда в конфиги

    Stemapp.sites.modes.admin = {

        right: 'black',

        rights: {
            users: function () {
                return Stemapp.opts.userType.CLIENT != Stemapp.userType;
            },
            options: true

        },

        menu: 'black',

        menus: {

            users: function () {
                return Stemapp.demoGuid != Stemapp.me.ABONENTGUID;
            },
            software: function () {
                return Stemapp.demoGuid != Stemapp.me.ABONENTGUID;
            },
            password: function () {
                return Stemapp.demoGuid != Stemapp.me.ABONENTGUID;
            },

            myobjects: false,
            options: false
        },

        clientType: [Stemapp.opts.userType.SUPERUSER, Stemapp.opts.userType.DILLER]
    };

});