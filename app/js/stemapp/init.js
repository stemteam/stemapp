define([
    '_app/views/app',
    '_app/views/windows/error',
    'jquery',
    '_app/config',
    '_app/views/templates/tab',
    '_app/views/templates/window',
    '_app/sites/init',
    '_app/sites/admin',
    'underscore',
    'backbone',
    'epoxy'

], function (App, View_Error, $) {

    require([
        'resource/config',

        'tmpl',
        '_app/misc/util',
        '_services/api',
        '_services/base',
        '_services/tranny',
        '_app/misc/event',
        '_app/misc/menu',
        '_app/misc/grid',
        '_app/misc/functions',
        'plugins',
        'navstat.select',
        'slickgrid'
    ], function (Config) {
        'use strict';

        // Переопределяем конфиг тем, который есть в ресурсах
        Stemapp.config = $.extend(true, Stemapp.config, Config);

        document.title = Stemapp.config.title;
        $('.js-global-title').text(Stemapp.config.title).addClass('show');

        if (!window.jasmine) {
            /**
             * Инициализация модулей
             */
            Stemapp._modules = {};
            for (var i in Stemapp.modules) {
                if (Stemapp.modules.hasOwnProperty(i)) {
                    (function (name) {
                        var namePath;
                        if (Boot.devmode) {
                            namePath = 'modules/' + name + '/name';
                        } else {
                            namePath = Stemapp.opts.path + '/modules/' + name + '/name';
                        }
                        $.get(namePath, function (module_name) {
                            var paths = {};
                            paths[module_name] = 'modules/' + name;
                            require.config({
                                paths: paths
                            });
                            require(['_modules/' + name + '/init'], function (boot) {
                                boot.init(name);
                                Stemapp.modules[name].model = boot.model ? boot.model : function () {
                                };
                                Stemapp.modules[name].name = module_name;
                            });

                            Stemapp._modules[module_name] = 'modules/' + name;
                        })
                    })(i);
                }
            }
        }
        Stemapp.Fatal_Error = View_Error;
        function checkLoadModules() {
            var isload = true;
            for (var i in Stemapp.modules) {
                if (Stemapp.modules.hasOwnProperty(i)) {
                    if (!Stemapp.modules[i].isload) {
                        isload = false;
                    }
                }
            }
            if (isload) {
                init();
            } else {
                _.delay(checkLoadModules, 100);
            }
        }

        checkLoadModules();
        function init() {
            /**
             * Все модули проинициализированы. Как сказал бы Гагарин: "Поехали!"
             */
            Stemapp.App = new App({login: false});
            Stemapp.App.createRouter();
            Stemapp.App.start();
        }
    });
});