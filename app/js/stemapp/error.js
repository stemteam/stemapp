define([], function () {

    Stemapp.Error = {

        /**
         *
         * @param xhr
         * @param view
         * @public
         */
        requestError: function (xhr, view) {
            var errors;
            var json;
            switch (xhr.status) {
                case 503:

                    this.showError('Происходит обновление сервиса, пожалуйста подождите' + '<br/>' + this.getErrorMessage(xhr.responseText));
                    break;
                case 502:
                    this.showError('Сервер недоступен' + '<br/>' + this.getErrorMessage(xhr.responseText));
                    break;
                case 400:
                    try {
                        json = $.parseJSON(xhr.responseText);
                        if (json && json.length) {
                            errors = [];
                            for (var i = 0; i < json.length; i++) {
                                errors.push({field: json[i].ExceptionObject, text: json[i].Message, code: 400});
                            }
                            if (view) {
                                view.trigger('error', this, errors);
                            } else {
                                for (i = 0; i < errors.length; i++) {
                                    this.showError(errors[i]);
                                }
                            }
                        }
                    } catch (e) {
                    }
                    break;
                case 404:
                    this.showError(this.getErrorMessage(xhr.responseText));
                    break;
                case 403:
                    this.showError('Нет доступа' + '<br/>' + this.getErrorMessage(xhr.responseText));
                    break;
                case 401:
                    this.showError('Пользователь неавторизован' + '<br/>' + this.getErrorMessage(xhr.responseText));
                    break;
                default:
                    var m, response;
                    if (xhr.responseText) {
                        m = this.getErrorMessage(xhr.responseText);
                    }
                    if (m) {
                        this.showError(m);
                    } else {
                        response = xhr.responseText;
                        json = $.parseJSON(response);
                        errors = [];
                        var operation = '';
                        if (json.error) {  // старый формат json
                            operation = I.jaxis.lastOperation;
                            errors.push({
                                title: json.error.toString().trim().split('\n')[0],
                                error: json.error
                            });
                        } else { // новый формат json2
                            operation = api.lastOperation;
                            for (i = 0; i < json.length; i++) {
                                errors.push({
                                    title: json[i].Exception + ' ' + json[i].CallStack.toString().trim().split('\n')[0],
                                    error: json[i].CallStack
                                });
                            }
                        }
                        var url = '';
                        if (operation) {
                            url = window.location.protocol + '//' + window.location.hostname + operation.url + '?' + Stemapp.util.paramToLink(operation.data);
                        }
                        var errorMessage = ['[b]Url:[/b] ' + url, '[b]Page:[/b] ' + window.location, '[b]Data:[/b] [code]' + (operation ? JSON.stringify(operation.data) : '') + '[/code]'].join('\n\n');

                        for (i = 0; i < errors.length; i++) {
                            errorMessage += '\n\n' + '[b]' + errors[i].title + '[/b]\n[code]' + errors[i].error + '[/code]'
                        }
                        this.sendError(errors[0].title, errorMessage);
                        if (view) {
                            view.trigger('fatalError', xhr);
                        } else {
                            Stemapp.App.fatalError(xhr);
                        }
                    }
                    break;
            }
        },

        /**
         *
         * @param title
         * @param message
         * @private
         */
        sendError: function (title, message) {

            var url = 'bugreport/Ticket.new',
                post = {
                    type: 'bug',
                    summary: Base64.encode(title),
                    message: Base64.encode(message),
                    auto: 1,
                    project: Stemapp.config.project,
                    apikey: Stemapp.config.bugKey,
                    field_os: 'Web'
                };

            $.post(url, post, function (text) {
            });
        },

        getErrorMessage: function (response) {
            var resText,
                m = '';
            try {
                resText = Array.isArray(JSON.parse(response)) ? JSON.parse(response)[0] : JSON.parse(response);
            } catch (e) {
                return false;
            }

            if (resText) {
                m = resText.Message;
                if (!m) {
                    resText.error = resText.error ? resText.error : resText.CallStack;
                    m = resText.error.match(/-=#([^#]+)#=-/);
                    m = (m && m[1]) ? m[1].trim() : m;
                }
            } else {
                m = response.toString().trim();
            }

            return m;
        },

        extend: function (children) {
            return _.extend(this, children);
        },

        showError: function (s) {
            Stemapp.App.showError(s);
        }
    };
});