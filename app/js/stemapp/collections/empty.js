define([
    '_app/template/epoxy/collection/main',
    '_app/models/empty',
], function (Epoxy_Collection_Main, Model_Empty) {
    'use strict';

    /**
     * @class Collection_Empty
     *
     * @extends Epoxy_Collection_Main
     */
    var Collection_Empty = Epoxy_Collection_Main.extend({

        model: Model_Empty

    });
    return Collection_Empty;
});