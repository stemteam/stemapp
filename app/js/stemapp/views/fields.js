define([
    '_app/views/templates/tab',
    '_app/views/control/table',
    '_app/models/meta',
    '_app/data/raw',
], function (Template_Tab, Control_Table, Model_Meta) {
    'use strict';
    return Template_Tab.extend({

        tagName: 'div',

        model: null,

        name: 'meta',

        caption: 'Загрузка...',

        tabButton: null,

        url: 'fields/{$namespace}/{$method}',

        icon: 'form',

        template: 'meta',

        method: null,

        namespace: null,


        dataName: 'raw',

        grid: {
            enable: true,

            checkbox: false,

            type: 'list'
        },

        initialize: function (opts) {

            this.namespace = opts.arguments[0];
            this.method = opts.arguments[1];
            this.caption = 'Поля ' + this.namespace + '/' + this.method;
            this.model = new Model_Meta();

            this.showGlobalAjax();

            this.meta_items = [];

            this.urlData = {method: this.method, namespace: this.namespace};


            this.grid.rowChanged = _.bind(this.rowChanged, this);
            this.grid.model = this.modelName;
            this.table = new Control_Table(this.dataName, this.grid);

            Template_Tab.prototype.initialize.apply(this, [opts]);
            this.load();
        },


        events: function () {

            return _.extend({},
                Template_Tab.prototype.events, {

                    "click .toolbar a.fields-create": "fieldsCreate",

                    "click .toolbar a.fields-edit": "fieldsEdit",

                    "click .toolbar a.fields-delete": "fieldsDelete"
                })
        },


        load: function (item) {
            var model = new Model_Meta();

            model.load('meta', 'FieldList', {
                success: _.bind(this.apply, this),
                data: {MethodName: this.namespace + '/' + this.method + 'Get'}
            });
        },

        fieldsCreate: function (e) {
            e.preventDefault();
            this.trigger('open', 'meta', ['meta', 'Field', 'new']);
        },

        fieldsEdit: function (e) {
            e.preventDefault();
            var cell = this.table.getActiveCell(),
                item = this.table.getItem(cell.row);
            this.trigger('open', 'meta', ['meta', 'Field', item.FieldId]);
        },

        fieldsDelete: function (e) {
            e.preventDefault();
            this.sendMessage('Нереализовано');
        },

        apply: function (data, metadata) {
            this.metadata = metadata;

            this.hideAjax();
            var parentFieldName,
                idFieldName;

            for (var i = 0; i < metadata.length; i++) {

                if (metadata[i].IsTreeParent) {
                    parentFieldName = metadata[i].Name;
                }

                if (metadata[i].IsId) {
                    idFieldName = metadata[i].Name;
                }
            }

            this.table.update(data, {
                columns: metadata,
                idFieldName: idFieldName,
                parentFieldName: parentFieldName
            });
        },

        items: function () {

            var array = [];
            array.push({
                cls: 'fields-create',
                text: 'Новое поле',
                icon: 'form-create'
            });
            return array.concat([

                {
                    cls: 'fields-edit',
                    text: 'Редактировать',
                    icon: 'form-edit',
                    invalidate: _.bind(this.itemInvalidate, this)
                },
                {
                    cls: 'fields-delete',
                    text: 'Удалить',
                    icon: 'form-delete',
                    invalidate: _.bind(this.itemInvalidate, this)
                },


                '>',
                {
                    type: 'input',
                    cls: 'search',
                    placeholder: 'Поиск по таблице'
                },
                {
                    cls: 'refresh',
                    text: 'Обновить',
                    icon: true,
                    compact: true
                },
                {
                    cls: 'filter',
                    text: 'Фильтры',
                    icon: true,
                    toggle: true,
                    compact: true
                },
                {
                    cls: 'export',
                    text: 'Экспорт в csv',
                    icon: true,
                    compact: true
                },
                {
                    cls: 'copyexcel',
                    text: 'Скопировать таблицу в буфер обмена',
                    icon: true,
                    compact: true
                }
            ]);
        }

    });
});