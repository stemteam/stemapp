define([
    '_app/views/templates/tab',
    '_app/models/client',
    'smarty!_app/tmpl/client.tpl',
], function (Template_Tab, Model_Client, Template_Client) {
    'use strict';
    return Template_Tab.extend({

        tagName: 'div',

        model: null,

        name: 'client',

        caption: 'Управление пользователями',

        tabButton: null,

        server: null,

        table: null,

        template: Template_Client,

        url: 'client{if $namespace}/{$namespace}{/if}{if $model}/{$model}{/if}{if $id}/{$id}{/if}',

        initialize: function (opts) {

            this.urlData = {id: this.modelId, model: this.modelName};

            this.model = new Model_Client();
            this.model.bind('success', this.apply, this);

            Template_Tab.prototype.initialize.apply(this, [opts]);

        },

        afterRender: function () {
            this.$('.refresh').attr('disabled', true);
            this.hideAjax();
        },

        events: function () {

            return _.extend({},
                Template_Tab.prototype.events, {

                    "submit form": "submit"

                });
        },

        error: function (message) {
            this.trigger('error', {}, message);
        },

        apply: function () {

            this.sendMessage('Пользователь удалён');
            this.remove();

        },

        submit: function () {
            var user = this.$('form :input.js-user').val();

            this.model.removeUser(user);

            return false;
        },

        items: function () {

            return [
                '>',
                {
                    cls: 'refresh',
                    text: 'Обновить',
                    icon: true,
                    compact: true
                }
            ];
        }

    });
});