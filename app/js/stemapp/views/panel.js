define([
    'backbone',
    'jquery',
    '_app/views/templates/main',
    '_app/models/panel',
    '_app/routers/inner',
    'smarty!_app/tmpl/app.tpl',
    'smarty!_app/tmpl/control/menu.tpl',
    'tmpl',
    'mlpushmenu',
    'cookie'
], function (Backbone, $, Template_Main, Model_Panel, Routes, Template_App, TMPL_Menu, /** @type {tmpl} **/ tmpl, mlPushMenu) {
    return (Template_Main.extend({

        className: 'panel',

        model: new Model_Panel,

        servers: null,

        view: null,

        routers: [],

        tabs: [],

        isPayed: false,

        mainMenu: null,

        initialize: function (opts) {

            this.servers = opts.servers;
            this.isPayed = opts.isPayed;
            this.model.bind('error', this.error);

            this.bind('resize', this.resize, this);

            this.checkRevision();
            Stemapp.unload = true;

            $(window).on('beforeunload', _.bind(this.beforeUnload, this));
            $(document).on('keydown', _.bind(this.keydown, this));
            Stemapp.logout = _.bind(this.logout, this);
            this.createRouter();

        },

        createRouter: function () {

            var fragment = Backbone.history.fragment;
            var i, j, router;

            // todo старый формат загрузки роутеров. Правила добавление роутов описаны в главном роуте _app/routers/main
            for (i in Stemapp.modules) {
                if (Stemapp.modules.hasOwnProperty(i) && Stemapp.modules[i].router && Stemapp.modules[i].router.inner) {
                    Stemapp.App.warn('Используется старая схема загрузки роутов');
                    // Загружаем внутренние роутеры модулей
                    for (j = 0; j < Stemapp.modules[i].router.inner.length; j++) {
                        router = new Stemapp.modules[i].router.inner[j]();
                        router.bind('open', this.open, this);
                        this.routers.push(router);
                    }
                }
            }

            // Загружаем роуты stemapp
            Stemapp.App.appendRoutes('stemapp', Routes);
            Stemapp.App.router.off('open');
            Stemapp.App.router.on('open', this.open, this);

            if (!this.restoreTabs(_.bind(this.completeLoad, this))) {

                var me = this;
                if (fragment == '') {
                    if (!Stemapp.config.showStartPage) {
                        //todo сделать стартовую страницу
                        var page = $.render(Stemapp.config.defaultPage, {me: Stemapp.me});
                        if (page != '') {
                            this.navigate('01/' + page, {trigger: true});
                        }
                    }
                    if (Stemapp.config.showStartMenu) {
                        if (!Stemapp.App.router.current) {
                            this.on('ready:mainmenu', function () {
                                me.showMenu();
                            });
                        }
                    }
                } else {
                    this.navigate(fragment, {trigger: true});
                }
            }
        },

        completeLoad: function () {
            // Предзагрузка модулей
            for (var i in Stemapp.modules) {
                if (Stemapp.modules.hasOwnProperty(i)) {

                    if (Stemapp.modules[i].boot.start) {
                        Stemapp.modules[i].boot.start({app: this});
                    }
                }
            }
        },

        events: function () {

            var events = {
                "click #content": "hideMenu",

                "click": "hideTabList",

                "click #tab-wrapper": "hideMenu",

                "click a.menu-help": "openHelp",

                "click a.menu-history": "openHistory",

                "click a.menu-change-pass": "openChangePass",

                "click a.menu-exit": "exit",

                "click .newversion .js-ok": "updateRevision",

                "click .newversion .js-cancel": "minimizeRevision",

                "click .js-left-tab": "leftSlideTab",

                "click .js-right-tab": "rightSlideTab",

                "click .js-tab-list-button": "toggleTabList",

                "click .js-tab-update-button": "toggleTabUpdate",

                "click #tab-list a": "clickTabList",

                "click a.menu-meta-list": "openMetaList",

                "click a.menu-client": "openClient",

                "click a.menu-support": "openSupport",

                "click a.menu-role": "openRole",

                "click a.js-usermenu": "openUserMenu" // Меню генерируемое из базы
            };

            var i, j;
            for (i in Stemapp.modules) {
                if (Stemapp.modules.hasOwnProperty(i)) {
                    //Биндим пункты меню
                    events = _.extend(events, this.bindMenus(Stemapp.modules[i].menu));
                    if (Stemapp.modules[i].menuServers) {
                        for (j = 0; j < Stemapp.modules[i].menuServers.length; j++) {
                            if (Stemapp.modules[i].menuServers[j].cls) {
                                events['click a.' + Stemapp.modules[i].menuServers[j].cls] = _.bind(this.clickMenu, this, Stemapp.modules[i].menuServers[j].callback);
                            }
                        }
                    }
                }
            }
            return events;
        },


        bindMenus: function (menus) {
            var events = [];
            for (var j = 0; j < menus.length; j++) {
                if (menus[j].cls && menus[j].callback) {
                    events['click a.' + menus[j].cls] = _.bind(this.clickMenu, this, menus[j].callback);
                    if (menus[j].children) {
                        events = _.extend(events, this.bindMenus(menus[j].children));
                    }
                }
            }
            return events;
        },

        openSupport: function (e) {
            e.preventDefault();
            if (Stemapp.config.supportPage.indexOf('mailto:') != -1) {
                Stemapp.unload = true;
                location.href = Stemapp.config.supportPage;
                Stemapp.unload = false;
            } else {
                window.open(Stemapp.config.supportPage);
            }
        },

        clickMenu: function (callback, e) {
            e.preventDefault();
            callback(e, this);
            this.hideMenu();
        },

        resize: function () {

            if (this.view && this.view.tabButton) {
                this.fixPositionTab(this.view.tabButton);
            }
        },

        open: function () {
            var args = [];
            Array.prototype.push.apply(args, arguments);
            var ind = args.shift(),
                name = args.shift();
            this.openTab(name, args);
        },

        keydown: function (e) {
            if (!e.ctrlKey && (e.keyCode == 116 && !Boot.devmode) || (e.keyCode == 117 && Boot.devmode)) {
                this.view.trigger('refresh');
                return false;
            }

            if (e.ctrlKey && e.keyCode == 82) {
                if (this.view.trigger('refresh')) {
                    e.stopPropagation();
                    return false;
                }
            }
        },

        window: function (name, params, callback) {
            this.trigger('window', name, params, callback);
        },

        dialog: function (arg) {
            this.trigger('dialog', arg);
        },

        getTabName: function (name) {
            return name.charAt(0).toUpperCase() + name.slice(1);
        },

        openModal: function (name, args, opts) {

            this.trigger('openModal', name, args, opts); // Передаем в App
        },

        openTab: function (name, args, fixedInd, callback) {
            if (!name) return false;
            name = this.getTabName(name);
            if (!this.isPayed) {
                this.trigger('dialog', {text: 'Сервис отключен за неуплату', buttons: 'ok'});
                return false;
            }
            if (!Stemapp.sites.checkTab(name.toLowerCase())) {
                this.trigger('dialog', {text: 'У вас не хватает прав для открытия этой вкладки', buttons: 'ok'});
                return false;
            }

            if (!fixedInd) {
                var exist = this.getDuplicateTab(name, args);
                var ind = 0;
                if (!_.isUndefined(exist) && this.tabs[exist].single) {
                    ind = this.tabs[exist].ind;
                }
                this.view = null;
                if (ind) {
                    var index = this.getIndexTab(name, ind);
                    if (!_.isNull(index)) {
                        this.focusTab(index);
                    }
                }
            }
            if (!this.view) {

                this.showGlobalAjax();
                var options = null;
                if (!_.isArray(args)) { // Новый формат передачи данных
                    options = args;
                    if (args) {
                        args = args.arguments;
                    } else {
                        args = [];
                    }

                } else { // Старый формат передачи данных
                    // Ничего не делаем
                    Stemapp.App.log('Используется старый формат передачи параметров в форму ' + name);
                }
                Stemapp.App.loadView(name, _.bind(function (View) {

                    if (fixedInd) {
                        ind = fixedInd;
                    } else {
                        ind = this.getMaxNum(name);
                    }

                    this.buildView(View, args, ind, name, options);
                    this.focusTab(this.tabs.length - 1, false, true);
                    this.hideGlobalAjax();

                    // save to localStorage new tab's info
                    this.saveTab(name, ind, options ? options : args, this.tabs.length - 1, callback);

                }, this));
            }

            this.viewName = name;

            return true;
        },

        buildView: function (View, args, ind, name, options) {

            if (!View) {
                return;
            }
            var id = 'view-id-' + Math.round(Math.random() * 110000000);

            var view = new View({arguments: args, ind: ind, id: id, name: name, options: options});

            this.tabs.push(view);

            this.$('#tab-bar').append(view.tabButton);

            this.$('#tab-panel').append(view.render().el);
            view.afterInsert();

            this.unFocusTab(this.tabs[this.tabs.length - 1].cid);

            view.trigger('resize');
            view.bind('navigate', this.navigate, this);
            view.bind('error', this.requestError, this);
            view.bind('close', this.closeTab, this);
            view.bind('unfocus', this.unFocusTab, this);
            view.bind('fatalError', this.fatalError, this);
            view.bind('open', this.openTab, this);
            view.bind('openModal', this.openModal, this);
            view.bind('dialog', this.dialog, this);
            view.bind('window', this.window, this);
            view.bind('refreshTabs', this.refreshTabs, this);
            view.bind('refreshHashTabs', this.refreshHashTabs, this);


            this.trigger('openedNewTab');
            this.viewName = name;
        },

        clickTabList: function (e) {

            this.$('#tab-list .selected').removeClass('selected');
            $(e.currentTarget).addClass('selected');
            this.focusTab($(e.currentTarget).attr('data-ind'));
            return false;
        },

        hideTabList: function () {

            this.$('#tab-list').stop(true).animate({opacity: 'hide'}, 300);
        },

        toggleTabList: function (e) {

            if (this.$('#tab-list:visible').length) {
                this.hideTabList();
            } else {
                this.showTabList();
            }
            e.stopPropagation();
            return false;
        },

        toggleTabUpdate: function () {
            this.view.trigger('refresh');
            return false;
        },

        showTabList: function () {

            this.$('#tab-list').empty();
            for (var i = 0; i < this.tabs.length; i++) {

                var $a = $('<a></a>').attr({href: '#'}).attr('data-ind', i).html('<i class="icon icon-' + this.tabs[i].icon.toLowerCase() + '"></i> ' + this.tabs[i].caption);

                if (this.tabs[i] == this.view) {
                    $a.addClass('selected');
                }
                this.$('#tab-list').append($('<li></li>').append($a));
            }
            this.$('#tab-list').stop(true).animate({opacity: 'show'}, 300);

            this.$('#tab-list').css({bottom: 'auto'});
            if (this.$('#tab-list').outerHeight() + this.$('#tab-list').offset().top > $(window).height()) {
                this.$('#tab-list').css({bottom: 0, overflowY: 'auto'});
            }
            return false;
        },

        leftSlideTab: function (e) {
            this.slideTab(e, -1);
            return false;
        },

        rightSlideTab: function (e) {
            this.slideTab(e, 1);
            return false;
        },

        slideTab: function (e, direct) {
            if ($(e.currentTarget).hasClass('disabled')) {
                return false;
            }
            var scroll = this.$('#tab-wrapper').scrollLeft(),
                shift = 50,
                scrollLeft = scroll + shift * direct;

            if (scrollLeft < 0) {
                scrollLeft = 0;
            } else {
                if (direct > 0) {
                    var len = this.tabs.length;
                    var offset = this.$('#tab-wrapper').scrollLeft() + this.$('#tab-wrapper').width() + this.$('#tab-bar').offset().left - this.tabs[len - 1].tabButton.offset().left - this.tabs[len - 1].tabButton.outerWidth();
                }
            }
            this.$('#tab-wrapper').scrollLeft(scrollLeft);
            this.checkTabBar();
        },

        fixPositionTab: function (tab, first) {

            if (tab) {
                var of = tab.offset();
                var arrowSize = 0;
                if (this.$('.js-left-tab:visible').length) {
                    arrowSize = this.$('.js-left-tab').outerWidth() + 6;
                }
                var offset = this.$('#tab-wrapper').scrollLeft() + this.$('#tab-wrapper').width() + this.$('#tab-bar').offset().left - tab.offset().left - tab.outerWidth();
                //todo почему-то не хватает несколько пикселей при создании вкладки, видимо в момент анимации
                if (first) {
                    offset -= 6;
                }

                if (offset < 0) {
                    this.$('#tab-wrapper').scrollLeft(-offset + this.$('#tab-wrapper').scrollLeft());
                }

                if (tab.offset().left - $('#menu-button').outerWidth() < 0) {
                    this.$('#tab-wrapper').scrollLeft(this.$('#tab-wrapper').scrollLeft() - (-tab.offset().left + tab.outerWidth() + arrowSize));
                }
            }
            this.checkTabBar();
        },

        checkTabBar: function () {

            var len = this.tabs.length;
            if (len < 2) {
                return false;
            }

            var lArrow = false,
                rArrow = false;

            var arrowSize = 0;
            if (this.$('.js-left-tab:visible').length) {
                arrowSize = this.$('.js-left-tab').outerWidth() + 6;
            }
            //todo 10 - магическое число, неправильно определяет браузер при загрузке вкладок их сдвиг слева
            if (this.tabs[0].tabButton.offset().left + 10 - $('#menu-button').outerWidth() - arrowSize < 0) {
                // нужна стрелка слева
                lArrow = true;
            }

            var offset = this.$('#tab-wrapper').scrollLeft() + this.$('#tab-wrapper').width() + this.$('#tab-bar').offset().left - this.tabs[len - 1].tabButton.offset().left - this.tabs[len - 1].tabButton.outerWidth();
            if (offset < 0) {
                // нужна стрелка справа
                rArrow = true;
            }

            if (lArrow || rArrow) {
                this.$('.js-tab-arrows').show();
                this.$('#tab-wrapper').addClass('arrows');

                if (lArrow) {
                    this.$('.js-left-tab').removeClass('disabled');
                } else {
                    this.$('.js-left-tab').addClass('disabled');
                }
                if (rArrow) {
                    this.$('.js-right-tab').removeClass('disabled');
                } else {
                    this.$('.js-right-tab').addClass('disabled');
                }
            } else {
                this.$('.js-tab-arrows').hide();
                this.$('#tab-wrapper').removeClass('arrows');
            }

        },

        checkPositionTab: function (tab) {
            if (tab.offset().left - $('#menu-button').outerWidth() < 0) {
                return -1;
            }
            var offset = this.$('#tab-wrapper').scrollLeft() + this.$('#tab-wrapper').width() - -this.$('#tab-bar').offset().left - tab.offset().left - tab.outerWidth();
            if (offset < 0) {
                return 1;
            }
            return 0;
        },

        focusTab: function (index, notnavigate, first) {

            notnavigate = !!notnavigate;
            first = !!first;
            if (this.tabs[index]) {
                this.view = this.tabs[index];
                if (!notnavigate) {
                    this.view.trigger('focus');
                    this.view.navigate();
                }
                this.fixPositionTab(this.view.tabButton, first);

                if (Stemapp.config.useLocalStorage) {
                    this.saveGUIDData('focusedTabInd', index);
                }
            }
        },

        getIndexTab: function (name, ind) {

            ind = ind ? ind : false;
            for (var i = 0; i < this.tabs.length; i++) {
                if (this.tabs[i].name == name && (this.tabs[i].ind == ind || ind === false)) {
                    return i;
                }
            }
            return null
        },

        getDuplicateTab: function (name, args) {

            for (var i = 0; i < this.tabs.length; i++) {
                if (this.tabs[i].name == name && this.tabs[i].checkTab(args)) {
                    return i;
                }
            }
        },

        getMaxNum: function (viewName) {

            var max = 1,
                find = true;

            while (find) {
                find = false;
                for (var i = 0; i < this.tabs.length; i++) {
                    if (this.tabs[i].name == viewName && this.tabs[i].ind == max) {
                        max++;
                        find = true;
                    }
                }
            }
            return max;
        },

        unFocusTab: function (cid) {

            this.$('.qtip').remove();
            for (var i = 0; i < this.tabs.length; i++) {
                if (this.tabs[i].cid != cid) {
                    this.tabs[i].trigger('blur');
                } else {
                    this.focusTab(i, true);
                }
            }
            this.hideTabList();
            this.hideMenu();
        },

        refreshHashTabs: function (hash) {

            for (var i = 0; i < this.tabs.length; i++) {
                if (this.tabs[i].hash == hash) {
                    this.tabs[i].trigger('refresh');
                }
            }
        },

        refreshTabs: function (name, args) {

            args = args ? args : false;
            for (var i = 0; i < this.tabs.length; i++) {
                if (this.tabs[i].name.toLowerCase() == name.toLowerCase() && (!args || this.tabs[i].checkTab(args))) {
                    this.tabs[i].trigger('refresh');
                }
            }
        },

        showMenu: function () {
            this.mainMenu.openMenu();
        },

        hideMenu: function () {
            this.mainMenu.resetMenu();
        },

        render: function () {
            var items = this.renderMenu(Stemapp.menu.create(this.servers));
            this.$el.html(tmpl(Template_App, {
                REV: Stemapp.opts.revision,
                menu_html: items,
                menu_title: Stemapp.config.menuTitle,
                mode: Stemapp.mode,
                showStartPage: Stemapp.config.showStartPage
            }));
            return this;
        },

        afterInsert: function () {
            this.mainMenu = new mlPushMenu(this.$('#mp-menu').get(0), this.$('#menu-button').get(0), {type: 'cover'});
            this.trigger('ready:mainmenu');
            Stemapp.App.trigger('ready:panel');
        },

        renderMenu: function (items) {
            for (var i = 0; i < items.length; i++) {
                if (items[i].children) {
                    items[i].child_html = this.renderMenu(items[i].children);
                } else {
                    items[i].child_html = '';
                }
            }
            return tmpl(TMPL_Menu, {items: items});
        },

        closeTab: function (cid) {
            for (var i = 0; i < this.tabs.length; i++) {
                if (this.tabs[i].cid == cid) {

                    //delete tab's info from localStorage
                    this.deleteTab(this.tabs[i].name, this.tabs[i].ind);

                    delete this.tabs[i];
                    this.tabs = this.tabs.splice(0, i).concat(this.tabs.splice(1));
                    if (this.tabs.length) {
                        for (var j = i; j >= 0; j--) {
                            if (this.tabs[j]) {
                                this.focusTab(j);
                                return;
                            }
                        }
                    } else {
                        if (Stemapp.config.showStartMenu) {
                            _.delay(_.bind(this.showMenu, this), 500);
                        }
                    }
                    break;
                }
            }
        },

        closeTabs: function () {
            while (this.tabs[0]) {
                this.tabs.pop();
            }
            location.hash = '';
            this.render();
        },

        openHelp: function () {
            this.openTab('Help');
            this.hideMenu();
            return false;
        },

        openHistory: function () {
            this.openTab('History');
            this.hideMenu();
            return false;
        },

        openChangePass: function () {
            this.trigger('window', ['ChangePass']);
            this.hideMenu();
            return false;
        },

        openUserMenu: function (e) {
            e.preventDefault();
            _.delay(_.bind(function () {
                var id = $(e.target).data('id');
                for (var i in Stemapp.UserMenu) {
                    if (Stemapp.UserMenu.hasOwnProperty(i)) {
                        if (Stemapp.UserMenu[i].MenuId == id) {
                            this.execUserMenu(Stemapp.UserMenu[i]);
                            return;
                        }
                    }
                }
            }, this), 150);
            this.hideMenu();
        },

        execUserMenu: function (menu) {

            if (menu.FormName) {

                switch (menu.ViewType) {
                    case 0:
                        this.openTab('meta/record', [menu.FormName]);
                        break;
                    case 1:
                        this.openTab('meta/records', [menu.FormName]);
                        break;
                    case 2:
                        this.openTab('meta/calendar', [menu.FormName]);
                        break;
                    default:
                        this.sendError('Неизвестный тип отображение. Обратитесь к администраторам');

                }
            } else {
                if (/window/i.exec(menu.MenuUrl)) {
                    var form = menu.MenuUrl.split('_');
                    this.trigger('window', [form[1]]);
                } else {
                    this.openTab(menu.MenuUrl);
                }
            }
        },

        openMetaList: function () {
            this.openTab('List');
            this.hideMenu();
            return false;
        },

        openClient: function () {
            this.openTab('Client');
            this.hideMenu();
            return false;
        },

        openRole: function () {
            this.openTab('Role');
            this.hideMenu();
            return false;
        },

        exit: function () {

            this.logout();
            this.hideMenu();
            return false;
        },

        logout: function (force) {
            force = !!force;
            Stemapp.unload = false;
            setTimeout(function () {
                if (force || confirm('Выйти из панели управления?')) {
                    Stemapp.App.logout();
                    window.location = Stemapp.config.base_url;
                }
                else
                    Stemapp.unload = true;
            }, 10);
        },

        success: function () {

            console.log('success', arguments);
        },

        fatalError: function (xhr) {
            this.trigger('fatalError', xhr);
        },

        navigate: function (url, opts) {
            opts = opts ? opts : {};
            Stemapp.App.navigate(url, opts);

            //todo старый формат роутов
            if (opts.trigger && this.routers.length) {
                for (var i = 0; i < this.routers.length; i++) {
                    this.routers[i].navigate(url, opts);
                }
            } else {
                if (this.routers.length) {
                    this.routers[0].navigate(url, opts);
                }
            }
        },

        checkRevision: function () {
            $.get('build?r=' + Math.random(), _.bind(function (data) {
                var version = data;
                if (Stemapp.version != version) {
                    this.newRevision(version);
                    return;
                }
                _.delay(_.bind(this.checkRevision, this), 300000 /* Раз в 5 минут*/ /*3600000*/); // 1час
            }, this), 'text');
        },

        newRevision: function (version) {
            console.log('Old version ' + Stemapp.version);
            console.log('New version ' + version);
            Stemapp.version = version;
            $('#newversion').scaleShow();
        },

        updateRevision: function () {

            Stemapp.unload = false;
            $('#main').scaleHide();
            Stemapp.App = null;
            location.reload();
            return false;
        },

        minimizeRevision: function () {

            this.$('#newversion').autoSize(this.$('#newversion .restart'));
            return false;
        },

        beforeUnload: function () {
            if (!Stemapp.unload || !Stemapp.config.askBeforeExit) return;
            return "Вы уверены, что хотите покинуть страницу? \nВсе данные во вкладках и вкладки не сохранятся!";
        },


        /**
         * Восстановить сохраненные в localStorage вкладки.
         * @returns {boolean} false - данных в localStorage не оказалось.
         */
        restoreTabs: function (callback) {
            if (!window.localStorage || !Stemapp.config.useLocalStorage) {
                return false;
            }
            var GUID = $.cookie('guid'),
                tabs = localStorage.getItem(GUID + '-tabsJSON'),
                result = false;

            if (tabs && tabs !== '[]') {
                tabs = JSON.parse(tabs);

                var arr = [];

                var completeRestoreTabs = _.after(tabs.length, _.bind(this.completeRestoreTabs, this, callback));

                var doRestoreTabs = function (name, ind, args, pos, View) {
                    if (View) {
                        arr.push({
                            constructor: View,
                            name: name,
                            ind: ind,
                            args: args,
                            pos: pos
                        });
                    }
                    completeRestoreTabs(arr);
                };

                //Для каждой вкладки подгружаем конструктор. После сохранения всех конструкторов, создаем экземпляры вкладок
                tabs.forEach(_.bind(function (tab, i) {

                    Stemapp.App.loadView(tab.name, _.bind(this.completeLoadView, this, _.bind(doRestoreTabs, this, tab.name, tab.ind, tab.args, i), tab.name, tab.ind));

                }, this));

                result = true;
            } else {
                callback();
            }
            return result;
        },

        completeLoadView: function (func, name, ind, View) {
            func(View);
            if (!View) {
                this.deleteTab(name, ind);
            }
        },

        completeRestoreTabs: function (callback, arr) {
            var focusedTabInd = this.getGUIDData('focusedTabInd');
            arr.sort(function (a, b) {
                if (a.pos > b.pos) return 1;
                if (a.pos < b.pos) return -1;
            });

            for (var y = 0; y < arr.length; y++) {
                // Проверяем в старом формате сохранено или в новом
                var options = {};
                if (!_.isArray(arr[y].args)) { // Новый формат передачи данных
                    options = arr[y].args;
                    if (arr[y].args) {
                        arr[y].args = arr[y].args.arguments;
                    } else {
                        arr[y].args = [];
                    }

                } else { // Старый формат передачи данных
                    // Ничего не делаем
                    Stemapp.App.log('Используется старый формат передачи параметров в форму ' + name);
                }
                this.buildView(arr[y].constructor, arr[y].args, arr[y].ind, arr[y].name, options);
            }
            this.focusTab(focusedTabInd, false, true);
            callback();
        },

        /**
         * Сохранить в localStorage информацию об открываемой вкладке.
         * @param name Имя вкладки.
         * @param ind Порядковый номер экземпляра вкладки с таким именем.
         * @param args Аргументы, передаваемые во вкладку. Сохранятся в localStorage.
         * @param callback Коллбэк, вызываемый после открытия вкладки. Сохранится в localStorage.
         */
        saveTab: function (name, ind, args) {

            if (!window.localStorage || !Stemapp.config.useLocalStorage) {
                return false;
            }
            var tabs = [],
                GUID = $.cookie('guid');

            if (localStorage.getItem(GUID + '-tabsJSON')) {
                tabs = JSON.parse(localStorage.getItem(GUID + '-tabsJSON'));
            }

            var exist = false;
            for (var i = tabs.length; i--;) {
                if (tabs[i].name === name && tabs[i].ind === ind) {
                    tabs.splice(i, 1);
                    exist = true;
                    break;
                }
            }

            tabs.push({
                name: name,
                ind: ind,
                args: args
            });

            // Конвертировать в JSON исключая некоторые (предположительно ненужные) объекты
            var str = JSON.stringify(tabs, function (key, value) {
                if (typeof value === 'object' && value !== null
                    && value.parentNode) {// html element
                    // Discard key
                    return;
                }
                return value;
            });
            localStorage.setItem(GUID + '-tabsJSON', str);


        },

        /**
         * Удалить информацию о вкладке из localStorage
         * @param tabName
         * @param tabInd
         */
        deleteTab: function (tabName, tabInd) {

            if (!window.localStorage || !Stemapp.config.useLocalStorage) {
                return false;
            }
            var tabs = [],
                GUID = $.cookie('guid'),
                i;

            if (localStorage.getItem(GUID + '-tabsJSON')) {
                tabs = JSON.parse(localStorage.getItem(GUID + '-tabsJSON'));
            }

            for (i = tabs.length; i--;) {
                if (tabs[i].name === tabName && tabs[i].ind === tabInd) {
                    tabs.splice(i, 1);
                    break;
                }
            }

            // очищаем localStorage
            for (i = this.tabs.length; i--;) {
                if (this.tabs[i].name === tabName && this.tabs[i].ind === tabInd) {
                    this.tabs[i].clearDataStorage();
                    break;
                }
            }

            // Конвертировать в JSON исключая некоторые (предположительно ненужные) объекты
            var str = JSON.stringify(tabs, function (key, value) {
                if (typeof value === 'object' && value !== null
                    && value.parentNode) {// html element
                    // Discard key
                    return;
                }
                return value;
            });
            localStorage.setItem(GUID + '-tabsJSON', str);

        },

        /**
         * Сохранить информацию для текущего пользователя(GUID) в localStorage.
         * @param key В localStorage будет добавлен префикс GUID_
         * @param value
         */
        saveGUIDData: function (key, value) {

            if (!window.localStorage) {
                return false;
            }
            var GUID = $.cookie('guid');
            var str = JSON.stringify(value, function (key, value) {
                if (typeof value === 'object' && value !== null
                    && value.parentNode) {// html element
                    // Discard key
                    return;
                }
                return value;
            });
            localStorage.setItem(GUID + '-specs-' + key, str);
            return true;
        },

        /**
         * Получить информацию для текущего пользователя(GUID) из localStorage.
         * @param key
         */
        getGUIDData: function (key) {

            if (!window.localStorage) {
                return false;
            }

            var GUID = $.cookie('guid');
            return JSON.parse(localStorage.getItem(GUID + '-specs-' + key));

        }

    }));
});