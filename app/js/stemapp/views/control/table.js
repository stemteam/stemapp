define([
    'backbone',
    'jquery',
    'slickgrid',
    '_jq/jquery.contextmenu'
], function (Backbone, $, Slick) {

    /**
     * @class Control_Table
     * @constructor
     * @extends Backbone.View
     */
    var Control_Table = Backbone.View.extend({


        tagName: 'div',

        className: 'grid',

        dataName: '',

        opts: {

            type: 'plain',

            enable: false,

            checkbox: true,

            searchInput: null,

            parentHeight: false,

            counter: true,

            customCheckbox: false,

            grouping: false,

            rowChanged: function () {
            },

            onSelect: function () {
            },

            dblClick: function () {
            },

            onContextMenu: function () {
            },

            onSort: function () {
            }
        },

        init: false,

        reinit: false,

        initialize: function (dataName, opts) {
            this.opts = $.extend({}, this.opts, opts);
            this.dataName = dataName;
            this.bind('redraw', this.redraw, this);
            this.bind('hide', this.hide, this);
            this.bind('show', this.show, this);
            $(window).on('resize', _.bind(this.redraw, this));
            this.indent = [];
            this.setInputFilter = _.debounce(_.bind(this.setInputFilter, this), 500);
        },

        /**
         * Для перерисовки грида следует использовать redraw
         * @returns {*}
         */
        render: function () {
            return this;
        },

        events: {
            "click .js-toggle-group": "clickToggleGroup"
        },

        remove: function () {
            this.unbind('redraw');
            this.unbind('hide');
            this.unbind('show');
            $(window).off('resize', _.bind(this.redraw, this));
            this.$el.remove();
        },

        update: function (response, opts) {

            opts = opts ? opts : {};
            this.opts = $.extend({}, this.opts, opts);

            var len = response.length;
            for (var i = 0; i < len; i++) {
                response[i].server = this.opts.server;
            }

            response = this.prepareData(response, opts);
            if (!this.init || this.reinit) {
                this.initGrid(response);
            } else {
                this.updateGrid(response);
            }
            this.redraw();
            this.trigger('update');
        },

        redraw: function () {
            if (!this.init || !this.$el.slick) {
                return;
            }
            if (this.opts.parentHeight) {

                var height = this.$el.parent().height();
                this.$el.parent().children().not(this.$el).each(function () {
                    if ($(this).hasClass('ajax-overlay')) {
                        return;
                    }
                    height -= $(this).outerHeight();
                });

                this.$el.height(height);

            } else {
                var h = 0;
                this.$el.parent().children().not(this.$el).each(function () {
                    if ($(this).hasClass('ajax-overlay')) {
                        return;
                    }
                    h += $(this).outerHeight();
                });
                if (this.opts['padding-top']) {
                    this.$el.height($('#tab-default').height() - h - this.opts['padding-top']);
                } else {
                    this.$el.height($('#tab-default').height() - h);
                }
            }

            this.$el.slick.resizeCanvas();
            this.$el.slick.invalidate();
            this.$el.slick.render();
        },

        hide: function () {
            this.$el.hide();
        },

        show: function () {
            this.$el.show();
        },


        updateColumns: function (data, columns) {
            switch (this.opts.type) {

                case 'tree':
                    Stemapp.grid.init.adminAbonents(this.$el, data, this.dataName, columns);
                    break;

                case 'dialogPlain':
                    Stemapp.grid.init.adminDialogPlainGrid(this.$el, data, this.dataName, columns);
                    break;

                case 'raw':
                    Stemapp.grid.init.adminRawGrid(this.$el, data, this.opts.dataRaw);
                    break;

                case 'mapDetailGrid':
                    Stemapp.grid.init.mapDetailGrid(this.$el, data);
                    break;

                case 'mapTreeGridObjects':
                case 'mapTreeGridZones':
                    Stemapp.grid.init.mapTreeGrid(this.$el, data, this.dataName, columns);
                    break;

                case 'list':
                    Stemapp.grid.init.adminListGrid(this.$el, data, this.dataName, columns, this.opts.columns);
                    break;

                case 'mapJournalGrid':
                    Stemapp.grid.init.mapJournalGrid(this.$el, data);
                    this.$el.slick.setSortColumn("JEEventTime", false);
                    this.$el.dataView.sort(function (a, b) {
                        return -(a.JEEventTime > b.JEEventTime ? 1 : ( a.JEEventTime < b.JEEventTime ? -1 : 0));
                    });
                    this.$el.dataView.reSort();
                    break;

                default :
                    Stemapp.grid.init.adminPlainGrid(this.$el, data, this.dataName, columns);
            }
        },

        initGrid: function (data) {
            this.init = true;


            var columns = [];

            if (this.opts.checkbox) {
                if (!this.checkboxSelector) {
                    this.checkboxSelector = new Slick.CheckboxSelectColumn();
                }
                columns.push(this.checkboxSelector.getColumnDefinition());
            }
            if (this.opts.counter) {
                if (!this.counterColumn) {
                    this.counterColumn = new Slick.CounterColumn();
                }
                columns.push(this.counterColumn.getColumnDefinition());
            }
            if (this.opts.customCheckbox) {
                if (!this.checkboxNavstatSelector) {
                    this.checkboxNavstatSelector = new Slick.CheckboxNavstat(this.opts.customCheckbox);
                }
                columns.push(this.checkboxNavstatSelector.getColumnDefinition());

            }
            this.$el.attr('id', 'table-' + this.cid);


            this.updateColumns(data, columns);

            if (!this.reinit) {

                if (this.opts.grouping) {
                    var groupItemMetadataProvider = new Slick.Data.GroupItemMetadataProvider();
                    this.$el.slick.registerPlugin(groupItemMetadataProvider);
                }
                if (this.opts.contextmenu) {
                    this.$el.contextMenu(_.extend({zIndex: 20}, this.opts.contextmenu));
                }
                if (this.opts.checkbox) {
                    this.$el.slick.registerPlugin(this.checkboxSelector);
                }
                if (this.opts.counter) {
                    this.$el.slick.registerPlugin(this.counterColumn);
                }
                if (this.opts.customCheckbox) {
                    this.$el.slick.registerPlugin(this.checkboxNavstatSelector);
                }
            }
            this.$el.slick.onDblClick.subscribe(this.opts.dblClick);
            this.$el.slick.onContextMenu.subscribe(this.opts.onContextMenu);
            this.$el.slick.onSort.subscribe(this.opts.onSort);
            this.$el.slick.onSelectedRowsChanged.subscribe(this.opts.onSelect);
            this.$el.slick.onActiveCellChanged.subscribe(this.opts.rowChanged);

            this.reinit = false;
        },

        setSortColumn: function (columnId, asceding) {
            this.$el.slick.setSortColumn(columnId, asceding);
        },

        setSortColumns: function (cols) {
            this.$el.slick.setSortColumns(cols);
        },

        getColumns: function () {
            if (this.$el.slick) {
                return this.$el.slick.getColumns();
            }
            return [];
        },

        getItem: function (row) {

            return this.$el.dataView.getItem(row);
        },

        getColumnIndex: function (id) {

            return this.$el.slick.getColumnIndex(id);
        },

        getItemById: function (id) {

            return this.$el.dataView.getItem(this.$el.dataView.getRowById(id));
        },

        getActiveItem: function () {

            var cell = this.$el.slick.getActiveCell();
            if (cell) {
                return this.getItem(cell.row);
            } else {
                return null;
            }
        },

        setActiveItem: function (row) {
            this.$el.slick.setActiveCell(row, 0);
        },

        updateItem: function (item, id) {
            id = id ? id : item.id;
            this.$el.dataView.updateItem(id, item);
        },

        deleteItem: function (item) {

            this.$el.dataView.deleteItem(item.id);
        },

        getActiveCell: function () {

            return this.$el.slick.getActiveCell();
        },

        getItems: function () {

            return this.$el.dataView.getItems();
        },

        getRowById: function (id) {

            return this.$el.dataView.getRowById(id);
        },

        setSelectedRows: function (selected) {
            this.$el.slick.setSelectedRows(selected);
        },

        sort: function (column, asc) {

            this.$el._sort(null, {grid: this.$el.slick, sortCol: column, sortAsc: asc, multiColumnSort: false});
        },

        getSelectedRows: function () {

            var rows = this.$el.slick.getSelectedRows(),
                items = [],
                item;

            var ids = this.$el.dataView.mapRowsToIds(rows);
            for (var i = 0; i < ids.length; i++) {

                item = this.$el.dataView.getItemById(ids[i]);
                items.push(item);
            }
            return items;
        },

        prepareData: function (data, opts) {

            var l = data.length,
                relative = Stemapp.relation[this.dataName],
                id = this.cid;
            if (!relative) {
                console.error('dataName не указан или указан неправильно');
                return;
            }
            for (var i = 0; i < l; i++) {

                if (data[i].id) {
                    continue;
                }

                if (opts.idFieldName && data[i][opts.idFieldName]) {
                    data[i].id = id + '-' + data[i][opts.idFieldName];
                }
                else {
                    data[i].id = id + '-' + ((relative.Id != '' && data[i][relative.Id]) ? data[i][relative.Id] : '_a' + i);
                }

                if (this.opts.type == 'tree') {
                    data[i].parent = data[i][relative.IdParent] == 0 ? 0 : id + '-' + data[i][relative.IdParent];
                }

                if (opts.parentFieldName) {
                    data[i].parent = data[i][opts.parentFieldName] == 0 ? 0 : id + '-' + data[i][opts.parentFieldName];
                    data[i].indent = this.calcIndent(data[i][opts.idFieldName], data[i][opts.parentFieldName]);
                }

                data[i]._table = this.$el;
            }
            return data;
        },

        calcIndent: function (child, parent) {
            if (parent == 0) {
                return 0;
            }
            var indent = 1;
            var tmp = this.indent[parent];

            while (typeof tmp == 'object') {
                indent++;
                tmp = this.indent[Object.keys(tmp)[0]];
            }

            this.indent[child] = [];
            this.indent[child][parent] = 1;
            return indent;
        },

        updateGrid: function (data) {


            switch (this.opts.type) {
                case 'mapTreeGridObjects':
                    this.updateMapObjectGrid(data);
                    break;

                case 'mapTreeGridZones':
                    this.updateMapZoneGrid(data);
                    break;

                case 'mapDetailGrid':

                    this.updateMapDetailGrid(data);
                    break;

                case 'mapJournalGrid':
                    this.updateJournalGrid(data);
                    break;

                case 'list':
                    this.updateListGrid(data);
                    break;

                case 'quickReplaceData':
                    this.updateQuickReplaceGrid(data);
                    break;

                default:
                    this.updateAdminGrid(data);

            }
        },
        updateJournalGrid: function (data) {

            for (var i = 0; i < data.length; i++) {
                this.$el.dataView.insertItem(null, data[i]);
            }
        },

        updateMapDetailGrid: function (data) {

            var dl = data.length;
            var olddata = this.$el.dataView.getItems();

            this.$el.dataView.beginUpdate();
            for (var i = 0; i < dl; i++) {

                data[i]._table = this.$el;
                data[i]._collapsed = olddata[i]._collapsed;
                this.$el.dataView.updateItem(data[i].id, data[i]);
            }
            this.$el.dataView.endUpdate();

        },

        updateMapObjectGrid: function (data) {

            var rows = this.$el.dataView.getItems(),
                arow = this.getActiveItem(),
                ar = this.$el.slick.getActiveCell(),
                rf = [],
                find,
                i, j;
            rows = rows.slice(0, rows.length);

            this.$el.dataView.beginUpdate();
            var dl = data.length,
                rl = rows.length;

            for (i = 0; i < rl; i++) {
                rf[i] = false;
            }

            for (i = 0; i < dl; i++) {

                find = false;
                for (j = 0; j < rl; j++) {

                    if (data[i].id == rows[j].id) {
                        find = true;
                        rf[j] = true;
                        break;
                    }
                }
                if (!find) {
                    // Не найден объект
                    if (data[i].ABId || data[i].MGId || data[i].MOId) {
                        this.addRowInTable(this.$el, data[i]);
                    }
                }
                else {
                    this.updateRowInGrid(this.$el, data[i]);
                }
            }

            for (j = 0; j < rl; j++) {
                if (!rf[j]) {
                    this.$el.dataView.deleteItem(rows[j].id);
                }
            }
            this.$el.dataView.endUpdate();

            if (arow) {
                var r = this.$el.dataView.getRowById(arow.id);
                this.$el.slick.setActiveCell(r, ar.cell);
            }
            this.sort(1, true);
        },

        updateMapZoneGrid: function (data) {

        },

        addRowInTable: function (table, item) {

            item._table = table;
            table.dataView.insertItem(item.parent == 0 ? null : table.dataView.getRowById(item.parent) + 1, item);
        },

        updateRowInGrid: function (table, data) {

            var items = table.dataView.getItems(),
                item = items[table.dataView.getRowById(data.id)];
            data._table = table;
            data.checkbox = item.checkbox;
            data._collapsed = item._collapsed;
            table.dataView.updateItem(data.id, data);
            table.dataView.reSort();
        },

        updateAdminGrid: function (data) {

            var rf = [],
                rows = this.$el.dataView.getItems(),
                dl = data.length,
                rl = rows.length,
                find,
                i, j;
            rows = [].concat(rows);


            this.$el.dataView.beginUpdate();

            var srows = this.$el.slick.getSelectedRows(),
                arow = this.getActiveItem(),
                ar = this.$el.slick.getActiveCell(),
                sids = [];

            for (i = 0; i < srows.length; i++) {
                sids.push(this.$el.dataView.getItemByIdx(srows[i]));
            }

            for (i = 0; i < rl; i++) {
                rf[i] = false;
            }


            for (i = 0; i < dl; i++) {

                find = false;
                for (j = 0; j < rl; j++) {

                    if (data[i].id == rows[j].id) {
                        find = true;
                        rf[j] = true;
                        break;
                    }
                }
                if (!find) {
                    if (this.opts.type == 'tree') {
                        this.$el.dataView.insertItem(data[i].parent == 0 ? null : this.$el.dataView.getRowById(data[i].parent) + 1, data[i]);
                    } else {
                        this.$el.dataView.addItem(data[i]);
                    }
                }
                else {
                    var items = this.$el.dataView.getItems(),
                        item = items[this.$el.dataView.getRowById(data[i].id)];
                    if (item) {
                        data[i]._tablel = this.$el;
                        data[i].checkbox = item.checkbox;
                        data[i]._collapsed = item._collapsed;
                        this.$el.dataView.updateItem(data[i].id, data[i]);
                    }
                }
            }
            for (j = 0; j < rl; j++) {
                if (!rf[j]) {
                    this.$el.dataView.deleteItem(rows[j].id);
                }
            }

            this.$el.dataView.reSort();

            srows = [];
            for (i = 0; i < sids.length; i++) {
                srows.push(this.$el.dataView.getRowById(sids[i].id));
            }
            this.$el.slick.setSelectedRows(srows);

            if (arow) {
                var r = this.$el.dataView.getRowById(arow.id);
                this.$el.slick.setActiveCell(r, ar.cell);
            }
            this.$el.dataView.endUpdate();

        },

        updateQuickReplaceGrid: function (data) {
            this.$el.dataView.setItems(data);
        },

        updateListGrid: function (data) {

            var rf = [],
                rows = this.$el.dataView.getItems(),
                dl = data.length,
                rl = rows.length,
                find,
                i, j;
            rows = [].concat(rows);


            this.$el.dataView.beginUpdate();

            var srows = this.$el.slick.getSelectedRows(),
                arow = this.getActiveItem(),
                ar = this.$el.slick.getActiveCell(),
                sids = [];

            for (i = 0; i < srows.length; i++) {
                sids.push(this.$el.dataView.getItemByIdx(srows[i]));
            }

            for (i = 0; i < rl; i++) {
                rf[i] = false;
            }


            for (i = 0; i < dl; i++) {

                find = false;
                for (j = 0; j < rl; j++) {

                    if (data[i].id == rows[j].id) {
                        find = true;
                        rf[j] = true;
                        break;
                    }
                }
                if (!find) {
                    if (this.opts.type == 'list') {
                        this.$el.dataView.insertItem(data[i].parent == 0 ? null : this.$el.dataView.getRowById(data[i].parent) + 1, data[i]);
                    } else {
                        this.$el.dataView.addItem(data[i]);
                    }
                }
                else {
                    var items = this.$el.dataView.getItems(),
                        item = items[this.$el.dataView.getRowById(data[i].id)];
                    data[i]._tablel = this.$el;
                    data[i].checkbox = item.checkbox;
                    data[i]._collapsed = item._collapsed;
                    this.$el.dataView.updateItem(data[i].id, data[i]);
                }
            }
            for (j = 0; j < rl; j++) {
                if (!rf[j]) {
                    this.$el.dataView.deleteItem(rows[j].id);
                }
            }

            this.$el.dataView.reSort();

            srows = [];
            for (i = 0; i < sids.length; i++) {
                srows.push(this.$el.dataView.getRowById(sids[i].id));
            }
            this.$el.slick.setSelectedRows(srows);

            if (arow) {
                var r = this.$el.dataView.getRowById(arow.id);
                this.$el.slick.setActiveCell(r, ar.cell);
            }
            this.$el.dataView.endUpdate();

        },

        toggleFilter: function () {
            if (this.$el.slick) {
                this.$el.slick.setHeaderRowVisibility(!this.$el.slick.getOptions().showHeaderRow);
                // стираем фильтры
                if (!this.$el.slick.getOptions().showHeaderRow) {
                    this.$el.find('.slick-headerrow input').val('').keyup();
                    this.trigger('filter');
                }

                var self = this;
                this.$el.find('.slick-headerrow input').off('input');
                this.$el.find('.slick-headerrow input').on('input', function () {
                    self.setInputFilter(this);
                });
            }
        },

        setInputFilter: function (input) { // оборачивается debounce в initilization
            $(input).trigger('filter');
            this.trigger('filter');
        },

        setFilter: function (input) {
            var val = $(input).val().toLowerCase();
            if (val != '') {

                _.delay(_.bind(function () {
                    Stemapp.grid.filterTreeGrid(this.$el, val, input);
                    this.trigger('filter');
                }, this), 300);

            } else {

                this.$el.searchString = '';
                this.$el.dataView.refresh();
            }

        },

        getFilteredItems: function () {
            return this.$el.dataView.getFilteredAndPagedItems(this.getItems());
        },

        clear: function () {

            this.$el.dataView && this.$el.dataView.setItems([]);
        },

        setGrouping: function (name) {


            var opts = {getter: name};
            if (this.opts.checkbox) {
                opts.formatter = function (group) {
                    //todo вместо ссылок сделать галочку
                    return group.value + ' [<a href="#" class="js-toggle-group" data-check="true" data-value="' + group.value + '" data-field="' + name + '"> ☑  Все(' + group.count + ')</a> | ' +
                        '<a href="#" class="js-toggle-group" data-check="false" data-value="' + group.value + '" data-field="' + name + '">☐ Ни одного</a></a>]';
                };
            }
            this.$el.dataView.setGrouping(opts);
        },

        clickToggleGroup: function (e) {
            e.preventDefault();
            var $link = $(e.target);
            var items = this.getItems();
            var srows = this.$el.slick.getSelectedRows();
            var find;
            var i;
            if ($link.data('check')) {
                for (i = 0; i < items.length; i++) {
                    if ($link.data('value') == items[i][$link.data('field')]) {
                        if (srows.indexOf(this.$el.dataView.getRowById(items[i].id)) == -1) {
                            srows.push(this.$el.dataView.getRowById(items[i].id));
                        }
                    }
                }
            } else {
                for (i = 0; i < items.length; i++) {
                    if ($link.data('value') == items[i][$link.data('field')]) {

                        find = srows.indexOf(this.$el.dataView.getRowById(items[i].id));
                        if (find != -1) {
                            srows.splice(find, 1);
                        }
                    }
                }

            }
            this.$el.slick.setSelectedRows(srows);
        },

        export: function (clipboard) {
            clipboard = !!clipboard;

            if (!this.$el.slick) {
                return false;
            }

            var columns = this.$el.slick.getColumns(),
                data = (this.$el.dataView.getFilteredAndPagedItems(this.$el.dataView.getItems())).rows,
                dl = data.length,
                sel = this.$el.slick.getSelectedRows(),
                sl = sel.length,
                model = this.$el.model,
                l = columns.length,
                formatters = [],
                csv = [],
                self = this;

            if (dl === 0) {
                return false;
            }

            formatters = self.prepareFormatters(columns, model, data, l);

            csv = csv.concat(self.addingColumnHeaders(columns, l, clipboard));

            csv = csv.concat(self.addCellsText(data, dl, sel, sl, columns, l, formatters, clipboard));

            // got final data
            csv = csv.join('\n');
            return csv;
        },

        addingColumnHeaders: function (columns, l, clipboard) {

            var row = [],
                csv = [];
            for (i = 0; i < l; i += 1) {
                if (columns[i].field !== 'checkbox') {
                    // push column name without html tag
                    row.push(columns[i].name.replace(/<\/?[^>]+>/gi, ''));
                }

            }
            if (clipboard) {
                csv.push('' + row.join('	') + '');
            } else {
                csv.push('"' + row.join('";"') + '"');
            }
            return csv;

        },

        prepareFormatters: function (columns, model, data, l) {
            var formatters = [],
                f,
                find = false,
                j;

            for (j = 0; j < l; j++) {

                if (columns[j].field !== 'checkbox') {

                    f = null;
                    find = false;
                    if (model) {
                        if (model.metadata) {
                            f = columns[j].exportFormatter;

                        } else {
                            f = model.getColumnAtName(columns[j].field).exportFormatter;
                            find = true;
                        }
                    }
                    if (!_.isFunction(f)) {
                        if (_.isBoolean(data[0][columns[j].field])) {
                            f = Stemapp.grid.exportFormatter.bool;
                        } else {
                            f = Stemapp.grid.exportFormatter.html;
                        }
                    }
                    formatters[j] = f;
                }
            }
            return formatters;
        },

        addCellsText: function (data, dl, sel, sl, columns, l, formatters, clipboard) {

            var i,
                csv = [],
                self = this,
                ind = 1;

            if (this.opts.grouping && sl) {
                sel.sort(function (a, b) {
                    return a - b;
                });

                this.$el.dataView.mapRowsToIds(sel).forEach(function (id) {
                    for (i = 0; i < dl; i++) {
                        if (data[i].id == id) {
                            csv = csv.concat(self.addDataToCSV(data, columns, formatters, i, l, ind++, clipboard));

                            break;
                        }
                    }
                })

            } else {

                if (this.opts.grouping && !sl) {

                    //отсортировать data в соответствии с порядком строк в гриде
                    var mapIdsToRows = this.$el.dataView.mapIdsToRows;
                    data.sort(function (dataA, dataB) {
                        var rowA = mapIdsToRows([dataA.id]),
                            rowB = mapIdsToRows([dataB.id]);
                        return rowA - rowB;
                    });
                }

                for (i = 0; i < dl; i++) {

                    if (sl == 0 || sel.indexOf(i) != -1 || self.opts.type == 'raw') {
                        csv = csv.concat(self.addDataToCSV(data, columns, formatters, i, l, ind++, clipboard));
                    }
                }
            }

            return csv;
        },

        addDataToCSV: function (data, columns, formatters, i, l, ind, clipboard) {

            var row = [],
                csv = [],
                j;

            for (j = 0; j < l; j++) {

                if (columns[j].field == 'counter') {
                    row.push(ind);
                } else if (columns[j].field !== 'checkbox') {

                    row.push(formatters[j](i, j, data[i][columns[j].field], columns[j], data[i]).replace(/"/g, '""').replace(/\n/g, " ").trim());
                }
            }
            if (clipboard) {
                csv.push('' + row.join('	') + '');
            } else {
                csv.push('"' + row.join('";"') + '"');
            }
            return csv;
        }


    });
    return Control_Table;
});
