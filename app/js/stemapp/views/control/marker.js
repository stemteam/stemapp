define([
    'jquery',
    'underscore',
    'backbone',
], function ($, _, Backbone) {

    /**
     * @class Control_Marker
     * @constructor
     * @extends Backbone.View
     */
    var Control_Marker = Backbone.View.extend({

        marker: null,

        opts: {},

        initialize: function (latlng, opts) {
            //Template.prototype.initialize.apply(this, arguments);

            this.opts = $.extend(this.opts, opts);

            ymaps.ready(_.bind(function () {
                this.marker = new ymaps.Placemark(latlng, {
                    balloonContent: opts.options ? opts.options : null,
                    hintContent: opts.hintContent ? opts.hintContent : null
                }, {
                    preset: opts.preset ? opts.preset : 'app#default',
                    draggable: opts.draggable ? opts.draggable : null
                });

            }, this));
        },

        events: {},

        closeBalloon: function () {
            ymaps.ready(_.bind(function () {
                this.marker.balloon.close(true); // Закрываем балун, а то карта позицинируется из-за него
            }, this));
        },

        setPreset: function (preset) {
            ymaps.ready(_.bind(function () {
                this.marker.options.set('preset', preset);
            }, this))
        },

        setBalloonContent: function (content) {
            ymaps.ready(_.bind(function () {
                this.marker.properties.set('balloonContent', content);
            }, this))
        },

        addEventDragend: function (func) {
            ymaps.ready(_.bind(function () {
                this.marker.events.add('dragend', func);
            }, this));
        },

        /**\
         * Set coordinates
         * @param coordinates array(lat,lng)
         */
        setCoordinates: function (coordinates) {
            ymaps.ready(_.bind(function () {

                this.marker.geometry.setCoordinates(coordinates);
            }, this))
        },

        getCoordinates: function () {
            if (!this.marker) {
                return null;
            }
            return this.marker.geometry.getCoordinates();
        },

        remove: function () {
        }

    });
    return Control_Marker;
});
