define([
    'backbone',
    'jquery',
    'smarty!_app/tmpl/control/paginator.tpl',
    'tmpl'
], function (Backbone, $, template, tmpl) {

    /**
     * @class Control_Paginator
     * @constructor
     * @extends Backbone.View
     */
    var Control_Paginator = Backbone.View.extend({
        className: 'pagination js-pagination',

        count: 0,

        limit: 20, // количество записей на страницу

        page: 1,

        maxpage: 0,

        template: template,

        opts: {},

        events: {
            'click a': "onChange"
        },

        initialize: function (opts) {
            this.opts = {};
            if (opts) {
                this.opts = $.extend(true, this.opts, opts);
                this.limit = opts.limit ? opts.limit : this.limit;
                this.count = opts.count ? opts.count : null;
                this.maxpage = opts.maxpage ? opts.maxpage : null;
                this.page = opts.page ? opts.page : this.page;
            }
            if (this.count) {
                this.setCount(this.count);
            }
            $(window).on('resize', _.bind(this.resize, this));
            this.bind('resize', this.resize, this);
        },

        onChange: function (e) {
            e.preventDefault();
            var page = $(e.target).data('page');
            if (this.opts.onChange) {
                this.opts.onChange(page);
            }
            this.setPage(page);
        },

        setCount: function (count) {
            this.count = count;
            if (this.limit) {
                this.maxpage = Math.ceil(this.count / this.limit);
            }
            this.render();
        },

        setPage: function (page) {
            this.page = page;
            this.render();
        },

        render: function () {
            this.$el.html(tmpl(this.template, {limit: this.limit, maxpage: this.maxpage, count: this.count, page: this.page}));
            this.delegateEvents();
            return this;
        },

        afterInsert: function () {

        },

        resize: function () {

        }

    });
    return Control_Paginator;
});