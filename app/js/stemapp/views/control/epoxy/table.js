define([
    '_app/views/control/table',
    '_app/misc/TreeNode',
    '_app/data/raw'
], function (Control_Table, toTree) {

    /**
     * @class Epoxy_Control_Table
     * @constructor
     * @extends Control_Table
     */
    var Epoxy_Control_Table = Control_Table.extend({

        opts: {

            idFieldName: null,

            parentFieldName: null,

            type: 'plain',

            enable: false,

            checkbox: true,

            searchInput: null,

            parentHeight: false,

            counter: true,

            customCheckbox: false,

            grouping: false,

            editable: false,

            rowChanged: function () {
            },

            onSelect: function () {
            },

            dblClick: function () {
            },

            onContextMenu: function () {
            },

            onSort: function () {
            }
        },


        initialize: function (dataName, opts) {
            this.opts = $.extend({}, this.opts, opts);
            this.dataName = dataName;
            if (typeof dataName == 'object') {
                this.collection = dataName;
                //this.collection.on('add change remove', _.bind(this.changeCollection, this));
                this.collection.on('afterSet remove', _.bind(this.changeCollection, this));
            } else {
                this.collection = null;
            }
            this.bind('redraw', this.redraw, this);
            this.bind('hide', this.hide, this);
            this.bind('show', this.show, this);
            $(window).on('resize', _.bind(this.redraw, this));
            this.indent = [];
            this.setInputFilter = _.debounce(_.bind(this.setInputFilter, this), 500);
        },

        changeCollection: function () {
            if (!this.init || this.reinit) {
                this.initGrid();
            } else {
                this.updateGrid();
            }
            this.redraw();
            this.trigger('update');
            this.sort(1, true);

        },

        prepareData: function () {
            var i;
            var id = this.cid;
            var data = [];
            for (i = 0; i < this.collection.length; i++) {
                var attr = this.collection.models[i].attributes;
                data.push(attr);
            }

            var l = this.collection.length;
            for (i = 0; i < l; i++) {

                if (this.opts.idFieldName && data[i][this.opts.idFieldName]) {
                    data[i].id = id + '-' + data[i][this.opts.idFieldName];
                } else {
                    data[i].id = id + '-' + this.collection.models[i].id;
                }

                if (this.collection.models[i].parentAttribute || this.opts.parentFieldName) {
                    if (this.opts.parentFieldName) {
                        data[i].parent = id + '-' + data[i][this.opts.parentFieldName];
                        data[i].indent = this.calcIndent(this.collection.models[i].id, data[i][this.opts.parentFieldName]);
                    }
                    else if (this.collection.models[i].parentAttribute) {
                        data[i].parent = id + '-' + data[i][this.collection.models[i].parentAttribute];
                        data[i].indent = this.calcIndent(this.collection.models[i].id, data[i][this.collection.models[i].parentAttribute]);
                    }
                }
                data[i]._table = this.$el;
                data[i]._model = this.collection.models[i];
            }
            return data;
        },

        updateColumns: function (data, columns) {
            switch (this.opts.type) {

                case 'dialogPlain':
                    Stemapp.grid.init.adminDialogPlainGrid(this.$el, data, this.dataName, columns);
                    break;

                case 'raw':
                    Stemapp.grid.init.adminRawGrid(this.$el, data, this.opts.dataRaw);
                    break;

                case 'mapDetailGrid':
                    Stemapp.grid.init.mapDetailGrid(this.$el, data);
                    break;

                case 'mapTreeGridObjects':
                case 'mapTreeGridZones':
                    Stemapp.grid.init.mapTreeGrid(this.$el, data, this.dataName, columns);
                    break;

                case 'list':
                    Stemapp.grid.init.adminListGrid(this.$el, data, this.dataName, columns, this.opts.columns);
                    break;

                case 'mapJournalGrid':
                    Stemapp.grid.init.mapJournalGrid(this.$el, data);
                    this.$el.slick.setSortColumn("JEEventTime", false);
                    this.$el.dataView.sort(function (a, b) {
                        return -(a.JEEventTime > b.JEEventTime ? 1 : ( a.JEEventTime < b.JEEventTime ? -1 : 0));
                    });
                    this.$el.dataView.reSort();
                    break;

                default :
                    this.initSlickGrid(columns);
            }
        },

        initSlickGrid: function (columns) {

            columns = columns ? columns : [];
            var options = {
                editable: this.opts.editable,
                enableAddRow: false,
                enableCellNavigation: true,
                asyncEditorLoading: false,
                rowHeight: 30,
                headerRowHeight: 30,
                explicitInitialization: true,
                enableTextSelectionOnCells: true,
                autoEdit: false
            };

            var me = this;

            var data = this.prepareData();

            var id = this.$el.attr('id'),
                table = this.$el,
                i;

            var model = Stemapp.data.raw;

            if (this.opts.columns) {
                // Список колонок
                var cols = this.opts.columns;
                for (i = 0; i < cols.length; i++) {
                    columns = this.addColumn(columns, {
                        id: cols[i].name,
                        name: cols[i].caption,
                        field: cols[i].name,
                        width: cols[i].width ? cols[i].width : 100,
                        minWidth: cols[i].width ? cols[i].width : 100,
                        formatter: cols[i].format,
                        cssClass: cols[i].cssClass,
                        linkId: cols[i].linkId,
                        headerCssClass: cols[i].headerCssClass,
                        editor: cols[i].hasOwnProperty('editor') ? cols[i].editor : Slick.ROEditors.Text,
                        exportFormatter: cols[i].exportFormatter,
                        sortable: true
                    });
                    model.columns.push({
                        name: cols[i].name,
                        exportFormatter: cols[i].exportFormatter ? cols[i].exportFormatter : Stemapp.grid.exportFormatter.text
                    });
                }

            } else {
                // Нет описания колонок, возьмем за основу модель
                var tempModel = new this.collection.model();
                for (var k in tempModel.attributes) {
                    if (tempModel.attributes.hasOwnProperty(k)) {
                        columns = this.addColumn(columns, {
                            id: k,
                            name: k,
                            field: k
                        });
                        model.columns.push({
                            name: k,
                            exportFormatter: Stemapp.grid.exportFormatter.text
                        });
                    }
                }
            }

            table.searchString = '';
            table.columnSearch = {};

            table.model = model;

            table.dataView = new Slick.Data.DataView({inlineFilters: true});
            table.dataView.beginUpdate();
            table.dataView.setItems(data);

            if (this.opts.type == 'tree') {
                table.dataView.setFilter(Stemapp.grid.filter.adminTreeGrid);
            } else {
                table.dataView.setFilter(Stemapp.grid.filter.adminPlainGrid);
            }

            table.dataView.endUpdate();
            table.dataView.onRowsChanged.subscribe(function (e, args) {
                table.slick.invalidateRows(args.rows);
                table.slick.render();
            });

            table.dataView.onRowCountChanged.subscribe(function (e, args) {
                table.slick.updateRowCount();
                table.slick.render();
            });

            Stemapp.grid.appendMetaData(table);

            table.slick = new Slick.Grid('#' + table.attr('id'), table.dataView, columns, options);
            table.slick.setSelectionModel(new Slick.RowSelectionModel({selectActiveRow: false}));
            table.dataView.syncGridSelection(table.slick, true);

            if (this.opts.type == 'tree') {
                // todo изменять надо коллекцию
                table.slick.onClick.subscribe(function (e, args) {
                    if ($(e.target).hasClass("toggle")) {
                        var item = table.dataView.getItem(args.row);
                        if (item) {
                            if (!item._collapsed) {
                                item._collapsed = true;
                            } else {
                                item._collapsed = false;
                            }

                            table.dataView.updateItem(item.id, item);
                        }
                        e.stopImmediatePropagation();
                    }
                });
            }


            table.slick.onHeaderCellRendered.subscribe(function () {
                table.find('.indeterminate').prop('indeterminate', true).removeClass('indeterminate');
            });

            table.slick.onSort.subscribe(table._sort = function (e, args) {

                var field = args.sortCol.field,
                    sortAsc = args.sortAsc;

                var items = table.dataView.getItems(),
                    l = items.length;

                // save selection row

                var rows = (table.slick.getSelectedRows()),
                    active = table.slick.getActiveCell() ? table.dataView.getItemByIdx(table.slick.getActiveCell().row) : null,
                    col = table.slick.getActiveCell() ? table.slick.getActiveCell().cell : null,
                    i,
                    sel = [];
                for (i = 0; i < rows.length; i++) {

                    sel.push(table.dataView.getItemByIdx(rows[i]))
                }

                if (me.opts.type == 'tree') {
                    var tree = toTree(items, function (a, b, field, sortAsc) {

                        if (a.data[field] == b.data[field]) {
                            return 0;
                        }
                        return (a.data[field] < b.data[field] ? -1 : 1) * ( sortAsc ? 1 : -1);
                    }, field, args.sortAsc);

                    items = (tree.getArray());

                } else {
                    items.sort(function (a, b) {

                        if (a[field] == b[field]) {
                            return 0;
                        }
                        return (a[field] < b[field] ? -1 : 1) * ( sortAsc ? 1 : -1);
                    });
                }
                table.dataView.beginUpdate();
                table.dataView.setItems(items);
                table.dataView.endUpdate();

                if (sel.length) {
                    // restore selected row
                    var srows = [];

                    for (i = 0; i < sel.length; i++) {

                        srows.push(table.dataView.getIdxById(sel[i].id));
                    }
                    table.slick.setSelectedRows(srows, 1);
                }

                if (active) {

                    table.slick.setActiveCell(table.dataView.getIdxById(active.id), col);
                }
            });

            table.slick.onHeaderRowCellRendered.subscribe(function (e, args) {
                $(args.node).empty();
                if (args.column.id == 'counter' || args.column.id == '_checkbox_selector') {
                    return '';
                }
                $("<input type='text'>")
                    .data("columnId", args.column.id)
                    .appendTo(args.node)
                    .on('keyup', function () {
                        table.columnSearch[args.column.id] = $(this).val();
                        table.dataView.refresh();
                    });

                table.columnSearch[args.column.id] = '';
            });

            table.slick.init();
            table.slick.updateColumnHeader('_checkbox_selector');
        },

        addColumn: function (columns, opts) {

            var def = {
                id: null, // идентификатор поля(название на латинице)
                name: null, // заголовок столбца (то что отображается в шапке)
                field: null, // название поля на латинице
                width: 100,
                minWidth: null,
                formatter: Stemapp.grid.exportFormatter.text,
                linkId: null,
                cssClass: null,
                headerCssClass: null,
                editor: Slick.ROEditors.Text,
                sortable: true
            };

            columns.push(_.extend(def, opts));
            return columns;
        },

        updateGrid: function () {

            switch (this.opts.type) {
                case 'mapTreeGridObjects':
                    this.updateMapObjectGrid(data);
                    break;

                case 'mapTreeGridZones':
                    this.updateMapZoneGrid(data);
                    break;

                case 'mapDetailGrid':

                    this.updateMapDetailGrid(data);
                    break;

                case 'mapJournalGrid':
                    this.updateJournalGrid(data);
                    break;

                case 'list':
                    this.updateListGrid(data);
                    break;

                default:
                    this.updateAdminGrid();

            }
        },

        updateAdminGrid: function () {

            var data = this.prepareData();
            var rf = [],
                rows = this.$el.dataView.getItems(),
                dl = data.length,
                rl = rows.length,
                find,
                i, j;
            rows = [].concat(rows);

            this.$el.dataView.beginUpdate();

            var srows = this.$el.slick.getSelectedRows(),
                arow = this.getActiveItem(),
                ar = this.$el.slick.getActiveCell(),
                sids = [];

            for (i = 0; i < srows.length; i++) {
                sids.push(this.$el.dataView.getItemByIdx(srows[i]));
            }

            for (i = 0; i < rl; i++) {
                rf[i] = false;
            }

            for (i = 0; i < dl; i++) {

                find = false;
                for (j = 0; j < rl; j++) {

                    if (data[i].id == rows[j].id) {
                        find = true;
                        rf[j] = true;
                        break;
                    }
                }
                if (!find) {
                    if (this.opts.type == 'tree') {
                        this.$el.dataView.insertItem(data[i].parent == 0 ? null : this.$el.dataView.getRowById(data[i].parent) + 1, data[i]);
                    } else {
                        this.$el.dataView.addItem(data[i]);
                    }
                }
                else {
                    var items = this.$el.dataView.getItems(),
                        item = items[this.$el.dataView.getRowById(data[i].id)];
                    if (item) {
                        data[i].checkbox = item.checkbox;
                        data[i]._collapsed = item._collapsed;
                        this.$el.dataView.updateItem(data[i].id, data[i]);
                    }
                }
            }
            for (j = 0; j < rl; j++) {
                if (!rf[j]) {
                    this.$el.dataView.deleteItem(rows[j].id);
                }
            }

            this.$el.dataView.reSort();

            srows = [];
            for (i = 0; i < sids.length; i++) {
                srows.push(this.$el.dataView.getRowById(sids[i].id));
            }
            this.$el.slick.setSelectedRows(srows);

            if (arow) {
                var r = this.$el.dataView.getRowById(arow.id);
                this.$el.slick.setActiveCell(r, ar.cell);
            }
            this.$el.dataView.endUpdate();

        },

        getModel: function (row) {
            return this.$el.dataView.getItem(row)._model;
        },

        getActiveModel: function () {
            var cell = this.getActiveCell();
            if (cell && cell.row !== null) {
                return this.getModel(cell.row);
            } else {
                return null;
            }
        },

        /**
         * Set columns
         * @param options
         * @public
         */
        setOptions: function (options) {
            for (var i in options) {
                this.opts[i] = options[i];
            }
        },

        /**
         * Get columns
         * @returns object
         * @public
         */
        getOptions: function () {
            return this.opts;
        }


    });

    return Epoxy_Control_Table;
});