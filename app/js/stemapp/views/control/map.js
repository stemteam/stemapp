define([
    'jquery',
    'underscore',
    'backbone',
    '_app/misc/presets'
], function ($, _, Backbone, Presets) {

    /**
     * @class Control_Map
     * @constructor
     * @extends Backbone.View
     */
    var Control_Map = Backbone.View.extend({

        map: null,
        opts: {
            center: null,
            controls: ['zoomControl', 'typeSelector', 'fullscreenControl'],
            zoom: 13
        },

        initialize: function (opts) {
            //Template.prototype.initialize.apply(this, arguments);

            this.opts = $.extend({}, this.opts, opts);
            if (!ymaps) {
                $('<script>').attr({type: 'text/javascript', src: 'https://api-maps.yandex.ru/2.1.15/?lang=ru_RU'}).appendTo($('body'));
            }
            Presets.init();
        },

        init: function (opts) {

            this.opts = $.extend(this.opts, opts);
            ymaps.ready(_.bind(function () {

                this.map = new ymaps.Map(this.opts.id, {
                    center: this.opts.center,
                    zoom: this.opts.zoom,
                    controls: this.opts.controls
                });

            }, this));
        },

        events: {},

        getMap: function () {
            return this.map;
        },

        addEventBoundsChange: function (func) {
            ymaps.ready(_.bind(function () {
                this.map.events.add('boundschange', func);
            }, this));
        },


        addButtonTaxi: function () {
            ymaps.ready(_.bind(function () {

                var buttonTaxi = ymaps.templateLayoutFactory.createClass(
                    '<ymaps class="map-taxi-button ymaps-2-1-15-button ymaps-2-1-15-button_theme_normal ' +
                    'ymaps-2-1-15-button_icon_only" tstyle="max-width: 90px" type="button" role="button">' +
                    "<div class='icon {% if state.selected %}my-button-selected{% endif %}'>" +
                    "Заказать такси" +
                    "</div>" +
                    "</ymaps>"
                );

                var yandexTaxi = new ymaps.control.Button({
                    options: {
                        layout: buttonTaxi
                    }
                });
                yandexTaxi.events.add('press', function () {
                    var $a = $('<a>').attr('href', 'https://taxi.yandex.ru/').attr('target', '_blank').text('X');
                    $a.appendTo($('body'));
                    $a.get(0).click();
                });

                this.map.controls.add(yandexTaxi, {float: 'none', position: {top: 45, right: 10}, floatIndex: 100});

            }, this));
        },

        addButtonGeolocation: function (locationchange, press) {
            ymaps.ready(_.bind(function () {

                var button = new ymaps.control.GeolocationControl({
                    selectOnClick: false,
                    maxWidth: [30]
                });
                if (locationchange) {
                    button.events.add('locationchange', locationchange);
                }
                if (press) {
                    button.events.add('press', press);
                }

                this.map.controls.add(button, {float: 'right', floatIndex: 100});

            }, this));
        },

        addMarker: function (marker) {
            ymaps.ready(_.bind(function () {
                this.map.geoObjects.add(marker.marker);
            }, this));
        },

        removeMarker: function (marker) {
            ymaps.ready(_.bind(function () {
                this.map.geoObjects.remove(marker.marker);
            }, this));
        },

        getBounds: function () {
            if (!this.map) {
                return null;
            }
            var ret = this.map.getBounds();
            return ret;
        },

        getProjection: function () {
            return this.map.options.get('projection');
        },

        setCenter: function (latlng, zoom, options) {
            this.map.setCenter(latlng, zoom, options);
        },

        setBounds: function (bounds, options) {
            if (this.map) {
                this.map.setBounds(bounds, options);
            } else {
                ymaps.ready(_.bind(function () {
                    this.map.setBounds(bounds, options);
                }, this));
            }
        },

        getZoom: function () {
            return this.map.getZoom();
        },

        remove: function () {
        }

    });

    return Control_Map;
});
