define([
    'backbone',
    'jquery',
    '_jq/jquery.navstat.dateinput'
], function (Backbone, $) {

    /**
     * @class Control_Toolbar
     * @constructor
     * @extends Backbone.View
     */
    var Control_Toolbar = Backbone.View.extend({

        className: 'toolbar',

        items: [],

        right: false,

        default: {
            type: 'button',
            text: 'Button',
            compact: false,
            icon: true,
            uri: '/',
            cls: '',
            invalidate: false,
            grouped: false,
            right: false,
            toggle: false,
            modes: {},
            onEnterPress: function () {
            }
        },

        mode: false, // тип представления (modal - модальное окно)

        events: {
            "mousedown .toggle": "toggle",

            "click .toolbar-item.fright.li-drop-down": "showDropDown"
        },

        initialize: function (items, parent) {

            this.parent = parent;
            this.setItems(items);
            $(window).on('resize', _.bind(this.resize, this));
        },

        render: function () {

            this.$el.empty();
            this.right = false;
            var rights = [];
            var events = {};

            for (var i = 0; i < this.items.length; i++) {

                this.items[i]._cls = 'js-toolbar-' + this.cid + '-' + i;

                if (this.items[i] == '>') {
                    this.right = true;

                } else if (this.items[i] == '-') {
                    this.$el.append(this.separator(this.items[i]));

                } else {
                    // Проверяем есть ли эта кнопка в таком представлении
                    if (!this.mode || this.items[i].modes[this.mode]) {

                        if (!this.right) {
                            this.$el.append(this[this.items[i].type](this.items[i]));
                            if (this.items[i].events) {
                                var event;
                                for (var ev in this.items[i].events) {
                                    if (this.items[i].events.hasOwnProperty(ev)) {
                                        event = ev + " ." + this.items[i]._cls;
                                        events[event] = _.bind(this.items[i].events[ev], this, this.parent);
                                    }
                                }
                            }
                        } else {
                            rights.push(this[this.items[i].type](this.items[i]));
                        }
                    }
                }
            }
            this.delegateEvents(events);
            this.$el.append(rights.reverse().join(''));

            return this;
        },

        afterInsert: function () {
            this.items.forEach(function (item) {
                if (item.type == 'datapicker') {
                    var cls = item.cls ? item.cls : 'toolbar-datapicker';
                    var datapicker = {
                        type: 'datetime',
                        datepicker: true,
                        defaultTime: '00:00:00',
                        date: item.date
                    };
                    if (item.maxValue) {
                        datapicker = _.extend(datapicker, {max: this.$('.' + item.maxValue)});
                    }
                    if (item.minValue) {
                        datapicker = _.extend(datapicker, {min: this.$('.' + item.minValue)});
                    }

                    this.$('.' + cls).customDateInput(datapicker);
                }
                if (item.type == 'datepicker') {
                    var cls = item.cls ? item.cls : 'toolbar-datepicker';
                    var datapicker = {
                        type: 'date',
                        datepicker: true,
                        date: item.date
                    };
                    if (item.maxValue) {
                        datapicker = _.extend(datapicker, {max: this.$('.' + item.maxValue)});
                    }
                    if (item.minValue) {
                        datapicker = _.extend(datapicker, {min: this.$('.' + item.minValue)});
                    }

                    this.$('.' + cls).customDateInput(datapicker);
                }
            }, this);

            this.resize();
        },

        setMode: function (mode) {
            this.mode = mode;
        },

        resize: function () {
            this.$el.removeClass('compact');
            var width = 0;
            this.$('.toolbar-item').each(function () {
                width += $(this).outerWidth();
            });

            if (width + 10 > this.$el.width()) {
                this.$el.addClass('compact');
                _.delay(_.bind(this.checkPanelOverflow, this), 300);
            }

        },

        checkPanelOverflow: function () {
            var itemOverflow = new Array();
            for (var i = 0; i < this.$('.toolbar-item').length; i++) {
                if (this.$(this.$('.toolbar-item')[i]).position().top > 15) {
                    !this.$(this.$('.toolbar-item')[i]).hasClass('li-drop-down') && itemOverflow.push(this.$(this.$('.toolbar-item')[i]));
                }
            }

            if (itemOverflow.length) {
                this.addDropDown(itemOverflow);
            } else {
                this.delDropDown();
            }
        },

        addDropDown: function (items) {
            if (!this.$('.toolbar-item.fright.li-drop-down').length) {
                this.$(this.$('.toolbar-item')[0]).before(
                    this.button({
                            cls: 'drop-down',
                            text: 'Дополнительно',
                            icon: 'drop-down',
                            compact: true,
                            type: 'button',
                            uri: '/',
                            invalidate: false,
                            grouped: false,
                            right: true,
                            toggle: false
                        }
                    )
                );
            }

            if (!this.$('.drop-down-panel').length)
                this.$el.append('<div class="drop-down-panel hide"></div>');

            this.addItemToDropDown(items);
        },

        showDropDown: function () {
            var toolbar = this.$el;

            toolbar.closest('.tab').addClass('overflow-hidden');

            if (toolbar.hasClass('expanded')) {
                toolbar.removeClass('expanded');
                _.delay(function () {
                    $(window).trigger('resize');
                    toolbar.closest('.tab').removeClass('overflow-hidden');
                }, 250);
            } else {
                toolbar.addClass('expanded')
                _.delay(function () {
                    $(window).trigger('resize');
                    toolbar.closest('.tab').removeClass('overflow-hidden');
                }, 250);
            }

            return false;
        },

        delDropDown: function () {
            this.$('.toolbar-item.fright.li-drop-down').remove()
            this.$('.drop-down-panel').remove();
        },

        addItemToDropDown: function (items) {
            this.$('.drop-down-panel').empty();
            items.forEach(function (item) {
                this.$('.drop-down-panel').append(item.clone());
            });
        },

        setItems: function (items) {

            this.items = items;
            for (var i = 0; i < this.items.length; i++) {
                if (_.isObject(this.items[i])) {
                    this.items[i] = $.extend({}, this.default, this.items[i]);
                    if (this.items[i].icon && this.items[i].icon === true) {
                        this.items[i].icon = this.items[i].cls;
                    }
                }
            }
        },

        toggle: function (e) {

            $(e.currentTarget).toggleClass('press');
            return false;
        },

        separator: function () {

            return '<li class="toolbar-item separator' + (this.right ? ' right' : '') + '"> </li>';
        },

        button: function (opts) {

            return [
                '<li class="toolbar-item',
                this.right ? ' fright' : '',
                ' li-', opts.cls,
                '"><a href="', opts.uri.match(/^https?:/) ? opts.uri : '#' + opts.uri, '" class="button ', opts.cls, ' ', opts._cls,
                opts.invalidate ? ' invalidate' : '',
                (opts.compact === true) ? ' button-icon' : '',
                opts.toggle ? ' toggle' : '',
                '" title="',
                opts.text,
                '"',
                opts.invalidate ? ' disabled="disabled"' : '',
                (typeof(opts.compact) == 'string') ? ' compact="' + opts.compact + '"' : '',
                opts.target ? ' target="' + opts.target + '"' : '',
                '>',
                (opts.icon ? ('<i class="icon icon-' + opts.icon + '"></i>') : '') + (opts.compact === true ? '' : '<span>' + opts.text + '</span>'),
                '</a></li>'
            ].join('');
        },

        input: function (opts) {

            return [
                '<li class="toolbar-item input ',
                opts.cls,
                opts.invalidate ? ' invalidate' : '',
                this.right ? ' fright' : '',
                '">',
                '<input type="text" ',
                ' class="', opts.cls, ' ', opts._cls, '"',
                opts.invalidate ? ' disabled="disabled"' : '',
                ' placeholder="', opts.placeholder, '"',
                'value="', opts.value, '" />',
                '</li>'
            ].join('');
        },

        checkbox: function (opts) {

            return [
                '<li class="toolbar-item checkbox',
                opts.cls.length ? ' ' + opts.cls : '',
                this.right ? ' fright' : '',
                '"><label for="checkbox_', this.cid, '"><input type="checkbox" id="checkbox_', this.cid, '" class="checkbox ',
                opts.cls.length ? ' ' + opts.cls : '', ' ', opts._cls,
                opts.invalidate ? ' invalidate' : '',
                '"',
                opts.checked ? ' checked="checked"' : '',
                ' />',
                opts.text.length ? '&nbsp;' + opts.text : '',
                '</label></li>'
            ].join('');
        },

        dateinput: function (opts) {

            return [
                '<li class="toolbar-item dateinput ',
                opts.cls,
                opts.invalidate ? ' hidden' : '',
                this.right ? ' right' : '',
                '">',
                opts.label ? '<label for="input_' + this.cid + '"><span>' + opts.label + '</span> ' : '',
                '<input id="date_', this.cid, '" type="text"data-type="dateinput" data-format="', opts.format, '" ',
                ' class="', opts.cls, ' ', opts._cls, '"',
                'value="', opts.value, '" autocomplete="off" />',
                opts.label ? '</label>' : '',
                '</li>'
            ].join('');
        },

        chooser: function (opts) {

            return [
                '<li class="toolbar-item ',
                opts.cls,
                opts.invalidate ? ' hidden' : '',
                this.right ? ' right' : '',
                '">',
                opts.label ? '<label for="choose_' + this.cid + '"><span>' + opts.label + '</span> ' : '',
                '<div class="forinput js-input-block"',
                opts.width ? ' style="width:' + opts.width + 'px;" ' : '',
                '>',
                '<input type="text" class="', opts.cls, ' ', opts._cls, ' field" id="choose_', this.cid, '"',
                ' value="', opts.value, '"',
                ' placeholder="' + opts.placeholder + '" readonly="readonly">',
                '<span class="input-icon"><a href="#" class="button button-icon no-border js-clear-input"',
                'title="Очистить поле">✕</a></span>',
                '<span class="input-addon">',
                '<button class="button button-icon js-form-field" type="button">...</button>',
                '</span>',
                '<input type="hidden" class="', opts.cls, '-id" value="', opts.valueId, '">',
                '</div>',
                opts.label ? '</label>' : '',
                '</li>'
            ].join('');


        },

        label: function (opts) {

            return [
                '<li class="toolbar-item label ',
                opts.cls,
                this.right ? ' right' : '',
                '"><span>',
                opts.text,
                '</span></li>'
            ].join('');
        },

        selectbox: function (opts) {

            var options = [],
                text = '',
                val = 0,
                id = 'select-' + Math.round(Math.random() * 10000);
            for (var op in opts.values) {
                if (opts.values.hasOwnProperty(op)) {
                    options.push('<li class="item"><a href="#" data-id="', op, '"',
                        '>', opts.values[op], '</a></li>');
                    if (op == opts.selection) {
                        text = opts.values[op];
                        val = op;
                    }
                }
            }

            return [
                '<li class="toolbar-item selectbox',

                opts.invalidate ? ' hidden' : '',
                this.right ? ' right' : '',
                '">',

                opts.label ? '<label><span>' + opts.label + '</span> ' : '',
                '<div class="select-input-block forselect"',
                opts.width ? ' style="width:' + opts.width + 'px;" ' : '',
                '>',
                '<input type="text" class="field select-input',
                opts.cls.length ? ' js-select-' + opts.cls : '',

                opts.invalidate ? ' invalidate' : '', '" value="',
                text,
                '" id="', id, '" ', opts.placeholder ? 'placeholder="' + opts.placeholder + '"' : '',
                opts.width ? ' style="width:' + opts.width + 'px;" ' : '',
                'autocomplete="off">',
                '<ul class="model_list select-input-list" data-input="', id, '" style="display: none;">',
                options.join(''),
                '</ul>',
                '<input type="hidden" data-input-main="', id, '" value="', val, '" ',
                (opts.cls.length ? 'class="' + opts.cls + '"' : ''), ' >',
                '</div>',
                opts.label ? '</label>' : '',
                '</li>'
            ].join('');
        },

        datapicker: function (opts) {
            var cls = opts.cls ? 'toolbar-datapicker ' + opts.cls : 'toolbar-datapicker';

            return [
                '<li class="toolbar-item dateTime-inline">',
                opts.label ? '<label for="dateStart">  ' + opts.label + '  </label>' : '',
                '<input type="text" name="dateStart" class="' + cls + '" id="dateStart" value="{$dStart|date_format:"%d.%m.%Y %H:%M:%S"}" />',
                '</li>'

            ].join('');
        },

        datepicker: function (opts) {
            var cls = opts.cls ? 'toolbar-datepicker ' + opts.cls : 'toolbar-datapicker';

            return [
                '<li class="toolbar-item dateTime-inline">',
                opts.label ? '<label for="dateStart">  ' + opts.label + '  </label>' : '',
                '<input type="text" name="dateStart" class="' + cls + '" id="dateStart" value="{$dStart|date_format:"%d.%m.%Y"}" />',
                '</li>'

            ].join('');
        }
    });
    return Control_Toolbar;
});