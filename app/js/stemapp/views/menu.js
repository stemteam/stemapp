define([
    '_app/views/templates/tab',
    '_app/models/menu',
    '_app/models/form',
    '_app/models/role',
    'smarty!_app/tmpl/menu/form.tpl',
], function (Template_Tab, Model_Menu, Model_Form, Model_Role, Template_Form) {
    'use strict';
    return Template_Tab.extend({

        tagName: 'div',

        /**
         * @class Model_Menu
         */
        model: null,

        template: Template_Form,

        name: 'menu',

        icon: 'application-sidebar-list',

        caption: 'Меню',

        url: 'menus{if $parent}/{$parent}{/if}{if $new}/{$new}{else}/{$id}{/if}',

        autoToolbar: false,

        single: true,

        notRender: true,

        viewType: {
            0: 'Форма',
            1: 'Таблица',
            2: 'Календарь'
        },

        initialize: function (opts) {

            Template_Tab.prototype.initialize.apply(this, [opts]);

            if (opts.arguments) {
                if (opts.arguments.length == 1) {
                    this.MenuId = opts.arguments[0];
                    this.new = opts.arguments[0] == 'new';
                    this.ParentId = null;
                } else {
                    this.MenuId = opts.arguments[1];
                    this.new = opts.arguments[1] == 'new';
                    this.ParentId = opts.arguments[0];
                }
            }
            this.urlData = {id: this.MenuId, parent: this.ParentId, 'new': this.new ? 'new' : ''};

            this.model = new Model_Menu({'new': this.new});
            this.model.bind('applyAdd', this.successAdd, this);
            this.modelForm = new Model_Form({'new': this.new});
            this.modelForm.bind('apply', this.successForm, this);

            this.modelRole = new Model_Role();
            this.modelRole.bind('loadAllRoles', this.loadAllRoles, this);
            if (this.new) {
                this.setCaption('Новое меню');
            }
            this.initModel();
            this.load();
        },

        events: {
            "submit form": "submit",
            "click .js-to-parent": "toParent"
        },

        load: function () {
            this.hideAjax();
            this.modelForm.load();
        },

        apply: function () {

            this.notRender = false;
            if (this.new) {
                this.tmplData = {
                    id: this.MenuId,
                    forms: this.forms,
                    parent: this.parent,
                    roles: this.roles,
                    menus: this.menus,
                    viewTypes: this.viewType
                };
            } else {
                this.tmplData = {
                    item: this.data,
                    parent: this.parent,
                    id: this.MenuId,
                    forms: this.forms,
                    roles: this.roles,
                    menus: this.menus,
                    viewTypes: this.viewType
                };
                this.setCaption(this.data.Description);
            }
            this.render();
            this.hideAjax();
        },

        successForm: function (data) {
            this.forms = data;
            this.model.load({data: {ShowAll: 1}, success: _.bind(this.successMenus, this)});
        },

        successMenus: function (data) {
            this.menus = data;
            this.modelRole.loadAllRoles();
        },

        loadAllRoles: function (data) {
            this.roles = data;
            if (this.MenuId && this.MenuId !== 'new') {
                this.model.getById({data: {MenuId: this.MenuId}, success: _.bind(this.successMenu, this)});
            } else if (this.ParentId) {
                this.model.getById({data: {MenuId: this.ParentId}, success: _.bind(this.successParent, this)});
            } else {
                this.apply();
            }
        },

        successMenu: function (data) {
            this.data = data;
            if (this.data.ParentId) {
                this.showAjax();
                this.model.getById({data: {MenuId: this.data.ParentId}, success: _.bind(this.successParent, this)});
            } else {
                this.apply();
            }
        },

        successParent: function (data) {
            this.parent = data;
            this.apply();
        },

        submit: function (e) {

            e.preventDefault();

            this.showAjax(this.$('.js-submit'));
            this.tabAjax();

            if (this.new) {
                this.model.save({
                    MenuName: this.$('[name="MenuName"]').val(),
                    MenuTooltip: this.$('[name="MenuTooltip"]').val(),
                    MenuNum: this.$('[name="MenuNum"]').val(),
                    MenuParentId: this.$('[name="MenuParentId"]').val(),
                    Param: this.$('[name="Param"]').val(),
                    Icon: this.$('[name="Icon"]').val(),
                    MenuUrl: this.$('[name="MenuUrl"]').val(),
                    ViewType: this.$('[name="ViewType"]').val(),
                    FormId: this.$('[name="FormId"]').val()
                }, {error: _.bind(this.requestError, this), success: _.bind(this.successAdd, this)});
            } else {

                this.model.save({
                    MenuName: this.$('[name="MenuName"]').val(),
                    MenuTooltip: this.$('[name="MenuTooltip"]').val(),
                    MenuNum: this.$('[name="MenuNum"]').val(),
                    FormId: this.$('[name="FormId"]').val(),
                    MenuParentId: this.$('[name="MenuParentId"]').val(),
                    MenuId: this.MenuId,
                    ViewType: this.$('[name="ViewType"]').val(),
                    Param: this.$('[name="Param"]').val(),
                    MenuUrl: this.$('[name="MenuUrl"]').val(),
                    Icon: this.$('[name="Icon"]').val()
                }, {error: _.bind(this.requestError, this), success: _.bind(this.successAdd, this)});
            }
        },

        toParent: function (e) {
            e.preventDefault();
            this.trigger('open', 'Menu', [this.ParentId]);
        },

        successAdd: function (response) {
            this.hideAjax();
            this.trigger('refreshTabs', 'Menus', []);

            if (this.new) {
                this.trigger('open', 'Menu', [response.MenuId]);
                this.sendMessage('Меню создано');
                this.remove();
            }
            else {
                this.sendMessage('Изменения сохранены');
            }
        }
    });
});