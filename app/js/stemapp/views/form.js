define([
    '_app/views/templates/tab',
    '_app/models/form',
    '_app/models/method',
    'smarty!_app/tmpl/form/form.tpl'
], function (Template_Tab, Model_Form, Model_Method, Template_Form) {
    'use strict';

    /**
     * @class View_Form
     * @constructor
     * @extends Template_Tab
     */
    var View_Form = Template_Tab.extend({

        tagName: 'div',

        /**
         * @type {Model_Form}
         */
        model: null,

        /**
         * @type {Model_Method}
         */
        model_method: null,

        template: Template_Form,

        name: 'form',

        icon: 'applications',

        caption: 'Форма',

        url: 'forms{if $parent}/{$parent}{/if}{if $new}/{$new}{else}/{$id}{/if}',

        autoToolbar: false,

        single: true,

        notRender: true,

        initialize: function (opts) {

            Template_Tab.prototype.initialize.apply(this, [opts]);

            if (opts.arguments.length == 1) {
                this.FormId = opts.arguments[0];
                this.new = opts.arguments[0] == 'new';
                this.ParentId = null;
            } else {
                this.FormId = opts.arguments[1];
                this.new = opts.arguments[1] == 'new';
                this.ParentId = opts.arguments[0];
            }
            this.urlData = {id: this.FormId, parent: this.ParentId, 'new': this.new ? 'new' : ''};

            this.model = new Model_Form({'new': this.new});
            this.model_method = new Model_Method();
            if (this.new) {
                this.setCaption('Новая форма');
            }
            this.initModel();
            this.load();
        },

        events: {
            "submit form": "submit",
            "click .js-to-parent": "toParent"
        },

        load: function () {

            this.model_method.load({success: _.bind(this.successMethodLoad, this)});
        },

        successMethodLoad: function (data) {
            this.methods = data;
            this.hideAjax();
            if (this.FormId && this.FormId !== 'new') {
                this.model.getById({data: {FormId: this.FormId}, success: _.bind(this.successForm, this)});
            } else if (this.ParentId) {
                this.model.getById({data: {FormId: this.ParentId}, success: _.bind(this.successParent, this)});
            } else {
                this.apply();
            }
        },
        apply: function () {

            this.notRender = false;
            if (this.new) {
                this.tmplData = {
                    id: this.FormId,
                    parent: this.parent,
                    methods: this.methods
                };
            } else {
                this.tmplData = {item: this.data, parent: this.parent, id: this.FormId, methods: this.methods};
                this.setCaption(this.data.Description || this.data.Name);
            }
            this.render();
            this.hideAjax();
        },

        successForm: function (data) {
            this.data = data;
            if (this.data.ParentId) {
                this.showAjax();
                this.model.getById({data: {FormId: this.data.ParentId}, success: _.bind(this.successParent, this)});
            } else {
                this.apply();
            }
        },

        successParent: function (data) {
            this.parent = data;
            this.apply();
        },


        submit: function (e) {

            e.preventDefault();

            this.showAjax(this.$('.js-submit'));
            this.tabAjax();

            if (this.new) {
                this.model.save({
                    Name: this.$('[name="Name"]').val(),
                    Description: this.$('[name="Description"]').val(),
                    ListDescription: this.$('[name="ListDescription"]').val(),
                    OrderNum: this.$('[name="OrderNum"]').val(),
                    FieldName: this.$('[name="FieldName"]').val(),
                    ParentFieldName: this.$('[name="ParentFieldName"]').val(),
                    ParentId: this.ParentId,
                    FrameId: this.$('[name="FrameId"]').val(),
                    MethodId: this.$('[name="MethodId"]').val()

                }, {error: _.bind(this.requestError, this), success: _.bind(this.successAdd, this)});
            } else {

                this.model.save({
                    Name: this.$('[name="Name"]').val(),
                    Description: this.$('[name="Description"]').val(),
                    FieldName: this.$('[name="FieldName"]').val(),
                    ParentFieldName: this.$('[name="ParentFieldName"]').val(),
                    ListDescription: this.$('[name="ListDescription"]').val(),
                    OrderNum: this.$('[name="OrderNum"]').val(),
                    FrameId: this.$('[name="FrameId"]').val(),
                    MethodId: this.$('[name="MethodId"]').val(),
                    ParentId: this.ParentId,
                    FormId: this.FormId
                }, {error: _.bind(this.requestError, this), success: _.bind(this.successAdd, this)});
            }

        },

        toParent: function (e) {
            e.preventDefault();
            this.trigger('open', 'Form', [this.ParentId]);
        },

        successAdd: function (response) {
            this.hideAjax();
            this.trigger('refreshTabs', 'Forms', []);

            if (this.new) {
                this.trigger('open', 'Form', [response.FormId]);
                this.sendMessage('Форма создана');
                this.remove();
            }
            else {
                this.sendMessage('Изменения сохранены');
            }

        }
    });
    return View_Form;
});