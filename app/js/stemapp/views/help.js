define([
    '_app/views/templates/tab',
], function (Template_Tab) {
    'use strict';
    return Template_Tab.extend({

        tagName: 'div',

        model: null,

        name: 'help',

        caption: 'Справка',

        tabButton: null,

        url: 'help',

        single: true,

        initialize: function (opts) {

            Template_Tab.prototype.initialize.apply(this, [opts]);
            this.load();
        },

        events: {},

        render: function () {

            return this;
        },

        load: function () {

            $.get('/wiki/' + Stemapp.config.helpPage, _.bind(this.apply, this));
        },

        apply: function (html) {
            this.$el.html(html);
            this.hideGlobalAjax();
            this.hideAjax(this.tabButton);
            this.$('a').attr('target', '_blank').each(function () {

                if ($(this).hasClass('reformed')) {
                    return;
                }
                if ($(this).attr('href')) {
                    $(this).attr('href', Stemapp.config.helpUrl + $(this).attr('href')).addClass('reformed');
                }
            });
        }


    });

});