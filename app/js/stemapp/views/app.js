define([
    'jquery',
    'backbone',
    '_app/views/templates/main',
    '_app/views/ajax',
    '_app/views/panel',
    '_app/views/windows/dialog',
    '_app/models/app',
    '_app/routers/main',
    'cookie',
    'slickgrid'
], function ($, Backbone, Template_Main, View_Ajax, View_Panel, View_Dialog, Model_App, Router_Main) {

    var App = Template_Main.extend({

        model: null,

        el: $('#main'),

        view: null,

        viewName: 'Ajax',

        router: null,

        isStart: false,

        isPayed: true,

        routers: [],

        route_ind: 1,

        initialize: function () {
            Stemapp.demoGuid = '57CAB551-969E-F21A-E040-ABD50C017468';
            this.$el = $('#main');
            Template_Main.prototype.initialize.apply(this);

            if (Stemapp.mode == 'pro') {
                Stemapp.GUID = $.cookie('GUID') || Stemapp.demoGuid;
            }

            this.view = new View_Ajax();
            this.model = new Model_App({view: this});
            this.model.bind('logStatus', this.logStatus, this);
            this.model.bind('loadApp', this.loadApp, this);
            this.model.bind('payed', this.payed, this);
        },

        createRouter: function () {
            this.router = new Router_Main();
            this.router.bind('routes', this.routes, this);
            this.router.bind('demo', this.demo, this);

            var i, j, router;

            // todo старый формат загрузки роутов. Правила добавление роутов описаны в главном роуте _app/routers/main
            // Загружаем внешние роутеры модулей
            for (i in Stemapp.modules) {
                if (Stemapp.modules.hasOwnProperty(i) && Stemapp.modules[i].router && Stemapp.modules[i].router.outer) {
                    Stemapp.App.warn('Используется старая схема загрузки роутов');
                    for (j = 0; j < Stemapp.modules[i].router.outer.length; j++) {
                        router = new Stemapp.modules[i].router.outer[j]();
                        router.bind('routes', this.routes, this);
                        this.routers.push(router);
                    }
                }
            }

            // todo новый формат загрузки роутов
            // Правила добавление роутов описаны в главном роуте _app/routers/main
            // Загружаем роуты
            for (i in Stemapp.modules) {
                if (Stemapp.modules.hasOwnProperty(i) && Stemapp.modules[i].routes) {
                    Stemapp.App.appendRoutes(i, Stemapp.modules[i].routes);
                }
            }
        },

        start: function (token) {
            token = _.isUndefined(token) ? $.cookie('token') : token;
            if (token) {
                api.token = token;
                this.init();
            } else {
                this.checkStartDelay(_.bind(this.logStatus, this, false));


            }

            Stemapp.global.token = $.cookie('token') ? $.cookie('token') : null;
            Stemapp.global.guid = $.cookie('guid') ? $.cookie('guid') : null;

            Template_Main.prototype.initialize.apply(this);
            this.render();

            $(window).on('resize', _.bind(this.resize, this));


            this.modKeyHandler();
        },

        checkStartDelay: function (callback) {

            var diff = (new Date()).getTime() - window.Boot.startTime.getTime();
            diff = window.Boot.minStartTime - Math.min(window.Boot.minStartTime, diff);
            var func = function () {
                var event;
                try {
                    event = new Event('started');
                } catch (e) {
                    event = document.createEvent("Event");
                    event.initEvent('started', true, false)
                }
                window.dispatchEvent(event);
                callback();
            };
            if (diff === 0) {
                func();
            } else {
                _.delay(func, diff);
            }
        },

        events: {
            "contextmenu": "contextMenu"
        },

        /**
         * @deprecated //todo По возможности найти другое решение
         */
        modKeyHandler: function () {
            $(document).keydown(function (event) {
                if (event.which == "17")
                    window.ctrlIsPressed = true;
            });
            $(document).keyup(function (event) {
                if (event.which == "17")
                    window.ctrlIsPressed = false;
            });
        },

        init: function () {
            this.model.init();
        },

        resize: function () {

            if (this.view) {
                this.view.trigger('resize');
            }
        },

        render: function () {
            return this;
        },

        /**
         * Отправка события смены адреса роутеру
         * @param url
         * @param opts
         */
        navigate: function (url, opts) {
            opts = $.extend({trigger: false}, opts);
            this.router.navigate(url, opts);
        },

        /**
         * Установка флага "Входа пользователя" и запуск роутера
         * @param status
         */
        logStatus: function (status) {
            this.router.setLogin(status);
            for (var i = 0; i < this.routers.length; i++) {
                if (this.routers[i].setLogin) {
                    this.routers[i].setLogin(status);
                }
            }
            if (!this.isStart) {
                Backbone.history.start();
                this.isStart = true;
            }
            if (!Stemapp.App.router.current) {
                this.navigate('signin', {trigger: true});
            }
        },

        /**
         * Если мы вошли - запускаем приложение
         * @param servers
         * @returns {boolean}
         */
        loadApp: function (servers) {
            this.logStatus(true);

            if (!this.changeView()) return;
            if (Backbone.history.fragment == 'signin' || Backbone.history.fragment == 'demo' || Backbone.history.fragment == 'remember' || Backbone.history.fragment == 'registration' || Backbone.history.fragment == 'confirm') {
                this.navigate('', {trigger: false});
            }

            Stemapp.me = Stemapp.me ? Stemapp.me : {};

            Stemapp.me.type = $.cookie('userType');

            this.view.hide();

            var func = _.bind(function () {
                this.view = new View_Panel({servers: servers, isPayed: this.isPayed});
                this.$el.append(this.view.render().el);
                this.view.afterInsert();
                this.view.trigger('resize');
                this.view.bind('error', this.requestError, this);
                this.view.bind('fatalError', this.fatalError, this);
                this.view.bind('dialog', this.dialog, this);
                this.view.bind('window', this.window, this);
                this.view.bind('openModal', this.openModal, this);
                if (!this.isPayed) {
                    this.dialog({text: 'Сервис отключен за неуплату', buttons: 'ok'});
                    this.navigate('', {trigger: false});
                    return false;
                }
            }, this);
            this.checkStartDelay(function () {
                _.delay(func, 500);
            });
        },

        dialog: function (arg) {

            var dialog = new View_Dialog(arg);
            this.$el.append(dialog.render().$el);
            dialog.trigger('resize');
        },

        /**
         * Открытие нового окна
         * @param {string} name название вьюхи
         * @param {object} params параметры, передаваемые во вьюху окна
         * @param {function} callback Вызывается при полной загрузке окна(первый параметр — конструктор окна)
         */
        window: function (name, params, callback) {

            params = params ? params : {};
            this.loadView('windows/' + name, _.bind(function (Window) {
                var window = new Window(params);
                this.hideGlobalAjax();
                window.bind('error', this.requestError, this);
                this.$el.append(window.render().$el);
                window.afterInsert();
                window.show();
                window.trigger('resize');
                window.bind('error', this.requestError, this);
                window.bind('fatalError', this.fatalError, this);
                window.bind('dialog', this.dialog, this);
                window.bind('window', this.window, this);
                window.bind('openModal', this.openModal, this);
                if (callback) {
                    callback(window);
                }
            }, this));
        },

        openModal: function (name, args, opts) {

            opts = opts ? opts : {};
            opts.callback = opts.callback ? opts.callback : function () {
            };
            if (!name) return false;
            name = name.charAt(0).toUpperCase() + name.slice(1);
            this.showGlobalAjax();
            var func = _.bind(
                /**
                 *
                 * @param {Window_Tab} window
                 */
                function (window) {
                    Stemapp.App.loadView(name, _.bind(function (View) {

                        var options = {};
                        if (!_.isArray(args)) { // Новый формат передачи данных
                            options = _.extend({}, args);
                            if (args) {
                                args = args.arguments;
                            } else {
                                args = [];
                            }

                        } else { // Старый формат передачи данных
                            // Ничего не делаем
                            Stemapp.App.log('Используется старый формат передачи параметров в форму ' + name);
                        }

                        var view = new View({arguments: args, name: name, options: options});

                        view.bind('navigate', this.navigate, this);
                        view.bind('error', this.requestError, this);
                        view.bind('close', this.closeTab, this);
                        view.bind('unfocus', this.unFocusTab, this);
                        view.bind('fatalError', this.fatalError, this);
                        view.bind('open', this.openTab, this);
                        view.bind('openModal', this.openModal, this);
                        view.bind('dialog', this.dialog, this);
                        view.bind('window', this.window, this);
                        view.bind('refreshTabs', this.refreshTabs, this);
                        window.setTab(view, opts);
                        window.render();
                    }, this));
                }, this);
            this.window('Tab', {}, func);
        },


        loadView: function (name, func) {
            name = name.toLowerCase();
            var module = '';
            for (var i in Stemapp.modules) {
                if (Stemapp.modules.hasOwnProperty(i) && Stemapp.modules[i].views) {
                    for (var j = 0; j < Stemapp.modules[i].views.length; j++) {
                        if (Stemapp.modules[i].views[j] == name || Stemapp.modules[i].views[j].replace(/\//g, '_') == name) {
                            module = i;
                        }
                    }
                }
            }
            name = name.replace(/_/g, '/');
            if (module) {

                require(['_modules/' + module + '/js/views/' + name], function (View) {
                    if (View) {
                        func(View, name);
                    } else {
                        require(['_modules/' + module + '/js/view/' + name], function (View) {
                            if (View) {
                                func(View, name);
                            } else {
                                func(false);
                            }
                        }, function (err) {
                            func(false);
                        });
                    }
                }, function (err) {
                    console.error(err);
                    require(['_modules/' + module + '/js/view/' + name], function (View) {
                        func(false);
                    }, function (err) {
                        func(false);
                    });
                });

            } else {
                require(['_app/views/' + name], function (View) {
                    if (View) {
                        func(View, name);
                    } else {
                        func(false);
                    }
                }, function (err) {
                    func(false);
                });
            }
        },

        login: function (response) {
            var start = new Date((new Date()).getTime() + 31536000000); // +1 year
            var token = response['token'] ? response['token'] : response['Token'];
            var guid = response['Id'] ? response['Id'] : null;
            Stemapp.global.guid = guid;
            $.cookie('token', '', {expires: -1, path: '/'});
            $.cookie('token', '', {expires: -1, path: '/', domain: Stemapp.config.cookieDomain});
            $.cookie('token', token, {expires: start, path: '/', domain: Stemapp.config.cookieDomain});
            $.cookie('guid', guid, {expires: start});
            $.cookie('userType', response['usertype'], {expires: start});
            $.cookie('isAdmin', response['is_admin'], {expires: start});

            $.cookie('version', Stemapp.version);
            api.token = token;
        },

        logout: function () {
            $.cookie('session', '', {expires: -1});
            $.cookie('token', '', {expires: -1});
            $.cookie('token', '', {expires: -1, path: '/'});
            $.cookie('token', '', {expires: -1, domain: Stemapp.config.cookieDomain, path: '/'});
            $.cookie('usertype', '', {expires: -1});
            $.cookie('is_admin', '', {expires: -1});
            $.cookie('state', '', {expires: -1});
            $.cookie('id_client', '', {expires: -1});
            $.cookie('id_expert', '', {expires: -1});
            $.cookie('guid', '', {expires: -1});
        },

        routes: function () {
            var args = [];
            Array.prototype.push.apply(args, arguments);
            var name = args.shift();
            if (!this.changeView(name.charAt(0).toUpperCase() + name.slice(1))) return;
            this.openWindow(name.charAt(0).toUpperCase() + name.slice(1), args);
        },

        confirm: function () {
            if (!this.changeView('Confirm')) return;
            this.openWindow('Confirm');
        },

        contextMenu: function (e) {
            if ($(e.target).is('input')) {
            }
            else {
                return false;
            }
        },

        demo: function () {
            this.routes('demo');
        },

        finish: function (name) {
        },

        openWindow: function (name, args, callback) {
            args = args ? args : {};
            this.loadView('windows/' + name, _.bind(function (View) {
                var view = new View(args);
                if (this.view && this.view.hide) {
                    this.view.hide();
                }
                this.view = view;
                this.viewName = name;
                this.view.bind('finish', _.bind(this.finish, this, name));
                if (callback) {
                    this.view.bind('finish', _.bind(callback, this, name));
                }
                this.$el.append(this.view.render().$el);
                this.view.bind('navigate', this.navigate, this);
                this.view.bind('routes', this.routes, this);
                this.view.bind('demo', this.demo, this);
                this.view.bind('init', this.init, this);
                this.view.bind('login', this.login, this);
                this.view.afterInsert();
                _.delay(_.bind(function () {

                    this.view.trigger('show');
                    this.view.trigger('resize');
                    this.view.trigger('focus');

                }, this), 290);
            }, this));
        },

        changeView: function (name) {

            if (name == this.viewName && name != 'Ajax') return;

            if (this.view) {
                this.view.trigger('blur');
                return !this.view.keep;
            }
            else {
                return true;
            }
        },

        existModule: function (name) {
            for (var i in Stemapp.modules) {
                if (Stemapp.modules.hasOwnProperty(i)) {
                    if (Stemapp.modules[i].name.replace(/^_/gm, '').replace(/_$/gm, '') == name.replace(/^_/gm, '').replace(/_$/gm, '')) {
                        return true;
                    }
                }
            }
            return false;
        },

        getModuleDir: function (name) {
            for (var i in Stemapp.modules) {
                if (Stemapp.modules.hasOwnProperty(i)) {
                    if (Stemapp.modules[i].name.replace(/^_/gm,'').replace(/_$/gm,'') == name.replace(/^_/gm,'').replace(/_$/gm,'')) {
                        return i;
                    }
                }
            }
        },

        checkRole: function (name) {
            var res = false;
            if (Stemapp.roles && Stemapp.roles.length) {
                for (var i = Stemapp.roles.length; i--; ) {
                    if (Stemapp.roles[i].RoleName === name) {
                        res = true;
                        break;
                    }
                }
            }

            return res;
        },

        /**
         * Append routes to main route
         * @param namespace
         * @param routes
         */
        appendRoutes: function (namespace, routes) {
            var k;
            var items;
            var name;
            if (routes.inner && routes.inner.routes) {
                items = routes.inner.routes;
                for (k in items) {
                    if (items.hasOwnProperty(k)) {
                        if (typeof items[k] == 'function') {
                            name = namespace + '_' + this.route_ind++;
                            this.router.route(k, name, _.bind(function (func) {
                                this.current = null;
                                if (!this.opts.login) return;
                                this.changePage(name);
                                var arr = Array.prototype.slice.call(arguments, 1);
                                func.apply(this, arr);
                            }, this.router, items[k]));
                        }
                        if (typeof items[k] == 'string') {
                            if (routes.inner[items[k]] && typeof routes.inner[items[k]] == 'function') {
                                name = namespace + '_' + items[k];
                                this.router.route(k, name, _.bind(function (func) {
                                    this.current = null;
                                    if (!this.opts.login) return;
                                    this.changePage(name);
                                    var arr = Array.prototype.slice.call(arguments, 1);
                                    func.apply(this, arr);
                                }, this.router, routes.inner[items[k]]));
                            }
                        }
                    }
                }
            }
            if (routes.outer && routes.outer.routes) {
                items = routes.outer.routes;
                for (k in items) {
                    if (items.hasOwnProperty(k)) {
                        if (typeof items[k] == 'function') {
                            name = namespace + '_' + this.route_ind++;
                            this.router.route(k, name, _.bind(function (func) {
                                this.current = null;
                                if (this.opts.login) return;
                                this.changePage(name);
                                var arr = Array.prototype.slice.call(arguments, 1);
                                func.apply(this, arr);
                            }, this.router, items[k]));
                        }
                        if (typeof items[k] == 'string') {
                            if (routes.outer[items[k]] && typeof routes.outer[items[k]] == 'function') {
                                name = namespace + '_' + items[k];
                                this.router.route(k, name, _.bind(function (func) {
                                    this.current = null;
                                    if (this.opts.login) return;
                                    this.changePage(name);
                                    var arr = Array.prototype.slice.call(arguments, 1);
                                    func.apply(this, arr);
                                }, this.router, routes.outer[items[k]]));
                            }
                        }
                    }
                }
            }
        },

        payed: function (value) {

            this.isPayed = !!value;
        },

        log: function () {
            if (window.Boot.devmode) {
                console.log.apply(console, arguments);
            }
        },

        warn: function () {
            if (window.Boot.devmode) {
                console.warn.apply(console, arguments);
            }
        },

        /**
         * Show simple message
         * @param text
         * @public
         */
        showMessage: function (text) {
            $.jgrowl('Сообщение!', text, 'message');
        },

        /**
         * Show error message
         * @param text
         * @public
         */
        showError: function (text) {
            $.jgrowl('Ошибка!', text, 'error');
        },

        fatalError: function (xhr) {
            var js = $.parseJSON(xhr.responseText),
                response = '';
            if (js.error) {
                response = js.error.trim().split('\n');
            } else {
                response = xhr.responseText.toString().trim().split('\n');
            }

            var window = new Stemapp.Fatal_Error(response);
            Stemapp.App.$el.append(window.render().$el);
            window.trigger('resize');
        },

    });

    return App;
});