define([
    '_app/views/templates/main',
    'tmpl'
], function (Template_Main, tmpl) {

    /**
     * @class Template_Window
     * @constructor
     * @extends Template_Main
     */
    var Template_Window = Template_Main.extend({

        className: 'window',

        container: null,

        template: '',

        tmplData: {title: 'Сообщение'},

        overlay: false,

        url: '',

        keep: false,

        model: null,

        width: null,

        initialize: function () {
            Template_Main.prototype.initialize.apply(this);

            this.container = $('<div>').addClass('content');
            this.container.appendTo(this.$el);
            this.container.hide();

            this.bind('resize', this.resize, this);
            this.bind('focus', this.focus, this);
            this.bind('show', this.show, this);
            $(window).on('resize', _.bind(this.resize, this));
            Stemapp.App.on('ready:panel', _.bind(function(){
                this.trigger('resize');
            },this));
        },

        focus: function () {
            if (this.url != '') {
                this.trigger('navigate', this.url);
            }
        },

        beforeRender: function () {

        },

        render: function () {
            this.beforeRender();
            if (this.overlay && !this.$('.overlay').length) {
                this.$el.append('<div class="overlay"></div>');
            }
            if (this.template != '') {
                this.container.html(tmpl(this.template, this.tmplData));
            }
            this.afterRender();
            return this;
        },

        afterRender: function () {
        },

        afterInsert: function () {
            this.trigger('finish');
        },

        show: function () {

            this.container.addClass('showScale').show();
            _.delay(_.bind(function () {
                this.$el.removeClass('showScale');
                //this.$('button').focus();
            }, this), 290);
        },

        hide: function () {
            $('.qtip').qtip('destroy');
            this.container.addClass('hideScale');
            _.delay(_.bind(function () {
                this.remove();
            }, this), 290);
        },

        windowInit: function (template) {
        },

        resize: function (e, repeat) {
            repeat = !!repeat;
            this.$el.css({top: 0, left: 0});

            if (!repeat) {
                this.$('.scroll-area').css({'overflowY': 'visible', height: 'auto'});
            }
            if (this.width) {
                this.$el.css({width: this.width});
            }
            var winWidth = $(window).width(),
                winHeight = $(window).height(),
                width = this.$el.width(),
                height = this.$el.height();

            var top = (winHeight - height) / 2,
                left = (winWidth - width) / 2;

            if (this.$('.js-all-size').length) {
                this.$('.js-all-size').css({
                    height: winHeight * 0.95 - this.$('.banner').outerHeight() - this.$('.buttons').outerHeight()
                });
                height = this.$el.height();
                this.$el.css('height', 'auto');
                if ((top < 0 || top + height > winHeight) && !repeat) {
                    this.resize(null, true);
                    return;
                }
            }

            if (top < 0 && !repeat) {

                if (this.$('.scroll-area').length) {
                    this.$('.scroll-area').css({
                        overflowY: 'scroll',
                        height: winHeight * 0.8 - this.$('.banner').outerHeight() - this.$('.buttons').outerHeight()
                    });
                    this.$el.css('height', 'auto');
                    this.resize(null, true);
                    return;
                }

            }

            var padding = 50;

            top = top < padding ? padding : top;

            this.$el.css({top: top, left: left});
            // Если body меньше чем окошко, то растягиваем его, чтоб не было белых полос
            $('.body').css('height', '100%');
            if (this.$el.parent().children().length == 1 && $('.body').height() < this.$el.height()) {
                $('.body').height(this.$el.height() + padding * 2);
            }
        },

        maxHeight: function () {

            return Math.round($(window).height() * 0.8);
        },

        remove: function () {
            Template_Main.prototype.remove.apply(this);
            this.$el.remove();
        },

        blur: function () {
            this.hide();
        }

    });
    return Template_Window;
});