define([
    'backbone',
    'jquery.qtip',
    'jquery.qtip.jgrowl',
    '_jq/jstree'
], function (Backbone) {
    return Backbone.View.extend({

        tagName: 'div',

        id: 'jstree',

        className: 'treeView',

        events: {
            "changed.jstree": "onSelectItem"
        },

        initialize: function () {
            this.id += this.cid;
            this.$el.attr('id', this.id);

        },

        onSelectItem: function (e, data) {
            this.trigger("onSelectItem", e, data);
        },

        render: function (groups, reports) {
            var data = [];
            groups.forEach(function (group) {
                var children = [];

                reports.forEach(function (report) {
                    if (group.id == report.groupId) {
                        var child = {
                            //'id' : report.id,
                            'id': report.name,
                            'data': report.id,
                            'text': report.title,
                            'type': 'noIcon'
                        };
                        children.push(child);
                    }
                });

                var root = {
                    //'id': group.id,
                    'id': group.name,
                    'text': group.title,
                    'type': 'noIcon',
                    'state': {'opened': true, 'disabled': true},
                    'children': children
                };
                data.push(root);
            });

//        console.log(data);
            this.$el.jstree({
                "core": {
                    "animation": 0,
                    "check_callback": true,
                    "themes": {"icons": false, "stripes": true, "responsive": false},
                    'data': data
                },
                "types": {
                    "#": {
                        "max_children": 1,
                        "max_depth": 4,
                        "valid_children": ["root"]
                    },
                    "root": {
                        "icon": "none",
                        "valid_children": ["default"]
                    },
                    "default": {
                        "valid_children": ["default", "file"]
                    },
                    "file": {
                        "icon": "glyphicon glyphicon-file",
                        "valid_children": []
                    },
                    "noIcon": {
                        "icon": false
                    }
                },
                "plugins": [
                    "dnd",
                    "state", "types", "wholerow"
                ]
            });
            return this;
        },

        getSelectedReport: function () {
            return this.$el.jstree('get_selected')[0];
        }

    });


});