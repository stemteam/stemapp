define([
    'backbone',
    'jquery.qtip',
    'jquery.qtip.jgrowl'
], function (Backbone) {

    /**
     * @class Template_Main
     * @constructor
     * @extends Backbone.View
     */
    var Template_Main = (Backbone.View.extend({

        initialize: function () {
            this.initModel();
        },

        /**
         * Добавляет прослушивание ошибок для моделей
         * @param model
         */
        initModel: function (model) {
            model = model ? model : this.model;
            if (model) {
                model.bind('invalid', _.bind(this.requestError, this));
                model.bind('error', _.bind(this.requestError, this));
                model.bind('fatalError', _.bind(this.fatalError, this));
                model.bind('unauthorized', _.bind(this.unauthorized, this));
            }
        },

        render: function () {

            this.container.append($.tmpl(this.template, {}));
            return this;
        },


        showGlobalAjax: function () {
            this.$el.append($('<div>').addClass('ajax-overlay'));
        },

        hideGlobalAjax: function () {
            this.$('.ajax-overlay').remove();
        },


        remove: function () {
            Backbone.View.prototype.remove.apply(this);
            this.$el.remove();
        },

        showAjax: function (element) {
            if ($(element).hasClass('.icon')) {
                $(element).addClass('icon-ajax');
            } else {
                $(element).find('.icon').addClass('icon-ajax');
            }
        },

        sendMessage: function (s) {

            $.jgrowl('Сообщение!', s, 'message');
        },

        sendDelayMessage: function (s, delay) {

            $.jgrowl('Сообщение!', s, 'message', delay);
        },

        sendError: function (s) {

            $.jgrowl('Ошибка!', s, 'error');
        },

        showFormErrors: function (errors) {
            if (_.isString(errors)) {
                this.sendError(errors);
                return;
            }
            for (var i = 0; i < errors.length; i++) {

                var $input = this.$('.js-' + errors[i].field + ', input[name="' + errors[i].field + '"]');
                if (!$input.length) {

                    this.sendError(errors[i].text + ' ' + errors[i].field);
                    return;
                }
                // Добавляем ошибку
                this.inputError($input, errors[i].text);
            }
        },

        inputError: function ($input, text) {
            var $api = $input.qtip({
                content: {
                    text: text
                },
                position: {
                    my: 'bottom right',//my[inputstyle - 1],
                    at: 'top right',//at[inputstyle - 1]

                    container: $('#content'),
                    viewport: this.$el
                },
                style: {
                    // tip: true,
                    classes: 'ui-tooltip-shadow ui-tooltip-red ui-tooltip-rounded'
                },
                show: {
                    event: false,
                    ready: true,
                    effect: 'fade'
                },
                hide: {
                    event: 'focus',
                    target: $input.parent().find('.required, input')
                }
            });
        },

        requestError: function (element, errors) {
            this.hideAjax();
            if (errors && errors.responseText) {
                this.processError(errors);
            } else {
                this.showFormErrors(errors);
            }
        },

        processError: function (errors) {
            var message = this.getErrorMessage(errors.responseText);
            this.trigger('error', {}, message);
        },

        getErrorMessage: function (response) { //console.log(JSON.parse(response));
            var resText,
                m = '';
            try {
                resText = Array.isArray(JSON.parse(response)) ? JSON.parse(response)[0] : JSON.parse(response);
            } catch (e) {
                return false;
            }

            if (resText) {
                m = resText.Message;
                if (!m) {
                    resText.error = resText.error ? resText.error : resText.CallStack;
                    m = resText.error.match(/-=#([^#]+)#=-/);
                    m = (m && m[1]) ? m[1].trim() : m;
                }
            } else {
                m = response.toString().trim();
            }

            return m;
        },

        getMessage: function (xhr) {
            var js = $.parseJSON(xhr.responseText);
            var m = js.error.match(/-=#([^#]+)#=-/);
            m = (m && m[1]) ? m[1].trim() : xhr.responseText;
            return m;
        },

        hideAjax: function (el) {
            if (el) {
                el.find('.icon-ajax').removeClass('icon-ajax');
            } else {
                this.$('.icon-ajax').removeClass('icon-ajax');
            }
        },

        fatalError: function (xhr) {
            var js = $.parseJSON(xhr.responseText),
                response = '';
            if (js.error) {
                response = js.error.trim().split('\n');
            } else {
                response = xhr.responseText.toString().trim().split('\n');
            }

            var window = new Stemapp.Fatal_Error(response);
            Stemapp.App.$el.append(window.render().$el);
            window.trigger('resize');
        },

        checkUserServices: function (service) {

            return Stemapp.Function.checkUserServices(service);
        },

        unauthorized: function () {
            this.trigger('window', 'Signin', {simple: true});
        }

    }));
    return Template_Main;

});