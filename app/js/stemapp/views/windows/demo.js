define([
    '_app/views/templates/window',
    '_app/models/signin',
    '_app/misc/util',
    '_services/api',
], function (Template_Window, Model_Signin) {
    'use strict';
    return (Template_Window.extend({


        el: $('#preload'),

        template: '',

        initialize: function () {

            this.bind('blur', this.blur, this);
            this.bind('focus', this.show, this);
            this.$('.text').text('Демо вход');

            this.model = new Model_Signin({mode: Stemapp.config.signin});
            this.model.bind('error', this.error, this);

            var password = Stemapp.config.demoPassword ? Stemapp.util.getPassHash(Stemapp.config.hashPassMethod, Stemapp.config.demoPassword, Stemapp.config.salt) : '';

            this.model.save({Login: Stemapp.config.demoLogin, Password: password});

        },

        hide: function () {

            this.$el.addClass('hideScale');
            _.delay(_.bind(function () {
                this.$el.hide();
            }, this), 290);
        },


        show: function () {

            this.$el.removeClass('hideScale').addClass('showScale').show();
            _.delay(_.bind(function () {
                this.$el.removeClass('showScale');
            }, this), 290);
        },

        error: function (response, message) {
            console.log(message);
            this.trigger('routes', 'signin');

        }

    }));
});