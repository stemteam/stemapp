define([
    '_app/views/templates/window',
    'smarty!_app/tmpl/apprise/dialog.tpl',
], function (Template_Window, Template_Dialog) {
    'use strict';
    return Template_Window.extend({


        template: Template_Dialog,

        data: {

            title: 'Сообщение',

            button: {
                ok: 'OK',

                save: 'Сохранить',

                cancel: 'Отмена',

                close: 'Закрыть',

                yes: 'Да',

                no: 'Нет'
            }
        },

        text: '',

        buttons: 'ok',


        overlay: true,

        initialize: function (opts) {

            this.callback = _.bind(this.close, this);

            this.tmplData.text = opts.text ? opts.text : this.text;
            this.tmplData.buttons = opts.buttons ? opts.buttons : this.buttons;
            this.tmplData.button = opts.button ? $.extend({}, this.data.button, opts.button) : this.data.button;

            this.callback = opts.callback ? opts.callback : this.callback;

            this.bind('blur', this.blur, this);
            Template_Window.prototype.initialize.apply(this);
            this.show();
        },

        callback: function(){

        },

        events: {
            "click button": "click"
        },

        showInfo: function () {
            if (this.$('.js-info').hasClass('hidden')) {
                this.$('.js-info').removeClass('hidden');
            } else {
                this.$('.js-info').addClass('hidden');
            }
            this.resize();
            return false;
        },

        click: function (e) {

            var value = $(e.currentTarget).attr('value');

            if (this.callback(value) !== false) {
                this.close();
            }

        },

        close: function () {

            this.hide();
        }

    });
});