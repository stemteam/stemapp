define([
    '_app/views/templates/window',
    '_app/models/registration',
    'smarty!_app/tmpl/registration.tpl',
    '_jq/jquery.pstrength-min.1.2',
    'cookie'
], function (Template_Window, Model_Registration, tmpl_registration) {
    'use strict';
    return Template_Window.extend({

        tagName: 'div',

        model: null,

        template: tmpl_registration,

        url: 'registration',

        initialize: function () {
            this.model = new Model_Registration();
            this.tmplData = {invite: Stemapp.config.registerByInvite};
            this.bind('blur', this.blur, this);
            Template_Window.prototype.initialize.apply(this);
            this.model.bind('apply', this.success, this);
        },

        afterRender: function () {

            $.cookie('email') && this.$('.js-email').val($.cookie('email'));
            return this;
        },

        events: {
            "submit .js-form": "submit",

            "click .js-signin": "signin",

            "click .js-cancel": "close",

            "click .js-confirm": "confirm"
        },

        afterInsert: function () {

            this.$('.js-password').pstrength().keyup();
            this.trigger('finish');
        },

        submit: function (e) {

            e.preventDefault();
            this.showAjax(this.$('.js-submit'));
            var firstName = this.$('.js-firstName').val(),
                lastName = this.$('.js-lastName').val(),
                middleName = this.$('.js-middleName').val(),
                mobilePhone = this.$('.js-mobilePhone').val(),
                email = this.$('.js-email').val(),
                password = this.$('.js-password').val(),
                invite = this.$('.js-invite').val();

            if (Stemapp.config.registerByInvite) {
                this.model.save({
                    Invite: invite,
                    Email: email,
                    Password: password
                });
            } else {
                this.model.save({
                    FirstName: firstName,
                    LastName: lastName,
                    MiddleName: middleName,
                    Phone: mobilePhone,
                    Email: email,
                    Password: password
                });
            }
        },

        signin: function (e) {
            e.preventDefault();
            this.trigger('routes', 'signin');
        },

        confirm: function (e) {
            e.preventDefault();
            if (this.phone) {
                this.trigger('routes', 'confirm', 'registration', this.phone);
            }
            else {
                this.trigger('routes', 'confirm', 'registration');
            }
        },

        success: function (attributes) {

            this.hideAjax();
            this.$('.js-submit').animate({opacity: 'hide'}, 200, _.bind(function () {
                this.$('.js-cancel').animate({opacity: 'show'}, 200);
                this.$('.js-confirm').animate({opacity: 'show'}, 200);
            }, this));

            this.$('.container').autoSize(this.$('.js-good'));

            this.phone = attributes.Phone;
        },

        close: function (e) {
            e.preventDefault();
            this.trigger('routes', 'signin');
        }

    });

});