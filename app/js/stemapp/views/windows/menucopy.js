define([
    '_app/views/templates/window',
    '_app/models/menu',
    'smarty!_app/tmpl/window/menucopy.tpl',
], function (Template_Window, Model_Menu, Template_Password) {
    'use strict';

    /**
     * @class Window_Menu_Copy
     * @constructor
     * @extends Template_Window
     */
    var Window_Menu_Copy = Template_Window.extend({

        template: Template_Password,

        data: {

            title: ''
        },

        opts: {

            maxWidth: 800,

            maxHeight: 800
        },

        text: '',

        buttons: 'ok',

        /**
         * @type {Model_Menu}
         */
        model: null,


        overlay: true,

        table: null,

        dataModel: null,

        tmplData: {
            title: 'Копирование меню'
        },

        countCopy: 0,
        countComplete: 0,

        initialize: function (opts) {

            this.callback = opts.callback ? opts.callback : _.bind(this.close, this);
            this.item = opts.item;
            this.tmplData.item = this.item;
            this.model = new Model_Menu({new: true});
            this.bind('blur', this.blur, this);
            Template_Window.prototype.initialize.apply(this);
            this.load();
        },

        events: {

            "click .cancel": "close",

            "click .success": "submit",

            "click .js-choose-menu": "chooseMenu"
        },

        load: function () {

            this.model.load({data: {ShowAll: 1}, success: _.bind(this.successLoadMenus, this)});
        },

        successLoadMenus: function (data) {
            this.menus = data;
            this.render();
            this.show();
        },

        chooseMenu: function (e) {
            e.preventDefault();
            this.trigger('openModal', 'Menus', [], {
                caption: 'Выберите меню',
                multi: false,
                fieldId: 'MenuId',
                value: this.MenuId,
                callback: _.bind(this.successChooseMenu, this)
            });
        },

        successChooseMenu: function (data) {
            this.MenuId = data.MenuId;
            this.$('.js-MenuName').val(data.MenuName);
            this.$('[name="MenuId"]').val(data.MenuId);
        },

        submit: function (e) {
            this.showAjax(e.currentTarget);
            var self = this;
            this.copyItem(this.item, this.MenuId, function (id) {
                self.searchItem(self.item.MenuId, id);
                self.countComplete++;
                self.checkComplete();
            });
        },

        copyItem: function (menu, parentId, func) {
            this.countCopy++;
            this.model.save({
                MenuParentId: parentId,
                MenuName: menu.MenuName,
                MenuNum: menu.MenuNum,
                MenuTooltip: menu.MenuTooltip,
                FormId: menu.FormId,
                MenuUrl: menu.MenuUrl,
                Param: menu.Param,
                ViewType: menu.ViewType,
                Icon: menu.Icon
            }, {error: _.bind(this.requestError, this), success: _.bind(this.successAdd, this, func)});
        },

        checkComplete: function () {
            if (this.countComplete == this.countCopy) {
                this.success();
            }
        },

        searchItem: function (menuId, nid) {
            var self = this;

            for (var i = 0; i < this.menus.length; i++) {
                if (this.menus[i].MenuParentId == menuId) {
                    var menu = this.menus[i];
                    this.copyItem(menu, nid, function (id) {
                        self.searchItem(menu.MenuId, id);
                        self.countComplete++;
                        self.checkComplete();
                    });
                }
            }
        },

        successAdd: function (func, model, data) {
            func(data.MenuId);
        },

        success: function () {
            this.close();
            this.callback();
        },

        close: function () {
            this.hide();
        }

    });
    return Window_Menu_Copy;
});