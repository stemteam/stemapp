define([
    '_app/views/templates/window',
    'smarty!_app/tmpl/apprise/error.tpl',
], function (Template_Window, tmpl_apprise) {
    'use strict';
    return Template_Window.extend({

        template: tmpl_apprise,

        data: {
            title: 'Критическая ошибка'
        },

        overlay: true,


        initialize: function (response) {

            this.tmplData.text = '<h3>Произошла непредвиденная ошибка</h3>' +
            '<p>Информация об ошибке уже отправлена в нашу службу поддержки, в скором времени мы ее обязательно починим!</p>' +
            '<a href="#" class="js-showinfo showErrorInforamtion">Посмотреть подробности ошибки</a>' +
            '<div class="hidden js-info errorInformation"><div>' + response+ '</div></div>' +
            '<div class="space"></div> ';

            this.bind('blur', this.blur, this);
            Template_Window.prototype.initialize.apply(this);
            this.show();
        },

        afterRender: function () {},

        events: {
            "click .js-showinfo": "showInfo",
            "click .js-close": "close"
        },

        showInfo: function () {
            if (this.$('.js-info').hasClass('hidden')) {
                this.$('.js-info').removeClass('hidden');
            } else {
                this.$('.js-info').addClass('hidden');
            }
            this.resize();
            return false;
        },

        close: function () {

            this.hide();
        }


    });

});