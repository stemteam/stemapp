define([
    '_app/views/templates/window',
    '_app/models/remember',
    'smarty!_app/tmpl/remember.tpl',
], function (Template_Window, Model_Remember, tmpl_remember) {
    'use strict';
    return Template_Window.extend({

        model: null,

        template: tmpl_remember,

        url: 'remember',

        initialize: function () {

            this.windowInit(this.template);
            this.bind('blur', this.blur, this);
            this.model = new Model_Remember();
            this.model.bind('error', this.error);
            this.model.bind('apply', this.apply, this);
            Template_Window.prototype.initialize.apply(this);

        },

        events: {

            "submit .js-form": "remember",

            "click .js-close": "close",

            "click .js-confirm": "confirm"
        },

        afterRender: function () {
            this.trigger('finish');
        },

        confirm: function () {
            if (this.phone)
                this.trigger('navigate', '//confirm/remember/' + this.phone);
            else
                this.trigger('navigate', '//confirm/remember');
            return false;
        },

        remember: function () {
            this.showAjax(this.$('.js-success'));


            this.model.save({
                Login: this.$('.js-mobilePhone').val()
            });

            return false;
        },

        apply: function (data) {
            this.$('.form').autoSize(this.$('.js-good'));
            this.$('.js-success').hide();
            this.$('.js-close').show();
            this.$('.js-confirm').show();
            this.phone = data.Phone;
        },

        close: function () {

            this.trigger('routes', 'signin');
            return false;
        }

    });

});