define([
    '_app/views/templates/window',
    '_app/models/confirm',
    'smarty!_app/tmpl/confirm/registration.tpl',
    'smarty!_app/tmpl/confirm/remember.tpl'
], function (Template_Window, Model_Confirm, tmpl_registration, tmpl_remember) {
    'use strict';
    return Template_Window.extend({

        tagName: 'div',

        model: null,

        template: {},

        page: '',

        url: 'confirm',

        initialize: function (args) {


            this.template['registration'] = tmpl_registration;
            this.template['remember'] = tmpl_remember;
            this.page = args[0];
            this.code = args[1] ? args[1] : '';

            this.model = new Model_Confirm();
            if (!this.template[this.page]) {
                this.sendError('Страница не найдена');
                return;
            }

            this.url = 'confirm/' + this.page;

            this.template = this.template[this.page];
            Template_Window.prototype.initialize.apply(this);

            this.bind('blur', this.blur, this);
            this.model.bind('error', this.error, this);
            this.model.bind('success', this.success, this);


            //todo Если это еще нужно, то написать тесты на эту штуку
            if (Stemapp.config.registerByInvite && this.code && this.page != 'remember') {
                this.model.save({Page: this.page, ActivationCode: this.code});
            }

        },

        events: {
            "submit .js-form": "submit",

            'click .js-close': "close",

            'click .js-enter': "enter",

            'click .js-confirm': "toConfirm"
        },

        beforeRender: function () {
            this.tmplData = {login: '', code: this.code, invite: Stemapp.config.registerByInvite};
        },

        afterInsert: function () {
            this.trigger('finish');
        },

        close: function (e) {
            e.preventDefault();
            this.trigger('routes', 'signin');
        },

        enter: function (e) {
            e.preventDefault();
            this.trigger('routes', 'signin');
        },

        toConfirm: function (e) {
            e.preventDefault();
            this.trigger('routes', 'confirm', 'remember');
        },

        submit: function (e) {
            e.preventDefault();
            var Login = this.$('.js-login').val(),
                code = this.$('.js-code').val(),
                password = this.$('.js-password').val() ? Stemapp.util.getPassHash(Stemapp.config.hashPassMethod, this.$('.js-password').val(), Stemapp.config.salt) : '';

            this.showAjax(this.$('.js-submit'));

            switch (this.page.toLowerCase()) {
                case 'remember':
                    this.model.save({
                        Login: Login,
                        Code: code,
                        NewPassword: password,
                        Page: this.page
                    });

                    break;
                case 'registration':
                    this.model.save({Login: Login, ActivationCode: code, Page: this.page});
                    break;
            }

            return false;
        },

        success: function (errorMessage) {
            this.hideAjax();
            switch (this.page) {

                case 'remember':
                    this.$('.js-text span').stop(true).animate({opacity: '0'}, 200, function () {
                        $(this).html('Пароль успешно изменён, теперь вы можете войти в систему.').animate({opacity: '1'}, 200);
                    });
                    this.$('.js-status').stop(true).animate({opacity: '0'}, 200, function () {
                        $(this).removeClass('ajax').addClass('complete').animate({opacity: '1'}, 200);
                    });

                    this.$('.js-enter').animate({opacity: 'show'}, 200);
                    this.$('.checkconfirm').animate({opacity: 'show'}, 200);
                    this.$('.js-submit').animate({opacity: 'hide'}, 200);
                    this.$('.js-container').animate({opacity: 'hide'}, 200);
                    break;

                case 'Registration':
//                this.trigger('login', response);
                    if (errorMessage) {
                        this.$('.js-text span').stop(true).animate({opacity: '0'}, 200, function () {
                            $(this).html(errorMessage).animate({opacity: '1'}, 200);
                        });
                        this.$('.js-status').stop(true).animate({opacity: '0'}, 200, function () {
                            $(this).removeClass('ajax').addClass('fail').animate({opacity: '1'}, 200);
                        });
                    } else {
                        this.$('.js-text span').stop(true).animate({opacity: '0'}, 200, function () {
                            $(this).html('Учётная запись активирована, теперь вы можете войти').animate({opacity: '1'}, 200);
                        });
                        this.$('.js-status').stop(true).animate({opacity: '0'}, 200, function () {
                            $(this).removeClass('ajax').addClass('complete').animate({opacity: '1'}, 200);
                        });
                    }

                    this.$('.js-enter').animate({opacity: 'show'}, 200);
                    this.$('.checkconfirm').animate({opacity: 'show'}, 200);
                    this.$('.js-submit').animate({opacity: 'hide'}, 200);
                    this.$('.js-container').animate({opacity: 'hide'}, 200);
                    break;
            }
        }

    });

});