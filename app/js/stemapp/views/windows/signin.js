define([
    '_app/views/templates/window',
    '_app/models/signin',
    'smarty!_app/tmpl/signin.tpl',
    'cookie',
    '_app/misc/util',
    '_services/api',
], function (Template_Window, Model_Signin, tmpl_signin) {
    'use strict';
    return Template_Window.extend({

        template: 'signin',

        tagName: 'div',

        tmplData: {},

        url: 'signin',

        initialize: function (opts) {

            this.simple = opts.simple || this.simple;
            this.template = tmpl_signin;
            this.model = new Model_Signin({mode: Stemapp.config.signin, simple: this.simple});
            this.model.bind('apply', this.apply, this);
            this.model.bind('error', this.error, this);
            this.tmplData = {
                registerEnable: Stemapp.config.registerEnable,
                demoEnable: Stemapp.config.demoEnable,
                restorePasswordEnable: Stemapp.config.restorePasswordEnable
            };
            Template_Window.prototype.initialize.apply(this);

            this.bind('blur', this.blur, this);

            if (Stemapp.config.signin == 'signinpro') {
                this.loadAbonents();
            }
        },

        afterRender: function () {

            this.$('.js-email').val($.cookie('username'));
            if (Stemapp.Auth && Stemapp.Auth.login) {
                this.autologin(Stemapp.Auth.login, Stemapp.Auth.password);
            }
            return this;
        },

        afterInsert: function () {
            setTimeout(function () {
                $('.js-email').focus();
            }, 500);
            this.trigger('finish');
        },

        events: {

            "submit .js-form": "submit",

            "click .js-remember": "remember",

            "click .js-registration": "registration",

            "click .js-demo": "demo"
        },

        loadAbonents: function () {
            var abonents = new Stemapp.Model.Abonents({server: ''});
            abonents.bind('applyAbonents', this.applyAbonents, this);
            abonents.loadByGuid(this.model.GUID);
        },

        applyAbonents: function (response) {

            if (Stemapp.demoGuid != Stemapp.GUID) {
                this.$('.js-company').removeAttr('disabled');
            }
            for (var i = 0; i < response.length; i++) {

                response[i].id = response[i].ABId;
                response[i].parent = response[i].ABParentId;
            }
            response[0].parent = 0;

            var tree = toTree(response, function (a, b, field, sortAsc) {

                if (a.data[field] == b.data[field]) {
                    return 0;
                }
                return (a.data[field] < b.data[field] ? -1 : 1) * ( sortAsc ? 1 : -1);
            }, 'ABName', true);

            var list = tree.getArray(),
                html = [],
                sep = '',
                diffLvl = list[0].ABCLevel;

            for (var i = 0; i < list.length; i++) {
                sep = '';
                for (var j = 0; j < list[i].ABCLevel - diffLvl; j++) {
                    sep += '&nbsp;&nbsp;';
                }
                html.push(
                    [
                        '<li class="item"><a href="#" tabindex="-1" data-id="',
                        list[i].ABGuidAbonent,
                        '">',
                        sep,
                        list[i].ABName,
                        '</a></li>'].join('')
                );
            }
            this.$('.js-company').parent().find('ul').html(html.join(''));
            this.$('.js-company').setVal(list[0].ABGuidAbonent);

        },

        submit: function (e) {
            e.preventDefault();
            this.enter();
        },

        enter: function () {
            this.showAjax(this.$('.js-submit'));
            var password = this.$('.js-pass').val() ? Stemapp.util.getPassHash(Stemapp.config.hashPassMethod, this.$('.js-pass').val(), Stemapp.config.salt) : '';
            this.model.save({Login: this.$('.js-email').val(), Password: password});
        },

        autologin: function (login, password) {
            this.$('.js-email').val(login);
            this.$('.js-pass').val(password);
            this.enter();
        },

        demo: function () {
            this.trigger('routes', 'demo');
            return false;
        },

        remember: function () {
            this.trigger('routes', 'remember');
            return false;
        },

        registration: function () {
            this.trigger('routes', 'registration');
            return false;
        },

        error: function (response) {
            this.hideAjax(this.$('.js-submit'));
        },

        apply: function (response) {

            if (this.simple) {
                this.remove();
            }

            switch (Stemapp.config.signin) {

                case 'signin':
                    break;

                case 'signinpro':
                    if (response.UsrToken == '') {
                        this.sendError('Неправильный логин или пароль');
                    }
                    break;
            }
        }

    });
});