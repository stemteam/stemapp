define([
    '_app/views/templates/window',
    'smarty!_app/tmpl/apprise/empty.tpl',
    'tmpl'
], function (Template_Window, TMPL_Empty, tmpl) {
    'use strict';

    /**
     * @class Window_Tab
     * @constructor
     * @extends Template_Window
     */
    var Window_Tab = Template_Window.extend({


        template: TMPL_Empty,

        overlay: true,


        tab: null,

        width: '90%',

        selected: [], // Все выбранный элементы из справочника

        selectedPage: [], // Выбранный элементы на текущей странице

        initialize: function (opts) {

            Template_Window.prototype.initialize.apply(this);
            this.show();
        },

        /**
         *
         * @param tab
         * @param opts
         * @param opts.multi Возможен ли выбор нескольких элементов в таблице
         * @param opts.caption Заголовок модального окна
         * @param opts.value Значение или массив значений
         * @param opts.fieldId Идентификатор столбца, в котором будут найдены значения opts.value
         * @param opts.callback Функция, куда будут возвращены значенич
         */
        setTab: function (tab, opts) {
            this.opts = opts;
            this.tab = tab;
            this.tab.setMode('modal');
            if (this.tab.table) {
                this.tab.table.opts.checkbox = this.opts.multi;
                this.tab.table.bind('update', _.bind(this.updateTable, this));
                var rowChange = this.tab.table.opts.rowChanged;
                this.tab.table.opts.rowChanged = function () {
                    rowChange();
                };
            }
        },

        updateTable: function () {


            var items = this.tab.table.getItems();
            if (!items.length) {
                return;
            }
            var i;
            var j;
            if (this.opts.fieldId) {
                var fieldId = this.opts.fieldId;
                if (this.opts.multi) {
                    var selectedRows = this.opts.value ? this.opts.value : [];
                    var rows = [];

                    // Надо проверить это мета форма или нет
                    if (items[0][fieldId]) {
                        // Если существует поле с таким названием, значит это не мета форма, и данные там находятся в виде {FielId:1}
                    } else {
                        // Значит это мета форма и данные там находятся в виде {f203_122_field_id:1}
                        for (i = 0; i < this.tab.fields.length; i++) {
                            if (this.tab.fields[i].FormFieldId == fieldId) {
                                fieldId = this.tab.fields[i].Name;
                            }
                        }
                    }

                    for (i = 0; i < items.length; i++) {
                        for (j = 0; j < selectedRows.length; j++) {
                            if (items[i][fieldId] == selectedRows[j]) {
                                rows.push(this.tab.table.getRowById(items[i].id));
                            }
                        }
                    }
                    this.tab.table.setSelectedRows(rows);
                } else {
                    // Надо проверить это мета форма или нет
                    if (items[0][fieldId]) {
                        // Если существует поле с таким названием, значит это не мета форма, и данные там находятся в виде {FielId:1}
                    } else {
                        // Значит это мета форма и данные там находятся в виде {f203_122_field_id:1}

                        for (i = 0; i < this.tab.fields.length; i++) {
                            if (this.tab.fields[i].FormFieldId == fieldId) {
                                fieldId = this.tab.fields[i].Name;
                            }
                        }
                    }

                    var value = this.opts.value ? this.opts.value : null;
                    if (value) {
                        for (i = 0; i < items.length; i++) {
                            if (items[i][fieldId] == value) {
                                this.tab.table.setActiveItem(this.tab.table.getRowById(items[i].id));
                                break;
                            }
                        }
                    }
                }
            }
        },

        render: function () {
            this.beforeRender();
            if (this.overlay && !this.$('.overlay').length) {
                this.$el.append('<div class="overlay"></div>');
            }
            if (this.tab) {
                if (this.tab.table) {
                    this.tab.table.opts.parentHeight = true;
                }
                this.container.html(tmpl(TMPL_Empty, {
                    title: this.opts.caption || this.tab.caption,
                    text: ''
                }));
                this.container.find('.js-window-content').append(this.tab.render().$el);
                this.resize();
                this.tab.redraw();
            }
            this.afterRender();
            return this;
        },

        afterInsert: function () {
            this.resize();
        },

        callback: function () {
        },

        events: {
            "click .js-close": "close",
            "click .js-submit": "submit"
        },

        submit: function (e) {
            var rows;
            if (this.opts.multi) {
                rows = this.tab.table.getSelectedRows();
                if (!rows.length) {
                    this.sendMessage('Отметьте флажки в таблице');
                    return;
                }
            } else {
                if (this.tab.getValue) {
                    rows = this.tab.getValue();
                } else {
                    rows = this.tab.table.getActiveItem();
                }
                if (!rows) {
                    this.sendMessage('Выберите строку в таблице');
                    return;
                }
            }
            this.opts.callback(rows, this.tab.fields);

            this.close();
        },

        close: function () {
            this.hide();
        }
    });

    return Window_Tab;
});