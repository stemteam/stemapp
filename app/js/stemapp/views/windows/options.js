define([
    '_app/views/templates/window',
    'smarty!_app/tmpl/window/options.tpl',
    'cookie'
], function (Template_Window, Template_Options) {
    'use strict';
    return Template_Window.extend({

        template: Template_Options,

        data: {

            title: ''
        },

        opts: {

            maxWidth: 800,

            maxHeight: 800
        },

        text: '',

        buttons: 'ok',

        overlay: true,

        table: null,

        tmplData: {
            title: 'Смена пароля'
        },

        initialize: function (opts) {

            this.callback = opts.callback ? opts.callback : _.bind(this.close, this);
            this.login = opts.login;

            this.tmplData.login = this.login;
            this.bind('blur', this.blur, this);

            Template_Window.prototype.initialize.apply(this);
            this.show();
        },

        events: {

            "click .cancel": "close",

            "submit .js-form": "submit",

            "click .js-demoGUID": "demo"
        },

        afterRender: function () {

            this.$('.js-guid').val(Stemapp.GUID);
        },

        demo: function () {
            this.$('.js-guid').val(Stemapp.demoGuid);
            return false;
        },

        submit: function (e) {

            var time = (new Date()).getTime() + 31536000000;
            $.cookie('GUID', this.$('.js-guid').val(), {expires: time});
            this.trigger('dialog', {
                text: 'Чтобы новые настройки вступили в силу, необходимо перезапустить программу.<br>Сделать это прямо сейчас?',
                buttons: 'yesno',
                callback: _.bind(this.success, this)
            });
            return false;
        },

        success: function (res) {

            if (res == 'yes') {
                Stemapp.logout(true);
            }
        },

        close: function () {
            this.hide();
            return false;
        }

    });
});