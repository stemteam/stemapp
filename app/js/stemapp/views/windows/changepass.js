define([
    '_app/views/templates/window',
    '_app/models/remember',
    'smarty!_app/tmpl/window/password.tpl',
], function (Template_Window, Model_Remember, Template_Password) {
    'use strict';
    return Template_Window.extend({

        template: Template_Password,

        data: {

            title: ''
        },

        opts: {

            maxWidth: 800,

            maxHeight: 800
        },

        text: '',

        buttons: 'ok',


        overlay: true,

        table: null,

        dataModel: null,

        tmplData: {
            title: 'Смена пароля'
        },

        initialize: function (opts) {

            this.callback = opts.callback ? opts.callback : _.bind(this.close, this);
            this.login = opts.login;

            this.tmplData.login = this.login;
            this.bind('blur', this.blur, this);


            this.model = new Model_Remember();
            this.model.bind('successChangePass', this.success, this);
            Template_Window.prototype.initialize.apply(this);
            this.show();
        },

        events: {

            "click .cancel": "close",

            "click .success": "submit"
        },

        submit: function (e) {
            this.showAjax(e.currentTarget);
            this.model.changePassword({
                OldPasswd: this.$('.js-change-pass-current').val(),
                NewPasswd: this.$('.js-change-pass-new').val(),
                NewPasswd2: this.$('.js-change-pass-repeat').val()
            });
        },

        success: function () {

            this.sendMessage('Пароль изменен');
            this.callback();
            this.close();
        },

        close: function () {
            this.hide();
        }

    });
});