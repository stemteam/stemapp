define([
    '_app/views/templates/window',
    '_app/models/form',
    '_app/models/formfields',
    'smarty!_app/tmpl/window/formcopy.tpl'
], function (Template_Window, Model_Form, Model_Form_Fields, Template_Password) {
    'use strict';

    /**
     * @class Window_Menu_Copy
     * @constructor
     * @extends Template_Window
     */
    var Window_Menu_Copy = Template_Window.extend({

        template: Template_Password,

        data: {

            title: ''
        },

        opts: {

            maxWidth: 800,

            maxHeight: 800
        },

        text: '',

        buttons: 'ok',

        /**
         * @type {Model_Form}
         */
        model: null,

        /**
         * @type {Model_Form_Fields}
         */
        modelFields: null,

        overlay: true,

        table: null,

        dataModel: null,

        tmplData: {
            title: 'Копирование форму'
        },

        countCopy: 0,
        countComplete: 0,

        initialize: function (opts) {

            this.callback = opts.callback ? opts.callback : _.bind(this.close, this);
            this.item = opts.item;
            this.tmplData.item = this.item;
            this.model = new Model_Form({new: true});
            this.modelFields = new Model_Form_Fields({new: true});
            this.initModel();
            this.initModel(this.modelFields);
            this.bind('blur', this.blur, this);
            Template_Window.prototype.initialize.apply(this);
            this.load();
        },

        events: {

            "click .cancel": "close",

            "click .success": "submit"
        },

        load: function () {
            this.model.load({data: {ShowAll: 1}, success: _.bind(this.successLoadForms, this)});
        },

        successLoadForms: function (data) {
            this.forms = data;
            this.render();
            this.show();
        },

        submit: function (e) {
            this.showAjax(e.currentTarget);
            var self = this;
            this.copyItem(this.item, 0, function (id) {
                self.copyFields(self.item, id);
                self.searchItem(self.item.FormId, id);
            });
        },

        copyItem: function (menu, parentId, func) {
            this.countCopy++;
            this.model.save({
                ParentId: parentId,
                FieldName: menu.FieldName,
                ParentFieldName: menu.ParentFieldName,
                Name: this.$('.js-Name').val() + '.' + menu.Name,
                MethodId: menu.MethodId,
                Description: menu.Description,
                ListDescription: menu.ListDescription,
                OrderNum: menu.OrderNum
            }, {error: _.bind(this.requestError, this), success: _.bind(this.successAdd, this, func)});
        },

        checkComplete: function () {
            if (this.countComplete == this.countCopy) {
                this.success();
            }
        },

        searchItem: function (formId, nid) {
            var self = this;

            for (var i = 0; i < this.forms.length; i++) {
                if (this.forms[i].ParentId == formId) {
                    var menu = this.forms[i];
                    this.copyItem(menu, nid, function (id) {
                        self.copyFields(menu, id);
                        self.searchItem(menu.FormId, id);
                    });
                }
            }
        },

        copyFields: function (menu, nid) {
            this.modelFields.load({
                data: {FormId: menu.FormId, IsAllFields: 0},
                success: _.bind(this.successCopyFields, this, nid, menu.FormId)
            });
        },

        /**
         *
         * @param nid Новый идентификатор формы
         * @param fid Старый идентификатор формы
         * @param fields
         */
        successCopyFields: function (nid, fid, fields) {
            this.countComplete++;
            for (var i = 0; i < fields.length; i++) {
                if (fields[i].FormId != fid) { // Если поле не из этой формы
                    continue;
                }
                this.countCopy++;
                var modelFields = new Model_Form_Fields({new: true});
                modelFields.save({
                    FormId: nid,
                    FieldId: fields[i].FieldId,
                    Caption: fields[i].Caption,
                    OrderNum: fields[i].OrderNum,
                    IsOrdered: fields[i].IsOrdered,
                    IsOrderedDesc: fields[i].IsOrderedDesc,
                    IsPrimary: fields[i].IsPrimary,
                    IsHidden: fields[i].IsHidden,
                    IsReadonly: fields[i].IsReadonly,
                    RelationFieldId: fields[i].RelationFieldId,
                    RelationFormType: fields[i].RelationFormType
                }, {success: _.bind(this.successAddField, this)});
            }
            this.checkComplete();
        },

        successAddField: function () {
            this.countComplete++;
            this.checkComplete();
        },

        successAdd: function (func, model, data) {
            func(data.FormId);
        },

        success: function () {
            this.close();
            this.callback();
        },

        close: function () {
            this.hide();
        }

    });
    return Window_Menu_Copy;
});