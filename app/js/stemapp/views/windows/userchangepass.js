define([
    '_app/views/templates/window',
    '_app/views/control/table',
    '_app/models/user',
    'smarty!_app/tmpl/window/changepass.tpl',
    '_app/data/devices',
], function (Template_Window, Control_Table, Model_User, Template_Changepass) {
    'use strict';
    return Template_Window.extend({


        template: Template_Changepass,

        data: {

            title: ''
        },

        opts: {

            maxWidth: 800,

            maxHeight: 800
        },

        text: '',

        buttons: 'ok',

        overlay: true,

        table: null,

        dataModel: null,

        tmplData: {
            title: 'Смена пароля'
        },

        initialize: function (opts) {
            this.callback = opts.callback ? opts.callback : _.bind(this.close, this);
            this.login = opts.login;

            this.tmplData.login = this.login;
            this.bind('blur', this.blur, this);


            this.model = new Model_User();
            this.model.bind('success', this.success, this);
            Template_Window.prototype.initialize.apply(this);
            this.show();
        },

        events: {

            "click .cancel": "close",
            "click .success": "submit"
        },

        submit: function (e) {
            this.showAjax(e.currentTarget);
            this.model.changePassword({
                Passwd: this.$('.js-password').val(),
                NewPasswd2: this.$('.js-conf-password').val(),
                Login: this.login
            });
        },

        success: function () {

            this.sendMessage('Пароль изменен');
            this.callback();
            this.close();
        },

        close: function () {
            this.hide();
        }

    });
});