define([
    '_app/views/templates/main',
], function (Template_Main) {
    return (Template_Main.extend({

        el: $('#preload'),

        template: '',

        initialize: function (opts) {

            opts = (opts && opts[0]) ? opts[0] : {};

            this.bind('blur', this.blur, this);
            this.bind('focus', this.show, this);
//        this.$('.text').text(opts.title ? opts.title : 'Новостат — партнёр');
        },

        hide: function () {

            this.$el.addClass('hideScale');
            _.delay(_.bind(function () {
                this.$el.hide();
            }, this), 290);
        },


        show: function () {

            this.$el.removeClass('hideScale').addClass('showScale').show();
            _.delay(_.bind(function () {
                this.$el.removeClass('showScale');
            }, this), 290);
        }

    }));
});