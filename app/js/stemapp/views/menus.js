define([
    '_app/views/templates/tab',
    '_app/views/control/table',
    '_app/models/menu',
    '_app/data/menus',
], function (Template_Tab, Control_Table, Model_Menu) {
    'use strict';

    /**
     * @class View_Menus
     * @constructor
     * @extends Template_Tab
     */
    var View_Menus = Template_Tab.extend({

        tagName: 'div',

        model: null,

        template: 'menu',

        name: 'menu',

        icon: 'application-sidebar-list',

        caption: 'Список меню',

        tabButton: null,

        server: null,

        table: null,

        dataName: 'menu',

        url: 'menus',

        grid: {

            type: 'tree',
            enable: true,
            counter: false
        },

        initialize: function (opts) {


            Template_Tab.prototype.initialize.apply(this, [opts]);
            this.model = new Model_Menu();
            this.model.bind('apply', this.apply, this);
            this.model.bind('applyDelete', this.applyDelete, this);
            this.model.bind('errorDelete', this.errorDelete, this);

            this.initModel();
            this.load();
            this.grid.rowChanged = _.bind(this.rowChanged, this);
            this.table = new Control_Table(this.dataName, this.grid);
        },

        events: function () {

            return _.extend({},
                Template_Tab.prototype.events, {

                    "click .toolbar a.js-menu-create": "createMenu",

                    "click .toolbar a.js-menu-child-create": "createChildMenu",

                    "click .toolbar a.js-menu-edit": "editMenu",

                    "click .toolbar a.js-menu-delete": "deleteMenu",

                    "click .toolbar a.js-menu-copy": "copyMenu"
                })
        },

        load: function () {

            this.showGlobalAjax();
            this.model.load({data: {ShowAll: 1}});
        },

        apply: function (response) {
            this.hideAjax();
            this.table.update(response);
            this.table.sort('MenuName', true);
        },
        createMenu: function (e) {
            e.preventDefault();
            this.trigger('open', 'Menu', ['new']);
            return false;
        },

        copyMenu: function (e) {
            e.preventDefault();
            var cell = this.table.getActiveCell(),
                item = this.table.getItem(cell.row);
            this.trigger('window', 'MenuCopy', {item: item, callback: _.bind(this.copyComplete, this)});
        },

        copyComplete: function () {
            this.refresh();
            this.sendMessage('Меню скопировано');
        },

        createChildMenu: function (e) {
            e.preventDefault();
            var cell = this.table.getActiveCell(),
                item = this.table.getItem(cell.row);
            this.trigger('open', 'Menu', [item.MenuId, 'new']);
            return false;
        },

        editMenu: function () {

            var cell = this.table.getActiveCell(),
                item = this.table.getItem(cell.row);
            this.trigger('open', 'Menu', [item.MenuId]);
            return false;
        },

        deleteMenu: function () {
            var item = this.table.getActiveItem();

            this.trigger('dialog', {
                text: 'Удалить меню ' + item.MenuId + '?',
                buttons: 'yesno',
                callback: _.bind(this.resultDeleteMenu, this, item)
            });

            return false;
        },

        resultDeleteMenu: function (item, result) {

            if (result == 'yes') {
                this.deleteRow(item, 'MenuId');
                item._delete = true;
                this.table.updateItem(item);
            }
        },

        applyDelete: function (item) {
            this.refresh();
        },

        errorDelete: function (item) {

            item._delete = false;
            this.table.updateItem(item);
        },


        items: function () {

            var array = [];
            return array.concat([

                {
                    cls: 'js-menu-child-create',
                    text: 'Новое дочернее меню',
                    icon: 'application-sidebar-list-create',
                    invalidate: _.bind(this.itemInvalidate, this)
                },

                {
                    cls: 'js-menu-edit',
                    text: 'Редактировать',
                    icon: 'application-sidebar-list-edit',
                    invalidate: _.bind(this.itemInvalidate, this)
                },
                {
                    cls: 'js-menu-delete',
                    text: 'Удалить',
                    icon: 'application-sidebar-list-delete',
                    invalidate: _.bind(this.itemInvalidate, this)
                },
                {
                    cls: 'js-menu-copy',
                    text: 'Скопировать',
                    icon: 'documents-stack',
                    invalidate: _.bind(this.itemInvalidate, this)
                },

                '>',
                {
                    type: 'input',
                    cls: 'search',
                    placeholder: 'Поиск по таблице'
                },
                {
                    cls: 'refresh',
                    text: 'Обновить',
                    icon: true,
                    compact: true
                },
                {
                    cls: 'filter',
                    text: 'Фильтры',
                    icon: true,
                    toggle: true,
                    compact: true
                },
                {
                    cls: 'export',
                    text: 'Экспорт в csv',
                    icon: true,
                    compact: true
                },
                {
                    cls: 'copyexcel',
                    text: 'Скопировать таблицу в буфер обмена',
                    icon: true,
                    compact: true
                }
            ]);
        }

    });

    return View_Menus;
});