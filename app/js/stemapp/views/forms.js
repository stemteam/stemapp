define([
    '_app/views/templates/tab',
    '_app/views/control/table',
    '_app/models/form',
    '_app/data/forms',
], function (Template_Tab, Control_Table, Model_Form) {
    'use strict';

    /**
     * @class View_Forms
     * @constructor
     * @extends Template_Tab
     */
    var View_Forms = Template_Tab.extend({

        tagName: 'div',


        /**
         * @type {Model_Form}
         */
        model: null,


        name: 'forms',

        icon: 'applications',

        caption: 'Формы',

        tabButton: null,

        server: null,

        table: null,

        dataName: 'forms',

        url: 'forms',

        grid: {

            type: 'tree',
            enable: true,
            counter: false
        },

        initialize: function (opts) {


            Template_Tab.prototype.initialize.apply(this, [opts]);
            this.model = new Model_Form();
            this.model.bind('apply', this.apply, this);
            this.model.bind('applyDelete', this.applyDelete, this);
            this.model.bind('errorDelete', this.errorDelete, this);

            this.initModel();
            this.load();
            this.grid.rowChanged = _.bind(this.rowChanged, this);
            this.table = new Control_Table(this.dataName, this.grid);
        },

        events: function () {

            return _.extend({},
                Template_Tab.prototype.events, {

                    "click .toolbar a.form-create": "createForm",

                    "click .toolbar a.form-child-create": "createChildForm",

                    "click .toolbar a.form-open": "openForm",

                    "click .toolbar a.form-edit": "editForm",

                    "click .toolbar a.form-delete": "deleteForm",

                    "click .toolbar a.fields": "editFields",

                    "click .toolbar a.js-rights": "toRights",

                    "click .toolbar a.js-copy": "copyForm"
                })
        },

        load: function () {

            this.showGlobalAjax();
            this.model.load();
        },

        apply: function (response) {
            this.hideAjax();
            this.table.update(response);
            this.table.sort('Name', true);
        },

        openForm: function (e) {
            e.preventDefault();
            var cell = this.table.getActiveCell(),
                item = this.table.getItem(cell.row);
            this.trigger('open', 'meta/Records', [item.Name]);
            return false;
        },

        createForm: function (e) {
            e.preventDefault();
            this.trigger('open', 'Form', ['new']);
            return false;
        },

        createChildForm: function (e) {
            e.preventDefault();
            var cell = this.table.getActiveCell(),
                item = this.table.getItem(cell.row);
            this.trigger('open', 'Form', [item.FormId, 'new']);
            return false;
        },

        editForm: function () {

            var cell = this.table.getActiveCell(),
                item = this.table.getItem(cell.row);
            this.trigger('open', 'Form', [item.FormId]);
            return false;
        },

        editFields: function (e) {
            e.preventDefault();
            var cell = this.table.getActiveCell(),
                item = this.table.getItem(cell.row);
            this.trigger('open', 'Form_Fields', [item.FormId]);
        },

        toRights: function (e) {
            e.preventDefault();
            var cell = this.table.getActiveCell(),
                item = this.table.getItem(cell.row);
            this.trigger('open', 'Form_Rights', [item.FormId]);
        },

        copyForm: function (e) {
            e.preventDefault();
            var cell = this.table.getActiveCell(),
                item = this.table.getItem(cell.row);
            this.trigger('window', 'FormCopy', {item: item, callback: _.bind(this.copyComplete, this)});
        },

        copyComplete: function () {
            this.refresh();
            this.sendMessage('Форма скопирована');
        },

        deleteForm: function () {
            var item = this.table.getActiveItem();

            this.trigger('dialog', {
                text: 'Удалить форму ' + item.FormId + '?',
                buttons: 'yesno',
                callback: _.bind(this.resultDeleteForm, this, item)
            });

            return false;
        },

        resultDeleteForm: function (item, result) {

            if (result == 'yes') {
                this.deleteRow(item, 'FormId');
                item._delete = true;
                this.table.updateItem(item);
            }
        },

        applyDelete: function (item) {
            this.refresh();
        },

        errorDelete: function (item) {

            item._delete = false;
            this.table.updateItem(item);
        },

        items: function () {

            var array = [];
            array = array.concat([
                {
                    cls: 'form-create',
                    text: 'Новая форма',
                    icon: 'applications-create'
                },
                {
                    cls: 'form-child-create',
                    text: 'Новая дочерняя форма',
                    icon: 'applications-child-create',
                    invalidate: _.bind(this.itemInvalidate, this)
                },
                {
                    cls: 'form-edit',
                    text: 'Редактировать',
                    icon: 'applications-edit',
                    invalidate: _.bind(this.itemInvalidate, this)
                },
                {
                    cls: 'form-delete',
                    text: 'Удалить',
                    icon: 'applications-delete',
                    invalidate: _.bind(this.itemInvalidate, this)
                },
                {
                    cls: 'form-open',
                    text: 'Открыть',
                    icon: 'blog',
                    invalidate: _.bind(this.itemInvalidate, this)
                },
                {
                    cls: 'fields',
                    text: 'Настроить поля',
                    icon: 'wrench',
                    invalidate: _.bind(this.itemInvalidate, this)
                },
                {
                    cls: 'js-rights',
                    text: 'Права',
                    icon: 'funnel_pencil',
                    invalidate: _.bind(this.itemInvalidateParent, this, 'ParentId')
                },
                {
                    cls: 'js-copy',
                    text: 'Скопировать',
                    icon: 'documents-stack',
                    invalidate: _.bind(this.itemInvalidateParent, this, 'ParentId')
                }
            ]);

            array = array.concat(this.getTableItems());
            return array;
        }

    });
    return View_Forms;
});