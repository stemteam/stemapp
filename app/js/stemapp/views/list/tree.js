Stemapp.View.List_Tree = (Backbone.View.extend({

    tagName: 'div',
    id: 'jstree',
    className: 'treeView',

    events: {
        "changed.jstree": "onSelectItem"
    },

    initialize: function() {
        this.id += this.cid;
        this.$el.attr('id',this.id);

    },

    onSelectItem: function(e,data) {
        this.trigger("onSelectItem",e,data);
    },

    render: function(data) {
/*       data = new Array();

        data.push({
            'id' : '12',
            'text': 'text',
            'children': true,
            'type': 'noIcon',
            'state': {'opened': true, 'disabled': true},
            'children': [{
                'id' : 'childName1',
                'data' : '12',
                'text' : 'children1',
                'type' : 'noIcon'
            },
                {
                'id' : 'childName2',
                'data' : '12',
                'text' : 'children2',
                'type' : 'noIcon'
            },
                {
                    'id' : 'childName3',
                    'data' : '123',
                    'text' : 'children3',
                    'type' : 'noIcon'
                }]

        });*/

        //console.log(data);
        this.$el.jstree({
            "core" : {
                "animation" : 0,
                "check_callback" : true,
                "themes" : { "icons" : false, "stripes": true, "responsive" : false },
                'data' : data
            },
            "types" : {
                "#" : {
                    "max_children" : 1,
                    "max_depth" : 4,
                    "valid_children" : ["root"]
                },
                "root" : {
                    "icon" : "none",
                    "valid_children" : ["default"]
                },
                "default" : {
                    "valid_children" : ["default","file"]
                },
                "file" : {
                    "icon" : "glyphicon glyphicon-file",
                    "valid_children" : []
                },
                "noIcon" : {
                    "icon" : false
                }
            },
            "plugins" : [
                "dnd",
                //"state",
                "types",
                "wholerow"
            ]
        });

        this.$el.jstree(true).settings.core.data = data;
        this.$el.jstree(true).refresh();
        //this.$el.jstree('deselect_all');

        return this;
    },

    getSelectedReport: function() {
        return this.$el.jstree('get_selected')[0];
    }

}));

