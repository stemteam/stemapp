define([
    '_app/views/templates/tab',
    '_app/models/role',
    'smarty!_app/tmpl/role.tpl',
], function (Template_Tab, Model_Role, Template_Form) {
    'use strict';
    return Template_Tab.extend({

        tagName: 'div',

        model: null,

        name: 'role',

        caption: 'Изменение ролей пользователя',

        tabButton: null,

        server: null,

        table: null,

        template: Template_Form,

        notRender: true,

        url: 'role{if $namespace}/{$namespace}{/if}{if $model}/{$model}{/if}{if $id}/{$id}{/if}',

        initialize: function (opts) {

            this.urlData = {id: this.modelId, model: this.modelName};

            this.model = new Model_Role();
            this.model.bind('success', this.apply, this);
            this.model.bind('loadedRoles', this.loadedRoles, this);

            Template_Tab.prototype.initialize.apply(this, [opts]);

            this.load();

        },

        loadedRoles: function (roles) {
            this.tmplData = {roles: roles};
            this.hideAjax();
            this.notRender = false;
            this.render();

        },

        afterRender: function () {
            this.$('.refresh').attr('disabled', true);
            this.hideAjax();
        },

        events: function () {

            return _.extend({},
                Template_Tab.prototype.events, {

                    "submit form": "submit",

                    'click form button.js-downShift': 'removeRole'

                });
        },

        load: function () {
            this.model.load();
            this.showGlobalAjax();
        },

        refresh: function () {
            this.showAjax(this.$('.toolbar a.refresh'));
            this.tabAjax();
            this.load();
            return false;
        },

        error: function (message) {
            this.trigger('error', {}, message);
        },

        gag: function () {
            return false;
        },

        apply: function (action) {

            if (action == 'add') {
                this.sendMessage('Роль успешно добавлена');
            }
            else {
                this.sendMessage('Роль успешно удалена');
            }
            this.remove();

        },

        submit: function () {
            var user = this.$('form :input.js-user').val();
            var role = this.$('form select.js-role').val();

            this.model.addRole(user, role);

            return false;
        },

        removeRole: function () {
            var user = this.$('form :input.js-user').val();
            var role = this.$('form select.js-role').val();

            this.model.removeRole(user, role);
        },

        items: function () {

            return [
                '>',
                {
                    cls: 'refresh',
                    text: 'Обновить',
                    icon: true,
                    compact: true
                }
            ];
        }

    });
});