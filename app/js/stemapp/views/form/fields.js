define([
    '_app/views/templates/tab',
    '_app/views/control/table',
    '_app/models/formfields',
    '_app/data/formfields'
], function (Template_Tab, Control_Table, Model_Form_Fields) {
    'use strict';

    /**
     * @class View_Form_Fields
     * @constructor
     * @extends Template_Tab
     */
    var View_Form_Fields = Template_Tab.extend({

        tagName: 'div',

        /**
         * @type {Model_Form_Fields}
         */
        model: null,

        name: 'form_fields',

        icon: 'wrench',

        caption: 'Настройка полей формы',

        tabButton: null,

        server: null,

        /**
         * @type {Control_Table}
         */
        table: null,

        dataName: 'formfields',

        url: 'form_fields/{$id}',

        grid: {

            enable: true,
            counter: false
        },

        initialize: function (opts) {

            this.FormId = opts.arguments[0];
            this.urlData = {id: this.FormId};
            Template_Tab.prototype.initialize.apply(this, [opts]);
            this.model = new Model_Form_Fields({FormId: this.FormId});
            this.model.bind('apply', this.apply, this);
            this.model.bind('applyDelete', this.applyDelete, this);
            this.model.bind('errorDelete', this.errorDelete, this);

            this.initModel();
            this.load();
            this.grid.rowChanged = _.bind(this.rowChanged, this);
            this.table = new Control_Table(this.dataName, this.grid);
        },

        events: function () {

            return _.extend({},
                Template_Tab.prototype.events, {

                    "click .toolbar a.form-field-create": "createField",

                    "click .toolbar a.form-field-edit": "editField",

                    "click .toolbar a.form-field-delete": "deleteField",

                    "click .toolbar a.form-field-meta": "metaField",

                    "click .grid a.js-FieldName": "openField"
                })
        },

        load: function () {
            this.showGlobalAjax();
            this.model.load({data: {IsAllFields: 1}});
        },

        apply: function (response) {

            //todo сделать возможность переноса полей из небытьия в форму
            this.hideAjax();
            this.table.update(response);
        },

        openField: function (e) {
            e.preventDefault();
            var item = this.table.getActiveItem();
            //todo нужно это сделать на сервере
            this.trigger('open', 'meta/Record', ['stemdb.stem_meta_field', item.FieldId]);
        },

        copySuccess: function () {
            this.sendMessage('Поля скопированы');
            this.refresh();
        },

        createField: function (e) {
            e.preventDefault();
            this.trigger('open', 'Form_Field', [this.FormId, 'new']);
        },

        editField: function (e) {
            e.preventDefault();
            var item = this.table.getActiveItem();
            if (!item.FormFieldId) {
                this.sendMessage('Вы перешли на форму добавление поля, так как выбранное поле не было добавлено на форму.');
                this.trigger('open', 'Form_Field', [this.FormId, 'new', item.FieldId]);
            } else {
                this.trigger('open', 'Form_Field', [this.FormId, item.FormFieldId]);
            }
        },

        metaField: function (e) {
            e.preventDefault();
            var item = this.table.getActiveItem();
            //todo нужно это сделать на сервере
            this.trigger('open', 'meta/Record', ['stemdb.stem_meta_field', item.FieldId]);
        },

        deleteField: function (e) {
            e.preventDefault();
            var item = this.table.getActiveItem();
            this.trigger('dialog', {
                text: 'Удалить поле ' + item.FormFieldId + '?',
                buttons: 'yesno',
                callback: _.bind(this.resultDeleteField, this, item)
            });
        },

        resultDeleteField: function (item, result) {

            if (result == 'yes') {
                this.deleteRow(item, 'FormFieldId');
                item._delete = true;
                this.table.updateItem(item);
            }
        },

        applyDelete: function (item) {

            this.table.deleteItem(item);
        },

        errorDelete: function (item) {

            item._delete = false;
            this.table.updateItem(item);
        },


        items: function () {

            var array = [];
            array.push({
                cls: 'form-field-create',
                text: 'Новое поле',
                icon: 'application-split-create'
            });
            array = array.concat([
                {
                    cls: 'form-field-edit',
                    text: 'Редактировать',
                    icon: 'application-split-edit',
                    invalidate: _.bind(this.itemInvalidate, this)
                },
                {
                    cls: 'form-field-meta',
                    text: 'Метаданные поля',
                    icon: '',
                    invalidate: _.bind(this.itemInvalidate, this)
                },
                {
                    cls: 'form-field-delete',
                    text: 'Удалить',
                    icon: 'application-split-delete',
                    invalidate: _.bind(this.itemInvalidate, this)
                }]);
            array = array.concat(this.getTableItems());
            return array;
        }

    });

    return View_Form_Fields;
});