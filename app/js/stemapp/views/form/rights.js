define([
    '_app/views/templates/tab',
    '_app/views/control/table',
    '_app/models/formrights',
    '_app/data/formrights'
], function (Template_Tab, Control_Table, Model_Form_Rights, Model_Role) {
    'use strict';

    /**
     * @class View_Form_Fields
     * @constructor
     * @extends Template_Tab
     */
    var View_Form_Fields = Template_Tab.extend({

        tagName: 'div',

        /**
         * @type {Model_Form_Rights}
         */
        model: null,



        name: 'form_rights',

        icon : 'funnel_pencil',

        caption: 'Права формы',

        tabButton: null,

        server: null,

        /**
         * @type {Control_Table}
         */
        table: null,

        dataName: 'formrights',

        url: 'form_rights/{$id}',

        grid: {

            enable: true,
            counter: false
        },

        initialize: function (opts) {

            this.FormId = opts.arguments[0];
            this.urlData = {id: this.FormId};
            Template_Tab.prototype.initialize.apply(this, [opts]);
            this.model = new Model_Form_Rights({FormId: this.FormId});
            this.model.bind('apply', this.apply, this);
            this.model.bind('applyDelete', this.applyDelete, this);
            this.model.bind('errorDelete', this.errorDelete, this);

            this.initModel();
            this.load();
            this.grid.rowChanged = _.bind(this.rowChanged, this);
            this.table = new Control_Table(this.dataName, this.grid);
        },

        events: function () {

            return _.extend({},
                Template_Tab.prototype.events, {

                    "click .toolbar a.form-right-create": "createRight",

                    "click .toolbar a.form-right-edit": "editRight",

                    "click .toolbar a.form-right-delete": "deleteRight"
                })
        },

        load: function () {
            this.showGlobalAjax();
            this.model.load({data: {}});
        },

        apply: function (response) {

            //todo сделать возможность переноса полей из небытьия в форму
            this.hideAjax();
            this.table.update(response);
        },

        copySuccess: function () {
            this.sendMessage('Поля скопированы');
            this.refresh();
        },

        createRight: function (e) {
            e.preventDefault();
            this.trigger('open', 'Form_Right', [this.FormId, 'new']);
        },

        editRight: function (e) {
            e.preventDefault();
            var item = this.table.getActiveItem();
            this.trigger('open', 'Form_Right', [this.FormId, item.FormRightId]);
        },

        deleteRight: function (e) {
            e.preventDefault();
            var item = this.table.getActiveItem();
            this.trigger('dialog', {
                text: 'Удалить поле ' + item.FormRightId + '?',
                buttons: 'yesno',
                callback: _.bind(this.resultDeleteField, this, item)
            });
        },

        resultDeleteField: function (item, result) {

            if (result == 'yes') {
                this.deleteRow(item, 'FormFieldId');
                item._delete = true;
                this.table.updateItem(item);
            }
        },

        applyDelete: function (item) {

            this.table.deleteItem(item);
        },

        errorDelete: function (item) {

            item._delete = false;
            this.table.updateItem(item);
        },


        items: function () {

            var array = [];
            array.push({
                cls: 'form-right-create',
                text: 'Новые права',
                icon: 'application-split-create'
            });
            array = array.concat([
                {
                    cls: 'form-right-edit',
                    text: 'Редактировать',
                    icon: 'application-split-edit',
                    invalidate: _.bind(this.itemInvalidate, this)
                },
                {
                    cls: 'form-right-delete',
                    text: 'Удалить',
                    icon: 'application-split-delete',
                    invalidate: _.bind(this.itemInvalidate, this)
                }]);
            array = array.concat(this.getTableItems());
            return array;
        }

    });

    return View_Form_Fields;
});