define([
    '_app/views/templates/tab',
    '_app/models/form',
    '_app/models/formfields',
    '_app/models/meta',
    'smarty!_app/tmpl/form/field/form.tpl'
], function (Template_Tab, Model_Form, Model_Form_Fields, Model_Meta, Template_Form) {
    'use strict';

    /**
     * @class View_Form_Field
     * @constructor
     * @extends Template_Tab
     */
    var View_Form_Field = Template_Tab.extend({

        tagName: 'div',

        /**
         * @type {Model_Form_Fields}
         */
        model: null,

        template: Template_Form,

        name: 'form_field',

        icon: 'wrench',

        caption: 'Настройка поля формы',

        url: 'form_fields/{$frame}{if $new}/{$new}{if $field}/{$field}{/if}{else}/{$id}{/if}',

        autoToolbar: false,

        single: true,

        notRender: true,

        initialize: function (opts) {

            Template_Tab.prototype.initialize.apply(this, [opts]);

            this.FormId = opts.arguments[0];
            this.FormFieldId = opts.arguments[1];
            this.new = opts.arguments[1] == 'new';
            if (opts.arguments[2]) {
                this.FieldId = opts.arguments[2];
            }
            this.urlData = {frame: this.FormId, id: this.FormFieldId, 'new': this.new ? 'new' : '', field: this.FieldId};

            this.model = new Model_Form_Fields({FormId: this.FormId, 'new': this.new});
            this.model.bind('apply', this.apply, this);
            if (this.new) {
                this.setCaption('Новое поле формы');
            }
            this.initModel();
            this.load();

        },

        events: {

            "submit form": "submit",
            "click .js-field-form": "openFieldForm",
            "click .js-form": "openFormForm",
            "click .js-form-field": "openRelativeFieldForm",
            "click .js-lookup": "openLookupForm"
        },

        load: function () {
            if (this.FormFieldId && this.FormFieldId !== 'new') {
                this.showGlobalAjax();
                this.model.getById({data: {FormFieldId: this.FormFieldId}});
            }
            else {
                this.model.fieldGetById({data: {FieldId: this.FieldId}, success: _.bind(this.successField, this)});
            }
        },

        successField: function (data) {
            if (data && data.FieldId && data.Name) {
                this.apply({FieldId: data.FieldId, FieldName: data.Name});
            } else {
                this.apply();

            }
        },

        openFormForm: function (e) {
            e.preventDefault();
            this.trigger('openModal', 'Forms', [], {
                caption: 'Выберите форму для связи',
                multi: false,
                fieldId: 'FormId',
                value: this.FormId,
                callback: _.bind(this.successFormForm, this)
            });
        },

        openRelativeFieldForm: function (e) {
            e.preventDefault();
            if (this.data.RelationFormId) {
                this.trigger('openModal', 'Form_Fields', {arguments: [this.data.RelationFormId]}, {
                    caption: 'Выберите поле для связи',
                    multi: false,
                    fieldId: 'FieldId',
                    value: this.data.RelationFieldId,
                    callback: _.bind(this.successRelativeFieldForm, this)
                });
            } else {
                this.sendMessage('Выберите связанную форму');
            }
        },

        successRelativeFieldForm: function (data) {
            this.$('[name="RelationFieldName"]').val(data.Name);
            this.$('[name="RelationFieldId"]').val(data.FormFieldId);
        },

        openLookupForm: function (e) {
            e.preventDefault();
            this.trigger('openModal', 'Forms', [], {
                caption: 'Выберите форму для связи',
                multi: false,
                fieldId: 'FormId',
                value: this.data ? this.data.LookupFormId : null,
                callback: _.bind(this.successLookupForm, this)
            });
        },

        successLookupForm: function (data) {
            this.$('[name="LookupFormName"]').val(data.Name);
            this.$('[name="LookupFormId"]').val(data.FormId);
        },

        successFormForm: function (data) {
            this.$('[name="RelationFormName"]').val(data.Name);
            this.$('[name="RelationFormId"]').val(data.FormId);
            this.data.RelationFormId = data.FormId;
        },

        openFieldForm: function (e) {
            e.preventDefault();
            this.trigger('openModal', 'Form_Fields', {arguments: [this.FormId]}, {
                caption: 'Выберите поле',
                multi: false,
                fieldId: 'FieldId',
                value: this.data ? this.data.FieldId : null,
                callback: _.bind(this.successFieldForm, this)
            });
        },

        successFieldForm: function (data) {
            this.$('[name="FieldName"]').val(data.FieldName);
            this.$('[name="FieldId"]').val(data.FieldId);
        },

        apply: function (response) {
            this.data = response;

            if (this.new) {
                this.tmplData = {
                    id: this.FormFieldId,
                    item: this.data
                };
            } else {
                this.tmplData = {item: this.data, fields: this.fields, id: this.FormFieldId};
                this.setCaption('Поле ' + this.data.FormFieldId);
            }
            this.notRender = false;
            this.render();
            this.hideAjax();
        },


        submit: function (e) {

            e.preventDefault();

            this.showAjax(this.$('.js-submit'));
            this.tabAjax();

            if (this.new) {
                this.model.save({
                    FormId: this.FormId,
                    FieldId: this.$('[name="FieldId"]').val(),
                    Caption: this.$('[name="Caption"]').val(),
                    RelationFieldId: this.$('[name="RelationFieldId"]').val(),
                    RelationFormType: this.$('[name="RelationFormType"]').val(),
                    OrderNum: this.$('[name="OrderNum"]').val(),
                    IsOrdered: this.$('[name="IsOrdered"]').prop('checked') ? 1 : 0,
                    IsOrderedDesc: this.$('[name="IsOrderedDesc"]').prop('checked') ? 1 : 0,
                    IsPrimary: this.$('[name="IsPrimary"]').prop('checked') ? 1 : 0,
                    IsHidden: this.$('[name="IsHidden"]').prop('checked') ? 1 : 0,
                    IsReadonly: this.$('[name="IsReadonly"]').prop('checked') ? 1 : 0,
                    LookupFormId: this.$('[name="LookupFormId"]').val()
                }, {error: _.bind(this.requestError, this), success: _.bind(this.successAdd, this)});
            } else {
                this.model.save({
                    Caption: this.$('[name="Caption"]').val(),
                    RelationFieldId: this.$('[name="RelationFieldId"]').val(),
                    RelationFormType: this.$('[name="RelationFormType"]').val(),
                    OrderNum: this.$('[name="OrderNum"]').val(),
                    FieldId: this.$('[name="FieldId"]').val(),
                    FormFieldId: this.FormFieldId,
                    IsOrdered: this.$('[name="IsOrdered"]').prop('checked') ? 1 : 0,
                    IsOrderedDesc: this.$('[name="IsOrderedDesc"]').prop('checked') ? 1 : 0,
                    IsPrimary: this.$('[name="IsPrimary"]').prop('checked') ? 1 : 0,
                    IsHidden: this.$('[name="IsHidden"]').prop('checked') ? 1 : 0,
                    IsReadonly: this.$('[name="IsReadonly"]').prop('checked') ? 1 : 0,
                    LookupFormId: this.$('[name="LookupFormId"]').val()
                }, {error: _.bind(this.requestError, this), success: _.bind(this.successAdd, this)});
            }

        },

        successAdd: function (model, response) {
            this.hideAjax();
            this.trigger('refreshTabs', 'Form_Fields', [this.FormId]);

            if (this.new) {
                this.trigger('open', 'Form_Field', [this.FormId, response.FormFieldId]);
                this.sendMessage('Поле создано');
                this.remove();
            }
            else {
                this.sendMessage('Изменения сохранены');
            }

        }


    });
    return View_Form_Field;
});