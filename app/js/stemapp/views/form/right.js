define([
    '_app/views/templates/tab',
    '_app/views/control/table',
    'tmpl',
    '_app/models/formrights',
    '_app/models/role',
    'smarty!_app/tmpl/form/right/form.tpl',
    '_app/data/filter'
], function (Template_Tab, Control_Table, tmpl, Model_Form_Rights, Model_Role, Template_Form) {
    'use strict';

    /**
     * @class View_Form_Field
     * @constructor
     * @extends Template_Tab
     */
    var View_Form_Field = Template_Tab.extend({

        tagName: 'div',

        /**
         * @type {Model_Form_Rights}
         */
        model: null,

        /**
         * @type {Model_Role}
         */
        modelRole: null,

        template: Template_Form,

        name: 'form_right',

        caption: 'Право на форму',

        icon : 'funnel_pencil',

        url: 'form_rights/{$frame}{if $new}/{$new}{else}/{$id}{/if}',

        autoToolbar: false,

        single: true,

        notRender: true,

        filter: null,

        dataName: 'filter',
        grid: {
            type: 'tree',
            enable: true,
            counter: false,
            parentHeight: 150
        },

        operations: [
            {OperationId: '=', OperationName: '='},
            {OperationId: '>', OperationName: '>'},
            {OperationId: '<', OperationName: '<'},
            {OperationId: '>=', OperationName: '>='},
            {OperationId: '<=', OperationName: '<='},
            {OperationId: 'like', OperationName: 'like'},
            {OperationId: 'ulike', OperationName: 'ulike'},
            {OperationId: 'in', OperationName: 'in'}
        ],

        uid: 1,

        initialize: function (opts) {

            Template_Tab.prototype.initialize.apply(this, [opts]);

            this.FormId = opts.arguments[0];
            this.FormRightId = opts.arguments[1];
            this.new = opts.arguments[1] == 'new';
            if (opts.arguments[2]) {
                this.FieldId = opts.arguments[2];
            }
            this.urlData = {frame: this.FormId, id: this.FormRightId, 'new': this.new ? 'new' : ''};
            this.table = new Control_Table(this.dataName, this.grid);
            this.model = new Model_Form_Rights({FormId: this.FormId, 'new': this.new});
            this.modelRole = new Model_Role();
            if (this.new) {
                this.setCaption('Новое поле формы');
            }
            this.initModel();

            this.load();

        },

        events: {

            "submit form": "submit",
            "click .js-form": "chooseForm",
            "click .js-add-record": "addRecord",
            "click .js-field": "chooseField",
            "click .js-delete-record": "deleteRecord"
        },

        render: function () {

            this.$el.empty();

            if (!this.notRender) {
                this.$el.append(tmpl(this.template, this.tmplData));
            }
            this.$('.js-filter-table').append(this.table.render().$el);

            this.afterRender();
            return this;
        },

        load: function () {
            this.modelRole.loadMetaRoles({success: _.bind(this.successRoleLoad, this)});
        },

        successRoleLoad: function (data) {
            this.roles = data;
            if (this.FormRightId && this.FormRightId !== 'new') {
                this.showGlobalAjax();
                this.model.getById({data: {FormRightId: this.FormRightId}, success: _.bind(this.apply, this)});
            } else {
                this.apply();
            }
        },

        chooseForm: function (e) {
            e.preventDefault();
            this.trigger('openModal', 'Forms', [], {
                caption: 'Выберите форму',
                multi: false,
                fieldId: 'FormId',
                value: this.FormId,
                callback: _.bind(this.successChooseForm, this)
            });
        },

        checkEnableType: function () {
            if (this.filter) {
                this.$('.js-disable-type').hide();
                this.$('.js-enable-type').show();
            } else {
                this.$('.js-disable-type').show();
                this.$('.js-enable-type').hide();
            }
        },

        successChooseForm: function (data) {
            this.$('[name="FormName"]').val(data.Name);
            this.$('[name="FormId"]').val(data.FormId);
            this.FormId = data.FormId;
        },

        chooseField: function (e) {
            e.preventDefault();
            this.trigger('openModal', 'Form_Fields', {arguments: [this.FormId]}, {
                caption: 'Выберите поле',
                multi: false,
                fieldId: 'FieldId',
                value: null,
                callback: _.bind(this.successFieldForm, this)
            });
        },

        successFieldForm: function (data) {
            this.$('[name="FieldName"]').val(data.Name);
            this.$('[name="FieldId"]').val(data.Name);
        },

        addRecord: function (e) {
            e.preventDefault();
            var item = this.filter;
            if (this.table.init) {
                var cell = this.table.getActiveCell();
                if (cell) {
                    item = this.searchItem(this.filter, this.table.getItem(cell.row));
                }
            }
            var filter = {};
            var type;
            filter.f = this.$('[name="FieldId"]').val();
            filter.o = this.$('.js-OperationId').val();
            filter.v = this.$('.js-Value').val();
            filter.FilterId = this.uid++;


            if (this.filter) {
                type = this.$('[name="Type"]:checked').val() ? this.$('[name="Type"]:checked').val() : 'or';
                if (type == 'or') {
                    if (!item.or) {
                        item.or = [];
                    }
                    item.or.push(filter);
                } else {
                    if (!item.and) {
                        item.and = [];
                    }
                    item.and.push(filter);
                }
            } else {
                this.filter = filter;
            }
            this.checkEnableType();
            this.updateFilter();
            this.updateTable();

        },

        deleteRecord: function (e) {
            e.preventDefault();
            var cell = this.table.getActiveCell();
            if (cell) {
                if (this.searchItem(this.filter, this.table.getItem(cell.row), true)) {
                    this.filter = null;
                }
                this.checkEnableType();
                this.updateFilter();
                this.updateTable();
            } else {
                this.sendMessage('Выберите строку в таблице');
            }
        },


        searchItem: function (filter, item, is_del) {
            is_del = !!is_del;
            var res;
            var i;
            if (!filter.FilterId) {
                filter.FilterId = this.uid++;
            }
            if (filter.FilterId == item.FilterId) {
                return filter;
            }
            if (filter.or) {
                for (i = 0; i < filter.or.length; i++) {
                    res = this.searchItem(filter.or[i], item);
                    if (res) {
                        if (is_del) {
                            filter.or[i] = null;
                            filter.or.splice(i, 1);
                            return false;
                        }
                        return res;
                    }
                }
            }
            if (filter.and) {
                for (i = 0; i < filter.and.length; i++) {
                    res = this.searchItem(filter.and[i], item);
                    if (res) {
                        if (is_del) {
                            filter.and[i] = null;
                            filter.and.splice(i, 1);
                            return false;
                        }
                        return res;
                    }
                }
            }
            return false;
        },

        updateFilter: function () {
            if (this.filter) {
                this.$('.js-Filter').val(JSON.stringify(this.filter));
            } else {
                this.$('.js-Filter').val('');
            }
        },

        updateTable: function () {

            var results = [];
            var level = 1;
            if (this.filter) {
                results.push($.extend({}, this.filter, {
                    or: null,
                    and: null,
                    level: level,
                    type: '',
                    ccount: (this.filter.or || this.filter.and) ? 1 : 0
                }));
                if (this.filter.or) {
                    results = results.concat(this.filterToTable(this.filter.or, level + 1, 'or', this.filter.FilterId));
                }
                if (this.filter.and) {
                    results = results.concat(this.filterToTable(this.filter.and, level + 1, 'and', this.filter.FilterId));
                }
            }
            this.table.update(results);
        },

        filterToTable: function (items, level, type, parent) {
            var results = [];
            for (var i = 0; i < items.length; i++) {
                results.push($.extend({}, items[i], {
                    or: null,
                    and: null,
                    parent: parent,
                    level: level,
                    type: type,
                    ccount: (items[i].or || items[i].and) ? 1 : 0
                }));
                if (items[i].or) {
                    results = results.concat(this.filterToTable(items[i].or, level + 1, 'or', items[i].FilterId));
                }
                if (items[i].and) {
                    results = results.concat(this.filterToTable(items[i].and, level + 1, 'and', items[i].FilterId));
                }
            }
            return results;
        },

        apply: function (response) {

            this.data = response;

            if (this.data && this.data.Filter) {
                this.filter = JSON.parse(this.data.Filter);
                this.searchItem(this.filter, {});// Create uni identity
            }

            if (this.new) {
                this.tmplData = {
                    id: this.FormRightId,
                    item: this.data,
                    roles: this.roles,
                    operations: this.operations
                };
            } else {
                this.tmplData = {
                    item: this.data,
                    fields: this.fields,
                    id: this.FormRightId,
                    roles: this.roles,
                    operations: this.operations
                };
                this.setCaption('Право на форму ' + this.data.FormRightId);
            }
            this.notRender = false;

            this.render();
            this.hideAjax();
            this.checkEnableType();
            this.updateTable();
        },


        submit: function (e) {

            e.preventDefault();

            this.showAjax(this.$('.js-submit'));
            this.tabAjax();

            if (this.new) {
                this.model.save({
                    FormId: this.$('[name="FormId"]').val(),
                    RoleId: this.$('[name="RoleId"]').val(),
                    Filter: this.$('[name="Filter"]').val(),
                    IsCreate: this.$('[name="IsCreate"]').prop('checked') ? 1 : 0,
                    IsRead: this.$('[name="IsRead"]').prop('checked') ? 1 : 0,
                    IsUpdate: this.$('[name="IsUpdate"]').prop('checked') ? 1 : 0,
                    IsDelete: this.$('[name="IsDelete"]').prop('checked') ? 1 : 0
                }, {error: _.bind(this.requestError, this), success: _.bind(this.successAdd, this)});
            } else {
                this.model.save({
                    FormRightId: this.FormRightId,
                    FormId: this.$('[name="FormId"]').val(),
                    RoleId: this.$('[name="RoleId"]').val(),
                    Filter: this.$('[name="Filter"]').val(),
                    IsCreate: this.$('[name="IsCreate"]').prop('checked') ? 1 : 0,
                    IsRead: this.$('[name="IsRead"]').prop('checked') ? 1 : 0,
                    IsUpdate: this.$('[name="IsUpdate"]').prop('checked') ? 1 : 0,
                    IsDelete: this.$('[name="IsDelete"]').prop('checked') ? 1 : 0
                }, {error: _.bind(this.requestError, this), success: _.bind(this.successAdd, this)});
            }

        },

        successAdd: function (model, response) {
            this.hideAjax();
            this.trigger('refreshTabs', 'Form_Rights', [this.FormId]);

            if (this.new) {
                //this.trigger('open', 'Form_Right', [this.FormId, response.FormRightId]);
                this.sendMessage('Права добавлены');
                this.remove();
            }
            else {
                this.sendMessage('Изменения сохранены');
            }

        }


    });
    return View_Form_Field;
});