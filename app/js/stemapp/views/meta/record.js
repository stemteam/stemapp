define([
    '_app/views/templates/tab',
    '_app/views/control/table',
    '_app/views/control/map',
    '_app/views/control/marker',
    '_app/models/meta',
    '_app/models/form',
    '_app/models/smartform',
    '_app/models/image',
    'smarty!_app/tmpl/meta/auto.tpl',
    'smarty!_app/tmpl/meta/form.tpl',
    'smarty!_app/tmpl/meta/frame.tpl',
    'smarty!_app/tmpl/meta/main.tpl',
    'tmpl',
    '_app/data/list'
], function (Template_Tab, Control_Table, Control_Map, Control_Marker, Model_Meta, Model_Form, Model_SmartForm, Model_Image, Template_Auto, Template_Form, Template_Frame, Template_Main, tmpl) {
    'use strict';

    /**
     * @class View_Record
     * @constructor
     * @extends Template_Tab
     */
    var View_Record = Template_Tab.extend({

        tagName: 'div',

        name: 'record',

        caption_add: 'Добавление записи',

        caption_edit: 'Редактирование записи',

        template: Template_Frame,

        template_block: Template_Form,

        template_auto: Template_Auto,

        url: 'records/{$name}/form/{if $new}new{else}{if $id}{$id}{/if}{/if}',

        notRender: true,

        data: {}, // Хранит глобальные переменные, которые передаются в шаблонизатор

        fields: {}, // Хранит все наборы полей по вызванным функциям

        html: Template_Main,

        colind: 0,

        namespace: 'auto',

        /**
         * @type {Model_Form}
         */
        model: null,

        /**
         * @type {Model_Meta}
         */
        modelMeta: null,

        /**
         * @type {Model_SmartForm}
         */
        modelSmart: null,

        /**
         * @type {Model_Image}
         */
        modelImage: null,

        /**
         * хранится главное поле формы(главный идентификатор)
         */
        primaryField: null,

        grid: {
            enable: true,
            parentHeight: 100,
            checkbox: false,
            type: 'list'
        },

        yamap: false,

        initialize: function (opts) {
            this.FormName = opts.arguments[0];
            this.data = {};
            this.showGlobalAjax();
            this.options = opts.options ? opts.options : {};
            this.data.id = opts.arguments[1];
            this.new = this.data.id == 'new';
            this.urlData = {name: this.FormName, id: this.data.id};
            this.model = new Model_Form();
            this.modelSmart = new Model_SmartForm();
            this.modelMeta = new Model_Meta();
            this.modelImage = new Model_Image();
            Template_Tab.prototype.initialize.apply(this, [opts]);

            this.load();
        },

        events: function () {
            return _.extend({},
                Template_Tab.prototype.events, {

                    "submit form": "submit",

                    "click .na-toggle-genblock": "toggleBlock",

                    "change .js-checkbox": "changeCheckbox",

                    "change .js-dateinput": "changeDateInput",

                    "click .js-submit": "submit",

                    "click .js-select": "clickSelect",

                    "click .js-delete-record": "deleteTableRecord",

                    "click .js-add-record": "addTableRecord",

                    "click .js-show-external-table": "showExternalTable",

                    "change input[type='file']": "uploadFile",

                    "click .js-upload-file": "chooseFile",

                    "click .js-delete-file": "deleteFile"
                });
        },

        load: function () {
            this.showAjax();
            this.count_parser_complete = 0;
            if (_.isNumber(this.FormName)) {
                this.model.getById({data: {FormId: this.FormName}, success: _.bind(this.apply, this)});
            } else {
                this.model.getById({data: {Name: this.FormName}, success: _.bind(this.apply, this)});
            }
        },

        apply: function (response) {
            this.FormData = response;
            this.setCaption(this.FormData.Description || ((this.new ? this.caption_add : this.caption_edit) + " в " + this.FormName));

            this.modelSmart.SmartFormFieldListGet({
                data: {FormId: this.FormData.FormId},
                success: _.bind(this.successFieldGet, this)
            });
        },

        successFieldGet: function (data) {
            this.fields = data;
            var i;
            for (i = 0; i < this.fields.length; i++) {
                if (this.fields[i].IsPrimary) {
                    this.primaryField = this.fields[i];
                }
                if (!this.fields[i].Presentation) {
                    this.fields[i].Presentation = this.fields[i].Datatype;
                }

            }

            if (this.options.relative) {
                for (i = 0; i < this.fields.length; i++) {
                    if (this.fields[i].FormFieldId == this.options.relative.RelationField.RelationFieldId) {

                        this.fields[i].IsReadonly = true;
                        this.fields[i].Presentation = 'TextId';
                        this.fields[i].Value = this.options.relative.Id;
                        this.data[this.fields[i].Name] = this.options.relative.Id;
                    }
                }
            }

            var Filter;
            if (this.new) {
                this.successLoad();
            } else {
                if (this.data.id) {
                    Filter = {f: this.primaryField.Name, v: this.data.id, o: '='};
                }
                this.modelSmart.SmartFormListGet({
                    data: {FormId: this.FormData.FormId, Filter: Filter ? JSON.stringify(Filter) : Filter},
                    success: _.bind(this.successFormsGet, this)
                });
            }

        },

        successFormsGet: function (data) {
            data = (data && data.length) ? data[0] : [];
            this.data = $.extend(this.data, data);
            this.successLoad();
        },

        successLoad: function () {
            this.hideAjax();

            //Вся загрузка завершилась

            for (var i = 0; i < this.fields.length; i++) {
                this.fields[i].Data = this.data[this.fields[i].Name];

                if (this.fields[i].Presentation == 'Image' && this.data[this.fields[i].Name]) {
                    this.modelImage.getImage({
                        data: {Guid: this.data[this.fields[i].Name], Width: 300, Height: 300},
                        success: _.bind(this.successLoadImage, this, this.fields[i].Name, this.data[this.fields[i].Name])
                    });
                }
                if (this.fields[i].Presentation == 'Longitude') {
                    for (var j = 0; j < this.fields.length; j++) {
                        if (this.fields[j].FormId == this.fields[i].FormId && this.fields[j].Presentation == 'Latitude') {
                            this.fields[i].Latitude = this.fields[j].Name;
                        }
                    }
                    if (!this.yamap) {
                        this.yamap = $('<script>').appendTo($('body'));
                        this.yamap.on('load', _.bind(this.loadYaMap, this));
                        this.yamap.attr({
                            type: 'text/javascript',
                            src: 'https://api-maps.yandex.ru/2.1.15/?lang=ru_RU'
                        });
                    }
                }
            }

            this.notRender = false;
            this.render();
        },

        render: function () {

            this.$el.empty();
            if (this.notRender) {
                return this;
            }
            if (this.toolbar) {
                this.$el.append(this.toolbar.render().$el);
                this.$el.append('<div class="tollbar-sep"></div>');
            }

            this.hideGlobalAjax();
            this.hideAjax();

            this.$el.empty();
            var html;
            for (var i = 0; i < this.fields.length; i++) {

            }
            html = tmpl(this.template_block, {
                data: this.data,
                fields: this.fields,
                cid: this.cid,
                new: this.new
            });

            //html = tmpl(this.template, {meta_block: html, name: this.new ? 'Добавление записи' : 'Редактирование записи'}, true);

            html = tmpl(this.template_auto, {
                meta_block: html,
                caption: this.caption,
                name: this.new ? 'Добавление записи' : 'Редактирование записи'
            }, true);
            this.$el.html(tmpl(this.html, {
                auto: html
            }, true));

            this.afterRender();
            return this;
        },

        afterRender: function () {
            this.$('.js-columns').columns({selector: '.gen-block', minWidth: 1100});

            if (!this.$('.js-submit-block-button').length) {
                this.$('.js-submit-block').hide();
            }
            this.$('.js-dateinput').each(function () {
                $(this).customDateInput({
                    type: $(this).data('format'),
                    defaultTime: '00:00:00',
                    date: $(this).val(),
                    datepicker: true
                });
            });

            // Обрабатываем мультиселект
            for (var i = 0; i < this.fields.length; i++) {
                if (this.fields[i].RelationFormId && this.fields[i].RelationFormType == 2) {

                    if (!this.fields[i]._table) {
                        this.fields[i]._table = new Control_Table('list', this.grid);
                    }
                    this.$('.js-' + this.fields[i].Name).append(this.fields[i]._table.render().$el);
                    var columns = [];
                    for (var j = 0; j < this.fields[i].RelationFormMetaData.length; j++) {
                        if (!this.fields[i].RelationFormMetaData[j].IsHidden) {
                            columns.push(this.fields[i].RelationFormMetaData[j]);
                        }
                    }
                    this.fields[i]._table.update(this.fields[i].Data, {
                        columns: columns
                    });
                    this.updateMultiSelect(this.fields[i]);
                }
            }
        },

        loadYaMap: function (e) {
            for (var i = 0; i < this.fields.length; i++) {
                if (this.fields[i].Presentation == 'Longitude') {
                    this.fields[i].Map = new Control_Map();
                    var lon = this.data[this.fields[i].Name] ? this.data[this.fields[i].Name] : 37.37;
                    var lat = this.data[this.fields[i].Latitude] ? this.data[this.fields[i].Latitude] : 55.45;
                    this.fields[i].Map.init({id: this.cid + '-' + this.fields[i].Name + '-map', center: [lat, lon]});
                    this.fields[i].Marker = new Control_Marker([lat, lon], {
                        preset: 'app#default',
                        draggable: true
                    });
                    this.fields[i].Marker.addEventDragend(_.bind(this.dragEndMarker, this, this.fields[i]));
                    this.fields[i].Map.addMarker(this.fields[i].Marker);

                }
            }
        },

        dragEndMarker: function (field) {
            var coord = field.Marker.getCoordinates();
            this.$('[name="' + field.Name + '"]').val(coord[1]);
            this.$('[name="' + field.Latitude + '"]').val(coord[0]);

        },

        submit: function (e) {
            e.preventDefault();
            this.sendForm(this.$('form'));
        },

        sendForm: function (form) {
            this.showGlobalAjax();
            var $form = $(form);

            var model = new Model_SmartForm({new: this.new});
            var data = {FormId: this.FormData.FormId};

            data = $.extend(data, $form.serializeObject());

            model.save(data, {
                error: _.bind(this.errorSubmit, this),
                success: _.bind(this.successSubmit, this)
            });
        },

        errorSubmit: function (model, xhr) {
            this.hideGlobalAjax();
            this.model.requestError(xhr);
        },

        successMultiSubmit: function () {
            this.sendMessage('Информация мультиселекта сохранена');
            this.hideGlobalAjax();
        },

        successSubmit: function () {
            if (this.new) {
                this.sendMessage('Запись создана');
                this.remove();
            } else {
                this.sendMessage('Информация сохранена');
            }
            if (this.options.dependForm && this.options.dependForm.hash) {
                this.trigger('refreshHashTabs', this.options.dependForm.hash);
            }

            this.hideGlobalAjax();
        },

        changeCheckbox: function (e) {
            $(e.target).closest('.js-checkbox-block').find('input[type="hidden"]').val($(e.target).prop('checked') ? 1 : 0);
        },

        changeDateInput: function (e) {
            var date = $(e.target).niGetValue();
            if (date instanceof Date) {
                $(e.target).val(date.getTime());
            }
        },

        getFieldByName: function (name) {
            for (var i = 0; i < this.fields.length; i++) {
                if (this.fields[i].Name == name) {
                    return this.fields[i];
                }
            }
        },

        clickSelect: function (e) {
            e.preventDefault();
            var fieldName = $(e.target).data('name');
            var i;
            var field = this.getFieldByName(fieldName);

            var fieldId;
            for (i = 0; i < field.RelationFormMetaData.length; i++) {
                if (field.RelationFormMetaData[i].FormFieldId == field.RelationFieldId) {
                    fieldId = field.RelationFormMetaData[i];
                    break;
                }
            }
            var data = null;
            if (this.data[field.Name] && this.data[field.Name].length) {
                data = this.data[field.Name][0][fieldId.Name];
            }

            this.trigger('openModal', 'meta/records', {arguments: [field.RelationFormId], relative: this.options.relative}, {
                caption: 'Выберите запись',
                multi: false,
                value: data,
                fieldId: field.RelationFieldId,
                callback: _.bind(this.successSelect, this, field)
            });
        },

        successSelect: function (field, data) {
            var id;
            var caption = '';
            for (var i = 0; i < field.RelationFormMetaData.length; i++) {
                if (field.RelationFormMetaData[i].IsId) {
                    id = data[field.RelationFormMetaData[i].Name];
                }
                if (field.RelationFormMetaData[i].IsCaption) {
                    caption += data[field.RelationFormMetaData[i].Name] + ' ';
                }
            }
            this.$('[Name="' + field.Name + '"]').val(id);
            this.$('.js-' + field.Name + 'Name').val(caption);
            this.data[field.Name] = id;
        },

        deleteTableRecord: function (e) {
            e.preventDefault();
            var fieldName = $(e.target).data('name');
            var field = this.getFieldByName(fieldName);
            var item = field._table.getActiveItem();
            if (item) {
                field._table.deleteItem(item);
            }
            this.updateMultiSelect(field);
        },

        getFieldId: function (field) {
            for (var i = 0; i < field.RelationFormMetaData.length; i++) {
                if (field.RelationFormMetaData[i].IsPrimary) {
                    return field.RelationFormMetaData[i];
                }
            }
            return '';
        },

        getTableIds: function (field, fieldName) {
            var items = field._table.getItems();
            var ids = [];
            for (var i = 0; i < items.length; i++) {
                ids.push(items[i][fieldName]);
            }
            return ids;
        },

        addTableRecord: function (e) {
            e.preventDefault();
            return;
            var fieldName = $(e.target).data('name');
            var field = this.getFieldByName(fieldName);

            var fieldId = this.getFieldId(field);
            if (!fieldId) {
                this.sendMessage('Не задан ключевой идентификатор');
                return;
            }

            this.trigger('openModal', 'meta/records', {arguments: [field.LookupFormId], relative: this.options.relative}, {
                caption: 'Выберите записи',
                multi: true,
                fieldId: fieldId.FieldName,
                value: this.getTableIds(field, fieldId.Name),
                callback: _.bind(this.successMultiselect, this, field)
            });
        },

        successMultiselect: function (field, data) {
            field._table.update(data);
            this.updateMultiSelect(field);
        },

        updateMultiSelect: function (field) {

            var fieldId = this.getFieldId(field);
            var ids = this.getTableIds(field, fieldId.Name);
            this.$('[name="' + field.Name + '"]').val(ids.join(','));
        },

        showExternalTable: function (e) {
            e.preventDefault();
            var field_name = $(e.target).data('field');
            for (var i = 0; i < this.fields.length; i++) {
                if (this.fields[i].Name == field_name) {
                    this.trigger('open', 'meta/records', {
                        arguments: [this.fields[i].RelationFormId],
                        relative: {
                            Id: this.data[this.primaryField.Name],
                            FieldName: this.primaryField.FieldName,
                            RelationField: $.extend(this.fields[i], {_table: null})
                        }
                    });
                    break;
                }
            }
        },

        chooseFile: function (e) {
            e.preventDefault();
            this.$('#' + $(e.target).data('name')).click();
        },

        uploadFile: function (e) {
            e.preventDefault();
            this.$('.js-image-preview').addClass('ajax-preloader');
            this.uploading = true;
            this.modelImage.fileupload(e.target, _.bind(this.successUploadFile, this, e.target));
        },

        successUploadFile: function (input, data) {
            this.modelImage.getImage({
                data: {Guid: data.ImageGuid, Width: 300, Height: 300},
                success: _.bind(this.successLoadImage, this, $(input).data('id'), data.ImageGuid)
            });
        },

        successLoadImage: function (field_id, ImageGuid, data) {
            this.$('.js-image-preview').removeClass('ajax-preloader');
            this.$('.js-preview-' + field_id).attr('src', data.Url);
            this.$('input[name="' + field_id + '"]').val(ImageGuid);
            if (this.uploading) {
                this.sendMessage('Изображение загружено, чтобы применить изменения, нужно сохранить профиль');
                this.uploading = false;
            }
        },

        deleteFile: function (e) {
            e.preventDefault();
            var field_id = $(e.target).data('name');
            this.$('.js-preview-' + field_id).attr('src', '/app/img/empty.png');
            this.$('input[name="' + field_id + '"]').val('');
            this.sendMessage('Чтобы применить изменения, нужно сохранить профиль');
        }
    });
    return View_Record;
});