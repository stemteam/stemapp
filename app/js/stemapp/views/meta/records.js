define([
    '_app/views/templates/tab',
    '_app/views/control/table',
    '_app/views/control/paginator',
    '_app/models/meta',
    '_app/models/form',
    '_app/models/smartform',
    'tmpl',
    '_app/data/list',
], function (Template_Tab, Control_Table, Control_Paginator, Model_Meta, Model_Form, Model_SmartForm, tmpl) {
    'use strict';

    /**
     * @class View_Records
     * @constructor
     * @extends Template_Tab
     */
    var View_Records = Template_Tab.extend({

        tagName: 'div',

        name: 'records',

        icon: 'blog',

        caption: '',

        tabButton: null,

        server: null,

        dataName: 'list',

        url: 'records/{$name}/grid{if $id}/{$id}{/if}',

        toolbar: true,

        limit: 100,

        /**
         * @type {Model_Form}
         */
        model: null,

        /**
         * @type {Model_Meta}
         */
        modelMeta: null,

        /**
         * @type {Model_SmartForm}
         */
        modelSmart: null,

        /**
         * @type {Control_Table}
         */
        table: null,

        grid: {
            enable: true,

            counter: true,

            checkbox: true,

            type: 'list',

            parentHeight: true
        },

        notRender: true,

        primaryField: null, // Первичное поле

        captionFields: [], //Поле заголовков

        form: {},

        page: 1,

        tableInit: false,

        sort: {text: '', columns: []},

        filter_val: '',

        filter_columns: null,

        options: {},

        initialize: function (opts) {

            this.FormName = opts.arguments[0];
            this.RecordId = opts.arguments[1];
            this.grid.onSort = _.bind(this.onSort, this);
            var date = new Date();
            this.default_start_date = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
            this.default_stop_date = new Date((new Date(date.getFullYear(), date.getMonth(), date.getDate())).getTime() + 1000 * 60 * 60 * 24 * 3);

            this.urlData = {name: this.FormName, id: this.RecordId};
            if (this.RecordId) {
                this.data.id = this.RecordId;
            }
            this.model = new Model_Form();
            this.model.bind('apply', this.apply, this);
            this.modelMeta = new Model_Meta();
            this.modelSmart = new Model_SmartForm();
            Template_Tab.prototype.initialize.apply(this, [opts]);

            this.options = opts.options;

            this.paginator = new Control_Paginator({limit: this.limit, onChange: _.bind(this.changePage, this)});
            this.initModel();
            this.initModel(this.modelMeta);
            this.initModel(this.modelSmart);
            this.preload();
            this.grid.rowChanged = _.bind(this.rowChanged, this);
            this.table = new Control_Table(this.dataName, this.grid);
            //  this.table.$el.bind('updateHeaderSearch', _.bind(this.updateHeaderSearch, this));
            this.applySearch = _.debounce(_.bind(this.applySearch, this), 300);

        },

        events: function () {

            return _.extend({},
                Template_Tab.prototype.events, {

                    "keyup .toolbar input.search": "search",

                    "click .toolbar a.record-create": "createRecord",

                    "click .toolbar a.record-edit": "editRecord",

                    "click .toolbar a.record-delete": "deleteRecord",
                    "keypress .toolbar .js-last-name": "searchByName"
                })
        },
        preload: function () {
            var attr = {};
            if ($.isNumeric(this.FormName)) {
                attr = {FormId: this.FormName};
            } else {
                attr = {Name: this.FormName};
            }

            this.model.getById({data: attr, success: _.bind(this.apply, this)});
            if (!this.caption_set) {
                this.setCaption(this.FormName);
            }
        },
        search: function (e) {
            var val = $(e.currentTarget).val();
            this.applySearch(val);
        },

        applySearch: function (val) { // оборачивается в debounce в 'initialize'
            if (this.filter_val != val) {
                this.filter_val = val;
                this.refresh();
            }
        },

        searchByName: function (e) {
            if (e.keyCode == 13) {
                this.refresh();
                e.preventDefault();
                e.stopPropagation();
            }
        },

        apply: function (data) {
            if (data) {
                this.form = data;
            }
            var self = this;
            var find = false;
            // Загрузка расширений
            for (var i in Stemapp.modules) {
                if (Stemapp.modules.hasOwnProperty(i)) {
                    if (Stemapp.modules[i].extensions) {
                        for (var j = 0; j < Stemapp.modules[i].extensions.length; j++) {
                            if (Stemapp.modules[i].extensions[j] == this.form.Name) {
                                find = true;
                                var name = Stemapp.modules[i].name + '/js/extensions/' + Stemapp.modules[i].extensions[j];
                                require([name + '/init'], function (Extension) {
                                    if (Extension) {
                                        self.loadExtension(Extension);
                                    } else {
                                        self.loadFormField();
                                    }
                                });
                            }
                        }
                    }
                }
            }
            if (!find) {
                self.loadFormField();
            }
            this.setCaption(this.form.ListDescription || this.form.Name);

        },

        load: function () {
            this.loadFormField();
        },

        loadFormField: function () {
            this.modelSmart.SmartFormFieldListGet({
                data: {FormId: this.form.FormId},
                success: _.bind(this.successFieldGet, this)
            });
        },

        loadExtension: function (extension) {
            this.extension = extension;
            this.createToolbar();
            this.loadFormField();
        },

        onSort: function (e, args) {
            this.sort = {text: '', columns: []};
            var sort = [];
            if (args.sortCols) {
                for (var i = 0; i < args.sortCols.length; i++) {
                    var id = /f\d+_(\d+)_/.exec(args.sortCols[i].sortCol.field);
                    sort.push(id[1] + '-' + (args.sortCols[i].sortAsc ? 'ASC' : 'DESC'));
                    if (id && id[1]) {
                        this.sort.columns.push({columnId: args.sortCols[i].sortCol.id, sortAsc: args.sortCols[i].sortAsc});
                    }
                }
                this.sort.text = sort.join(';');
            }
            this.refresh();
        },

        refresh: function () {
            this.showAjax(this.$('.toolbar a.refresh'));
            this.tabAjax();
            this.showGlobalAjax();
            this.load();
            return false;
        },

        successFieldGet: function (data) {
            var i;
            this.fields = data;
            for (i = 0; i < this.fields.length; i++) {
                if (this.fields[i].IsPrimary) {
                    this.primaryField = this.fields[i];
                }
                if (this.fields[i].IsCaption) {
                    this.captionFields.push(this.fields[i]);
                }
                if (this.options && this.options.relative) {
                    if (this.fields[i].FieldName == this.options.relative.FieldName) {
                        if (!this.filter_columns) {
                            this.filter_columns = {};
                        }
                        this.filter_columns[this.fields[i].Name] = this.options.relative.Id;
                    }

                }
            }
            var Filter = null;
            //if (this.form.ParentFieldName == '@') {
            //    if (Stemapp.global && Stemapp.global[this.form.FieldName]) {
            //        for (i = 0; i < this.fields.length; i++) {
            //            if (this.fields[i].FieldName == this.form.FieldName) {
            //                Filter = {f: this.fields[i].Name, o: '=', v: Stemapp.global[this.form.FieldName]};
            //                break;
            //            }
            //        }
            //        if (!Filter.f) {
            //            this.sendError('В полях формы нет поля "' + this.form.FieldName + '"');
            //        }
            //
            //    } else {
            //        this.sendError('Невозможно загрузить форму, нет значения "' + this.form.FieldName + '" у текущего пользователя');
            //        this.hideAjax();
            //        return;
            //    }
            //}

            if (this.FormName == 'expert_job' || this.FormName == 'expert_job_clinic' || this.FormName == 101) {//todo хардкор

                var f_start = '';
                var f_end = '';
                var f_name = '';
                if (this.FormName == 'expert_job' || this.FormName == 101) {
                    f_start = 'f104_85_begin_time';
                    f_end = 'f104_94_end_time';
                }
                if (this.FormName == 'expert_job_clinic') {
                    f_start = 'f330_881_begin_time';
                    f_end = 'f330_882_end_time';
                    f_name = 'f333_893_last_name';
                }
                var start_date = this.$('.js-start-date').length ? new Date(this.$('.js-start-date').niGetValue()) : this.default_start_date;
                var stop_date = this.$('.js-stop-date').length ? new Date(this.$('.js-stop-date').niGetValue()) : this.default_stop_date;
                if (this.FormName == 'expert_job_clinic') {
                    var last_name = this.$('input.js-last-name').val();
                }

                if (!Filter) {
                    Filter = {f: f_start, o: '>=', v: start_date.getTime(), and: []};
                } else {
                    Filter.and = [];
                    Filter.and.push({f: f_start, o: '>=', v: start_date.getTime()});
                }
                Filter.and.push({f: f_end, o: '<', v: stop_date.getTime() + 1000 * 60 * 60 * 24});
                if (this.FormName == 'expert_job_clinic') {
                    if (last_name && (last_name.length > 0)) {
                        Filter.and.push({f: f_name, o: 'like', v: '%' + last_name + '%'});
                    }
                }

            }
            // фильтр "Поиск по таблице"
            if (this.filter_val) {
                for (i = 0; i < this.fields.length; i++) {
                    if (this.fields[i].Datatype == 'String') {
                        if (Filter) {
                            if (!Filter.or) {
                                Filter.or = [];
                            }
                            Filter.or.push({f: this.fields[i].Name, o: 'like', v: '%' + this.filter_val + '%'});
                        } else {
                            Filter = {f: this.fields[i].Name, o: 'like', v: '%' + this.filter_val + '%'};
                        }
                    }
                }
            }
            // фильтры по колонкам


            if (this.filter_columns) {
                for (i in this.filter_columns) {
                    if (this.filter_columns.hasOwnProperty(i)) {
                        if (!Filter) {
                            Filter = {f: i, o: '=', v: this.filter_columns[i]};
                        } else {
                            if (!Filter.and) {
                                Filter.and = [];
                            }
                            Filter.and.push({f: i, o: '=', v: this.filter_columns[i]});
                        }
                    }
                }
            }
            this.Filter = Filter;

            this.loadData();
        },

        loadData: function () {
            this.modelSmart.SmartFormListGet({
                data: {
                    FormId: this.form.FormId,
                    Limit: this.limit,
                    Offset: (this.page - 1) * this.limit,
                    Filter: this.Filter ? JSON.stringify(this.Filter) : this.Filter,
                    Sort: this.sort.text
                },
                success: _.bind(this.successFormsGet, this)
            });
        },

        successFormsGet: function (data) {
            for (var i = 1; i <= data.length; i++) {
                data[i - 1]['counter'] = i + (this.page - 1) * this.limit;
            }
            this.data = data;
            this.successLoad();
        },

        successLoad: function () {
            this.hideAjax();
            if (this.notRender) {
                this.notRender = false;
                this.render();
            }
            if (this.table && this.tableInit) {
                this.table.reinit = true;
            }
            if (this.data && this.data[0]) {
                this.paginator.setCount(this.data[0]['count']);
            }
            this.tableInit = true;
            this.table.update(this.data, {
                columns: this.getVisibleFields()
            });
            if (this.sort.text) {
                this.table.setSortColumns(this.sort.columns);
            }
        },

        getVisibleFields: function () {
            var fields = [];
            for (var i = 0; i < this.fields.length; i++) {
                if (!this.fields[i].IsHidden && !this.fields[i].RelationFormId) {
                    fields.push(this.fields[i]);
                }
            }
            return fields;
        },

        createRecord: function (e) {
            e.preventDefault();
            this.trigger('open', 'Meta/Record', {
                arguments: [this.FormName, 'new'],
                relative: (this.options && this.options.relative) ? this.options.relative : null,
                dependForm: {hash: this.hash}
            });
            return false;
        },

        changePage: function (page) {
            this.showGlobalAjax();

            this.page = page;
            this.offset = this.limit * page;
            this.load();
        },

        editRecord: function (e) {

            e.preventDefault();
            var cell = this.table.getActiveCell(),
                item = this.table.getItem(cell.row);
            this.trigger('open', 'Meta/Record', {
                arguments: [this.FormName, item[this.primaryField.Name]],
                relative: (this.options && this.options.relative) ? this.options.relative : null,
                dependForm: {hash: this.hash}
            });
            return false;
        },

        deleteRecord: function (e) {
            e.preventDefault();
            var item = this.table.getActiveItem();

            var name = [];
            for (var i = 0; i < this.captionFields.length; i++) {
                name.push(item[this.captionFields[i].Name]);
            }
            if (name.length) {
                name = name.join(' ');
            } else {
                name = '';
            }
            this.trigger('dialog', {
                text: 'Удалить запись ' + name + '?',
                buttons: 'yesno',
                callback: _.bind(this.resultDeleteFrame, this, item)
            });


        },

        resultDeleteFrame: function (item, result) {

            if (result == 'yes') {
                var attr = {FormId: this.form.FormId};
                attr[this.primaryField.Name] = item[this.primaryField.Name];
                console.log(attr);

                this.modelSmart.delete(item, {data: attr, success: _.bind(this.successDeleteFrame, this)});
                item._delete = true;
                this.table.updateItem(item);

            }
        },

        successDeleteFrame: function () {
            this.load();
        },

        render: function () {

            this.$el.empty();
            if (this.notRender) {
                return this;
            }
            if (this.toolbar) {
                this.$el.append(this.toolbar.render().$el);
                this.$el.append('<div class="tollbar-sep"></div>');
                this.toolbar.afterInsert();
            }

            if (this.table) {
                this.$el.append(this.table.render().$el);
            } else {
                this.$el.append(tmpl(this.template, this.tmplData));
            }
            if (this.paginator) {
                this.$el.append('<div class="tollbar-sep"></div>');
                this.$el.append(this.paginator.render().$el);
            }
            this.afterRender();

            return this;
        },

        applyDelete: function (item) {

            this.table.deleteItem(item);
        },

        errorDelete: function (item) {

            item._delete = false;
            this.table.updateItem(item);
        },

        items: function () {

            var array = [];
            if (this.FormName == 'expert_job' || this.FormName == 'expert_job_clinic') { //todo хардкор
                array.push(
                    {
                        type: 'datepicker',
                        cls: 'js-start-date',
                        label: 'с: ',
                        maxValue: 'js-stop-date',
                        date: this.default_start_date
                    },
                    {
                        type: 'datepicker',
                        cls: 'js-stop-date',
                        label: 'по: ',
                        minValue: 'js-start-date',
                        date: this.default_stop_date
                    }, {
                        type: 'label',
                        text: 'фамилия:'
                    }, {
                        type: 'input',
                        cls: 'js-last-name'
                    });
            }
            array.push({
                    cls: 'record-create',
                    text: 'Новая запись',
                    icon: 'blog-create'
                },
                {
                    cls: 'record-edit',
                    text: 'Редактировать',
                    icon: 'blog-edit',
                    invalidate: _.bind(this.itemInvalidate, this)
                },
                {
                    cls: 'record-delete',
                    text: 'Удалить',
                    icon: 'blog-delete',
                    invalidate: _.bind(this.itemInvalidate, this)
                }
            );

            var items = this.getTableItems();
            for (var i in items) {
                if (items.hasOwnProperty(i)) {
                    if (items[i].cls == 'filter') {
                        items.splice(i, 1);
                        break;
                    }
                }
            }
            array = array.concat(items);

            return array;
        }

    });
    return View_Records;
});