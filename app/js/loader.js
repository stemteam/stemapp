(function ($) {


    if (!console.time) {
        console.time = function () {};
    }
    if (!console.timeEnd) {
        console.timeEnd = function () {};
    }
    if (!console.log) {
        console.log = function () {};
    }
    Stemapp.opts = $.extend(Stemapp.opts, {

        tmplPath: '/v' + Stemapp.opts.revision + '/tmpl/',

        geocoding: {

            "key": "c78ce099219746c08f77f19619511f6e"
        },

        cache: false
    });

    if (window.boot) {
        window.boot.load();
    }

})(jQuery);