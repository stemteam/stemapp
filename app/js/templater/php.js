var PHP_JS = function () {

    this.window = window;

    this.setlocale = function (category, locale) {

        // http://kevin.vanzonneveld.net
        // +   original by: Brett Zamir (http://brett-zamir.me)
        // +   derived from: Blues at http://hacks.bluesmoon.info/strftime/strftime.js
        // +   derived from: YUI Library: http://developer.yahoo.com/yui/docs/YAHOO.util.DateLocale.html
        // -    depends on: getenv
        // %          note 1: Is extensible, but currently only implements locales en,
        // %          note 1: en_US, en_GB, en_AU, fr, and fr_CA for LC_TIME only; C for LC_CTYPE;
        // %          note 1: C and en for LC_MONETARY/LC_NUMERIC; en for LC_COLLATE
        // %          note 2: Uses global: php_js to store locale info
        // %          note 3: Consider using http://demo.icu-project.org/icu-bin/locexp as basis for localization (as in i18n_loc_set_default())
        // *     example 1: setlocale('LC_ALL', 'en_US');
        // *     returns 1: 'en_US'
        var categ = '',
            cats = [],
            i = 0,
            d = this.window.document;

        // BEGIN STATIC
        var _copy = function _copy(orig) {
            if (orig instanceof RegExp) {
                return new RegExp(orig);
            } else if (orig instanceof Date) {
                return new Date(orig);
            }
            var newObj = {};
            for (var i in orig) {
                if (typeof orig[i] === 'object') {
                    newObj[i] = _copy(orig[i]);
                } else {
                    newObj[i] = orig[i];
                }
            }
            return newObj;
        };

        // Function usable by a ngettext implementation (apparently not an accessible part of setlocale(), but locale-specific)
        // See http://www.gnu.org/software/gettext/manual/gettext.html#Plural-forms though amended with others from
        // https://developer.mozilla.org/En/Localization_and_Plurals (new categories noted with "MDC" below, though
        // not sure of whether there is a convention for the relative order of these newer groups as far as ngettext)
        // The function name indicates the number of plural forms (nplural)
        // Need to look into http://cldr.unicode.org/ (maybe future JavaScript); Dojo has some functions (under new BSD),
        // including JSON conversions of LDML XML from CLDR: http://bugs.dojotoolkit.org/browser/dojo/trunk/cldr
        // and docs at http://api.dojotoolkit.org/jsdoc/HEAD/dojo.cldr
        var _nplurals1 = function (n) { // e.g., Japanese
            return 0;
        };
        var _nplurals2a = function (n) { // e.g., English
            return n !== 1 ? 1 : 0;
        };
        var _nplurals2b = function (n) { // e.g., French
            return n > 1 ? 1 : 0;
        };
        var _nplurals2c = function (n) { // e.g., Icelandic (MDC)
            return n % 10 === 1 && n % 100 !== 11 ? 0 : 1;
        };
        var _nplurals3a = function (n) { // e.g., Latvian (MDC has a different order from gettext)
            return n % 10 === 1 && n % 100 !== 11 ? 0 : n !== 0 ? 1 : 2;
        };
        var _nplurals3b = function (n) { // e.g., Scottish Gaelic
            return n === 1 ? 0 : n === 2 ? 1 : 2;
        };
        var _nplurals3c = function (n) { // e.g., Romanian
            return n === 1 ? 0 : (n === 0 || (n % 100 > 0 && n % 100 < 20)) ? 1 : 2;
        };
        var _nplurals3d = function (n) { // e.g., Lithuanian (MDC has a different order from gettext)
            return n % 10 === 1 && n % 100 !== 11 ? 0 : n % 10 >= 2 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2;
        };
        var _nplurals3e = function (n) { // e.g., Croatian
            return n % 10 === 1 && n % 100 !== 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2;
        };
        var _nplurals3f = function (n) { // e.g., Slovak
            return n === 1 ? 0 : n >= 2 && n <= 4 ? 1 : 2;
        };
        var _nplurals3g = function (n) { // e.g., Polish
            return n === 1 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2;
        };
        var _nplurals3h = function (n) { // e.g., Macedonian (MDC)
            return n % 10 === 1 ? 0 : n % 10 === 2 ? 1 : 2;
        };
        var _nplurals4a = function (n) { // e.g., Slovenian
            return n % 100 === 1 ? 0 : n % 100 === 2 ? 1 : n % 100 === 3 || n % 100 === 4 ? 2 : 3;
        };
        var _nplurals4b = function (n) { // e.g., Maltese (MDC)
            return n === 1 ? 0 : n === 0 || (n % 100 && n % 100 <= 10) ? 1 : n % 100 >= 11 && n % 100 <= 19 ? 2 : 3;
        };
        var _nplurals5 = function (n) { // e.g., Irish Gaeilge (MDC)
            return n === 1 ? 0 : n === 2 ? 1 : n >= 3 && n <= 6 ? 2 : n >= 7 && n <= 10 ? 3 : 4;
        };
        var _nplurals6 = function (n) { // e.g., Arabic (MDC) - Per MDC puts 0 as last group
            return n === 0 ? 5 : n === 1 ? 0 : n === 2 ? 1 : n % 100 >= 3 && n % 100 <= 10 ? 2 : n % 100 >= 11 && n % 100 <= 99 ? 3 : 4;
        };
        // END STATIC
        // BEGIN REDUNDANT
        this.php_js = this.php_js || {};

        var phpjs = this.php_js;

        // Reconcile Windows vs. *nix locale names?
        // Allow different priority orders of languages, esp. if implement gettext as in
        //     LANGUAGE env. var.? (e.g., show German if French is not available)
        if (!phpjs.locales) {
            // Can add to the locales
            phpjs.locales = {};

            phpjs.locales.en = {
                'LC_COLLATE':// For strcoll


                    function (str1, str2) { // Fix: This one taken from strcmp, but need for other locales; we don't use localeCompare since its locale is not settable
                        return (str1 == str2) ? 0 : ((str1 > str2) ? 1 : -1);
                    },
                'LC_CTYPE': { // Need to change any of these for English as opposed to C?
                    an: /^[A-Za-z\d]+$/g,
                    al: /^[A-Za-z]+$/g,
                    ct: /^[\u0000-\u001F\u007F]+$/g,
                    dg: /^[\d]+$/g,
                    gr: /^[\u0021-\u007E]+$/g,
                    lw: /^[a-z]+$/g,
                    pr: /^[\u0020-\u007E]+$/g,
                    pu: /^[\u0021-\u002F\u003A-\u0040\u005B-\u0060\u007B-\u007E]+$/g,
                    sp: /^[\f\n\r\t\v ]+$/g,
                    up: /^[A-Z]+$/g,
                    xd: /^[A-Fa-f\d]+$/g,
                    CODESET: 'UTF-8',
                    // Used by sql_regcase
                    lower: 'abcdefghijklmnopqrstuvwxyz',
                    upper: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                },
                'LC_TIME': { // Comments include nl_langinfo() constant equivalents and any changes from Blues' implementation
                    a: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                    // ABDAY_
                    A: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                    // DAY_
                    b: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    // ABMON_
                    B: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    N: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    // MON_
                    c: '%a %d %b %Y %r %Z',
                    // D_T_FMT // changed %T to %r per results
                    p: ['AM', 'PM'],
                    // AM_STR/PM_STR
                    P: ['am', 'pm'],
                    // Not available in nl_langinfo()
                    r: '%I:%M:%S %p',
                    // T_FMT_AMPM (Fixed for all locales)
                    x: '%m/%d/%Y',
                    // D_FMT // switched order of %m and %d; changed %y to %Y (C uses %y)
                    X: '%r',
                    // T_FMT // changed from %T to %r  (%T is default for C, not English US)
                    // Following are from nl_langinfo() or http://www.cptec.inpe.br/sx4/sx4man2/g1ab02e/strftime.4.html
                    alt_digits: '',
                    // e.g., ordinal
                    ERA: '',
                    ERA_YEAR: '',
                    ERA_D_T_FMT: '',
                    ERA_D_FMT: '',
                    ERA_T_FMT: ''
                },
                // Assuming distinction between numeric and monetary is thus:
                // See below for C locale
                'LC_MONETARY': { // Based on Windows "english" (English_United States.1252) locale
                    int_curr_symbol: 'USD',
                    currency_symbol: '$',
                    mon_decimal_point: '.',
                    mon_thousands_sep: ',',
                    mon_grouping: [3],
                    // use mon_thousands_sep; "" for no grouping; additional array members indicate successive group lengths after first group (e.g., if to be 1,23,456, could be [3, 2])
                    positive_sign: '',
                    negative_sign: '-',
                    int_frac_digits: 2,
                    // Fractional digits only for money defaults?
                    frac_digits: 2,
                    p_cs_precedes: 1,
                    // positive currency symbol follows value = 0; precedes value = 1
                    p_sep_by_space: 0,
                    // 0: no space between curr. symbol and value; 1: space sep. them unless symb. and sign are adjacent then space sep. them from value; 2: space sep. sign and value unless symb. and sign are adjacent then space separates
                    n_cs_precedes: 1,
                    // see p_cs_precedes
                    n_sep_by_space: 0,
                    // see p_sep_by_space
                    p_sign_posn: 3,
                    // 0: parentheses surround quantity and curr. symbol; 1: sign precedes them; 2: sign follows them; 3: sign immed. precedes curr. symbol; 4: sign immed. succeeds curr. symbol
                    n_sign_posn: 0 // see p_sign_posn
                },
                'LC_NUMERIC': { // Based on Windows "english" (English_United States.1252) locale
                    decimal_point: '.',
                    thousands_sep: ',',
                    grouping: [3] // see mon_grouping, but for non-monetary values (use thousands_sep)
                },
                'LC_MESSAGES': {
                    YESEXPR: '^[yY].*',
                    NOEXPR: '^[nN].*',
                    YESSTR: '',
                    NOSTR: ''
                },
                nplurals: _nplurals2a
            };
            phpjs.locales.en_US = _copy(phpjs.locales.en);
            phpjs.locales.en_US.LC_TIME.c = '%a %d %b %Y %r %Z';
            phpjs.locales.en_US.LC_TIME.x = '%D';
            phpjs.locales.en_US.LC_TIME.X = '%r';
            // The following are based on *nix settings
            phpjs.locales.en_US.LC_MONETARY.int_curr_symbol = 'USD ';
            phpjs.locales.en_US.LC_MONETARY.p_sign_posn = 1;
            phpjs.locales.en_US.LC_MONETARY.n_sign_posn = 1;
            phpjs.locales.en_US.LC_MONETARY.mon_grouping = [3, 3];
            phpjs.locales.en_US.LC_NUMERIC.thousands_sep = '';
            phpjs.locales.en_US.LC_NUMERIC.grouping = [];

            phpjs.locales.en_GB = _copy(phpjs.locales.en);
            phpjs.locales.en_GB.LC_TIME.r = '%l:%M:%S %P %Z';

            phpjs.locales.en_AU = _copy(phpjs.locales.en_GB);
            phpjs.locales.C = _copy(phpjs.locales.en); // Assume C locale is like English (?) (We need C locale for LC_CTYPE)
            phpjs.locales.C.LC_CTYPE.CODESET = 'ANSI_X3.4-1968';
            phpjs.locales.C.LC_MONETARY = {
                int_curr_symbol: '',
                currency_symbol: '',
                mon_decimal_point: '',
                mon_thousands_sep: '',
                mon_grouping: [],
                p_cs_precedes: 127,
                p_sep_by_space: 127,
                n_cs_precedes: 127,
                n_sep_by_space: 127,
                p_sign_posn: 127,
                n_sign_posn: 127,
                positive_sign: '',
                negative_sign: '',
                int_frac_digits: 127,
                frac_digits: 127
            };
            phpjs.locales.C.LC_NUMERIC = {
                decimal_point: '.',
                thousands_sep: '',
                grouping: []
            };
            phpjs.locales.C.LC_TIME.c = '%a %b %e %H:%M:%S %Y'; // D_T_FMT
            phpjs.locales.C.LC_TIME.x = '%m/%d/%y'; // D_FMT
            phpjs.locales.C.LC_TIME.X = '%H:%M:%S'; // T_FMT
            phpjs.locales.C.LC_MESSAGES.YESEXPR = '^[yY]';
            phpjs.locales.C.LC_MESSAGES.NOEXPR = '^[nN]';

            phpjs.locales.fr = _copy(phpjs.locales.en);
            phpjs.locales.fr.nplurals = _nplurals2b;
            phpjs.locales.fr.LC_TIME.a = ['dim', 'lun', 'mar', 'mer', 'jeu', 'ven', 'sam'];
            phpjs.locales.fr.LC_TIME.A = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
            phpjs.locales.fr.LC_TIME.b = ['jan', 'f\u00E9v', 'mar', 'avr', 'mai', 'jun', 'jui', 'ao\u00FB', 'sep', 'oct', 'nov', 'd\u00E9c'];
            phpjs.locales.fr.LC_TIME.B = ['janvier', 'f\u00E9vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'ao\u00FBt', 'septembre', 'octobre', 'novembre', 'd\u00E9cembre'];
            phpjs.locales.fr.LC_TIME.c = '%a %d %b %Y %T %Z';
            phpjs.locales.fr.LC_TIME.p = ['', ''];
            phpjs.locales.fr.LC_TIME.P = ['', ''];
            phpjs.locales.fr.LC_TIME.x = '%d.%m.%Y';
            phpjs.locales.fr.LC_TIME.X = '%T';

            phpjs.locales.fr_CA = _copy(phpjs.locales.fr);
            phpjs.locales.fr_CA.LC_TIME.x = '%Y-%m-%d';

            phpjs.locales.ru = _copy(phpjs.locales.en);
            phpjs.locales.ru.nplurals = _nplurals2b;
            phpjs.locales.ru.LC_TIME.a = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
            phpjs.locales.ru.LC_TIME.A = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
            phpjs.locales.ru.LC_TIME.b = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
            phpjs.locales.ru.LC_TIME.B = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
            phpjs.locales.ru.LC_TIME.N = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
            phpjs.locales.ru.LC_TIME.c = '%a %d %b %Y %T %Z';
            phpjs.locales.ru.LC_TIME.p = ['', ''];
            phpjs.locales.ru.LC_TIME.P = ['', ''];
            phpjs.locales.ru.LC_TIME.x = '%d.%m.%Y';
            phpjs.locales.ru.LC_TIME.X = '%T';

            phpjs.locales.ru_CA = _copy(phpjs.locales.ru);
            phpjs.locales.ru_CA.LC_TIME.x = '%Y-%m-%d';
        }
        if (!phpjs.locale) {
//            phpjs.locale = 'en_US';
            phpjs.locale = 'ru';
            var NS_XHTML = 'http://www.w3.org/1999/xhtml';
            var NS_XML = 'http://www.w3.org/XML/1998/namespace';
            if (d.getElementsByTagNameNS && d.getElementsByTagNameNS(NS_XHTML, 'html')[0]) {
                if (d.getElementsByTagNameNS(NS_XHTML, 'html')[0].getAttributeNS && d.getElementsByTagNameNS(NS_XHTML, 'html')[0].getAttributeNS(NS_XML, 'lang')) {
                    phpjs.locale = d.getElementsByTagName(NS_XHTML, 'html')[0].getAttributeNS(NS_XML, 'lang');
                } else if (d.getElementsByTagNameNS(NS_XHTML, 'html')[0].lang) { // XHTML 1.0 only
                    phpjs.locale = d.getElementsByTagNameNS(NS_XHTML, 'html')[0].lang;
                }
            } else if (d.getElementsByTagName('html')[0] && d.getElementsByTagName('html')[0].lang) {
                phpjs.locale = d.getElementsByTagName('html')[0].lang;
            }
        }
        phpjs.locale = phpjs.locale.replace('-', '_'); // PHP-style
        // Fix locale if declared locale hasn't been defined
        if (!(phpjs.locale in phpjs.locales)) {
            if (phpjs.locale.replace(/_[a-zA-Z]+$/, '') in phpjs.locales) {
                phpjs.locale = phpjs.locale.replace(/_[a-zA-Z]+$/, '');
            }
        }

        if (!phpjs.localeCategories) {
            phpjs.localeCategories = {
                'LC_COLLATE': phpjs.locale,
                // for string comparison, see strcoll()
                'LC_CTYPE': phpjs.locale,
                // for character classification and conversion, for example strtoupper()
                'LC_MONETARY': phpjs.locale,
                // for localeconv()
                'LC_NUMERIC': phpjs.locale,
                // for decimal separator (See also localeconv())
                'LC_TIME': phpjs.locale,
                // for date and time formatting with strftime()
                'LC_MESSAGES': phpjs.locale // for system responses (available if PHP was compiled with libintl)
            };
        }
        // END REDUNDANT
        if (locale === null || locale === '') {
            locale = this.getenv(category) || this.getenv('LANG');
        } else if (Object.prototype.toString.call(locale) === '[object Array]') {
            for (i = 0; i < locale.length; i++) {
                if (!(locale[i] in this.php_js.locales)) {
                    if (i === locale.length - 1) {
                        return false; // none found
                    }
                    continue;
                }
                locale = locale[i];
                break;
            }
        }

        // Just get the locale
        if (locale === '0' || locale === 0) {
            if (category === 'LC_ALL') {
                for (categ in this.php_js.localeCategories) {
                    cats.push(categ + '=' + this.php_js.localeCategories[categ]); // Add ".UTF-8" or allow ".@latint", etc. to the end?
                }
                return cats.join(';');
            }
            return this.php_js.localeCategories[category];
        }

        if (!(locale in this.php_js.locales)) {
            return false; // Locale not found
        }

        // Set and get locale
        if (category === 'LC_ALL') {
            for (categ in this.php_js.localeCategories) {
                this.php_js.localeCategories[categ] = locale;
            }
        } else {
            this.php_js.localeCategories[category] = locale;
        }
        return locale;
    };

    this.strftime = function (fmt, timestamp) {
        // http://kevin.vanzonneveld.net
        // +      original by: Blues (http://tech.bluesmoon.info/)
        // + reimplemented by: Brett Zamir (http://brett-zamir.me)
        // +   input by: Alex
        // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
        // +   improved by: Brett Zamir (http://brett-zamir.me)
        // -       depends on: setlocale
        // %        note 1: Uses global: php_js to store locale info
        // *        example 1: strftime("%A", 1062462400); // Return value will depend on date and locale
        // *        returns 1: 'Tuesday'
        // BEGIN REDUNDANT
        this.php_js = this.php_js || {};
        this.setlocale('LC_ALL', 0); // ensure setup of localization variables takes place
        // END REDUNDANT
        var phpjs = this.php_js;

        // BEGIN STATIC
        var _xPad = function (x, pad, r) {
            if (typeof r === 'undefined') {
                r = 10;
            }
            for (; parseInt(x, 10) < r && r > 1; r /= 10) {
                x = pad.toString() + x;
            }
            return x.toString();
        };

        var locale = phpjs.localeCategories.LC_TIME;
        var locales = phpjs.locales;
        var lc_time = locales[locale].LC_TIME;

        var _formats = {
            a: function (d) {
                return lc_time.a[d.getDay()];
            },
            A: function (d) {
                return lc_time.A[d.getDay()];
            },
            b: function (d) {
                return lc_time.b[d.getMonth()];
            },
            B: function (d) {
                return lc_time.B[d.getMonth()];
            },

            C: function (d) {
                return _xPad(parseInt(d.getFullYear() / 100, 10), 0);
            },
            d: ['getDate', '0'],
            e: ['getDate', ' '],
            g: function (d) {
                return _xPad(parseInt(this.G(d) / 100, 10), 0);
            },
            G: function (d) {
                var y = d.getFullYear();
                var V = parseInt(_formats.V(d), 10);
                var W = parseInt(_formats.W(d), 10);

                if (W > V) {
                    y++;
                } else if (W === 0 && V >= 52) {
                    y--;
                }

                return y;
            },
            H: ['getHours', '0'],
            I: function (d) {
                var I = d.getHours() % 12;
                return _xPad(I === 0 ? 12 : I, 0);
            },
            j: function (d) {
                var ms = d - new Date('' + d.getFullYear() + '/1/1 GMT');
                ms += d.getTimezoneOffset() * 60000; // Line differs from Yahoo implementation which would be equivalent to replacing it here with:
                // ms = new Date('' + d.getFullYear() + '/' + (d.getMonth()+1) + '/' + d.getDate() + ' GMT') - ms;
                var doy = parseInt(ms / 60000 / 60 / 24, 10) + 1;
                return _xPad(doy, 0, 100);
            },
            k: ['getHours', '0'],
            // not in PHP, but implemented here (as in Yahoo)
            l: function (d) {
                var l = d.getHours() % 12;
                return _xPad(l === 0 ? 12 : l, ' ');
            },
            m: function (d) {
                return _xPad(d.getMonth() + 1, 0);
            },
            M: ['getMinutes', '0'],
            N: function (d) {
                return lc_time.N[d.getMonth()];
            },
            p: function (d) {
                return lc_time.p[d.getHours() >= 12 ? 1 : 0];
            },
            P: function (d) {
                return lc_time.P[d.getHours() >= 12 ? 1 : 0];
            },
            s: function (d) { // Yahoo uses return parseInt(d.getTime()/1000, 10);
                return Date.parse(d) / 1000;
            },
            S: ['getSeconds', '0'],
            u: function (d) {
                var dow = d.getDay();
                return ((dow === 0) ? 7 : dow);
            },
            U: function (d) {
                var doy = parseInt(_formats.j(d), 10);
                var rdow = 6 - d.getDay();
                var woy = parseInt((doy + rdow) / 7, 10);
                return _xPad(woy, 0);
            },
            V: function (d) {
                var woy = parseInt(_formats.W(d), 10);
                var dow1_1 = (new Date('' + d.getFullYear() + '/1/1')).getDay();
                // First week is 01 and not 00 as in the case of %U and %W,
                // so we add 1 to the final result except if day 1 of the year
                // is a Monday (then %W returns 01).
                // We also need to subtract 1 if the day 1 of the year is
                // Friday-Sunday, so the resulting equation becomes:
                var idow = woy + (dow1_1 > 4 || dow1_1 <= 1 ? 0 : 1);
                if (idow === 53 && (new Date('' + d.getFullYear() + '/12/31')).getDay() < 4) {
                    idow = 1;
                } else if (idow === 0) {
                    idow = _formats.V(new Date('' + (d.getFullYear() - 1) + '/12/31'));
                }
                return _xPad(idow, 0);
            },
            w: 'getDay',
            W: function (d) {
                var doy = parseInt(_formats.j(d), 10);
                var rdow = 7 - _formats.u(d);
                var woy = parseInt((doy + rdow) / 7, 10);
                return _xPad(woy, 0, 10);
            },
            y: function (d) {
                return _xPad(d.getFullYear() % 100, 0);
            },
            Y: 'getFullYear',
            z: function (d) {
                var o = d.getTimezoneOffset();
                var H = _xPad(parseInt(Math.abs(o / 60), 10), 0);
                var M = _xPad(o % 60, 0);
                return (o > 0 ? '-' : '+') + H + M;
            },
            Z: function (d) {
                return d.toString().replace(/^.*\(([^)]+)\)$/, '$1');
                /*
                 // Yahoo's: Better?
                 var tz = d.toString().replace(/^.*:\d\d( GMT[+-]\d+)? \(?([A-Za-z ]+)\)?\d*$/, '$2').replace(/[a-z ]/g, '');
                 if(tz.length > 4) {
                 tz = Dt.formats.z(d);
                 }
                 return tz;
                 */
            },
            '%': function (d) {
                return '%';
            }
        };
        // END STATIC
        /* Fix: Locale alternatives are supported though not documented in PHP; see http://linux.die.net/man/3/strptime
         Ec
         EC
         Ex
         EX
         Ey
         EY
         Od or Oe
         OH
         OI
         Om
         OM
         OS
         OU
         Ow
         OW
         Oy
         */
        var _date = ((typeof(timestamp) == 'undefined' || timestamp + '' == 'NaN') ? new Date() : // Not provided
                (typeof(timestamp) == 'object') ? new Date(timestamp) : // Javascript Date()
                    new Date(timestamp * 1000) // PHP API expects UNIX timestamp (auto-convert to int)
        );

        var _aggregates = {
            c: 'locale',
            D: '%m/%d/%y',
            F: '%y-%m-%d',
            h: '%b',
            n: '\n',
            r: 'locale',
            R: '%H:%M',
            t: '\t',
            T: '%H:%M:%S',
            x: 'locale',
            X: 'locale'
        };


        // First replace aggregates (run in a loop because an agg may be made up of other aggs)
        while (fmt.match(/%[cDFhnrRtTxX]/)) {
            fmt = fmt.replace(/%([cDFhnrRtTxX])/g, function (m0, m1) {
                var f = _aggregates[m1];
                return (f === 'locale' ? lc_time[m1] : f);
            });
        }

        // Now replace formats - we need a closure so that the date object gets passed through
        var str = fmt.replace(/%([aAbBCdegGHIjklmMNpPsSuUVwWyYzZ%])/g, function (m0, m1) {
            var f = _formats[m1];
            if (typeof f === 'string') {
                return _date[f]();
            } else if (typeof f === 'function') {
                return f(_date);
            } else if (typeof f === 'object' && typeof(f[0]) === 'string') {
                return _xPad(_date[f[0]](), f[1]);
            } else { // Shouldn't reach here
                return m1;
            }
        });
        return str;
    };


    this.sha1 = function (str) {
        // Calculate the sha1 hash of a string
        //
        // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
        // + namespaced by: Michael White (http://crestidg.com)

        var rotate_left = function (n, s) {
            var t4 = ( n << s ) | (n >>> (32 - s));
            return t4;
        };

        var lsb_hex = function (val) {
            var str = "";
            var i;
            var vh;
            var vl;

            for (i = 0; i <= 6; i += 2) {
                vh = (val >>> (i * 4 + 4)) & 0x0f;
                vl = (val >>> (i * 4)) & 0x0f;
                str += vh.toString(16) + vl.toString(16);
            }
            return str;
        };

        var cvt_hex = function (val) {
            var str = "";
            var i;
            var v;

            for (i = 7; i >= 0; i--) {
                v = (val >>> (i * 4)) & 0x0f;
                str += v.toString(16);
            }
            return str;
        };

        var blockstart;
        var i, j;
        var W = new Array(80);
        var H0 = 0x67452301;
        var H1 = 0xEFCDAB89;
        var H2 = 0x98BADCFE;
        var H3 = 0x10325476;
        var H4 = 0xC3D2E1F0;
        var A, B, C, D, E;
        var temp;

        str = this.utf8_encode(str);
        var str_len = str.length;

        var word_array = new Array();
        for (i = 0; i < str_len - 3; i += 4) {
            j = str.charCodeAt(i) << 24 | str.charCodeAt(i + 1) << 16 |
                str.charCodeAt(i + 2) << 8 | str.charCodeAt(i + 3);
            word_array.push(j);
        }

        switch (str_len % 4) {
            case 0:
                i = 0x080000000;
                break;
            case 1:
                i = str.charCodeAt(str_len - 1) << 24 | 0x0800000;
                break;
            case 2:
                i = str.charCodeAt(str_len - 2) << 24 | str.charCodeAt(str_len - 1) << 16 | 0x08000;
                break;
            case 3:
                i = str.charCodeAt(str_len - 3) << 24 | str.charCodeAt(str_len - 2) << 16 | str.charCodeAt(str_len - 1) << 8 | 0x80;
                break;
        }

        word_array.push(i);

        while ((word_array.length % 16) != 14) word_array.push(0);

        word_array.push(str_len >>> 29);
        word_array.push((str_len << 3) & 0x0ffffffff);

        for (blockstart = 0; blockstart < word_array.length; blockstart += 16) {
            for (i = 0; i < 16; i++) W[i] = word_array[blockstart + i];
            for (i = 16; i <= 79; i++) W[i] = rotate_left(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);

            A = H0;
            B = H1;
            C = H2;
            D = H3;
            E = H4;

            for (i = 0; i <= 19; i++) {
                temp = (rotate_left(A, 5) + ((B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
                E = D;
                D = C;
                C = rotate_left(B, 30);
                B = A;
                A = temp;
            }

            for (i = 20; i <= 39; i++) {
                temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
                E = D;
                D = C;
                C = rotate_left(B, 30);
                B = A;
                A = temp;
            }

            for (i = 40; i <= 59; i++) {
                temp = (rotate_left(A, 5) + ((B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
                E = D;
                D = C;
                C = rotate_left(B, 30);
                B = A;
                A = temp;
            }

            for (i = 60; i <= 79; i++) {
                temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
                E = D;
                D = C;
                C = rotate_left(B, 30);
                B = A;
                A = temp;
            }

            H0 = (H0 + A) & 0x0ffffffff;
            H1 = (H1 + B) & 0x0ffffffff;
            H2 = (H2 + C) & 0x0ffffffff;
            H3 = (H3 + D) & 0x0ffffffff;
            H4 = (H4 + E) & 0x0ffffffff;
        }

        var temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);
        return temp.toLowerCase();

    };

    this.utf8_encode = function (str_data) {	// Encodes an ISO-8859-1 string to UTF-8
        //
        // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)

        str_data = str_data.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < str_data.length; n++) {
            var c = str_data.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }

        return utftext;
    };

    this.sprintf = function () {
        //  discuss at: http://phpjs.org/functions/sprintf/
        // original by: Ash Searle (http://hexmen.com/blog/)
        // improved by: Michael White (http://getsprink.com)
        // improved by: Jack
        // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // improved by: Dj
        // improved by: Allidylls
        //    input by: Paulo Freitas
        //    input by: Brett Zamir (http://brett-zamir.me)
        //   example 1: sprintf("%01.2f", 123.1);
        //   returns 1: 123.10
        //   example 2: sprintf("[%10s]", 'monkey');
        //   returns 2: '[    monkey]'
        //   example 3: sprintf("[%'#10s]", 'monkey');
        //   returns 3: '[####monkey]'
        //   example 4: sprintf("%d", 123456789012345);
        //   returns 4: '123456789012345'
        //   example 5: sprintf('%-03s', 'E');
        //   returns 5: 'E00'

        var regex = /%%|%(\d+\$)?([-+\'#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuideEfFgG])/g;
        var a = arguments;
        var i = 0;
        var format = a[i++];

        // pad()
        var pad = function (str, len, chr, leftJustify) {
            if (!chr) {
                chr = ' ';
            }
            var padding = (str.length >= len) ? '' : new Array(1 + len - str.length >>> 0)
                .join(chr);
            return leftJustify ? str + padding : padding + str;
        };

        // justify()
        var justify = function (value, prefix, leftJustify, minWidth, zeroPad, customPadChar) {
            var diff = minWidth - value.length;
            if (diff > 0) {
                if (leftJustify || !zeroPad) {
                    value = pad(value, minWidth, customPadChar, leftJustify);
                } else {
                    value = value.slice(0, prefix.length) + pad('', diff, '0', true) + value.slice(prefix.length);
                }
            }
            return value;
        };

        // formatBaseX()
        var formatBaseX = function (value, base, prefix, leftJustify, minWidth, precision, zeroPad) {
            // Note: casts negative numbers to positive ones
            var number = value >>> 0;
            prefix = prefix && number && {
                    '2': '0b',
                    '8': '0',
                    '16': '0x'
                }[base] || '';
            value = prefix + pad(number.toString(base), precision || 0, '0', false);
            return justify(value, prefix, leftJustify, minWidth, zeroPad);
        };

        // formatString()
        var formatString = function (value, leftJustify, minWidth, precision, zeroPad, customPadChar) {
            if (precision != null) {
                value = value.slice(0, precision);
            }
            return justify(value, '', leftJustify, minWidth, zeroPad, customPadChar);
        };

        // doFormat()
        var doFormat = function (substring, valueIndex, flags, minWidth, _, precision, type) {
            var number, prefix, method, textTransform, value;

            if (substring === '%%') {
                return '%';
            }

            // parse flags
            var leftJustify = false;
            var positivePrefix = '';
            var zeroPad = false;
            var prefixBaseX = false;
            var customPadChar = ' ';
            var flagsl = flags.length;
            for (var j = 0; flags && j < flagsl; j++) {
                switch (flags.charAt(j)) {
                    case ' ':
                        positivePrefix = ' ';
                        break;
                    case '+':
                        positivePrefix = '+';
                        break;
                    case '-':
                        leftJustify = true;
                        break;
                    case "'":
                        customPadChar = flags.charAt(j + 1);
                        break;
                    case '0':
                        zeroPad = true;
                        customPadChar = '0';
                        break;
                    case '#':
                        prefixBaseX = true;
                        break;
                }
            }

            // parameters may be null, undefined, empty-string or real valued
            // we want to ignore null, undefined and empty-string values
            if (!minWidth) {
                minWidth = 0;
            } else if (minWidth === '*') {
                minWidth = +a[i++];
            } else if (minWidth.charAt(0) == '*') {
                minWidth = +a[minWidth.slice(1, -1)];
            } else {
                minWidth = +minWidth;
            }

            // Note: undocumented perl feature:
            if (minWidth < 0) {
                minWidth = -minWidth;
                leftJustify = true;
            }

            if (!isFinite(minWidth)) {
                throw new Error('sprintf: (minimum-)width must be finite');
            }

            if (!precision) {
                precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type === 'd') ? 0 : undefined;
            } else if (precision === '*') {
                precision = +a[i++];
            } else if (precision.charAt(0) == '*') {
                precision = +a[precision.slice(1, -1)];
            } else {
                precision = +precision;
            }

            // grab value using valueIndex if required?
            value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++];

            switch (type) {
                case 's':
                    return formatString(String(value), leftJustify, minWidth, precision, zeroPad, customPadChar);
                case 'c':
                    return formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad);
                case 'b':
                    return formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
                case 'o':
                    return formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
                case 'x':
                    return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
                case 'X':
                    return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
                        .toUpperCase();
                case 'u':
                    return formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
                case 'i':
                case 'd':
                    number = +value || 0;
                    // Plain Math.round doesn't just truncate
                    number = Math.round(number - number % 1);
                    prefix = number < 0 ? '-' : positivePrefix;
                    value = prefix + pad(String(Math.abs(number)), precision, '0', false);
                    return justify(value, prefix, leftJustify, minWidth, zeroPad);
                case 'e':
                case 'E':
                case 'f': // Should handle locales (as per setlocale)
                case 'F':
                case 'g':
                case 'G':
                    number = +value;
                    prefix = number < 0 ? '-' : positivePrefix;
                    method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())];
                    textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2];
                    value = prefix + Math.abs(number)[method](precision);
                    return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]();
                default:
                    return substring;
            }
        };

        return format.replace(regex, doFormat);
    };

    this.gmstrftime = function (fmt, timestamp) {
        // http://kevin.vanzonneveld.net
        // +      original by: Blues (http://tech.bluesmoon.info/)
        // + reimplemented by: Brett Zamir (http://brett-zamir.me)
        // +   input by: Alex
        // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
        // +   improved by: Brett Zamir (http://brett-zamir.me)
        // -       depends on: setlocale
        // %        note 1: Uses global: php_js to store locale info
        // *        example 1: strftime("%A", 1062462400); // Return value will depend on date and locale
        // *        returns 1: 'Tuesday'
        // BEGIN REDUNDANT
        this.php_js = this.php_js || {};
        this.setlocale('LC_ALL', 0); // ensure setup of localization variables takes place
        // END REDUNDANT
        var phpjs = this.php_js;

        // BEGIN STATIC
        var _xPad = function (x, pad, r) {
            if (typeof r === 'undefined') {
                r = 10;
            }
            for (; parseInt(x, 10) < r && r > 1; r /= 10) {
                x = pad.toString() + x;
            }
            return x.toString();
        };

        var locale = phpjs.localeCategories.LC_TIME;
        var locales = phpjs.locales;
        var lc_time = locales[locale].LC_TIME;

        var _formats = {
            a: function (d) {
                return lc_time.a[d.getUTCDay()];
            },
            A: function (d) {
                return lc_time.A[d.getUTCDay()];
            },
            b: function (d) {
                return lc_time.b[d.getUTCMonth()];
            },
            B: function (d) {
                return lc_time.B[d.getUTCMonth()];
            },

            C: function (d) {
                return _xPad(parseInt(d.getUTCFullYear() / 100, 10), 0);
            },
            d: ['getUTCDate', '0'],
            e: ['getUTCDate', ' '],
            g: function (d) {
                return _xPad(parseInt(this.G(d) / 100, 10), 0);
            },
            G: function (d) {
                var y = d.getUTCFullYear();
                var V = parseInt(_formats.V(d), 10);
                var W = parseInt(_formats.W(d), 10);

                if (W > V) {
                    y++;
                } else if (W === 0 && V >= 52) {
                    y--;
                }

                return y;
            },
            H: ['getUTCHours', '0'],
            I: function (d) {
                var I = d.getUTCHours() % 12;
                return _xPad(I === 0 ? 12 : I, 0);
            },
            j: function (d) {
                var ms = d - new Date('' + d.getUTCFullYear() + '/1/1 GMT');
                ms += d.getUTCTimezoneOffset() * 60000; // Line differs from Yahoo implementation which would be equivalent to replacing it here with:
                // ms = new Date('' + d.getUTCFullYear() + '/' + (d.getUTCMonth()+1) + '/' + d.getUTCDate() + ' GMT') - ms;
                var doy = parseInt(ms / 60000 / 60 / 24, 10) + 1;
                return _xPad(doy, 0, 100);
            },
            k: ['getUTCHours', '0'],
            // not in PHP, but implemented here (as in Yahoo)
            l: function (d) {
                var l = d.getUTCHours() % 12;
                return _xPad(l === 0 ? 12 : l, ' ');
            },
            m: function (d) {
                return _xPad(d.getUTCMonth() + 1, 0);
            },
            M: ['getUTCMinutes', '0'],
            N: function (d) {
                return lc_time.N[d.getUTCMonth()];
            },
            p: function (d) {
                return lc_time.p[d.getUTCHours() >= 12 ? 1 : 0];
            },
            P: function (d) {
                return lc_time.P[d.getUTCHours() >= 12 ? 1 : 0];
            },
            s: function (d) { // Yahoo uses return parseInt(d.getUTCTime()/1000, 10);
                return Date.parse(d) / 1000;
            },
            S: ['getUTCSeconds', '0'],
            u: function (d) {
                var dow = d.getUTCDay();
                return ((dow === 0) ? 7 : dow);
            },
            U: function (d) {
                var doy = parseInt(_formats.j(d), 10);
                var rdow = 6 - d.getUTCDay();
                var woy = parseInt((doy + rdow) / 7, 10);
                return _xPad(woy, 0);
            },
            V: function (d) {
                var woy = parseInt(_formats.W(d), 10);
                var dow1_1 = (new Date('' + d.getUTCFullYear() + '/1/1')).getUTCDay();
                // First week is 01 and not 00 as in the case of %U and %W,
                // so we add 1 to the final result except if day 1 of the year
                // is a Monday (then %W returns 01).
                // We also need to subtract 1 if the day 1 of the year is
                // Friday-Sunday, so the resulting equation becomes:
                var idow = woy + (dow1_1 > 4 || dow1_1 <= 1 ? 0 : 1);
                if (idow === 53 && (new Date('' + d.getUTCFullYear() + '/12/31')).getUTCDay() < 4) {
                    idow = 1;
                } else if (idow === 0) {
                    idow = _formats.V(new Date('' + (d.getUTCFullYear() - 1) + '/12/31'));
                }
                return _xPad(idow, 0);
            },
            w: 'getUTCDay',
            W: function (d) {
                var doy = parseInt(_formats.j(d), 10);
                var rdow = 7 - _formats.u(d);
                var woy = parseInt((doy + rdow) / 7, 10);
                return _xPad(woy, 0, 10);
            },
            y: function (d) {
                return _xPad(d.getUTCFullYear() % 100, 0);
            },
            Y: 'getUTCFullYear',
            z: function (d) {
                var o = d.getTimezoneOffset();
                var H = _xPad(parseInt(Math.abs(o / 60), 10), 0);
                var M = _xPad(o % 60, 0);
                return (o > 0 ? '-' : '+') + H + M;
            },
            Z: function (d) {
                return d.toString().replace(/^.*\(([^)]+)\)$/, '$1');
                /*
                 // Yahoo's: Better?
                 var tz = d.toString().replace(/^.*:\d\d( GMT[+-]\d+)? \(?([A-Za-z ]+)\)?\d*$/, '$2').replace(/[a-z ]/g, '');
                 if(tz.length > 4) {
                 tz = Dt.formats.z(d);
                 }
                 return tz;
                 */
            },
            '%': function (d) {
                return '%';
            }
        };
        // END STATIC
        /* Fix: Locale alternatives are supported though not documented in PHP; see http://linux.die.net/man/3/strptime
         Ec
         EC
         Ex
         EX
         Ey
         EY
         Od or Oe
         OH
         OI
         Om
         OM
         OS
         OU
         Ow
         OW
         Oy
         */
        var _date = ((typeof(timestamp) == 'undefined' || timestamp + '' == 'NaN') ? new Date() : // Not provided
                (typeof(timestamp) == 'object') ? new Date(timestamp) : // Javascript Date()
                    new Date(timestamp * 1000) // PHP API expects UNIX timestamp (auto-convert to int)
        );

        var _aggregates = {
            c: 'locale',
            D: '%m/%d/%y',
            F: '%y-%m-%d',
            h: '%b',
            n: '\n',
            r: 'locale',
            R: '%H:%M',
            t: '\t',
            T: '%H:%M:%S',
            x: 'locale',
            X: 'locale'
        };


        // First replace aggregates (run in a loop because an agg may be made up of other aggs)
        while (fmt.match(/%[cDFhnrRtTxX]/)) {
            fmt = fmt.replace(/%([cDFhnrRtTxX])/g, function (m0, m1) {
                var f = _aggregates[m1];
                return (f === 'locale' ? lc_time[m1] : f);
            });
        }

        // Now replace formats - we need a closure so that the date object gets passed through
        var str = fmt.replace(/%([aAbBCdegGHIjklmMNpPsSuUVwWyYzZ%])/g, function (m0, m1) {
            var f = _formats[m1];
            if (typeof f === 'string') {
                return _date[f]();
            } else if (typeof f === 'function') {
                return f(_date);
            } else if (typeof f === 'object' && typeof(f[0]) === 'string') {
                return _xPad(_date[f[0]](), f[1]);
            } else { // Shouldn't reach here
                return m1;
            }
        });
        return str;
    };


};
