define([
    'jssmart'
], function (jSmart) {


    jSmart.prototype.registerPlugin(
        'modifier',
        'slice',
        function (s, start, stop) {
            return s.slice(start, stop);
        }
    );

    jSmart.prototype.registerPlugin(
        'modifier',
        'preg',
        function (s, search, replace) {
            try {
                return eval('s.replace(' + search + ', "' + replace + '");');
            }
            finally {
                return eval('s.replace("' + search + '", "' + replace + '");');
            }
        }
    );


    jSmart.prototype.registerPlugin(
        'modifier',
        'urlencode',
        function (s) {
            return encodeURIComponent(s);
        }
    );

    jSmart.prototype.registerPlugin(
        'modifier',
        'urldecode',
        function (s) {
            return decodeURIComponent(s);
        }
    );

    jSmart.prototype.registerPlugin(
        'modifier',
        'plural',
        function (int, one, two, many) {

            if (int % 10 == 1 && int % 100 != 11) return one;
            else if (int % 10 >= 2 && int % 10 <= 4 && ( int % 100 < 10 || int % 100 >= 20)) return two;
            else return many;
        }
    );

    jSmart.prototype.registerPlugin(
        'modifier',
        'upperfirstletter',
        function(word)
        {
            return  word.charAt(0).toUpperCase() + word.slice(1)
        }
    );

    jSmart.prototype.registerPlugin(
        'modifier',
        'join',
        function(s, replaceWith)
        {
            if (s.join){
                return s.join(replaceWith);
            }else {
                replaceWith = replaceWith ? replaceWith : ' ';
                return (new String(s)).replace(/[\s]+/g, replaceWith);
            }
        }
    );

});