(function (window, document, $) {
    "use strict";


    var Tmpl,
    // uni templates function
        compile = function (text) {
            var js = new jSmart(text);
            js.escape_html = true;
            return js;
        },
        render = function (tpl, params) {
            return tpl.fetch(params);
        };

    $.tmpl = Tmpl = function (template, params, force) {

        params = $.extend(params, $.tmpl.globals);
        if (!Tmpl.templates.hasOwnProperty(template) || force) {

            if (!$('#tmpl-' + template).length) {
                console.error('Not found template ' + template);
                return 'Not found template ' + template;
            }
            Tmpl.templates[template] = compile($('#tmpl-' + template).html());
        }

        return render(Tmpl.templates[template], params);
    };

    /**
     * @class tmpl
     * @param text
     * @param params
     * @param not_escape
     * @param not_global
     * @returns {*}
     */
    $.render = function (text, params, not_escape, not_global) {
        var js = new jSmart(text);
        js.escape_html = !not_escape;
        if (!not_global) {
            params = $.extend(params, $.tmpl.globals);
        }
        return js.fetch(params);
    };

    $.tmpl.globals = {};

    $.tmpl.global = function (opts) {

        $.extend($.tmpl.globals, opts);
    };

    $.extend(Tmpl, {

        templates: {}
    });

    $.fn.tmpl = function (name, data) {

        // Render template
        this.html($.tmpl(name, data));
        return this;
    };

}(window, document, jQuery));


