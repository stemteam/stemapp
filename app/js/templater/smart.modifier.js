


jSmart.prototype.registerPlugin(
    'modifier',
    'slice',
    function(s,start,stop)
    {
        return s.slice(start, stop);
    }
);

jSmart.prototype.registerPlugin(
    'modifier',
    'preg',
    function(s,search,replace)
    {
        try{
        return eval('s.replace('+search+', "'+replace+'");');
        }
        finally
        {
            return eval('s.replace("'+search+'", "'+replace+'");');
        }
    }
);


jSmart.prototype.registerPlugin(
    'modifier',
    'urlencode',
    function(s)
    {
        return encodeURIComponent(s);
    }
);

jSmart.prototype.registerPlugin(
    'modifier',
    'urldecode',
    function(s)
    {
        return decodeURIComponent(s);
    }
);