/**
 * Created by phplamer on 16.10.14.
 */


(function (window, $) {
    "use strict";

    var utils = new Object();

    function setFirstZero(value) {
        return value < 10 ? '0'+value : value;
    }

    utils = {

    }

    utils.convert = {
        timestampToDateTimeRU: function(timestamp) {
            return setFirstZero((new Date(timestamp)).getDate())+'.'+
                setFirstZero(((new Date(timestamp)).getMonth()+1))+'.'+
                (new Date(timestamp)).getFullYear()+' '+
                setFirstZero((new Date(timestamp)).getHours())+':'+
                setFirstZero((new Date(timestamp)).getMinutes())+':'+
                setFirstZero((new Date(timestamp)).getSeconds());
        }
    }


    window.utils = utils;
}(window, jQuery));
