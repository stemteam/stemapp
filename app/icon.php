<?php

// http://stackoverflow.com/questions/1890409/change-hue-of-an-image-with-php-gd-library

date_default_timezone_set('Europe/Moscow');

define('IMAGES', realpath(dirname(__FILE__)) . '/img/map/objects/');
define('DEFAULT_ICON', IMAGES . 'Object_Car1_Big.png');

if (!isset($_GET['icon']) || !isset($_GET['color'])) {
	error();
}
$size = isset($_GET['size']) ? $_GET['size'] : 'Big';

$icon = IMAGES . $_GET['icon'] . '_' . $size . '.png';
if (!file_exists($icon)) {
	error();
}

list($H, $S, $L) = explode(';', str_replace(',', '.', ($_GET['color'] === 'null') ? '1;1;0,5' : $_GET['color']));
if (!is_numeric($H) || !is_numeric($S) || !is_numeric($L)) {
	error();
}
if ($_GET['color'] !== 'null') {
	$image = imagecreatefrompng($icon);
	imagesavealpha($image, true);


	$width = imagesx($image);
	$height = imagesy($image);

	for ($x = 0; $x < $width; $x++) {
		for ($y = 0; $y < $height; $y++) {
			$rgb = imagecolorat($image, $x, $y);
			$r = ($rgb >> 16) & 0xFF;
			$g = ($rgb >> 8) & 0xFF;
			$b = $rgb & 0xFF;
			$alpha = ($rgb & 0x7F000000) >> 24;

			list($h, $s, $l) = rgb2hsl($r, $g, $b);

			$h *= $H;
			$s *= $S;
			$l *= $L;
			list($r, $g, $b) = hsl2rgb($h, $s, $l);
			imagesetpixel($image, $x, $y, imagecolorallocatealpha($image, $r, $g, $b, $alpha));
		}
	}


	$file = imagepng($image);
} else {

	$file = file_get_contents($icon);
}
header('Content-type: image/png');

$lastModified = gmmktime(0, 0, 0, 1, 1, 2010); // В прошлом!
$etag = md5($file);
$ifModifiedSince = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
$etagHeader = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $lastModified) . ' GMT');
header('Etag: ' . $etag);
header('Cache-Control: public');

if (($etagHeader === $etag) || $ifModifiedSince && (@strtotime($ifModifiedSince) === $lastModified)) {
	header('HTTP/1.1 304 Not Modified');
	exit;
}

die($file);


function error()
{
	header('Content-type: image/png');
	die(file_get_contents(DEFAULT_ICON));
}

function hsl2rgb($h, $s, $l)
{
	$normalisedH = $h;

	if ($l == 0) {
		$r = $g = $b = 0;
	} else {
		if ($s == 0) {
			$r = $g = $b = $l;
		} else {
			$temp2 = (($l <= 0.5) ? $l * (1.0 + $s) : $l + $s - ($l * $s));
			$temp1 = 2 * $l - $temp2;

			//double[] t3=new double[]{normalisedH+1.0/3.0,normalisedH,normalisedH-1.0/3.0};
			$t3 = array($normalisedH + 1.0 / 3.0, $normalisedH, $normalisedH - 1.0 / 3.0);
			$clr = array(0, 0, 0);

			for ($i = 0; $i < 3; $i++) {
				if ($t3[$i] < 0) {
					$t3[$i] += 1.0;
				}

				if ($t3[$i] > 1) {
					$t3[$i] -= 1.0;
				}

				if (6.0 * $t3[$i] < 1.0) {
					$clr[$i] = $temp1 + ($temp2 - $temp1) * $t3[$i] * 6.0;
				} else {
					if (2.0 * $t3[$i] < 1.0) {
						$clr[$i] = $temp2;
					} else {
						if (3.0 * $t3[$i] < 2.0) {
							$clr[$i] = ($temp1 + ($temp2 - $temp1) * ((2.0 / 3.0) - $t3[$i]) * 6.0);
						} else {
							$clr[$i] = $temp1;
						}
					}
				}

			}

			$r = $clr[0];
			$g = $clr[1];
			$b = $clr[2];
		}

	}
	//return Color.FromArgb(a, (int)(255*r),(int)(255*g),(int)(255*b));
	return array(
		$r * 255,
		$g * 255,
		$b * 255
	);
}

function rgb2hsl($r, $g, $b)
{
	$var_R = ($r / 255);
	$var_G = ($g / 255);
	$var_B = ($b / 255);

	$var_Min = min($var_R, $var_G, $var_B);
	$var_Max = max($var_R, $var_G, $var_B);
	$del_Max = $var_Max - $var_Min;

	$v = $var_Max;

	if ($del_Max == 0) {
		$h = 0;
		$s = 0;
	} else {
		$s = $del_Max / $var_Max;

		$del_R = ((($var_Max - $var_R) / 6) + ($del_Max / 2)) / $del_Max;
		$del_G = ((($var_Max - $var_G) / 6) + ($del_Max / 2)) / $del_Max;
		$del_B = ((($var_Max - $var_B) / 6) + ($del_Max / 2)) / $del_Max;

		if ($var_R == $var_Max) {
			$h = $del_B - $del_G;
		} else {
			if ($var_G == $var_Max) {
				$h = (1 / 3) + $del_R - $del_B;
			} else {
				if ($var_B == $var_Max) {
					$h = (2 / 3) + $del_G - $del_R;
				}
			}
		}

		if ($h < 0) {
			$h++;
		}
		if ($h > 1) {
			$h--;
		}
	}

	return array($h, $s, $v);
}
