(function () {
    var fs = require('fs');
    var path = require('path');
    var exec = require('child_process').exec;
    var modules_dir = 'modules/';
    var resource_dir = 'resource/';
    var app_dir = 'app/';
    var out = [];
    var paths = [];
    var less_array = [];
    var name;
    // Ищем модули
    fs.readdir(modules_dir, function (err, files) {
        files.forEach(function (value) {
            if (fs.lstatSync(modules_dir + value).isDirectory()) {
                out.push(value);
                name = fs.readFileSync(modules_dir + value + '/name');
                paths.push("'" + name + "':'" + modules_dir + value + "'");
                fs.writeFileSync(modules_dir + value + '/css/init.less', '@base' + name + '_url:"@{base_url}' + modules_dir + value + '/";');
                less_array.push('@import "../../' + modules_dir + value + '/css/main.less' + '";');
                exec('cd ' + modules_dir + value+ ' && npm install');
            }
        });
        fs.writeFileSync('config_modules.json', JSON.stringify(out));
        // создаем измененный конфиг requirejs
        var config = fs.readFileSync('config.js', {encoding: 'utf8'});

        config = config.replace(/(\s*)\},\s*shim/, ',\n' + paths.join(',\n') + '},\nshim');
        fs.writeFileSync('_config.js', config);
        fs.writeFileSync(resource_dir + 'css/modules.less', less_array.join("\n"));
    });

    if (fs.existsSync(resource_dir + 'index.html')) {
        console.log('Use resource index');
        fs.createReadStream(resource_dir + 'index.html').pipe(fs.createWriteStream('index.html'));
    }else {
        fs.createReadStream(app_dir + 'index.html').pipe(fs.createWriteStream('index.html'));
        console.log('Use app index');
    }

    function scanDirectory(dir, exts, items) {
        items = items ? items : [];
        if (fs.existsSync(dir)) {
            fs.readdirSync(dir).forEach(function (filename) {
                if (fs.lstatSync(dir + '/' + filename).isDirectory()) {
                    items = items.concat(scanDirectory(dir + '/' + filename, exts));
                } else {
                    var ext = path.extname(filename).slice(1);
                    if (exts.indexOf(ext) != -1) {
                        items.push(dir + '/' + filename);
                    }
                }
            });
        }
        return items;
    }

})();