var require = {
    baseUrl: '',
    waitSeconds: 15,
    paths: {
        'jquery': 'node_modules/jquery/dist/jquery.min',
        'backbone': 'node_modules/backbone/backbone-min',
        'epoxy': 'node_modules/backbone.epoxy/backbone.epoxy.min',
        'underscore': 'node_modules/underscore/underscore-min',
        'less': 'node_modules/less/dist/less.min',
        'jssmart': 'app/js/templater/jssmart',
        'smarty': 'app/js/templater/smarty',
        'tmpl': 'app/js/templater/tmpl.jquery',
        'php_js': 'app/js/templater/php',
        'jquery.qtip': 'app/js/jq/jquery.qtip.min',
        'cookie': 'app/js/jq/cookie',
        'jquery.qtip.jgrowl': 'app/js/jq/jquery.qtip.jgrowl',
        'jquery-ui': 'app/js/jq/jquery-ui',
        'plugins': 'app/js/jq/plugins',
        'slick.core': 'app/js/jq/slickgrid/slick.core',
        'slickgrid': 'app/js/jq/slickgrid/slick.grid',
        'columns': 'app/js/jq/jquery.columns',
        'zeroclipboard': 'app/js/jq/ZeroClipboard',
        'navstat.select': 'app/js/jq/jquery.navstat.select',
        'crypto': 'app/js/misc/sha1',
        'mlpushmenu': 'app/js/jq/mlpushmenu',
        '_app': 'app/js/stemapp',
        '_misc': 'app/js/misc',
        '_services': 'app/js/services',
        '_jq': 'app/js/jq',
        '_modules': 'modules'
    },
    shim: {
        'underscore': {
            exports: '_'
        },
        'jquery': {
            exports: '$'
        },
        'backbone': {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        },
        'epoxy': {
            deps: ['backbone']
        },
        'less': {
            exports: "less"
        },
        'jssmart': {
            exports: "jSmart"
        },
        'cookie': {
            deps: ["jquery"],
            exports: "$.cookie"
        },
        'tmpl': {
            deps: ['smarty', 'php_js', 'jssmart', 'app/js/templater/jssmart.modifier'],
            exports: "$.render"
        },
        'plugins': {
            exports: '$.fn'
        },
        'php_js': {
            exports: 'PHP_JS'
        },
        'jquery.qtip': {
            deps: ["jquery"]
        },
        'columns': {
            deps: ["jquery"]
        },
        'jquery-ui': {
            deps: ["jquery"]
        },
        'zeroclipboard': {
            deps: ["jquery"]
        },
        'navstat.select': {
            deps: ["jquery", "_jq/jquery.scrollTo-min"]
        },
        'mlpushmenu': {
            deps: ['_jq/modernizr.custom'],
            exports: "mlPushMenu"
        },
        'jquery.qtip.jgrowl': {
            deps: ["jquery"]
        },
        '_jq/datepicker': {
            deps: ["jquery"]
        },
        '_jq/jquery.navstat.dateinput': {
            deps: ["jquery"]
        },
        'slick.core': {
            deps: ["jquery"]
        },
        'app/js/jq/slickgrid/slick.dataview': {
            deps: ['slick.core']
        },
        'app/js/jq/slickgrid/slick.ROeditors': {
            deps: ['slick.core']
        },
        'app/js/jq/slickgrid/plugins/slick.countercolumn': {
            deps: ['slick.core']
        },
        'app/js/jq/slickgrid/plugins/slick.checkboxselectcolumn': {
            deps: ['slick.core']
        },
        'app/js/jq/slickgrid/plugins/slick.checkboxNavstat': {
            deps: ['slick.core']
        },
        'app/js/jq/slickgrid/plugins/slick.rowselectionmodel': {
            deps: ['slick.core']
        },
        'app/js/jq/slickgrid/plugins/slick.groupitemmetadataprovider': {
            deps: ['slick.core']
        },
        'app/js/jq/jquery.event.drag-2.2': {
            deps: ["jquery"]
        },
        'app/js/jq/jquery-ui-1.10.4.custom.min': {
            deps: ["jquery"]
        },
        'app/js/jq/datepicker': {
            deps: ["jquery"]
        },
        'app/js/jq/jquery.navstat.dateinput': {
            deps: ["jquery"]
        },
        'slickgrid': {
            deps: [
                'slick.core',
                'app/js/jq/slickgrid/slick.dataview',
                'app/js/jq/slickgrid/slick.ROeditors',
                'app/js/jq/slickgrid/plugins/slick.countercolumn',
                'app/js/jq/slickgrid/plugins/slick.checkboxselectcolumn',
                'app/js/jq/slickgrid/plugins/slick.checkboxNavstat',
                'app/js/jq/slickgrid/plugins/slick.rowselectionmodel',
                'app/js/jq/slickgrid/plugins/slick.groupitemmetadataprovider',
                'app/js/jq/jquery.event.drag-2.2',
                'app/js/jq/jquery-ui-1.10.4.custom.min'
            ],
            exports: 'Slick'
        },
        'crypto': {
            'exports': 'CryptoJS'
        }
    },
    callback: function () {
        'use strict';
        //require(['_app/init']);
    }
};
