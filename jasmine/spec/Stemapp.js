//todo Сделать тесты для api
//todo сделать тесты для util
//todo сделать тесты для grid
jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;
describe("Stemapp", function () {
    var boot = window.Boot;
    beforeAll(function () {
        spyOn(boot, 'init').and.callThrough();
        spyOn(boot, 'checkDevMode').and.returnValue(true);
        spyOn(boot, 'loadBootScript').and.callThrough();

    });

    describe("Boot loader", function () {

        it("exist boot loader", function () {
            expect(boot).toBeDefined();
        });

        it("init", function (done) {
            boot.init(done);

            expect(boot.init).toHaveBeenCalled();
        });

        it("call checkDevMode", function () {

            expect(boot.checkDevMode).toHaveBeenCalled();
        });

        it("call loadBootScript", function () {
            expect(boot.loadBootScript).toHaveBeenCalled();
        });

    });


    describe("App", function () {

        var me = this;

        beforeAll(function (done) {
            require(['_app/views/app'], function (App) {
                me.App = App;
                done();
            });
            Stemapp.modules = [];

        });

        beforeEach(function (done) {
            require(['_app/routers/main'], function (Router) {
                me.router = new Router();
                done();
            });
        });

        it("defined", function () {
            expect(me.App).toBeDefined();
        });

        it("initialize application and check general stuffs", function () {
            var main = document.createElement('div');
            main.id = 'main';
            document.body.appendChild(main);

            me.app = new me.App({login: false});
            expect(me.app.model).toBeDefined();
            expect(me.app.router).toBeDefined();
            expect(me.app.view).toBeDefined();
        });


        it("start application", function () {

            location.hash = '';
            me.app.router = me.router;
            expect(me.app.start).toBeDefined();
            spyOn(me.app, 'logStatus');
            me.app.start(null);
            expect(me.app.logStatus).toHaveBeenCalledWith(false);
            expect(me.app.isStart).toBeFalsy(); // поскольку мы подменили вызов logStatus, приложение не должно быть запущено
        });

        describe('router', function () {

            beforeEach(function (done) {
                require(['_app/routers/main'], function (Router) {
                    spyOn(Router.prototype, 'navigate').and.callThrough();
                    spyOn(Router.prototype, 'open');
                    spyOn(Router.prototype, 'unknown');
                    spyOn(Router.prototype, 'demo');
                    me.router = new Router();
                    done();
                });
            });


            it("check navigate home", function (done) {
                me.app.logStatus(false);
                setTimeout(function () {
                    expect(me.router.unknown).not.toHaveBeenCalled();
                    expect(me.router.open).toHaveBeenCalled();
                    done();
                });
            });

            it("check navigate unknown function", function (done) {
                me.app.navigate('some strange string');
                setTimeout(function () {
                    expect(me.router.unknown).toHaveBeenCalled();
                    expect(me.router.open).not.toHaveBeenCalled();
                    done();
                }, 1);
            });

            it("check navigate demo url", function (done) {
                location.hash = 'demo';
                setTimeout(function () {
                    expect(me.router.demo).toHaveBeenCalled();
                    done();
                }, 1);
            });
        });


        it("check window load", function () {
            //todo проверить правильность добавление окошек в DOM
        });


        describe('Signin form', function () {

            var view;
            beforeEach(function (done) {
                $('#main').empty();
                spyOn(me.App.prototype, 'routes');
                spyOn(me.App.prototype, 'navigate');
                me.app = new me.App({login: false});
                me.app.openWindow('signin', {}, function () {
                    view = me.app.view;
                    done();
                });
            });

            it("exist", function () {
                expect(view).toBeDefined();
                expect(me.app.viewName).toBe('signin');
                expect(view.$el.closest('body').length).toBe(1);
            });

            it("click demo", function () {
                expect(view.$('.js-demo')).toBeDefined();
                spyOn(view, 'demo').and.callThrough();
                view.delegateEvents();
                view.$('.js-demo').click();
                expect(view.demo).toHaveBeenCalled();
                expect(me.app.routes).toHaveBeenCalledWith('demo');
            });

            it("click registration", function () {
                expect(view.$('.js-registration')).toBeDefined();
                spyOn(view, 'registration').and.callThrough();
                view.delegateEvents();
                view.$('.js-registration').click();
                expect(view.registration).toHaveBeenCalled();
                expect(me.app.routes).toHaveBeenCalledWith('registration');
            });

            it("click remember", function () {
                expect(view.$('.js-remember')).toBeDefined();
                spyOn(view, 'remember').and.callThrough();
                view.delegateEvents();
                view.$('.js-remember').click();
                expect(view.remember).toHaveBeenCalled();
                expect(me.app.routes).toHaveBeenCalledWith('remember');
            });

            it("click button", function () {
                expect(view.$('.js-submit')).toBeDefined();
                spyOn(view, 'submit').and.callFake(function (e) {
                    e.preventDefault();
                });
                view.delegateEvents();
                view.$('.js-submit').click();
                expect(view.submit).toHaveBeenCalled();
            });

            it("submit form error", function () {
                spyOn(view, 'submit').and.callThrough();
                spyOn(view, 'showFormErrors').and.callThrough();
                spyOn(view, 'inputError').and.callThrough();

                view.delegateEvents();
                view.$('form').submit();
                expect(view.submit).toHaveBeenCalled();
                expect(view.showFormErrors).toHaveBeenCalled();
                expect(view.inputError).toHaveBeenCalled();
                // проверяем подсказки
                expect(view.$('.js-email').attr('data-hasqtip')).toBeDefined();
                expect(view.$('.js-pass').attr('data-hasqtip')).toBeDefined();
            });


            it("submit form success", function (done) {
                spyOn(view, 'submit').and.callThrough();
                spyOn(api, 'call').and.callFake(function (opts) {
                    expect(opts.method).toBe('Login');
                    expect(opts.data.Login).toBe('test');
                    expect(opts.data.Password).toBe(Stemapp.util.getPassHash(Stemapp.config.hashPassMethod, 'passtest', Stemapp.config.salt));
                    done();
                });
                spyOn(view.model, 'sync').and.callThrough();
                view.delegateEvents();
                view.$('.js-email').val('test');
                view.$('.js-pass').val('passtest');
                view.$('form').submit();
                expect(view.submit).toHaveBeenCalled();
                expect(view.model.sync).toHaveBeenCalled();
                // проверяем подсказки
                expect(view.$('.js-email').attr('data-hasqtip')).not.toBeDefined();
                expect(view.$('.js-pass').attr('data-hasqtip')).not.toBeDefined();
            });

            it("submit form jaxis success", function (done) {
                spyOn(view, 'submit').and.callThrough();
                spyOn(api, 'call').and.callThrough();
                spyOn(api, 'success').and.callFake(function (data, status, xhr) {
                    expect(status).toBe('success');
                    expect(data.Id).toBeDefined();
                    done();
                });
                spyOn(api, 'error').and.callFake(function (xhr, status, error) {
                    expect(status).toBe('success');
                    done();
                });
                spyOn(view.model, 'sync').and.callThrough();
                view.delegateEvents();
                view.$('.js-email').val('demo@navstat.ru');
                view.$('.js-pass').val('demo');
                view.$('form').submit();
                expect(view.submit).toHaveBeenCalled();
                expect(view.model.sync).toHaveBeenCalled();
            });
        });

        describe('registration form with invite', function () {

            var view;
            beforeEach(function (done) {
                $('#main').empty();
                spyOn(me.App.prototype, 'routes');
                spyOn(me.App.prototype, 'navigate');
                me.app = new me.App({login: false});
                me.app.openWindow('registration', [], function () {
                    Stemapp.config.registerByInvite = true;
                    view = me.app.view;
                    done();
                });
            });

            it("exist", function () {
                expect(view).toBeDefined();
                expect(me.app.viewName).toBe('registration');
                expect(view.$el.closest('body').length).toBe(1);
            });

            it("click signin", function () {
                expect(view.$('.js-signin')).toBeDefined();
                spyOn(view, 'signin').and.callThrough();
                view.delegateEvents();
                view.$('.js-signin').click();
                expect(view.signin).toHaveBeenCalled();
                expect(me.app.routes).toHaveBeenCalledWith('signin');
            });

            it("click button", function () {
                expect(view.$('.js-submit')).toBeDefined();
                spyOn(view, 'submit').and.callFake(function (e) {
                    e.preventDefault();
                });
                view.delegateEvents();
                view.$('.js-submit').click();
                expect(view.submit).toHaveBeenCalled();
            });

            it("submit form error", function () {
                spyOn(view, 'submit').and.callThrough();
                spyOn(view, 'showFormErrors').and.callThrough();
                spyOn(view, 'inputError').and.callThrough();

                view.delegateEvents();
                view.$('form').submit();
                expect(view.submit).toHaveBeenCalled();
                expect(view.showFormErrors).toHaveBeenCalled();
                expect(view.inputError).toHaveBeenCalled();
                // проверяем подсказки
                expect(view.$('.js-invite').attr('data-hasqtip')).toBeDefined();
                expect(view.$('.js-email').attr('data-hasqtip')).toBeDefined();
                expect(view.$('.js-password').attr('data-hasqtip')).toBeDefined();

            });

            it("submit form success", function (done) {
                spyOn(view, 'submit').and.callThrough();
                spyOn(api, 'call').and.callFake(function (opts) {
                    expect(opts.method).toBe('Register');
                    expect(opts.data.Email).toBe('test');
                    expect(opts.data.Invite).toBe('testinvite');
                    expect(opts.data.Password).toBe(Stemapp.util.getPassHash(Stemapp.config.hashPassMethod, 'passtest', Stemapp.config.salt));
                    done();
                });
                spyOn(view.model, 'sync').and.callThrough();
                view.delegateEvents();
                view.$('.js-invite').val('testinvite');
                view.$('.js-email').val('test');
                view.$('.js-password').val('passtest');
                view.$('form').submit();
                expect(view.submit).toHaveBeenCalled();
                expect(view.model.sync).toHaveBeenCalled();
                // проверяем подсказки
                expect(view.$('.js-email').attr('data-hasqtip')).not.toBeDefined();
                expect(view.$('.js-pass').attr('data-hasqtip')).not.toBeDefined();
                expect(view.$('.js-invite').attr('data-hasqtip')).not.toBeDefined();
            });

            it("submit form jaxis error", function (done) {
                spyOn(view, 'submit').and.callThrough();
                spyOn(api, 'call').and.callThrough();
                spyOn(api, 'error').and.callFake(function (xhr, status, error) {
                    expect(status).toBe('error');
                    expect(error).toBe('Bad Request');
                    done();
                });
                spyOn(view.model, 'sync').and.callThrough();
                view.delegateEvents();
                view.$('.js-invite').val('testinvite');
                view.$('.js-email').val('test');
                view.$('.js-password').val('passtest');
                view.$('form').submit();
                expect(view.submit).toHaveBeenCalled();
                expect(view.model.sync).toHaveBeenCalled();
            });

        });

        describe('registration form without invite', function () {

            var view;
            beforeEach(function (done) {
                $('#main').empty();
                spyOn(me.App.prototype, 'routes');
                spyOn(me.App.prototype, 'navigate');

                me.app = new me.App({login: false});
                me.app.openWindow('registration', {}, function () {
                    Stemapp.config.registerByInvite = false;
                    view = me.app.view;
                    done();
                });
            });

            it("exist confirm", function () {
                expect(view.$('.js-confirm').length).toBe(0);
            });

            it("check working dificult pass", function () {
                var $pstrenght = view.$('.js-password').parent().find('.pstrength-info');
                view.$('.js-password').val('passtest').keyup();
                expect($pstrenght.text()).toBe('Очень легкий');
                view.$('.js-password').val('M)(MHN)3h0').keyup();
                expect($pstrenght.text()).toBe('Средний');
            });

        });

        describe('confirm registration form', function () {

            var view;
            beforeEach(function (done) {
                $('#main').empty();
                spyOn(me.App.prototype, 'routes');
                spyOn(me.App.prototype, 'navigate');
                me.app = null;
                me.app = new me.App({login: false});
                me.app.navigate('');
                me.app.openWindow('confirm', ['registration'], function () {
                    Stemapp.config.registerByInvite = false;
                    view = me.app.view;
                    done();
                });
            });

            it("exist", function () {
                expect(view).toBeDefined();
                expect(me.app.viewName).toBe('confirm');
                expect(view.$el.closest('body').length).toBe(1);
            });

            it("click button submit", function () {
                expect(view.$('.js-submit')).toBeDefined();
                spyOn(view, 'submit').and.callFake(function (e) {
                    e.preventDefault();
                });
                view.delegateEvents();
                view.$('.js-submit').click();
                expect(view.submit).toHaveBeenCalled();
            });

            it("click button cancel", function () {
                expect(view.$('.js-close')).toBeDefined();
                spyOn(view, 'close').and.callThrough();
                view.delegateEvents();
                view.$('.js-close').click();
                expect(view.close).toHaveBeenCalled();
                expect(me.app.routes).toHaveBeenCalledWith('signin');
            });


            it("submit form error", function () {
                spyOn(view, 'submit').and.callThrough();
                spyOn(view, 'showFormErrors').and.callThrough();
                spyOn(view, 'inputError').and.callThrough();

                view.delegateEvents();
                view.$('form').submit();
                expect(view.submit).toHaveBeenCalled();
                expect(view.showFormErrors).toHaveBeenCalled();
                expect(view.inputError).toHaveBeenCalled();
                // проверяем подсказки
                expect(view.$('.js-login').attr('data-hasqtip')).toBeDefined();
                expect(view.$('.js-code').attr('data-hasqtip')).toBeDefined();
            });

            it("submit form success sending, wrong response", function (done) {
                spyOn(view, 'submit').and.callThrough();
                spyOn(view, 'inputError').and.callThrough();
                spyOn(view.model, 'sync').and.callThrough();
                spyOn(api, 'call').and.callThrough();
                spyOn(api, 'error').and.callFake(function (xhr, status, error) {
                    expect(status).toBe('error');
                    expect(error).toBe('Bad Request');
                    expect(api.options.method).toBe('Activate');
                    done();
                });
                view.delegateEvents();
                view.$('.js-login').val('phone');
                view.$('.js-code').val('code');
                view.$('form').submit();
                expect(view.submit).toHaveBeenCalled();
                expect(view.inputError).not.toHaveBeenCalled();
                expect(view.model.sync).toHaveBeenCalled();
            });

        });

        describe('confirm remember form', function () {

            var view;
            beforeEach(function (done) {
                $('#main').empty();
                spyOn(me.App.prototype, 'routes');
                spyOn(me.App.prototype, 'navigate');
                me.app = null;
                me.app = new me.App({login: false});
                me.app.navigate('');
                me.app.openWindow('confirm', ['remember'], function () {
                    view = me.app.view;
                    done();
                });
            });

            it("exist", function () {
                expect(view).toBeDefined();
                expect(me.app.viewName).toBe('confirm');
                expect(view.$el.closest('body').length).toBe(1);
            });

            it("click button cancel", function () {
                expect(view.$('.js-close')).toBeDefined();
                spyOn(view, 'close').and.callThrough();
                view.delegateEvents();
                view.$('.js-close').click();
                expect(view.close).toHaveBeenCalled();
                expect(me.app.routes).toHaveBeenCalledWith('signin');
            });

            it("submit form error", function () {
                spyOn(view, 'submit').and.callThrough();
                spyOn(view, 'showFormErrors').and.callThrough();
                spyOn(view, 'inputError').and.callThrough();

                view.delegateEvents();
                view.$('form').submit();
                expect(view.submit).toHaveBeenCalled();
                expect(view.showFormErrors).toHaveBeenCalled();
                expect(view.inputError).toHaveBeenCalled();
                // проверяем подсказки
                expect(view.$('.js-login').attr('data-hasqtip')).toBeDefined();
                expect(view.$('.js-code').attr('data-hasqtip')).toBeDefined();
            });

            it("submit form success sending, wrong response", function (done) {
                spyOn(view, 'submit').and.callThrough();
                spyOn(view, 'inputError').and.callThrough();
                spyOn(view.model, 'sync').and.callThrough();
                spyOn(api, 'call').and.callThrough();
                spyOn(api, 'error').and.callFake(function (xhr, status, error) {
                    expect(status).toBe('error');
                    expect(error).toBe('Internal Server Error');
                    expect(api.options.method).toBe('ForgotPasswordConfirm');
                    done();
                });
                view.delegateEvents();
                view.$('.js-login').val('phone');
                view.$('.js-code').val('code');
                view.$('form').submit();
                expect(view.submit).toHaveBeenCalled();
                expect(view.inputError).not.toHaveBeenCalled();
                expect(view.model.sync).toHaveBeenCalled();
            });
        });
    });
});
