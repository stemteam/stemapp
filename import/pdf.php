<?php

//curl -F file="@/tmp/2014-06-24_demos_demosdemos_sver.pdf;type=application/pdf" -F magic="a2b3e63c49944a173ea75f1f84ea9313" http://crafter.navstat.local/import/pdf.php
//curl -F file="@/tmp/2014-05-31_00003_00001_avans_00000000002.pdf;type=application/pdf" -F magic="a2b3e63c49944a173ea75f1f84ea9313" http://fuel.navstat.local/import/pdf.php

date_default_timezone_set('Europe/Moscow');

$magic = 'a2b3e63c49944a173ea75f1f84ea9313';
$regex = '/^(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})_(?P<client>[-\w]+)_(?P<contract>[-\w]+)_(?P<type>sver|nakl|oper|oplat|avans|fak|akt)_(\w+)\.pdf$/siU';

$dir = realpath(dirname(__FILE__).'/../').'/uploads/docs/';

$matches = array();

// Request checks
if ($_SERVER['REQUEST_METHOD'] != 'POST')
{
	die('ERROR: wrong request method ('.$_SERVER['REQUEST_METHOD'].").\n");
}
elseif (!isset($_POST['magic']) || ($_POST['magic'] != $magic))
{
	die("ERROR: wrong magic or no spell casted.\n");
}
elseif (!isset($_FILES['file']))
{
	die("ERROR: no file to upload.\n");
}
else if ($_FILES['file']['type'] != 'application/pdf')
{
	die('ERROR: wrong filetype ('.$_FILES['file']['type'].").\n");
}
elseif ($_FILES['file']['error'] != 0)
{
	die('ERROR: something went wrong while uploading file (code '.$_FILES['file']['error'].").\n");
}
elseif ($_FILES['file']['size'] == 0)
{
	die("ERROR: empty file.\n");
}
elseif (!preg_match($regex, $_FILES['file']['name'], $matches))
{
	die('ERROR: unknown file name format ('.$_FILES['file']['name'].").\n");
}

$target = $matches['year'].'_'.$matches['month'].'/';
echo 'Uploading to: ', $target, "\n";

$target = $dir.$target;

// checking for directory and creating it if not exists
if (!is_dir($target))
{
	echo 'Target directory not found, creating... ';

	if (!@mkdir($target, 0777, true))
	{
		die("ERROR!\n");
	}

	echo "OK\n";
}

echo date('Y.m.d H:i:s - ').$_FILES['file']['name'].' - ';

if (move_uploaded_file($_FILES['file']['tmp_name'], $target.$_FILES['file']['name']))
{
	file_put_contents(dirname(__FILE__).'/lastupload.log',
		$target.$_FILES['file']['name']."\n",
		FILE_APPEND);

	file_put_contents(dirname(__FILE__).'/pdf.log',
		date('Y.m.d H:i:s - ').$_FILES['file']['name'].' ('.$_FILES['file']['size'].' b) from '.$_SERVER['REMOTE_ADDR']." uploaded.\n",
		FILE_APPEND);
	die("OK.\n");
}
else
{
	file_put_contents(dirname(__FILE__).'/pdf.log',
		date('Y.m.d H:i:s - ').$_FILES['file']['name'].' ('.$_FILES['file']['size'].' b) from '.$_SERVER['REMOTE_ADDR']." unable to move to ".$target.".\n",
		FILE_APPEND);
}

echo "ERROR!\n";
