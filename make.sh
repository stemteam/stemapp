#! /bin/sh

#===============================================================================
# Скрипт для сборки проекта и его модулей
#===============================================================================

export PATH="/usr/local/bin:$PATH"

INIT_DIR=`pwd`

# полный путь до скрипта
BASE_DIR=$(cd $(dirname $0) && pwd)
BUILD_DIR="$BASE_DIR"
MODULE_DIR="$BUILD_DIR/modules"
APP_DIR="$BUILD_DIR"
COLLECTOR_DIR="$BUILD_DIR/collector"

cd "$BUILD_DIR"
npm install
grunt updateModules
grunt
BUILD_COMPLETE=$?

if [ ! $BUILD_COMPLETE -eq 0 ] ; then
    cd "$BASE_DIR"
    echo "ERROR: Build has failed."
    exit 1
fi

echo "Preparing..."
# building modules
cd "$BUILD_DIR"
for MODULE in `ls -1 $MODULE_DIR` ; do
    if [ -x "$MODULE_DIR/$MODULE/build.sh" ] ; then
        echo "Building module: ${MODULE}..."
        "$MODULE_DIR/$MODULE/build.sh"
        if [ ! $? -eq 0 ] ; then
            echo "ERROR: Build $module has failed."
            exit 2
        fi
        echo "${MODULE}: Ok"
    fi
done

echo "Build succeeded."
