.PHONY: all, clean, install

PROJECT := "fuel.ktscard.ru"
INSTALL_DIR := $(DESTDIR)/www/$(PROJECT)
SOURCE_DIR :=.
USER := apache
GROUP := apache

all:
clean:

install: all
	install -d -o $(USER) -g $(GROUP) $(INSTALL_DIR)
	rsync -az $(SOURCE_DIR) $(INSTALL_DIR)/ --delete-after --exclude .htpasswd --exclude .htaccess --exclude Makefile --exclude uploads
	chown -R $(USER).$(GROUP) $(INSTALL_DIR)/*
